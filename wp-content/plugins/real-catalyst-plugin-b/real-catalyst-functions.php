<?php 
function APISyncMasterData(){

global $wpdb; // this is how you get access to the database
 
	    $API_URL = APIBaseURL.'/utilities/masterdata'; // access_token received from /oauth2/token call 
	 
	  $attributes = array("Token"=>TokenApi); // Api Url for masterdata 
  
	  $API_URL_COMPANY = APIBaseURL.'/companies/all/1'; // access_token received from /oauth2/token call 
	 $attributes_company = array("Token"=> TokenCompany); // Api Url for masterdata 
 
	 $data = CallAPI($API_URL, $attributes); 
 // echo "<pre>";print_r($data['Result']); die;
    $data_companies = CallAPI($API_URL_COMPANY, $attributes_company);
	  if($data_companies['StatusCode'] == 200){
	 if(  !empty($data_companies['Result']) ) {
		 $companies = $data_companies['Result'] ;
		 syncCompanies($companies);
		 } 
		 else{
		 echo "Developers Not found !!";
		 }
		 }
		 else{
		 print_r($data_companies); 
		 }
		 if($data['StatusCode'] == 200){
			 if(  !empty($data['Result']['Cities'] ) ) {
			 $Cities = $data['Result']['Cities'] ;
			 syncLocation($Cities);
			 } 
			 else{
			 echo "Locations Not found !!";
			 }
			if(  !empty($data['Result']['Categories'] ) ) {
			 $categories = $data['Result']['Categories'] ;
			 syncCategories($categories);
			 }
			else{
			 echo "Categories Not found !!";
			 }

		}
		else{
		 print_r($data); 
		 }
  echo "<p><b>All Done !!</b></p>";
  die();

}

function APIGetProjects($keyword,$TypeID, $CityID, $CompanyID){
	$API_URL = APIBaseURL.'/search/'.UserID; // access_token received from /oauth2/token call 
	$data ="";
	$opt ="";
	if(!empty($CityID)){
	$data .="CityId eq ".$CityID;
	$opt =" and ";
	}
	if(!empty($CompanyID)){
	$data .= $opt." builderCompanyId eq ".$CompanyID; 
	$opt =" and ";
	}
	if(!empty($TypeID)){
	$data .= $opt." CategoryTypeId eq ".$TypeID;
	} 
	if(!empty($keyword)){
	$data .= $opt." title eq '".$keyword."'";
	}  
	$attributes = array("Token"=> TokenCompany); 
	$postdata = array();
	if(!empty($data)){
	$postdata = array('$filter' => $data); 
	} 
	   $result =  CallAPI($API_URL, $attributes ,$postdata);
	   
	 if(!empty($result['Result'])){
	 $row =""; 
	 foreach($result['Result'] as $project){
	  
	 $title= $project['title'];
	 $CategoryTypeId= getTermName('wpcf-categoryid',$project['CategoryTypeId']);
	 $builderCompanyId= getTermName('wpcf-id',$project['builderCompanyId']);
	 $minPrice= $project['minPrice'];
	 $maxPrice= $project['maxPrice'];
	 $address=  getTermName('wpcf-cityid',$project['CityId']);
	  
	 $row .='<tr>
	<th class="check-column" scope="row">
	<input type="checkbox" value="'.$project['Id'].'" name="projects[]"></th>
	<td  class="has-row-actions column-primary">'.$title .'</td>
	<td> '. $CategoryTypeId[0].'</td>
	<td>'. $CategoryTypeId[0].'</td>
	<td>'.$builderCompanyId[0].'</td>
	<td>'.$address[0].'</td>
	<td>'.$minPrice.' - '.$maxPrice.'</td>
	</tr>';
	 
	 
	 }
	  
	 return $row;
	  }
 
}
function APIImportProject($ProjectID){
	 
	parse_str($_POST['checkprojects'], $projects);
	$projectAdded  = array();
	$API_URL = ''; // access_token received from /oauth2/token call 
	$attributes  = array("Token"=> TokenCompany); // Api Url for masterdata 
	foreach($projects['projects'] as $projectid){ 
	$API_URL = APIBaseURL.'/project/'.$projectid.'/'.UserID.'/';
	$projectData =  CallAPI($API_URL, $attributes);
	 
	if(!empty($projectData['Result'])){
	 $projectAdded[] = CreateProjectPost($projectData['Result']);
	}
	
	}
	 
	return   implode(",",$projectAdded);
	 
	
}
function CreateProjectPost($data){
// echo "<pre>";
// print_r($data);
// die;  

	$sizes = array(); 
	$BedRooms = array(); 
	$MinimumPrice = array(); 
	$maximumPrice = array(); 
	$title = $data['title'];
	$masterPlan = SiteLink.$data['logoimage'];  
	$sitePlan = SiteLink.$data['topImage3']; 
	$fave_video_url =  $data['topImage2']; 
	$featureImage = SiteLink.$data['topImage1'];
	$minPrice =  $data['minPrice'];
	$maxPrice =  $data['maxPrice']; 
	$description = $data['aboutSubProject'];
	$ProjectStatus = $data['ProjectStatus']; 
	$Locality = $data['Locality']; 
	$CategoryTypeId= getTermName('wpcf-categoryid',$data['CategoryTypeId']);
	$builderCompanyId= getTermName('wpcf-id',$data['builderCompanyId']); 
	$location=  getTermName('wpcf-cityid',$data['CityId']);
	$plan = array();
	$subproperties = array();
	$Specifications = array();
	$planEnable = 'disable';
	 if(!empty($data['PropertySize'])){
	 foreach($data['PropertySize'] as $plans){
	 $sizes[] = $plans['size'];
	 $BedRooms[] = $plans['BedRooms'];
	 $MinimumPrice[] = $plans['MinimumPrice'];
	 $plan[] = array(
	 "fave_plan_title"=>$plans['title'],
	 "fave_plan_rooms"=>$plans['BedRooms'],
	 "fave_plan_bathrooms"=>$plans['BathRooms'],
	 "fave_plan_price"=>$plans['MinimumPrice'],
	 "fave_plan_size"=>$plans['size'],
	 "fave_plan_image"=>"",
	 "fave_plan_description"=>$plans['AboutProperty'] 
	 ); 
	 $subproperties[] = array(
	 "fave_mu_title"=>$plans['title'],
	 "fave_mu_price"=>$plans['MinimumPrice'],
	 "fave_mu_price_postfix"=>'',
	 "fave_mu_beds"=>$plans['BedRooms'],
	 "fave_mu_baths"=>$plans['BathRooms'],
	 "fave_mu_size"=>$plans['size'],
	 "fave_mu_size_postfix"=>'sq ft',
	 "fave_mu_type"=>$plans['AboutProperty']
	 );
	  
	 if(!empty($plans['Specifications'])){
	  
			 $Specifications = $plans['Specifications']; 
			 
	 }
	 } 
	  $planEnable = 'enable';
	  $maximumPrice[]= $plans['MaximumPrice'];
	 } 
 
	$bhk = implode(',',array_unique($BedRooms)); 
	  $area = min($sizes); 
	  $MinimumPrice = min($MinimumPrice); 
	  $maximumPrice = max($maximumPrice); 
	  $area2 = min($sizes)." - ".max($sizes); 
	 $additional_features = array(); 
	  $fave_additional_features_enable = 'disable';
	 if(!empty($data['SpecialFeatures'])){
	 foreach($data['SpecialFeatures'] as $Afeature){ 		 		
	 $additional_features[] = array(
	 "fave_additional_feature_title"=>$Afeature['FeatureName'],
	 "fave_additional_feature_value"=>$Afeature['Description']  
	 );
	 }
	  $fave_additional_features_enable = 'enable';
	 }
	   $specificationHTML =  specificationTable(array_sort($Specifications, 'AreaName')); 
	 if(!wp_exist_post_by_title($title)){
	 $new_project = array(
        'post_title' => $title,
        'post_content' => $description,
        'post_status' => 'publish',
        'post_date' => date('Y-m-d H:i:s'),
        'post_author' => '1',
        'post_type' => propertyPostType,
		 'tax_input' => array(
            propertyCity => array($location[1]),
            propertyType =>  array($CategoryTypeId[1]),
            company =>  array($builderCompanyId[1])
        )
    );
 
	  $post_id = wp_insert_post($new_project); 
	  
	 $masterPlanId =  fetch_media('masterPlan',$masterPlan,$post_id);
	 
	 $sitePlanId =  fetch_media('sitePlan',$sitePlan,$post_id);
	  
	 $featureImageId =  fetch_media('featureImage',$featureImage,$post_id); 
			add_post_meta($post_id,'fave_property_images',$sitePlanId); 
			add_post_meta($post_id,'fave_property_images',$masterPlanId);
			add_post_meta($post_id,'sitePlanId',$sitePlanId);
			add_post_meta($post_id,'masterPlanId',$masterPlanId);
			add_post_meta($post_id,'_thumbnail_id',$featureImageId);
			add_post_meta($post_id,'fave_property_price',$MinimumPrice); 
			add_post_meta($post_id,'fave_property_sec_price_',$maximumPrice); 
			add_post_meta($post_id,'floor_plans',$plan);
			add_post_meta($post_id,'fave_floor_plans_enable',$planEnable);
			add_post_meta($post_id,'fave_property_price_postfix','sq ft');
			add_post_meta($post_id,'fave_property_size',$area);
			add_post_meta($post_id,'fave_property_sizes',$area2);
			add_post_meta($post_id,'fave_additional_features_enable',$fave_additional_features_enable);
			add_post_meta($post_id,'additional_features',$additional_features);
			add_post_meta($post_id,'fave_video_url',$fave_video_url);
			add_post_meta($post_id,'fave_property_map','hide');
			add_post_meta($post_id,'fave_agent_display_option','none');
			add_post_meta($post_id,'fave_prop_homeslider','no');
			add_post_meta($post_id,'fave_specifications',$specificationHTML);
			add_post_meta($post_id,'fave_multi_units',$subproperties);
			add_post_meta($post_id,'fave_multiunit_plans_enable','enable');
			add_post_meta($post_id,'fave_property_map_address',$Locality.' '.$location[0]);
            $getLatLong = getLatLong( $location[0]);
		    add_post_meta($post_id,'fave_property_location',$getLatLong);
			add_post_meta($post_id,'fave_property_map_street_view','hide');
			add_post_meta($post_id,'fave_property_map',1);
			add_post_meta($post_id,'fave_property_country','IN');
			add_post_meta($post_id,'amenities',$data['Amenities']);
			add_post_meta($post_id,'PossessionMonth',$data['PossessionMonth']);
			add_post_meta($post_id,'PossessionYear',$data['PossessionYear']); 
			add_post_meta($post_id,'Towers',$data['Towers']); 
			add_post_meta($post_id,'NoOfUnits',$data['NoOfUnits']); 
			add_post_meta($post_id,'NumberOfParking',$data['NumberOfParking']); 
			add_post_meta($post_id,'LocationAdvantages',$data['KeyDistances']); 
			add_post_meta($post_id,'currentOffers',$data['currentOffers']); 
			add_post_meta($post_id,'SpecialFeatures',$data['SpecialFeatures']); 
			add_post_meta($post_id,'ProjectLenders',$data['ProjectLenders']); 
			add_post_meta($post_id,'FAQs',$data['FAQs']); 
			add_post_meta($post_id,'bhk',$bhk); 
			
	$name = array(
	   "name" => $ProjectStatus,
	   "description" => ''
	   );
      $termss = insertTerm(status,$name,$extradata=array());
	  
	 if(!empty($termss)){ 
		 wp_set_object_terms($post_id, (int)$termss['term_id'], status);
	 }
	 else{
	 $category = get_term_by('name', $ProjectStatus, status);
	 
	  wp_set_object_terms($post_id, (int)$category->term_id, status);
	 }
	return  $post_id;
	}
	else{
	return $title." - Already exist!!<br/>";
	}

}

function CallAPI($API_URL, $attributes,$postdata=false){

global $wpdb; // this is how you get access to the database
 
	$args['headers'] =  $attributes ;
	if($postdata){
	$args['body'] = $postdata; 
	}
	 
   $response = wp_remote_get( $API_URL,$args );
 
 return   json_decode($response['body'], true);
 
	wp_die();

}

 function insertTerm($tax,$title,$extradata,$parent=0){
 
 $term = term_exists($title['name'], $tax);
		if ($term == 0 && $term == null) {
		$termdata =  wp_insert_term(
			$title['name'],   // the term 
			$tax, // the taxonomy
			array(
				'description' => $title['description'], 
				'parent'      => $parent
			)
		);
		$termid =  $termdata['term_id'] ;
		  if( $termid ){
		  if(!empty($extradata)){
				  foreach($extradata as $key => $value)
				  {
				  
				   add_term_meta($termid, $key, $value); 
				  
				  } 
			}
		  
		  }
		return array($termid,"imported ".$tax." : ".$title['name']);
		}
		else{
		return false;
		}
 
 }
 function syncCategories($categories){
  
  foreach($categories as $category){

 $title = array(
	   "name" => $category['name'],
	   "description" => $category['description']
	   );
   
   $extradata = array(  
	   "wpcf-mapsizeids"=> $category['MapSizeIds'],
	   "wpcf-categoryid"=> $category['id'],
	   "wpcf-area1"=> $category['Area1'],
	   "wpcf-area2"=> $category['Area2'],
	   "wpcf-area3"=> $category['Area3'],
	   "wpcf-area4"=> $category['Area4'],
	   "wpcf-istowerapplicable"=> $category['isTowerApplicable'],
	   "wpcf-showdimension"=> $category['ShowDimension'],
	   "wpcf-showbedroombathroom"=> $category['ShowBedroomBathroom'],
	   "wpcf-isbuiltupproperty"=> $category['IsBuiltUpProperty']
	   );
   $msg = insertTerm(propertyType,$title,$extradata);
   $pid = "";
   $pid = $msg[0];
	    if($msg[1]){     echo $msg[1]."<br>"; }
		 
		if(!empty($category['CategoryTypes'])){
		
			 foreach($category['CategoryTypes'] as $categorytype){  
		
	 $title = array(
	   "name" => $categorytype['name'],
	   "description" => $categorytype['description']
	   );
   
   $extradata = array(  
	   "wpcf-mapsizeids"=> $categorytype['MapSizeIds'],
	   "wpcf-categoryid"=> $categorytype['id'],
	   "wpcf-area1"=> $categorytype['Area1'],
	   "wpcf-area2"=> $categorytype['Area2'],
	   "wpcf-area3"=> $categorytype['Area3'],
	   "wpcf-area4"=> $categorytype['Area4'],
	   "wpcf-istowerapplicable"=> $categorytype['isTowerApplicable'],
	   "wpcf-showdimension"=> $categorytype['ShowDimension'],
	   "wpcf-showbedroombathroom"=> $categorytype['ShowBedroomBathroom'],
	   "wpcf-isbuiltupproperty"=> $categorytype['IsBuiltUpProperty']
	   );
   $msg = insertTerm(propertyType,$title,$extradata,$pid);
   if($msg[1]){     echo $msg[1]."<br>"; }
		
		
		
		}
			  
		
		}
		 
		}
 }
 
 function syncLocation($Cities){
 
 
   foreach($Cities as $location){ 
   
   $title = array(
	   "name" => $location['cityname'],
	   "description" => $location['citydescription']
	   );
   
   $extradata = array(  
	   "wpcf-cityid"=> $location['cityid'],
	   "wpcf-countryid"=> $location['countryid'],
	   "wpcf-stateId"=> $location['stateId']
	   );
   $msg = insertTerm(propertyCity,$title,$extradata);
    
	  if($msg[1]){ echo $msg[1]."<br>"; }
	  if(!empty($location['Regions'])){
	 
	   foreach($location['Regions'] as $regions){ 
	     
	    insertTerm(Regions,array('name'=>$regions,'description'=>''),$extradata = array());
	   }
	  }
    
}
 
 }
 function syncCompanies($companies){
 
  foreach($companies as $company){
		$title = array(
	   "name" => $company['name'],
	   "description" => $company['aboutCompany']
	   );
	   $extradata = array(  
	   "wpcf-id"=> $company['id'],
	   "wpcf-companyowner"=> $company['CompanyOwner'],
	   "wpcf-logo"=> $company['logo']
	   );
	  $msg =  insertTerm(company,$title,$extradata);
	   if($msg[1]){     echo $msg[1]."<br>"; }
		}
 }
 function getTermName($key,$value){
 global $wpdb;
 $result =  $wpdb->get_row( "SELECT T.name,T.term_id FROM wp_terms as T INNER JOIN wp_termmeta as M ON M.meta_key = '".$key."' AND M.meta_value = ".$value." WHERE T.term_id = M.term_id ", ARRAY_N );
 return $result;
 }
 function wp_exist_post_by_title( $title ) {
    global $wpdb;
    $return = $wpdb->get_row( 'SELECT ID FROM wp_posts WHERE post_title = "' . $title . '" && post_type = "'.propertyPostType.'" ', 'ARRAY_N' );
    if( empty( $return ) ) {
        return false;
    } else {
        return true;
    }
}
  
function fetch_media($name,$file_url, $post_id) {
	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-admin/includes/image.php');
	global $wpdb;

	if(!$post_id) {
		return false;
	}
	$file_url = str_replace(' ', '%20', $file_url);
	//directory to import to	
	$artDir = 'wp-content/uploads/importedmedia/';

	//if the directory doesn't exist, create it	
	if(!file_exists(ABSPATH.$artDir)) {
		mkdir(ABSPATH.$artDir);
	}

	//rename the file... alternatively, you could explode on "/" and keep the original file name
	   $ext = array_pop(explode(".", $file_url)); 
	  $new_filename = $name.'-'.$post_id.".".$ext; //if your post has multiple files, you may need to add a random number to the file name to prevent overwrites 
	if (@fclose(@fopen($file_url, "r"))) { //make sure the file actually exists
		copy($file_url, ABSPATH.$artDir.$new_filename);

		$siteurl = get_option('siteurl');
		$file_info = getimagesize(ABSPATH.$artDir.$new_filename);

		//create an array of attachment data to insert into wp_posts table
		$artdata = array();
		$artdata = array(
			'post_author' => 1, 
			'post_date' => current_time('mysql'),
			'post_date_gmt' => current_time('mysql'),
			'post_title' => $new_filename, 
			'post_status' => 'inherit',
			'comment_status' => 'closed',
			'ping_status' => 'closed',
			'post_name' => sanitize_title_with_dashes(str_replace("_", "-", $new_filename)),											'post_modified' => current_time('mysql'),
			'post_modified_gmt' => current_time('mysql'),
			'post_parent' => $post_id,
			'post_type' => 'attachment',
			'guid' => $siteurl.'/'.$artDir.$new_filename,
			'post_mime_type' => $file_info['mime'],
			'post_excerpt' => '',
			'post_content' => ''
		);

		$uploads = wp_upload_dir();
		$save_path = $uploads['basedir'].'/importedmedia/'.$new_filename;

		//insert the database record
		$attach_id = wp_insert_attachment( $artdata, $save_path, $post_id );

		//generate metadata and thumbnails
		if ($attach_data = wp_generate_attachment_metadata( $attach_id, $save_path)) {
			wp_update_attachment_metadata($attach_id, $attach_data);
		}  
	}
	else {
		return 'Media not imported';
	}

	return $attach_id;
}

function specificationTable($data){ 
$htm ="";
if(!empty($data)){
$htm .= '<h2 class="title-left">Specifications</h2><table class="table  table-striped table-multi-properties">
                    <thead>
                    <tr>
                        <th>Area Name</th>
                        <th>Title</th>
                        <th>BSP Type</th>
                        <th>Description</th> 
                    </tr>
                    </thead>
                    <tbody>';
					
		 foreach($data as $row){
					
					$htm .='<tr><td  width="25%" class="title blue"> '.$row["AreaName"].' </td>
                        <td><p> '.$row["Title"].'</p></td>
                        <td> '.$row["BSPType"].'</td>                         
                        <td> '.$row["Description"].'</td>
                    </tr>' ;
					
		 } 
					$htm .= ' </tbody>  </table>';
					return $htm;
					}
					else{
					return "Specifications not found!";
					}
}
function getLatLong($address){
    if(!empty($address)){
        //Formatted address
        $formattedAddr = str_replace(' ','+',$address);
        //Send request and receive json data by address
        $geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false'); 
        $output = json_decode($geocodeFromAddr);
        //Get latitude and longitute from json data
        $data['latitude']  = $output->results[0]->geometry->location->lat; 
        $data['longitude'] = $output->results[0]->geometry->location->lng;
        //Return latitude and longitude of the given address
        if(!empty($data)){
            return $data['latitude'].','.$data['longitude'];
        }else{
            return false;
        }
    }else{
        return false;   
    }
}

function array_sort($array, $on, $order=SORT_ASC){

    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}