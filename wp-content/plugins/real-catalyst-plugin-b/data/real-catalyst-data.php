<?php
 ob_start();
 
 


if( !function_exists( 'houzez_property_post_types' ) ){
    function houzez_property_post_types(){

      $labels = array(
            'name' => __( 'Properties','houzez'),
            'singular_name' => __( 'Property','houzez' ),
            'add_new' => __('Add New','houzez'),
            'add_new_item' => __('Add New Property','houzez'),
            'edit_item' => __('Edit Property','houzez'),
            'new_item' => __('New Property','houzez'),
            'view_item' => __('View Property','houzez'),
            'search_items' => __('Search Property','houzez'),
            'not_found' =>  __('No Property found','houzez'),
            'not_found_in_trash' => __('No Property found in Trash','houzez'),
            'parent_item_colon' => ''
          );

      $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'has_archive' => true,
            'capability_type' => 'post',
            'map_meta_cap'    => true,
            //'capabilities'    => houzez_get_property_capabilities(),
            'hierarchical' => true,
            'menu_icon' => 'dashicons-building',
            'menu_position' => 6,
            'can_export' => true,
            'supports' => array('title','editor','thumbnail','revisions','author','page-attributes','excerpt'),
            //'rewrite' => array( 'slug' => 'property' ),

             // The rewrite handles the URL structure.
            'rewrite' => array(
                  'slug'       => propertyPostType,
                  'with_front' => false,
                  'pages'      => true,
                  'feeds'      => true,
                  'ep_mask'    => '',
            ),
      );

      register_post_type(propertyPostType,$args);

    }
}
add_action('init', 'houzez_property_post_types');

 
	function register_taxonomy_developer()
{
    $labels = [
        'name'              => _x('Developers', 'taxonomy general name'),
        'singular_name'     => _x('Developer', 'taxonomy singular name'),
        'search_items'      => __('Search Developers'),
        'all_items'         => __('All Developers'),
        'parent_item'       => __('Parent Developer'),
        'parent_item_colon' => __('Parent Developer:'),
        'edit_item'         => __('Edit Developer'),
        'update_item'       => __('Update Developer'),
        'add_new_item'      => __('Add New Developer'),
        'new_item_name'     => __('New Developer Name'),
        'menu_name'         => __('Developer'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => company],
    ];
    register_taxonomy(company, propertyPostType, $args);
}
add_action('init', 'register_taxonomy_developer');
 

function wptp_add_categories_to_attachments() {
    register_taxonomy_for_object_type( 'Developers', 'attachment' );
}
add_action( 'init' , 'wptp_add_categories_to_attachments' );
 
 
 
	function register_taxonomy_property_status()
{
    $labels = [
        'name'              => _x('Property status', 'taxonomy general name'),
        'singular_name'     => _x('Status', 'taxonomy singular name'),
        'search_items'      => __('Search Status'),
        'all_items'         => __('All Status'),
        'parent_item'       => __('Parent Status'),
        'parent_item_colon' => __('Parent Status:'),
        'edit_item'         => __('Edit Status'),
        'update_item'       => __('Update Status'),
        'add_new_item'      => __('Add New Status'),
        'new_item_name'     => __('New Status Name'),
        'menu_name'         => __('Status'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => status],
    ];
    register_taxonomy( status, propertyPostType, $args);
}
add_action('init', 'register_taxonomy_property_status');
 
 
 
 
	function register_taxonomy_property_type()
{
    $labels = [
        'name'              => _x('Property Type', 'taxonomy general name'),
        'singular_name'     => _x('Type', 'taxonomy singular name'),
        'search_items'      => __('Search Type'),
        'all_items'         => __('All Type'),
        'parent_item'       => __('Parent Type'),
        'parent_item_colon' => __('Parent Type:'),
        'edit_item'         => __('Edit Type'),
        'update_item'       => __('Update Type'),
        'add_new_item'      => __('Add New Type'),
        'new_item_name'     => __('New Type Name'),
        'menu_name'         => __('Type'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => propertyType],
    ];
    register_taxonomy(propertyType, propertyPostType, $args);
}
add_action('init', 'register_taxonomy_property_type');
 
 
 
 	function register_taxonomy_property_city()
{
    $labels = [
        'name'              => _x('Property City', 'taxonomy general name'),
        'singular_name'     => _x('City', 'taxonomy singular name'),
        'search_items'      => __('Search City'),
        'all_items'         => __('All Cities'),
        'parent_item'       => __('Parent City'),
        'parent_item_colon' => __('Parent City:'),
        'edit_item'         => __('Edit City'),
        'update_item'       => __('Update City'),
        'add_new_item'      => __('Add New City'),
        'new_item_name'     => __('New City Name'),
        'menu_name'         => __('Property City'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => propertyCity],
    ];
    register_taxonomy(propertyCity, propertyPostType, $args);
}
add_action('init', 'register_taxonomy_property_city');

 
 
 	function register_taxonomy_property_locality()
{
    $labels = [
        'name'              => _x('Property Locality', 'taxonomy general name'),
        'singular_name'     => _x('Locality', 'taxonomy singular name'),
        'search_items'      => __('Search Locality'),
        'all_items'         => __('All Locality'),
        'parent_item'       => __('Parent Locality'),
        'parent_item_colon' => __('Parent Locality:'),
        'edit_item'         => __('Edit Locality'),
        'update_item'       => __('Update Locality'),
        'add_new_item'      => __('Add New Locality'),
        'new_item_name'     => __('New Locality Name'),
        'menu_name'         => __('Property Locality'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => Regions],
    ];
    register_taxonomy(Regions, propertyPostType, $args);
}
add_action('init', 'register_taxonomy_property_locality');



function register_taxonomy_property_state()
{
    $labels = [
        'name'              => _x('Property State', 'taxonomy general name'),
        'singular_name'     => _x('State', 'taxonomy singular name'),
        'search_items'      => __('Search State'),
        'all_items'         => __('All State'),
        'parent_item'       => __('Parent State'),
        'parent_item_colon' => __('Parent State:'),
        'edit_item'         => __('Edit State'),
        'update_item'       => __('Update State'),
        'add_new_item'      => __('Add New State'),
        'new_item_name'     => __('New State Name'),
        'menu_name'         => __('Property State'),
    ];
    $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => ['slug' => STATE],
    ];
    register_taxonomy(STATE, propertyPostType, $args);
}
add_action('init', 'register_taxonomy_property_state');

 add_action( 'add_meta_boxes', 'acme_add_meta_boxes' );
    // add_filter( 'rwmb_meta_boxes', 'prefix_register_meta_boxes' ); 
    function acme_add_meta_boxes() {
  $meta_boxes = array(
            'id' => 'fave_real_meta_box',
            'title' => 'property Data',
            'pages' => array(propertyPostType),
            'tabs' => array(
                'property_details' => array(
                    'label' =>'Basic Information',
                    'icon' => 'dashicons-admin-home',
                ),
                'property_map' => array(
                    'label' => 'Map',
                    'icon' => 'dashicons-location',
                ),
                'property_settings' => array(
                    'label' => 'Property Setting',
                    'icon' => 'dashicons-admin-generic',
                ),
                'gallery' => array(
                    'label' => 'Gallery Images',
                    'icon' => 'dashicons-format-gallery',
                ),
                'video' => array(
                    'label' => 'Property Video',
                    'icon' => 'dashicons-format-video',
                ),
                'virtual_tour' => array(
                    'label' => '360° Virtual Tour',
                    'icon' => 'dashicons-format-video',
                ),
                'agent' => array(
                    'label' => 'Agent Information',
                    'icon' => 'dashicons-businessman',
                ),
                'home_slider' => array(
                    'label' => 'Property Slider',
                    'icon' => 'dashicons-images-alt',
                ),
                'multi_units' => array(
                    'label' => 'Multi Units / Sub Properties',
                    'icon' => 'dashicons-layout',
                ),
                'floor_plans' => array(
                    'label' => 'Floor Plans',
                    'icon' => 'dashicons-layout',
                ),
                'attachments' => array(
                    'label' => 'Attachments',
                    'icon' => 'dashicons-book',
                ),
				'specifications' => array(
                    'label' => 'Specifications',
                    'icon' => 'dashicons-book',
                )

            ),
            'tab_style' => 'left',
            'fields' => array(

                // Property Details
                array(
                    'id' => "fave_property_price",
                    'name' => 'Sale or Rent Price ( Only digits )',
                    'desc' => 'Eg: 557000',
                    'type' => 'text',
                    'std' => "",
                    'columns' => 12,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_sec_price",
                    'name' => 'Second Price ( Display optional price for rental or square feet )',
                    'desc' => 'Eg: 700',
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_price_postfix",
                    'name' => 'After Price Label',
                    'desc' => 'Eg: Per Month',
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_size",
                    'name' => 'Area Size ( Only digits )',
                    'desc' => 'Eg: 1500',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'area_size',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_size_prefix",
                    'name' => 'Size Prefix',
                    'desc' => 'Eg: Sq Ft',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'area_size',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_bedrooms",
                    'name' => 'Bedrooms',
                    'desc' => 'Eg: 4',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'beds_hidden',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_bathrooms",
                    'name' => 'Bathrooms',
                    'desc' => 'Eg: 3',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'baths_hidden',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_garage",
                    'name' => 'Garages',
                    'desc' => 'Eg: 1',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'garages',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_garage_size",
                    'name' => 'Garages Size',
                    'desc' => "",
                    'type' => 'text',
                    'std' => "",
                    'class' => 'garage_size',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_year",
                    'name' => 'Year Built',
                    'desc' => "",
                    'type' => 'date',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_id",
                    'name' => 'Property ID',
                    'desc' => 'It will help you search a property directly.',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'prop_id',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),

                // Property Map
                array(
                    'type' => 'divider',
                    'columns' => 12,
                    'id' => 'google_map_divider',
                    'tab' => 'property_details',
                ),
                array(
                    'name' => 'Property Map ?',
                    'id' => "fave_property_map",
                    'type' => 'radio',
                    'std' => 0,
                    'options' => array(
                        1 => 'Show ',
                        0 => 'Hide'
                    ),
                    'columns' => 12,
                    'tab' => 'property_map',
                ),
                array(
                    'id' => "fave_property_map_address",
                    'name' => 'Property Address',
                    'desc' => 'Leave it empty if you want to hide map on property detail page.',
                    'type' => 'text',
                    'std' => '',
                    'columns' => 12,
                    'tab' => 'property_map',
                ),
                array(
                    'id' => "fave_property_location",
                    'name' => 'Property Location at Google Map*',
                    'desc' => 'Drag the google map marker to point your property location. You can also use the address field above to search for your property.',
                    'type' => 'map',
                    'std' => '25.686540,-80.431345,15',   // 'latitude,longitude[,zoom]' (zoom is optional)
                    'style' => 'width: 95%; height: 400px',
                    'address_field' => "fave_property_map_address",
                    'columns' => 12,
                    'tab' => 'property_map',
                ),
                array(
                    'name' => 'Google Map Street View',
                    'id' => "fave_property_map_street_view",
                    'type' => 'select',
                    'std' => 'hide',
                    'options' => array(
                        'hide' => 'Hide',
                        'show' => 'Show '
                    ),
                    'columns' => 12,
                    'tab' => 'property_map',
                ),

                // Property Settings
                array(
                    'id' => "fave_property_address",
                    'name' => 'Address(*only street name and building no)',
                    'desc' => "",
                    'type' => 'textarea',
                    'columns' => 6,
                    'tab' => 'property_settings',
                ),
                array(
                    'id' => "fave_property_zip",
                    'name' => 'Zip',
                    'desc' => "",
                    'type' => 'text',
                    'columns' => 6,
                    'tab' => 'property_settings',
                ),
                array(
                    'id' => "fave_property_country",
                    'name' => 'Country',
                    'desc' => "",
                    'std' => 'default_country',
                    'type' => 'select',
                    'options' => 'countries_array',
                    'columns' => 6,
                    'tab' => 'property_settings',
                ),
                array(
                    'name' => 'Mark this property as featured ?',
                    'id' => "fave_featured",
                    'type' => 'radio',
                    'std' => 0,
                    'options' => array(
                        1 => 'Yes ',
                        0 => 'No'
                    ),
                    'columns' => 6,
                    'tab' => 'property_settings',
                ),

                // Gallery
                array(
                    'name' => 'Property Gallery Images',
                    'id' => "fave_property_images",
                    'desc' => 'Recommend image size 1170 x 738',
                    'type' => 'image_advanced',
                    'max_file_uploads' => 48,
                    'columns' => 12,
                    'tab' => 'gallery',
                ),

                // Property Video
                array(
                    'id' => "fave_video_url",
                    'name' => 'Video URL',
                    'desc' => 'Provide video URL. YouTube, Vimeo, SWF File and MOV File are supported',
                    'type' => 'text',
                    'columns' => 12,
                    'tab' => 'video',
                ),
                array(
                    'name' => 'Video Image',
                    'id' => "fave_video_image",
                    'desc' => 'Provide an image that will be displayed as a place holder and when user will click over it the video will be opened in a lightbox. You must provide this image otherwise the video will not be displayed. Image should have minimum width of 818px and minimum height 417px. Bigger size images will be cropped automatically.',
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'columns' => 12,
                    'tab' => 'video',
                ),

                //Virtual Tour
                array(
                    'id' => "fave_virtual_tour",
                    'name' => 'Virtual Tour',
                    'desc' => 'Enter virtual tour embeded code',
                    'type' => 'textarea',
                    'columns' => 12,
                    'tab' => 'virtual_tour',
                ), 
                // Homepage Slider
                array(
                    'name' => 'Do you want to add this property in property slider?',
                    'id' => "fave_prop_homeslider",
                    'desc' => 'If yes, then provide slider image below.',
                    'type' => 'radio',
                    'options' => array(
                        'yes' => 'Yes',
                        'no'  => 'No',
                    ),
                    'columns' => 12,
                    'tab' => 'home_slider',
                ),
                array(
                    'name' => 'Slider Image',
                    'id' => "fave_prop_slider_image",
                    'desc' => 'The recommended image size in 2000 x 700. You can use bigger and smaller image but keep same height to width ratio. Use same size images for all properties which you want to add in slider',
                    'type' => 'image_advanced',
                    'max_file_uploads' => 1,
                    'columns' => 12,
                    'tab' => 'home_slider',
                ),

                //Multi Units / Sub Properties
                array(
                    'id' => "fave_multiunit_plans_enable",
                    'name' => 'Multi Units / Sub Properties',
                    'desc' => 'Enable/Disable',
                    'type' => 'select',
                    'std' => "disable",
                    'options' => array('disable' => 'Disable', 'enable' => 'Enable'),
                    'columns' => 12,
                    'tab' => 'multi_units'
                ),
                array(
                    'id'     => "fave_multi_units",
                    // Gropu field
                    'type'   => 'group',
                    // Clone whole group?
                    'clone'  => true,
                    'sort_clone' => true,
                    'tab' => 'multi_units',
                    // Sub-fields
                    'fields' => array(
                        array(
                            'name' =>  'Title',
                            'id'   => "fave_mu_title",
                            'type' => 'text',
                            'columns' => 12,
                        ),
                        array(
                            'name' =>  'Price',
                            'id'   => "fave_mu_price",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Price Postfix',
                            'id'   => "fave_mu_price_postfix",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Bedrooms',
                            'id'   => "fave_mu_beds",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Bathrooms',
                            'id'   => "fave_mu_baths",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Property Size',
                            'id'   => "fave_mu_size",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Size Postfix',
                            'id'   => "fave_mu_size_postfix",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Property Type',
                            'id'   => "fave_mu_type",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Availability Date',
                            'id'   => "fave_mu_availability_date",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        /*array(
                            'name' =>  'Image',
                            'id'   => "fave_mu_image",
                            'type' => 'file_input',
                            'columns' => 12,
                        )*/

                    ),
                ),

                //Floor Plans
                array(
                    'id' => "fave_floor_plans_enable",
                    'name' => 'Floor Plans',
                    'desc' => 'Enable/Disable floor plans',
                    'type' => 'select',
                    'std' => "disable",
                    'options' => array('disable' =>  'Disable', 'enable' =>  'Enable'),
                    'columns' => 12,
                    'tab' => 'floor_plans'
                ),
                array(
                    'id'     => 'floor_plans',
                    // Gropu field
                    'type'   => 'group',
                    // Clone whole group?
                    'clone'  => true,
                    'sort_clone' => true,
                    'tab' => 'floor_plans',
                    // Sub-fields
                    'fields' => array(
                        array(
                            'name' =>  'Plan Title',
                            'id'   => "fave_plan_title",
                            'type' => 'text',
                            'columns' => 12,
                        ),
                        array(
                            'name' =>  'Plan Bebrooms',
                            'id'   => "fave_plan_rooms",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Plan Bathrooms',
                            'id'   => "fave_plan_bathrooms",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Plan Price',
                            'id'   => "fave_plan_price",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Price Postfix',
                            'id'   => "fave_plan_price_postfix",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Plan Size',
                            'id'   => "fave_plan_size",
                            'type' => 'text',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Plan Image',
                            'id'   => "fave_plan_image",
                            'type' => 'file_input',
                            'columns' => 6,
                        ),
                        array(
                            'name' =>  'Plan Description',
                            'id'   => "fave_plan_description",
                            'type' => 'textarea',
                            'columns' => 12,
                        ),

                    ),
                ),

                // Attachments
                array(
                    'id' => "fave_attachments",
                    'name' => 'Attachments',
                    'desc' => 'You can attach PDF files, Map images OR other documents to provide further details related to property.',
                    'type' => 'file_advanced',
                    'mime_type' => '',
                    'columns' => 12,
                    'tab' => 'attachments',
                ),
				
				// Specifications
                array(
                    'id' => "fave_specifications",
                    'name' => 'Specifications',
                    //'desc' => 'You can attach PDF files, Map images OR other documents to provide further details related to property.',
                    'type' => 'textarea',
                    'mime_type' => '',
                    'columns' => 12,
                    'tab' => 'specifications',
                )
            )
        );

        return $meta_boxes;
 
} // End Meta boxes
 
 
 