<?php
/*
Plugin Name: Real Catalyst Projects
Version: 1.1
Description: Real Catalyst Projects API CALL
Author: ikb
Author URI:  
Plugin URI:  
*/

define("UserID", get_option('api_user_id'));
define("APIKey", get_option('api_auth'));
define("APIBaseURL", get_option('api_url'));
define("TokenApi", '9B5699AA-37FA-4276-8E84-156ECF6F9B45');

// Token for live api
  define("TokenCompany", '6b108516-b8b1-473c-8612-e965c9758137');
  
  // token for QA api
// define("TokenCompany", '222314cf-c958-4a80-b0c2-2fef7d57fe9f');

//website Link 
define("SiteLink", 'http://developer.realsynergy.estate');
// taxnomies
define("company", 'developer');
define("propertyType", 'property_type');
define("propertyCity", 'property_city');
define("Regions", 'property_area');
define("STATE", 'property_state');
define("status", 'property_status');
// Post type
define("propertyPostType", 'property');
  
real_inc_files();
function wp_gear_manager_admin_scripts() {
wp_enqueue_script('jquery');
} 
add_action('admin_print_scripts', 'wp_gear_manager_admin_scripts');
 
add_action('admin_menu', 'real_catalyst_menu');


	
function real_catalyst_menu(){
		add_menu_page('Real Catalyst', 'Real Catalyst', 'manage_options', 'real-catalyst','real_catalyst_index');
		add_submenu_page( 'real-catalyst', 'Real Catalyst Project', 'Real Catalyst Project','manage_options','real-catalyst','real_catalyst_index');
		add_submenu_page( 'real-catalyst', 'Settings', 'Settings','manage_options', 'real-catalyst-settings','real_catalyst_setting');
}
function real_catalyst_index(){
 
  include('real-catalyst-index.php');
 
 } 
function real_catalyst_setting(){

 include('real-catalyst-settings.php');
 
}
  
/**
 * Register the settings
 */
function wporg_register_settings() {
      register_setting( 'API_setting', 'api_url' );
      register_setting( 'API_setting', 'api_user_id' );
      register_setting( 'API_setting', 'api_auth' );
 
}
add_action( 'admin_init', 'wporg_register_settings' );

add_action( 'wp_ajax_my_action', 'my_action_callback' );
add_action( 'wp_ajax_ApiFilterData', 'ApiFilterData_callback' ); 
add_action( 'wp_ajax_ApiImportData', 'ApiImportData_callback' ); 
function my_action_callback(){
APISyncMasterData();
}
function ApiImportData_callback(){
 echo "Imported Project Ids - ".APIImportProject(); 
die();
}
function ApiFilterData_callback(){ 
 $TypeID =  $_POST['category_type'];
 $CityID  =  $_POST['city'];
 $CompanyID  =  $_POST['developer'];
 $keyword  =  $_POST['keyword'];
 $result =   APIGetProjects($keyword,$TypeID, $CityID, $CompanyID);
 if(!empty($result)){
 echo $result ;
 }
 else{
 echo 0;
 }
 die;
} 
 function real_inc_files() {
	include('data/real-catalyst-data.php');     
	include('real-catalyst-functions.php');
	include('data/meta/metaboxes.php');  
	include('data/meta/property-status-meta.php');  
	include('data/meta/property-label-meta.php');  
	include('data/meta/property-area-meta.php');   
	include('data/meta/houzez-meta-boxes.php');  
 if (!class_exists('RW_Meta_Box')) {
                require_once('data/meta/extensions/meta-box/meta-box.php');
            }
            if (!class_exists('RWMB_Tabs')) {
                require_once('data/meta/extensions/meta-box/addons/meta-box-tabs/meta-box-tabs.php');
            }
            if (!class_exists('RWMB_Columns')) {
                require_once('data/meta/extensions/meta-box/addons/meta-box-columns/meta-box-columns.php');
            }
            if (!class_exists('RWMB_Group')) {
                require_once('data/meta/extensions/meta-box/addons/meta-box-group/meta-box-group.php');
            }


 }