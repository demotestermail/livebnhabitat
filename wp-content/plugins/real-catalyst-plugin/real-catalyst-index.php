<div class="wrap">
<input id="sync_data" class="button button-primary" type="button" value="Sync Api Data" name=""> 
<br/>
<div id="sync_data_"></div>

<form id="filterdata" action="" method="post" onsubmit="return false;">
			  <table class="form-table"> 
			 
				<tbody>
				<tr valign="top"> 
				<td>
				<input id="keyword" type="text" name="keyword" placeholder="keyword"/>
				 
				<?php
				$categories = get_terms( propertyType, array(
					'orderby'    => 'count',
					'hide_empty' => 0,
					 'parent'   => 0
				) );
				$childcategory = array();
				 foreach($categories as $projectCatregory){
				 $label = $projectCatregory->name;  
				 $categories_child = get_terms( propertyType, array(
					'orderby'    => 'count',
					'hide_empty' => 0,
					 'parent'   => $projectCatregory->term_id
				) );
				  foreach($categories_child as $projectCatregoryChild){
				   $valuechild = get_term_meta($projectCatregoryChild->term_id, 'wpcf-categoryid',true);
				  $childcategory[] = array('id'=>$valuechild,'name'=>$projectCatregoryChild->name); 
				  }
				 
				 } 
				 
				?>
			  
				 <select id="category_type" name="category_type">
				  <option value="" >Select project type</option>
				 <?php
				 foreach($childcategory as $type){
				 
				 echo '<option value='.$type["id"].'>'.$type["name"].'</option>';
				 
				 }
				 ?></select> 
				 <select id="developer" name="developer">
				 <option value="" >Select Company</option>
				 <?php 
				 $companies = get_terms( company, array(
					'orderby'    => 'count',
					'hide_empty' => 0 
				) );
				 foreach($companies as $developer){
				$label = $developer->name;
				 $value = get_term_meta($developer->term_id, 'wpcf-id',true); 
				 echo 	'<option value="'. $value.'">'.$label.'</option>';
				 }
				?>  
				</select>
				 <select  id="city" name="city">
				  <option value="" >Select City</option>
				 <?php 
				 $location = get_terms( propertyCity, array(
					'orderby'    => 'count',
					'hide_empty' => 0 
				) );
				 foreach($location as $city){
				$label = $city->name;
				 $value = get_term_meta($city->term_id, 'wpcf-cityid',true); 
				 echo 	'<option value="'. $value.'">'.$label.'</option>';
				 }
				?> </select>
				 <input type="submit" value="Filter" class="button button-primary" id="filterApiData" name="submit"/> 
				 <div id="loader" style="float: right; width: 37%;"></div>
				</td>
				</tr>  
				 
  
    </tbody></table>
   </form>
   
   <table class="wp-list-table widefat fixed striped projects">
	<thead>  
	<tr>
		<td class="manage-column column-cb check-column" id="cb">
		<label for="cb-select-all-1" class="screen-reader-text">Select All</label>
		<input type="checkbox" id="cb-select-all-1">
		</td>
		<th class="manage-column column-username column-primary sortable desc" id="username" scope="col">
		<span>Project Name</span></th>
		<th class="manage-column" scope="col">Type</th>
		<th class="manage-column" scope="col">Category</th>
		<th class="manage-column" scope="col">Developer</th>
		<th class="manage-column" scope="col">Location/Address</th>	
		<th class="manage-column" scope="col">Price Range</th>	
		</tr>
	</thead>

	<tbody id="the_list_projects">
		
	 
	</tbody>

	
</table>
<p>
<input type="hidden" name="selectedData" id="selectedData">
<input type="button" name="importDataApi" id="importDataApi" class="button button-primary" value="Import Selected Projects" style="display:none;"></p><div id="message"></div>
</div>
<?php 

add_action( 'admin_footer', 'my_action_javascript' ); // Write our JS below here 

function my_action_javascript() { ?>
	<script type="text/javascript" >
	jQuery(document).ready(function() {
	
	jQuery('#sync_data').click(function() {
 
		var data = {
			'action': 'my_action' 
		};

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			jQuery('#sync_data_').html(response);
		});
		
		});
		
	 jQuery('#filterdata').on('submit', function() {
	 jQuery('#loader').html('<img style=" width: 26px;" src="<?php echo  plugins_url('real-catalyst-plugin/images');?>/loader.gif">'); 
       var developer = jQuery("#developer").val();
       var category_type = jQuery("#category_type").val();
       var category = jQuery("#category").val();
       var keyword = jQuery("#keyword").val();
       var city = jQuery("#city").val();
		var data = {
			 type: 'post',
            developer: developer,
            category_type: category_type,
            category: category,
            keyword: keyword,
            city: city,
			action: 'ApiFilterData' 
		}; 
		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
		jQuery('#loader').html(" ");
		if(response == 0){
		jQuery('#importDataApi').hide();
		jQuery('#the_list_projects').html('<tr><td colspan="6"><b>Result Not Found !!</b></td></tr>'); 
		}
		else{
			jQuery('#the_list_projects').html(response); 
			jQuery('#importDataApi').show();
		}
			 
			  
		});
		
		});
		 
	jQuery('#importDataApi').click(function() { 
	
	jQuery('#message').html('<img src="<?php echo  plugins_url('real-catalyst-plugin/images');?>/loader.gif">'); 
	var checkProjects = jQuery('input[name="projects[]"]:checked').serialize();
		  var city = jQuery("#city").val();
		var data = {
			 type: 'post',
            checkprojects: checkProjects, 
			action: 'ApiImportData' 
		}; 
		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) { 
			  jQuery('#message').html(response); 
			   
		});
			
		});
	});
	</script> <?php
}
