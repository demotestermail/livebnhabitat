<?php
// Field Array
$prefix = 'custom_';
$custom_meta_fields = $meta_boxes = array( 
                // Property Details
                array(
                    'id' => "fave_property_price",
                    'name' => 'Sale or Rent Price ( Only digits )',
                    'desc' => 'Eg: 557000',
                    'type' => 'text',
                    'std' => "",
                    'columns' => 12,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_sec_price",
                    'name' => 'Second Price ( Display optional price for rental or square feet )',
                    'desc' => 'Eg: 700',
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_price_postfix",
                    'name' => 'After Price Label',
                    'desc' => 'Eg: Per Month',
                    'type' => 'text',
                    'std' => "",
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_size",
                    'name' => 'Area Size ( Only digits )',
                    'desc' => 'Eg: 1500',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'area_size',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_size_prefix",
                    'name' => 'Size Prefix',
                    'desc' => 'Eg: Sq Ft',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'area_size',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_bedrooms",
                    'name' => 'Bedrooms',
                    'desc' => 'Eg: 4',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'beds_hidden',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_bathrooms",
                    'name' => 'Bathrooms',
                    'desc' => 'Eg: 3',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'baths_hidden',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_garage",
                    'name' => 'Garages',
                    'desc' => 'Eg: 1',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'garages',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),
                array(
                    'id' => "fave_property_garage_size",
                    'name' => 'Garages Size',
                    'desc' => "",
                    'type' => 'text',
                    'std' => "",
                    'class' => 'garage_size',
                    'columns' => 6,
                    'tab' => 'property_details',
                ), 
                array(
                    'id' => "fave_property_id",
                    'name' => 'Property ID',
                    'desc' => 'It will help you search a property directly.',
                    'type' => 'text',
                    'std' => "",
                    'class' => 'prop_id',
                    'columns' => 6,
                    'tab' => 'property_details',
                ),

                // Property Map
                array(
                    'type' => 'divider',
                    'columns' => 12,
                    'id' => 'google_map_divider',
                    'tab' => 'property_details',
                ), 
                array(
                    'id' => "fave_property_map_address",
                    'name' => 'Property Address',
                    'desc' => 'Leave it empty if you want to hide map on property detail page.',
                    'type' => 'text',
                    'std' => '',
                    'columns' => 12,
                    'tab' => 'property_map',
                ), 
                // Property Settings
                array(
                    'id' => "fave_property_address",
                    'name' => 'Address(*only street name and building no)',
                    'desc' => "",
                    'type' => 'textarea',
                    'columns' => 6,
                    'tab' => 'property_settings',
                ),
                array(
                    'id' => "fave_property_zip",
                    'name' => 'Zip',
                    'desc' => "",
                    'type' => 'text',
                    'columns' => 6,
                    'tab' => 'property_settings',
                ), 

                // Gallery
                // array(
                    // 'name' => 'Property Gallery Images',
                    // 'id' => "fave_property_images",
                    // 'desc' => 'Recommend image size 1170 x 738',
                    // 'type' => 'image_advanced',
                    // 'max_file_uploads' => 48,
                    // 'columns' => 12,
                    // 'tab' => 'gallery',
                // ),

                // Property Video
                array(
                    'id' => "fave_video_url",
                    'name' => 'Video URL',
                    'desc' => 'Provide video URL. YouTube, Vimeo, SWF File and MOV File are supported',
                    'type' => 'text',
                    'columns' => 12,
                    'tab' => 'video',
                ), 
 
				// Specifications
                array(
                    'id' => "fave_specifications",
                    'name' => 'Specifications',
                    //'desc' => 'You can attach PDF files, Map images OR other documents to provide further details related to property.',
                    'type' => 'textarea',
                    'mime_type' => '',
                    'columns' => 12,
                    'tab' => 'specifications',
                )
            );
     
function add_custom_meta_box() {
 
        add_meta_box(
            'custom_meta_box', // $id
            'Custom Meta Box', // $title 
            'show_custom_meta_box', // $callback
             propertyPostType,
            'normal', // $context
            'high' // $priority
        );
    
}
add_action('add_meta_boxes', 'add_custom_meta_box');


function show_custom_meta_box() {
global $custom_meta_fields, $post; 
// Use nonce for verification
echo '<input type="hidden" name="custom_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
     
    // Begin the field table and loop
    echo '<table class="form-table">';
    foreach ($custom_meta_fields as $field) {
        // get value of this field if it exists for this post
        $meta = get_post_meta($post->ID, $field['id'], true);
        // begin a table row with
        echo '<tr>
                <th><label for="'.$field['id'].'">'.$field['name'].'</label></th>
                <td>';
                switch($field['type']) {
                    // case items will go here
					// text
					case 'text':
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
							<br /><span class="description">'.$field['desc'].'</span>';
					break;
					// textarea
					case 'textarea':
						echo '<textarea name="'.$field['id'].'" id="'.$field['id'].'" cols="60" rows="4">'.$meta.'</textarea>
							<br /><span class="description">'.$field['desc'].'</span>';
					break;
											// checkbox
						case 'checkbox':
							echo '<input type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ',$meta ? ' checked="checked"' : '','/>
								<label for="'.$field['id'].'">'.$field['desc'].'</label>';
						break;
					// select
					case 'select':
						echo '<select name="'.$field['id'].'" id="'.$field['id'].'">';
						foreach ($field['options'] as $option) {
							echo '<option', $meta == $option['value'] ? ' selected="selected"' : '', ' value="'.$option['value'].'">'.$option['label'].'</option>';
						}
						echo '</select><br /><span class="description">'.$field['desc'].'</span>';
					break;
                } //end switch
        echo '</td></tr>';
    } // end foreach
    echo '</table>'; // end table
}


function save_custom_meta($post_id) {
    global $custom_meta_fields;
     
    // verify nonce
    if (!wp_verify_nonce($_POST['custom_meta_box_nonce'], basename(__FILE__))) 
        return $post_id;
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }
     
    // loop through fields and save the data
    foreach ($custom_meta_fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    } // end foreach
}
add_action('save_post', 'save_custom_meta');



function hhs_get_sample_options() {
	$options = array (
		'Option 1' => 'option1',
		'Option 2' => 'option2',
		'Option 3' => 'option3',
		'Option 4' => 'option4',
	);
	
	return $options;
}
add_action('admin_init', 'hhs_add_meta_boxes', 1);
function hhs_add_meta_boxes() {
	add_meta_box( 'repeatable-fields', 'Sub Properties', 'hhs_repeatable_meta_box_display', propertyPostType, 'normal', 'default');
}
function hhs_repeatable_meta_box_display() {
	global $post;
	$fave_multi_units = get_post_meta($post->ID, 'fave_multi_units', true);
	$options = hhs_get_sample_options();
	wp_nonce_field( 'hhs_repeatable_meta_box_nonce', 'hhs_repeatable_meta_box_nonce' );
	?>
	<script type="text/javascript">
	jQuery(document).ready(function( $ ){
		$( '#add-row' ).on('click', function() {
			var row = $( '.empty-row.screen-reader-text' ).clone(true);
			row.removeClass( 'empty-row screen-reader-text' );
			row.insertBefore( '#repeatable-fieldset-one tbody>tr:last' );
			return false;
		});
  	
		$( '.remove-row' ).on('click', function() {
			$(this).parents('tr').remove();
			return false;
		});
	});
	</script>
  
	<table id="repeatable-fieldset-one" width="100%">
	<thead>
		<tr> 
			<th width="30%">Title</th>
			<th width="10%">Price</th>
			<th width="15%">Property Size</th>
			<th width="20%">Size Postfix</th>
			<th width="20%">Property Type</th>
			<th width="15%"></th>
		</tr>
	</thead>
	<tbody>
	<?php
	
	if ( $fave_multi_units ) :
	
	foreach ( $fave_multi_units as $field ) {
	?>
	<tr>
		<td><input type="text" class="widefat" name="fave_mu_title[]" value="<?php if($field['fave_mu_title'] != '') echo esc_attr( $field['fave_mu_title'] ); ?>" /></td>
	<td><input type="text" class="widefat" name="fave_mu_price[]" value="<?php if($field['fave_mu_price'] != '') echo esc_attr( $field['fave_mu_price'] ); ?>" /></td>
	<td><input type="text" class="widefat" name="fave_mu_size[]" value="<?php if($field['fave_mu_size'] != '') echo esc_attr( $field['fave_mu_size'] ); ?>" /></td>
	<td><input type="text" class="widefat" name="fave_mu_size_postfix[]" value="<?php if($field['fave_mu_size_postfix'] != '') echo esc_attr( $field['fave_mu_size_postfix'] ); ?>" /></td>
	<td><input type="text" class="widefat" name="fave_mu_type[]" value="<?php if($field['fave_mu_type'] != '') echo esc_attr( $field['fave_mu_type'] ); ?>" /></td>
	
		<td><a class="button remove-row" href="#">Remove</a></td>
	</tr>
	<?php
	}
	else :
	// show a blank one
	?>
	<tr>
		<td><input type="text" class="widefat" name="fave_mu_title[]" value="" /></td>
	<td><input type="text" class="widefat" name="fave_mu_price[]" value="" /></td>
	<td><input type="text" class="widefat" name="fave_mu_size[]" value="" /></td>
	<td><input type="text" class="widefat" name="fave_mu_size_postfix[]" value="" /></td>
	<td><input type="text" class="widefat" name="fave_mu_type[]" value="" /></td>
	 
		<td><a class="button remove-row" href="#">Remove</a></td>
	</tr>
	<?php endif; ?>
	
	<!-- empty hidden one for jQuery -->
	<tr class="empty-row screen-reader-text">
		<td><input type="text" class="widefat" name="fave_mu_title[]" value="" /></td>
	<td><input type="text" class="widefat" name="fave_mu_price[]" value="" /></td>
	<td><input type="text" class="widefat" name="fave_mu_size[]" value="" /></td>
	<td><input type="text" class="widefat" name="fave_mu_size_postfix[]" value="" /></td>
	<td><input type="text" class="widefat" name="fave_mu_type[]" value="" /></td>
	 
		  
		<td><a class="button remove-row" href="#">Remove</a></td>
	</tr>
	</tbody>
	</table>
	
	<p><a id="add-row" class="button" href="#">Add another</a></p>
	<?php
}
add_action('save_post', 'hhs_repeatable_meta_box_save');
function hhs_repeatable_meta_box_save($post_id) {
	if ( ! isset( $_POST['hhs_repeatable_meta_box_nonce'] ) ||
	! wp_verify_nonce( $_POST['hhs_repeatable_meta_box_nonce'], 'hhs_repeatable_meta_box_nonce' ) )
		return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	
	if (!current_user_can('edit_post', $post_id))
		return;
	
	$old = get_post_meta($post_id, 'fave_multi_units', true);
	$new = array();
	$options = hhs_get_sample_options();
	
	// $names = $_POST['name'];
	// $selects = $_POST['select'];
	// $urls = $_POST['url'];
	
		$fave_mu_title = $_POST['fave_mu_title'];
	$fave_mu_price = $_POST['fave_mu_price'];
	$fave_mu_size = $_POST['fave_mu_size'];
	$fave_mu_size_postfix = $_POST['fave_mu_size_postfix'];
	$fave_mu_type = $_POST['fave_mu_type'];
	$count = count( $fave_mu_title );
	
	for ( $i = 0; $i < $count; $i++ ) {
		if ( $fave_mu_title[$i] != '' ) :
			$new[$i]['fave_mu_title'] = stripslashes( strip_tags( $fave_mu_title[$i] ) );
			$new[$i]['fave_mu_price'] = stripslashes( strip_tags( $fave_mu_price[$i] ) );
			$new[$i]['fave_mu_size'] = stripslashes( strip_tags( $fave_mu_size[$i] ) );
			$new[$i]['fave_mu_size_postfix'] = stripslashes( strip_tags( $fave_mu_size_postfix[$i] ) );
			$new[$i]['fave_mu_type'] = stripslashes( strip_tags( $fave_mu_type[$i] ) );
			 
		endif;
	}
	if ( !empty( $new ) && $new != $old )
		update_post_meta( $post_id, 'fave_multi_units', $new );
	elseif ( empty($new) && $old )
		delete_post_meta( $post_id, 'fave_multi_units', $old );
}


function my_gallery_metabox_post_types( $types ) {
    $types[] = propertyPostType; // Add to portfolio
    return $types;
}
add_filter( 'wpex_gallery_metabox_post_types', 'my_gallery_metabox_post_types' );


?>