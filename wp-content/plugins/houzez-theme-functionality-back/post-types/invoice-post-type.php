<?php
/**
 * Invoice Post Type
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 04/02/16
 * Time: 3:44 AM
 */
if( !function_exists( 'houzez_invoice_post_type' ) ){
    function houzez_invoice_post_type(){
        $labels = array(
            'name' => __( 'Invoices','houzez'),
            'singular_name' => __( 'Invoice','houzez' ),
            'add_new' => __('Add New','houzez'),
            'add_new_item' => __('Add New Invoice','houzez'),
            'edit_item' => __('Edit Invoice','houzez'),
            'new_item' => __('New Invoice','houzez'),
            'view_item' => __('View Invoice','houzez'),
            'search_items' => __('Search Invoice','houzez'),
            'not_found' =>  __('No Invoice found','houzez'),
            'not_found_in_trash' => __('No Invoice found in Trash','houzez'),
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_icon' => 'dashicons-book',
            'menu_position' => 10,
            'supports' => array('title'),
            'exclude_from_search'   => true,
            'can_export' => true,
            'rewrite' => array( 'slug' => __('invoice', 'houzez') )
        );

        register_post_type('houzez_invoice',$args);
    }
}
add_action( 'init', 'houzez_invoice_post_type' );

/**************************************************************************
 * Add Custom Columns
 **************************************************************************/
add_filter("manage_edit-houzez_invoice_columns", "houzez_invoices_edit_columns");
if( !function_exists( 'houzez_invoices_edit_columns' ) ){
    function houzez_invoices_edit_columns($columns)
    {

        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __( 'Invoice Title','houzez' ),
            "invoice_price" => __( 'Price','houzez' ),
            "invoice_type" => __('Invoice Type','houzez'),
            "billing_for" => __('Billion For','houzez'),
            "invoice_buyer" => __('Buyer','houzez'),
            "invoice_status" => __( 'Status','houzez' ),
            "date" => __( 'Date','houzez' )
        );

        return $columns;
    }
}


add_action( 'manage_posts_custom_column', 'houzez_invoice_populate_columns' );
if( !function_exists( 'houzez_invoice_populate_columns' ) ){
    function houzez_invoice_populate_columns($column){
        global $post;

        $invoice_meta = get_post_meta( $post->ID, '_favethemes_invoice_meta', true );
        switch ($column)
        {
            case 'invoice_price':
                echo esc_attr( $invoice_meta['invoice_item_price'] );
                break;

            case 'invoice_type':
                echo esc_attr( $invoice_meta['invoice_billing_type'] );
                break;

            case 'billing_for':
                echo esc_attr( $invoice_meta['invoice_billion_for'] );
                break;

            case 'invoice_buyer':
                $user_info = get_userdata($invoice_meta['invoice_buyer_id']);
                echo esc_attr( $user_info->display_name );
                break;

            case 'invoice_status':
                $invoice_status = get_post_meta(  $post->ID, 'invoice_payment_status', true );
                if( $invoice_status == 0 ) {
                    echo '<span class="fave_admin_label float-none label-red">'.__('Not Paid','houzez').'</span>';
                } else {
                    echo '<span class="fave_admin_label float-none label-green">'.__('Paid','houzez').'</span>';
                }
                break;
        }
    }
}

add_filter( 'manage_edit-houzez_invoice_sortable_columns', 'houzez_invoice_sort' );
if( !function_exists('houzez_invoice_sort') ):
    function houzez_invoice_sort( $columns ) {
        $columns['invoice_price']  = 'invoice_price';
        $columns['invoice_type']   = 'invoice_type';
        $columns['billing_for']    = 'billing_for';
        $columns['invoice_buyer']  = 'invoice_buyer';
        $columns['invoice_status'] = 'invoice_status';
        return $columns;
    }
endif;

// Save Invoice
if( !function_exists('houzez_add_invoice') ):
    function houzez_add_invoice( $billingFor, $billionType, $packageID, $invoiceDate, $userID, $featured, $upgrade, $paypalTaxID ) {

        $price_per_submission = houzez_option('price_listing_submission');
        $price_featured_submission = houzez_option('price_featured_listing_submission');

        $price_per_submission      = floatval( $price_per_submission );
        $price_featured_submission = floatval( $price_featured_submission );

        $args = array(
            'post_title'	=> 'Invoice ',
            'post_status'	=> 'publish',
            'post_type'     => 'houzez_invoice'
        );
        $inserted_post_id =  wp_insert_post( $args );

        if( $billionType != 'one_time' ) {
            $billionType = __( 'Recurring', 'houzez' );
        } else {
            $billionType = __( 'One Time', 'houzez' );
        }

        if( $billingFor != 'package' ) {
            if( $upgrade == 1 ) {
                $total_price = $price_featured_submission;

            } else {
                if( $featured == 1 ) {
                    $total_price = $price_per_submission+$price_featured_submission;
                } else {
                    $total_price = $price_per_submission;
                }
            }
        } else {
            $total_price = get_post_meta( $packageID, 'fave_package_price', true);
        }

        $fave_meta = array();

        $fave_meta['invoice_billion_for'] = $billingFor;
        $fave_meta['invoice_billing_type'] = $billionType;
        $fave_meta['invoice_item_id'] = $packageID;
        $fave_meta['invoice_item_price'] = $total_price;
        $fave_meta['invoice_purchase_date'] = $invoiceDate;
        $fave_meta['invoice_buyer_id'] = $userID;
        $fave_meta['paypal_txn_id'] = $paypalTaxID;

        update_post_meta( $inserted_post_id, 'HOUZEZ_invoice_buyer', $userID );
        update_post_meta( $inserted_post_id, 'HOUZEZ_invoice_type', $billionType );
        update_post_meta( $inserted_post_id, 'HOUZEZ_invoice_for', $billingFor );
        update_post_meta( $inserted_post_id, 'HOUZEZ_invoice_item_id', $packageID );
        update_post_meta( $inserted_post_id, 'HOUZEZ_invoice_price', $total_price );
        update_post_meta( $inserted_post_id, 'HOUZEZ_invoice_date', $invoiceDate );
        update_post_meta( $inserted_post_id, 'HOUZEZ_paypal_txn_id', $paypalTaxID );

        update_post_meta( $inserted_post_id, '_favethemes_invoice_meta', $fave_meta );

        // Update post title
        $update_post = array(
            'ID'         => $inserted_post_id,
            'post_title' => 'Invoice '.$inserted_post_id,
        );
        wp_update_post( $update_post );
        return $inserted_post_id;
    }
endif;