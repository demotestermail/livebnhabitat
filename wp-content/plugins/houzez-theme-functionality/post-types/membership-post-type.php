<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 06/10/15
 * Time: 12:39 PM
 */

// register the custom post type
add_action( 'init', 'houzez_create_membership_type' );

if( !function_exists('houzez_create_membership_type') ):
    function houzez_create_membership_type() {
        register_post_type( 'houzez_packages',
            array(
                'labels' => array(
                    'name'          => __( 'Membership Packages','houzez'),
                    'singular_name' => __( 'Membership Packages','houzez'),
                    'add_new'       => __('Add New Membership Package','houzez'),
                    'add_new_item'          =>  __('Add Membership Packages','houzez'),
                    'edit'                  =>  __('Edit Membership Packages' ,'houzez'),
                    'edit_item'             =>  __('Edit Membership Package','houzez'),
                    'new_item'              =>  __('New Membership Packages','houzez'),
                    'view'                  =>  __('View Membership Packages','houzez'),
                    'view_item'             =>  __('View Membership Packages','houzez'),
                    'search_items'          =>  __('Search Membership Packages','houzez'),
                    'not_found'             =>  __('No Membership Packages found','houzez'),
                    'not_found_in_trash'    =>  __('No Membership Packages found','houzez'),
                    'parent'                =>  __('Parent Membership Package','houzez')
                ),
                'public' => true,
                'has_archive' => true,
                'rewrite' => array('slug' => 'package'),
                'supports' => array('title'),
                'exclude_from_search'   => true,
                'can_export' => true,
                'menu_icon'=> 'dashicons-money'
            )
        );
    }
endif; // end   houzez_create_membership_type