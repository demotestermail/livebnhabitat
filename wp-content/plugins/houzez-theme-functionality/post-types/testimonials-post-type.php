<?php
/**
 * Custom Post Type Testimmonials
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 07/01/16
 * Time: 2:45 PM
 */
if( !function_exists( 'houzez_testimonial_post_type' ) ){
    function houzez_testimonial_post_type(){
        $labels = array(
            'name' => __( 'Testimonials','houzez'),
            'singular_name' => __( 'Testimonial','houzez' ),
            'add_new' => __('Add New','houzez'),
            'add_new_item' => __('Add New Testimonial','houzez'),
            'edit_item' => __('Edit Testimonial','houzez'),
            'new_item' => __('New Testimonial','houzez'),
            'view_item' => __('View Testimonial','houzez'),
            'search_items' => __('Search Agent','houzez'),
            'not_found' =>  __('No Testimonial found','houzez'),
            'not_found_in_trash' => __('No Testimonial found in Trash','houzez'),
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_icon' => 'dashicons-businessman',
            'menu_position' => 5,
            'supports' => array('title', 'page-attributes','revisions'),
            'rewrite' => array( 'slug' => __('testimonials', 'houzez') )
        );

        register_post_type('houzez_testimonials',$args);
    }
}
add_action( 'init', 'houzez_testimonial_post_type' );
?>