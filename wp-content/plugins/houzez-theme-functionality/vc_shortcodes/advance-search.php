<?php
/*-----------------------------------------------------------------------------------*/
/*	Advance Search
/*-----------------------------------------------------------------------------------*/
if( !function_exists('houzez_advance_search') ) {
    function houzez_advance_search($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'search_title' => ''
        ), $atts));

        ob_start();

        $search_template = houzez_get_template_link('template/template-search.php');
        $measurement_unit_adv_search = houzez_option('measurement_unit_adv_search');
        if( $measurement_unit_adv_search == 'sqft' ) {
            $measurement_unit_adv_search = houzez_option('measurement_unit_sqft_text');
        } elseif( $measurement_unit_adv_search == 'sq_meter' ) {
            $measurement_unit_adv_search = houzez_option('measurement_unit_square_meter_text');
        }


        $adv_search_price_slider = houzez_option('adv_search_price_slider');
        $adv_show_hide = houzez_option('adv_show_hide');
        $hide_advanced = false;

        $keyword_field = houzez_option('keyword_field');

        if( $keyword_field == 'prop_title' ) {
            $keyword_field_placeholder = esc_html__('Enter keyword...', 'houzez');

        } else if( $keyword_field == 'prop_city_state_county' ) {
            $keyword_field_placeholder = esc_html__('Search City, State or Area', 'houzez');
        }else {
            $keyword_field_placeholder = esc_html__('Enter an address, town, street, or zip', 'houzez');
        }

        $status = $type = $location = $area = '';
        if (isset($_GET['status'])) {
            $status = $_GET['status'];
        }
        if (isset($_GET['type'])) {
            $type = $_GET['type'];
        }
        if (isset($_GET['location'])) {
            $location = $_GET['location'];
        }
        if (isset($_GET['area'])) {
            $location = $_GET['area'];
        }
         if (isset($_GET['developer'])) {
            $developer = $_GET['developer'];
        }

        if( $adv_show_hide['status']         != 0 &&
            $adv_show_hide['type']           != 0 &&
            $adv_show_hide['beds']           != 0 &&
            $adv_show_hide['baths']          != 0 &&
            $adv_show_hide['min_area']       != 0 &&
            $adv_show_hide['max_area']       != 0 &&
            $adv_show_hide['min_price']      != 0 &&
            $adv_show_hide['max_price']      != 0 &&
            $adv_show_hide['price_slider']   != 0 &&
            $adv_show_hide['area_slider']    != 0 &&
            $adv_show_hide['other_features'] != 0  ) {

            $hide_advanced = true;
        }
        $keyword_field = houzez_option('keyword_field');
        ?>

        <!--start advance search module-->
        <div class="advanced-search advanced-search-module houzez-adv-price-range">

            <?php if (!empty($search_title)) { ?>
                <h3 class="advance-title"><i class="fa fa-search"></i> <?php echo esc_attr($search_title); ?></h3>
            <?php } ?>
            <form method="get" action="<?php echo esc_url($search_template); ?>">
                <div class="row">
                    <div class="col-sm-4 col-xs-12 vc_keyword_search_field">
                        <div class="form-group no-margin">
                            <input type="text" class="houzez_geocomplete form-control"
                                   value="<?php echo isset ($_GET['keyword']) ? $_GET['keyword'] : ''; ?>"
                                   name="keyword" placeholder="<?php esc_html_e( $keyword_field_placeholder ); ?>">
                        </div>
                    </div>

                    <?php if( $adv_show_hide['cities'] != 1 ) { ?>
                    <div class="col-sm-2 col-xs-12">
                        <div class="form-group no-margin">
                            <select name="location" class="selectpicker" data-live-search="false"
                                    data-live-search-style="begins">
                                <?php
                                // All Option
                                echo '<option value="">' . esc_html__('All Cities', 'houzez') . '</option>';

                                $prop_city = get_terms(
                                    array(
                                        "property_city"
                                    ),
                                    array(
                                        'orderby' => 'name',
                                        'order' => 'ASC',
                                        'hide_empty' => true,
                                        'parent' => 0
                                    )
                                );
                                houzez_hirarchical_options('property_city', $prop_city, $location);
                                ?>
                            </select>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $adv_show_hide['areas'] != 1 ) { ?>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group no-margin areas-select">
                            <select name="area" class="selectpicker" data-live-search="false" data-live-search-style="begins">
                                <?php
                                // All Option
                                echo '<option value="">'.esc_html__( 'All Areas', 'houzez' ).'</option>';

                                $prop_area = get_terms (
                                    array(
                                        "property_area"
                                    ),
                                    array(
                                        'orderby' => 'name',
                                        'order' => 'ASC',
                                        'hide_empty' => true,
                                        'parent' => 0
                                    )
                                );
                                houzez_hirarchical_options('property_area', $prop_area, $area );
                                ?>
                            </select>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $adv_show_hide['type'] != 1 ) { ?>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <select class="selectpicker" name="type" data-live-search="false"
                                    data-live-search-style="begins">
                                <?php
                                // All Option
                                echo '<option value="">' . esc_html__('All Types', 'houzez') . '</option>';

                                $prop_type = get_terms(
                                    array(
                                        "property_type"
                                    ),
                                    array(
                                        'orderby' => 'name',
                                        'order' => 'ASC',
                                        'hide_empty' => false,
                                        'parent' => 0
                                    )
                                );
                                houzez_hirarchical_options('property_type', $prop_type, $type);
                                ?>
                            </select>
                        </div>
                    </div>
                    <?php } ?>

                   <?php if( $adv_show_hide['status'] != 1 ) { ?>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <select class="selectpicker" id="selected_status_module" name="status"
                                    data-live-search="false" data-live-search-style="begins"
                                    >
                                <?php
                                // All Option
                                echo '<option value="">' . esc_html__('All Status', 'houzez') . '</option>';

                                $prop_status = get_terms(
                                    array(
                                        "property_status"
                                    ),
                                    array(
                                        'orderby' => 'name',
                                        'order' => 'ASC',
                                        'hideesc_html_empty' => false,
                                        'parent' => 0
                                    )
                                );
                                houzez_hirarchical_options('property_status', $prop_status, $status);
                                ?>
                            </select>
                        </div>
                    </div>
                   <?php } ?>

                    <?php if( $adv_show_hide['beds'] != 1 ) { ?>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <select name="bedrooms" class="selectpicker" data-live-search="false"
                                    data-live-search-style="begins" title="">
                                <option value=""><?php esc_html_e( 'Beds', 'houzez' ); ?></option>
                                <?php houzez_number_list('bedrooms'); ?>
                            </select>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $adv_show_hide['baths'] != 1 ) { ?>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <select name="bathrooms" class="selectpicker" data-live-search="false"
                                    data-live-search-style="begins" title="">
                                <option value=""><?php esc_html_e( 'Baths', 'houzez' ); ?></option>
                                <?php houzez_number_list('bathrooms'); ?>
                            </select>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $adv_show_hide['min_area'] != 1 ) { ?>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <input type="text" class="form-control"
                                   value="<?php echo isset ($_GET['min-area']) ? $_GET['min-area'] : ''; ?>"
                                   name="min-area" placeholder="<?php esc_html_e( 'Min Area', 'houzez' ); echo " ($measurement_unit_adv_search)";?>">
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $adv_show_hide['max_area'] != 1 ) { ?>
                    <div class="col-sm-2 col-xs-6">
                        <div class="form-group">
                            <input type="text" class="form-control"
                                   value="<?php echo isset ($_GET['max-area']) ? $_GET['max-area'] : ''; ?>"
                                   name="max-area" placeholder="<?php esc_html_e( 'Max Area', 'houzez' ); echo " ($measurement_unit_adv_search)"; ?>">
                        </div>
                    </div>
                    <?php } ?>

                    <?php if( $adv_search_price_slider != 0 ) { ?>
                         <?php if( $adv_show_hide['price_slider'] != 1 ) { ?>
                            <div class="col-sm-4 col-xs-6">
                                <div class="range-advanced-main">
                                    <div class="range-text">
                                        <input type="hidden" name="min-price" class="min-price-range-hidden range-input" readonly >
                                        <input type="hidden" name="max-price" class="max-price-range-hidden range-input" readonly >
                                        <p><span class="range-title"><?php echo esc_html_e('Price Range:','houzez');?></span> <?php echo esc_html_e('from','houzez');?> <span class="min-price-range"></span> <?php echo esc_html_e('to','houzez');?> <span class="max-price-range"></span></p>
                                    </div>
                                    <div class="range-wrap">
                                        <div class="price-range-advanced"></div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                    <?php } else { ?>

                        <?php if( $adv_show_hide['min_price'] != 1 ) { ?>
                        <div class="col-sm-2 col-xs-6">
                            <div class="form-group prices-for-all">
                                <select name="min-price" class="selectpicker" data-live-search="false"
                                        data-live-search-style="begins" title="">
                                    <option value=""><?php esc_html_e( 'Min Price', 'houzez' ); ?></option>
                                    <?php houzez_min_price_list(); ?>
                                </select>
                            </div>
                            <div class="form-group hide prices-only-for-rent">
                                <select name="min-price" class="selectpicker" data-live-search="false"
                                        data-live-search-style="begins" title="">
                                    <option value=""><?php esc_html_e( 'Min Price', 'houzez' ); ?></option>
                                    <?php houzez_min_price_list_for_rent(); ?>
                                </select>
                            </div>
                        </div>
                        <?php } ?>

                        <?php if( $adv_show_hide['max_price'] != 1 ) { ?>
                        <div class="col-sm-2 col-xs-6">
                            <div class="form-group prices-for-all">
                                <select name="max-price" class="selectpicker" data-live-search="false"
                                        data-live-search-style="begins" title="">
                                    <option value=""><?php esc_html_e( 'Max Price', 'houzez' ); ?></option>
                                    <?php houzez_max_price_list() ?>
                                </select>
                            </div>
                            <div class="form-group hide prices-only-for-rent">
                                <select name="max-price" class="selectpicker" data-live-search="false"
                                        data-live-search-style="begins" title="">
                                    <option value=""><?php esc_html_e( 'Max Price', 'houzez' ); ?></option>
                                    <?php houzez_max_price_list_for_rent() ?>
                                </select>
                            </div>
                        </div>
                        <?php } ?>
                    <?php } ?>


                    <div class="col-sm-12 col-xs-12 other-features clearfix">
                        <div class="row">
                            <div class="col-sm-2 col-xs-12 pull-right">
                                <button type="submit" class="btn btn-orange"><i
                                        class="fa fa-search"></i></i><?php esc_html_e('Search', 'houzez'); ?></button>
                            </div>

                            <?php if( $adv_show_hide['other_features'] != 1 ) { ?>
                            <div class="col-sm-6 col-xs-12 pull-left">
                                <label class="title advance-trigger"><i
                                        class="fa fa-plus-square"></i> <?php esc_html_e('Other Features', 'houzez'); ?>
                                </label>
                            </div>
                            <?php } ?>

                        </div>
                    </div>

                    <?php if( $adv_show_hide['other_features'] != 1 ) { ?>
                    <div class="col-sm-12 col-xs-12 features-list field-expand">
                        <?php
                        if (taxonomy_exists('property_feature')) {
                            $prop_features = get_terms(
                                array(
                                    "property_feature"
                                ),
                                array(
                                    'orderby' => 'name',
                                    'order' => 'ASC',
                                    'hide_empty' => false,
                                    'parent' => 0
                                )
                            );

                            if (!empty($prop_features)) {
                                foreach ($prop_features as $feature):
                                    echo '<label class="checkbox-inline">';
                                    echo '<input name="feature[]" id="feature-' . esc_attr($feature->slug) . '" type="checkbox" value="' . esc_attr($feature->slug) . '">';
                                    echo esc_attr($feature->name);
                                    echo '</label>';
                                endforeach;
                            }
                        }
                        ?>
                    </div>
                    <?php } ?>

                </div>
            </form>

        </div>
        <!--end advance search module-->

        <?php
        $result = ob_get_contents();
        ob_end_clean();
        return $result;

    }

    add_shortcode('hz-advance-search', 'houzez_advance_search');
}
?>