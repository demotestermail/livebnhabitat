<?php
/**
 * Role Management
 *
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 08/08/16
 * Time: 9:38 PM
 */
/*function add_roles_on_plugin_activation() {
    add_role( 'custom_role', 'Custom Subscriber', array( 'read' => true, 'level_0' => true ) );
}
register_activation_hook( __FILE__, 'add_roles_on_plugin_activation' );*/

add_role(
    'houzez_basic_user',
    __( 'Houzez Basic User' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
        'delete_posts' => false, // Use false to explicitly deny
    )
);

add_role(
    'houzez_agent',
    __( 'Houzez Agent' ),
    array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
        'delete_posts' => false, // Use false to explicitly deny
    )
);