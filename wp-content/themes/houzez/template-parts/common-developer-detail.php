<?php
/**
 * Common Taxonomy - Used by property taxonomies
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 08/01/16
 * Time: 6:09 PM
 */
global $post, $taxonomy_title, $taxonomy_name;
$listing_view = "list-view";

if( $listing_view == 'grid_view' ) {
$listing_view = 'grid-view';
} else {
$listing_view = 'list-view';
}
// Title
    $current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $taxonomy_title = $current_term->name;
    $sticky_sidebar = houzez_option('sticky_sidebar');
    $taxonomy_name = get_query_var( 'taxonomy' );
    $developer_id=$current_term->term_id;
    $Established_year=get_term_meta($developer_id,'wpcf-established-in',true);
    $structred=get_term_meta($developer_id,'wpcf-structure',true);
    $developer_logo = get_term_meta($developer_id,'wpcf-developer-logo',true);
    // print_r($current_term); die();
?>
    <?php get_template_part('template-parts/properties-head'); ?>
    <div class="developer_detail_page">
            <div class="profile-detail-block developers_list">
                <?php if(!empty($developer_logo)){ ?>
                <div class="developers_list_img"> <img src="<?=$developer_logo?>"></div>
                <?php } ?>
                <div class="media">
                    <div class="media-body">
                        <div class="profile-description">
                            <!-- <h4 class="position">
                                    Title
                                </h4> -->
                            <table>
                                <tbody>
                                    <tr>
                                        <th>
                                            <?php echo $taxonomy_title; ?>
                                        </th>
                                        <?php if(!empty($Established_year)){ ?> 
                                        <th>Established in
                                            <?php echo $Established_year; ?> </th>
                                            <?php } 
                                            if(!empty($structred)){  ?>
                                        <th> Structure :
                                            <?php echo $structred; ?>
                                        </th>
                                        <?php } ?>
                                    </tr>
                                    <tr>
                                        <td>Total Projects :
                                            <?=$current_term->count;?>
                                        </td>
                                        <!-- <td> Ongoing Projects :
                                            <?=$current_term->count;?>
                                        </td>
                                        <td> Upcoming Projects :
                                            <?=$current_term->count;?>
                                        </td> -->
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
          <!--   <div class="min_menu_de">
                <ul class="nav navbar-nav">
                    <li><a href="#">About</a></li>
                    <li><a href="#">Management</a></li> 
                    <li><a href="#">Township(s)</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Completed Projects</a></li>
                            <li><a href="#">Ongoing Projects </a></li>
                            <li><a href="#"> Upcoming Projects </a></li>
                        </ul>
                    </li>
                    <li><a href="#"> News & Updates</a></li>
                </ul>
            </div> -->
            <div id="about" class="property-description box_shadow_col overview_sect mt20">
                <div class="row">
                    <h2 class="title-left">About</h2>
                    <p>
                        <?php echo $current_term->description; ?>
                    </p>
                </div>
            </div>
         <!--    <div id="management" class="property-description box_shadow_col overview_sect mt20">
                <div class="row">
                    <h2 class="title-left">Management</h2>
                    <div class="management_cols_1">
                        <div class="management_cols">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/img001.png">
                            <h5>Anurag Bhargava - Chairman</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</p>
                        </div>
                        <div class="management_cols">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/img001.png">
                            <h5>Anurag Bhargava - Chairman</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</p>
                        </div>
                        <div class="management_cols">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/img001.png">
                            <h5>Anurag Bhargava - Chairman</h5>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem</p>
                        </div>
                    </div>
                </div>
            </div> -->
            <?php $township = array(
  'posts_per_page'   => 5,
  'offset'           => 0,
  'post_type'        => 'property',
  'tax_query' => 
    array (
    array (
      'taxonomy' => 'property_type',
      'terms' => array ('township'),
      'field' => 'slug',
    ),
    array (
      'taxonomy' => 'developer',
      'terms' => array ($current_term->slug),
      'field' => 'slug',
    ),
)
);
$township_posts = get_posts( $township ); ?>
            <?php if(count($township_posts)>0) { ?>
            <div id="township" class="property-description box_shadow_col overview_sect mt20">
                <div class="row">
                    <h2 class="title-left">Township</h2>
                    <div class="slider_item">
                        <?php foreach ($township_posts as  $t_post) {
                          ?>
                        <a href="<?=get_permalink($t_post);?>">
                            <div class="item">
                                <div class="township_col">
                                    <img src="<?php the_post_thumbnail_url(); ?>">
                                    <h6><?=$t_post->post_title;?></h6>
                                </div>
                            </div>
                        </a>
                        <?php 
                        }
                          ?>
                    </div>
                </div>
            </div>
            <?php } ?>
<?php $residential = array(
  'posts_per_page'   => 5,
  'offset'           => 0,
  'post_type'        => 'property',
  'tax_query' => 
    array (
    array (
      'taxonomy' => 'property_type',
      'terms' => array ('residential'),
      'field' => 'slug',
    ),
    array (
      'taxonomy' => 'developer',
      'terms' => array ($current_term->slug),
      'field' => 'slug',
    ),
)
);
$residential_posts = get_posts( $residential ); ?>
<?php if(count($residential_posts)>0){ ?>
            <div id="residential-projects" class="property-description box_shadow_col overview_sect mt20">
                <div class="row">
                    <h2 class="title-left">Residential Projects</h2>
                    <div class="slider_item">
                         <?php foreach ($residential_posts as  $r_post) { ?>
                        <a href="<?=get_permalink($r_post);?>">
                            <div class="item">
                                <div class="township_col">
                                    <img src="<?php the_post_thumbnail_url(); ?>">
                                    <h6><?=$r_post->post_title;?></h6>
                                </div>
                            </div>
                        </a>
                        <?php  }  ?>
                    </div>
                </div>
            </div>
            <?php }
            ?>
            <?php $commercial = array(
  'posts_per_page'   => 5,
  'offset'           => 0,
  'post_type'        => 'property',
  'tax_query' => 
    array (
    array (
      'taxonomy' => 'property_type',
      'terms' => array ('commercial'),
      'field' => 'slug',
    ),
    array (
      'taxonomy' => 'developer',
      'terms' => array ($current_term->slug),
      'field' => 'slug',
    ),
)
);
$commercial_posts = get_posts( $commercial );
 ?>
<?php if(count($commercial_posts)>0){ ?>
    <div id="commercial-projects" class="property-description box_shadow_col overview_sect mt20">
                <div class="row">
                    <h2 class="title-left">Commercial Projects</h2>
                    <div class="slider_item">
                         <?php foreach ($commercial_posts as  $c_post) { ?>
                        <a href="<?=get_permalink($c_post);?>">
                            <div class="item">
                                <div class="township_col">
                                    <img src="<?php the_post_thumbnail_url(); ?>">
                                    <h6><?=$c_post->post_title;?></h6>
                                </div>
                            </div>
                        </a>
                        <?php } ?>
                    </div>
                </div>
           </div>
            <?php } ?>
    </div>