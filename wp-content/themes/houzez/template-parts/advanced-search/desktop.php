<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 20/01/16
 * Time: 6:31 PM
 */
global $status,
       $search_template,
       $type, $location,
       $area,
       $measurement_unit_adv_search,
       $adv_search_price_slider,
       $hide_advanced,
       $keyword_field_placeholder,
       $adv_show_hide;

$search_width = houzez_option('search_width');
$search_sticky = houzez_option('main-search-sticky');
$search_style = houzez_option('search_style');
$main_menu_sticky = houzez_option('main-menu-sticky');
$features_limit = houzez_option('features_limit');
$keyword_field_placeholder="Locality/Area/Village/Keyword";
wp_enqueue_script('jquery_ui', 'https://code.jquery.com/ui/1.10.3/jquery-ui.min.js',array('jquery'), HOUZEZ_THEME_VERSION, true);
wp_enqueue_script('date_range_slider', get_template_directory_uri().'/js/jQDateRangeSlider-min.js',array('jquery'), HOUZEZ_THEME_VERSION, true);
  wp_enqueue_style( 'range_slider', get_template_directory_uri() . '/css/classic-min.css', array(), '1.0', 'all' );


if( isset($_GET['search_style']) && $_GET['search_style'] == 'v1' ) {
    $search_style = 'style_1';
} else if( isset($_GET['search_style']) && $_GET['search_style'] == 'v2' ) {
    $search_style = 'style_2';

} else if( isset($_GET['search_style']) && $_GET['search_style'] == 'min1' ) {
    $search_style = 'style_1';
    $hide_advanced = true;

} else if( isset($_GET['search_style']) && $_GET['search_style'] == 'min2' ) {
    $search_style = 'style_2';
    $hide_advanced = true;
}

if( !is_404() && !is_search() ) {
    $adv_search_enable = get_post_meta($post->ID, 'fave_adv_search_enable', true);
    $adv_search = get_post_meta($post->ID, 'fave_adv_search', true);
}

$class = $sticky = '';

if( $main_menu_sticky != 1 ) {
    if ((!empty($adv_search_enable) && $adv_search_enable != 'global')) {
        if ($adv_search == 'hide_show') {
            $sticky = '1';
            $class = 'search-hidden';
        } else {
            $sticky = '0';
            $class = '';
        }
    } else {
        $sticky = $search_sticky;
    }
} else {
    $sticky = '0';
}

/*if( $hide_advanced ) {
    $adv_col_class = 'col-sm-4';
    $location_select_class = '';
} else {
    $adv_col_class = 'col-sm-3';
    $location_select_class = 'location-select';
}*/

if( $adv_show_hide['cities'] != 1 && $adv_show_hide['areas'] != 1 && $hide_advanced == false ) {
    $col_1_classes = "col-md-6 col-sm-6";
    $col_2_classes = "col-md-6 col-sm-6";
    $adv_col_class = 'col-sm-3';
    $location_select_class = 'location-select';

} elseif( $adv_show_hide['cities'] != 1 && $adv_show_hide['areas'] != 1 && $hide_advanced == true ) {
    $col_1_classes = "col-md-6 col-sm-6";
    $col_2_classes = "col-md-6 col-sm-6";
    $adv_col_class = 'col-sm-4';
    $location_select_class = '';

} elseif( $adv_show_hide['cities'] != 0 && $adv_show_hide['areas'] != 0  && $hide_advanced == false ) {
    $col_1_classes = "col-md-8 col-sm-8";
    $col_2_classes = "col-md-4 col-sm-4";
    $adv_col_class = 'col-sm-6';

} elseif( $adv_show_hide['cities'] != 0 && $adv_show_hide['areas'] != 0  && $hide_advanced == true ) {
    $col_1_classes = "col-md-10 col-sm-10";
    $col_2_classes = "col-md-2 col-sm-2";
    $adv_col_class = 'col-sm-12';

} elseif( ( $adv_show_hide['cities'] != 0 || $adv_show_hide['areas'] != 0 ) && $hide_advanced != false ) {
    $col_1_classes = "col-md-8 col-sm-8";
    $col_2_classes = "col-md-4 col-sm-4";
    $adv_col_class = 'col-sm-6';

} elseif( ( $adv_show_hide['cities'] != 0 || $adv_show_hide['areas'] != 0 ) && $hide_advanced != true ) {
    $col_1_classes = "col-md-7 col-sm-7";
    $col_2_classes = "col-md-5 col-sm-5";
    $adv_col_class = 'col-sm-4';
}
$keyword_field = houzez_option('keyword_field');

/*if( $keyword_field == 'prop_geocomplete' && $hide_advanced != true ) {
    $col_1_classes = "col-md-8 col-sm-8";
    $col_2_classes = "col-md-4 col-sm-4";
    $adv_col_class = 'col-sm-4';
} elseif ( $keyword_field == 'prop_geocomplete' && $hide_advanced != false ) {
    $col_1_classes = "col-md-8 col-sm-8";
    $col_2_classes = "col-md-4 col-sm-4";
    $adv_col_class = 'col-sm-6';
}*/
?>
    <!--start advanced search section-->
    <div id="inner_page_filter1" class="advanced-search advance-search-header houzez-adv-price-range houzez-adv-price-range_banner <?php echo esc_attr( $class ); ?>">
        <div class="<?php echo esc_attr( $search_width ); ?>">
            <div class="row">
                <div class="col-sm-12">
                    <form method="get" action="<?php echo esc_url( $search_template ); ?>">
                        <?php if( $search_style == 'style_1' ) { ?>
                        <div class="row">
                            <div class="<?php echo esc_attr( $col_1_classes );?>">
                                <div class="form-group no-margin">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                        <input type="text" id="keyword" class="houzez_geocomplete form-control" value="<?php echo isset ( $_GET['keyword'] ) ? $_GET['keyword'] : ''; ?>" name="keyword" placeholder="<?php echo $keyword_field_placeholder; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="<?php echo esc_attr( $col_2_classes );?>">
                                <div class="row">
                                    <?php if( $adv_show_hide['cities'] != 1 ) { ?>
                                    <div class="<?php echo esc_attr($adv_col_class); ?> col-xs-12">
                                        <div class="form-group no-margin <?php echo esc_attr( $location_select_class ); ?>">
                                            <select name="location" class="selectpicker" data-live-search="false" data-live-search-style="begins">
                                                <?php
                                                // All Option
                                                echo '<option value="">'.esc_html__( 'All Cities', 'houzez' ).'</option>';

                                                $prop_city = get_terms (
                                                    array(
                                                        "property_city"
                                                    ),
                                                    array(
                                                        'orderby' => 'name',
                                                        'order' => 'ASC',
                                                        'hide_empty' => true,
                                                        'parent' => 0
                                                    )
                                                );
                                                houzez_hirarchical_options('property_city', $prop_city, $location );
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if( $adv_show_hide['areas'] != 1 ) { ?>
                                    <div class="<?php echo esc_attr($adv_col_class); ?> col-xs-6">
                                        <div class="form-group no-margin areas-select">
                                            <select name="area" class="selectpicker" data-live-search="false" data-live-search-style="begins">
                                                <?php
                                                // All Option
                                                echo '<option value="">'.esc_html__( 'All Areas', 'houzez' ).'</option>';

                                                $prop_area = get_terms (
                                                    array(
                                                        "property_area"
                                                    ),
                                                    array(
                                                        'orderby' => 'name',
                                                        'order' => 'ASC',
                                                        'hide_empty' => true,
                                                        'parent' => 0
                                                    )
                                                );
                                                houzez_hirarchical_options('property_area', $prop_area, $area );
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <?php if( $hide_advanced != true ) { ?>
                                    <div class="<?php echo esc_attr($adv_col_class); ?> col-xs-6 text-center">
                                        <button class="advance-btn blue btn" type="button"><i class="fa fa-gear"></i>
                                            <?php esc_html_e( 'Advanced', 'houzez' ); ?>
                                        </button>
                                    </div>
                                    <?php } ?>
                                    <div class="<?php echo esc_attr($adv_col_class); ?> col-xs-6">
                                        <button type="submit" class="btn btn-orange btn-block houzez-theme-button">
                                            <?php esc_html_e( 'Search', 'houzez' ); ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <?php } elseif( $search_style == 'style_2' ) { ?>
                        

                        <?php if( $adv_show_hide['type'] != 1 ) { ?>


                        <div class="search-bar-section_col">
                            <div class="inner_page_filter">
                             <select class="selectpicker" name="type" data-live-search="false" data-live-search-style="begins">
                                    <?php
                                        // All Option
                                        echo '<option value="">'.esc_html__( 'All Types', 'houzez' ).'</option>';

                                        $prop_type = get_terms (
                                            array(
                                                "property_type"
                                            ),
                                            array(
                                                'orderby' => 'name',
                                                'order' => 'ASC',
                                                'hide_empty' => false,
                                                'parent' => 0
                                            )
                                        );
                                        houzez_hirarchical_options('property_type', $prop_type, $type );
                                        ?>
                                </select> 
                                  <select class="hidden page_type" name="status" data-live-search="false" data-live-search-style="begins">
                                    <?php
                                        // All Option
                                        echo '<option value="">'.esc_html__( 'Page Types', 'houzez' ).'</option>';

                                        $prop_type = get_terms (
                                            array(
                                                "property_status"
                                            ),
                                            array(
                                                'orderby' => 'name',
                                                'order' => 'ASC',
                                                'hide_empty' => false,
                                                'parent' => 0
                                            )
                                        );
                                        houzez_hirarchical_options('property_type', $prop_type, $type );
                                        ?>
                                </select> 
                                <div class="search-col1">
                                     <input class="houzez_geocomplete form-control" type="text" value="<?php echo isset ( $_GET['keyword'] ) ? $_GET['keyword'] : ''; ?>" name="keyword" placeholder="<?php echo $keyword_field_placeholder; ?>">
                                     <input name="" value="Go" type="submit"> 
                                     <button class="advance-btn advanced_bar_btn btn" type="button">
                                     <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon08.png" />
                                      <!-- <i class="fa fa-gear"></i> -->
                                    <?php esc_html_e( '', 'houzez' ); ?>
                                </button> 
                                   <!--    <button class="btn btn-orange">
                                    <?php esc_html_e( 'Go', 'houzez' ); ?>
                                </button>  -->


<div class="ad_filter advanced_bar_block display-hide">
  
                                    <div class="range-advanced-main">
                                    <span>Budget</span> 
                                        <div class="range-text">
                                            <input type="hidden" name="min-price" class="min-price-range-hidden range-input" readonly>
                                            <input type="hidden" name="max-price" class="max-price-range-hidden range-input" readonly>
                                            <p><!-- <span class="range-title"><?php echo esc_html_e('Price Range:','houzez');?></span> --> 

                                                <!-- <?php echo esc_html_e('from','houzez');?>  -->

                                                <span class="min-price-range"></span>
                                              <!--   <?php echo esc_html_e('to','houzez');?>  -->
                                                <span class="max-price-range"></span>
                                                </p>
                                        </div>
                                        <div class="range-wrap">
                                            <div class="price-range-advanced"></div>
                                        </div>
                                    </div>  

                                    <?php if( $adv_show_hide['beds'] != 1 ) { ?>
                                <div class="beds_col2">
                                    <span>Beds</span>
                                    <select name="bedrooms" class="selectpicker" data-live-search="false" data-live-search-style="begins" title="">
                                            <option value="">
                                                <?php esc_html_e( 'Beds', 'houzez' ); ?>
                                            </option>
                                            <?php houzez_number_list('bedrooms'); ?>
                                        </select>
                                        </div>
                                 <?php } ?> 

                                  <!-- Possession Date Slider -->
                                 <div class="possesion_rang">
                                    <span>Date of Possesion</span> 
                                    <div class="label_possession_min"></div>
                                    <div class="label_possession_max"></div>
                                    <div class="sliderContainer">
                                        <div id="dateSlider"></div>
                                    </div>
                                   
                                    <input type="hidden" style="    color: #000;" id="possession_date_min" name="possession_date_min" >
                                    <input type="hidden" style="    color: #000;" id="possession_date_max" name="possession_date_max" >
                                </div> 
                                <div class="select_col_3">
                                <div class="select_col_1">
                                <!-- Filter By Locality -->
                                <?php if( $adv_show_hide['areas'] != 1 ) { ?>
                                <select name="area" class="selectpicker" data-live-search="false" data-live-search-style="begins">
                                    <?php
                                    // All Option
                                    echo '<option value="">'.esc_html__( 'Filter By Locality', 'houzez' ).'</option>';

                                    $prop_area = get_terms (
                                        array(
                                            "property_area"
                                        ),
                                        array(
                                            'orderby' => 'name',
                                            'order' => 'ASC',
                                            'hide_empty' => true,
                                            'parent' => 0
                                        )
                                    );
                                    houzez_hirarchical_options('property_area', $prop_area, $area );
                                    ?>
                                </select>
                                <?php } ?>
                                </div>
                                <!-- Filter By Developer -->
                                   <div class="select_col_1 filterbydev"> 
                                <?php if( $adv_show_hide['areas'] != 1 ) { ?>
                                <select name="developer" class="selectpicker" data-live-search="false" data-live-search-style="begins">
                                    <?php
                                    // All Option
                                    echo '<option value="">'.esc_html__( 'Filter By Developer', 'houzez' ).'</option>';

                                    $prop_area = get_terms (
                                        array(
                                            "developer"
                                        ),
                                        array(
                                            'orderby' => 'name',
                                            'order' => 'ASC',
                                            'hide_empty' => true,
                                            'parent' => 0
                                        )
                                    );
                                    houzez_hirarchical_options('developer', $prop_area, $developer );
                                    ?>
                                </select>
                                <?php } ?>
                                </div>
                                 <!-- Filter By beds -->
                                <div class="beds_col2 range-bedrooms-main">
                                    <span>Beds</span>
                                    <select name="bedrooms" class="selectpicker" data-live-search="false" data-live-search-style="begins" title="">
                                            <option value="">
                                                <?php esc_html_e( 'Beds', 'houzez' ); ?>
                                            </option>
                                            <?php houzez_number_list('bedrooms'); ?>
                                        </select>
                                        </div>
                                 <!-- Filter By Size -->
                                   <div class="select_col_1 range-area-main">
                               <?php if( $adv_show_hide['min_area'] != 1 ) { ?>
   <span>Sizes</span>
                           <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="<?php echo isset ( $_GET['min-area'] ) ? $_GET['min-area'] : ''; ?>" name="min-area" placeholder="<?php esc_html_e( 'Min Area', 'houzez' ); ?>">
                                </div>
                            </div> 
                            <?php } ?>

                <?php if( $adv_show_hide['max_area'] != 1 ) { ?>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="<?php echo isset ( $_GET['max-area'] ) ? $_GET['max-area'] : ''; ?>" name="max-area" placeholder="<?php esc_html_e( 'Max Area', 'houzez' ); ?>">
                                </div>
                            </div> 
                <?php } ?>
                                </div> 

                                </div> 

<div class="bottom_button">
 <button class="btn btn-orange">
<?php esc_html_e( 'Go', 'houzez' ); ?>
</button>                           
</div>
</div>   

                                </div>


                        </div>

                      

 </div>
 
 
                        <?php } ?>
                     
                        <?php } ?>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
    //<!--
    (function($) {
        <?php
         if (isset($_GET['possession_date_min']) && !empty($_GET['possession_date_min']) && isset($_GET['possession_date_max']) && !empty($_GET['possession_date_max'])) { 
               list($s_month, $s_year) = split('[/.-]', $_GET['possession_date_min']);
               list($e_month, $e_year) = split('[/.-]', $_GET['possession_date_max']);
         } else {
            $s_month=01;
            $s_year=date('Y',time());
            $e_month=12;
            $e_year=date('Y',time());
         }

        ?>


        var Months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var d_start_date = new Date(<?=$s_year?>, <?=$s_month?>, 10);
        var d_end_date = new Date(<?=$e_year?>, <?=$e_month?>, 22);
        $("#possession_date_min").val(d_start_date.getMonth() + '-' + d_start_date.getFullYear());
        $("#possession_date_max").val(d_end_date.getMonth() + '-' + d_end_date.getFullYear());
        $(".label_possession_min").html(Months[d_start_date.getMonth()] + '-' + d_start_date.getFullYear());
        $(".label_possession_max").html(Months[d_end_date.getMonth()] + '-' + d_end_date.getFullYear());
        $(document).ready(function() {
           
            $("#dateSlider").dateRangeSlider({
                bounds: {
                    min: new Date(2010, 0, 1),
                    max: new Date(2025, 11, 31, 12, 59, 59)
                },
                defaultValues: {
                    min: new Date(<?=$s_year?>, <?=$s_month?>, 10),
                    max: new Date(<?=$e_year-3?>, <?=$e_month?>, 22)
                },
                scales: [{
                    next: function(val) {
                        var next = new Date(val);
                        return new Date(next.setMonth(next.getMonth() + 1));
                    },
                    label: function(val) {
                        return Months[val.getMonth()];
                    }
                }],
            });
        });

        $("#dateSlider").bind("valuesChanging", function(e, data) {
            // console.log("Something moved. min: " + data.values.min + " max: " + data.values.max);
            var min_date = data.values.min;
            var max_date = data.values.max;
            var min_date_strng = Months[min_date.getMonth()] + '-' + min_date.getFullYear();
            var max_date_strng = Months[max_date.getMonth()] + '-' + max_date.getFullYear();
            $(".label_possession_min").html(min_date_strng);
            $(".label_possession_max").html(max_date_strng);
            $("#possession_date_min").val(min_date.getMonth() + '-' + min_date.getFullYear());
            $("#possession_date_max").val(max_date.getMonth() + '-' + max_date.getFullYear());
        });

    })(jQuery);


    //-->
    </script>
    <style>
    .ui-rangeSlider-label {
        display: none !important;
    }
    
    div#dateSlider {
        position: relative;
        display: block;
    }
    
    .ui-rangeSlider-innerBar {
        position: absolute;
        top: 0px;
        left: 0px;
    }
    
    .ui-rangeSlider-container {
        width: 100%;
    }
    </style>
