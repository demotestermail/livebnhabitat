<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 17/12/15
 * Time: 4:47 PM
 */

global $post, $prop_images, $houzez_local, $current_page_template, $taxonomy_name,$prop_agent_email;



$prop_agent_num = $agent_num_call = $prop_agent_email = $gallery_view = $map_view = $street_view = '';
$Towers = get_post_meta( get_the_ID(), 'Towers', true ); 
$enableDisable_agent_forms = houzez_option('agent_forms');
$NumberOfParking = get_post_meta( get_the_ID(), 'NumberOfParking', true );
$project_size = get_post_meta( get_the_ID(), 'wpcf-project-size', true );
$no_floors_per_towers = get_post_meta( get_the_ID(), 'wpcf-numbers-of-floors-per-tower', true );

if( $prop_agent_display != '-1' && $agent_display_option == 'agent_info' ) {
    $prop_agent_id = get_post_meta( get_the_ID(), 'fave_agents', true );
    $prop_agent_email = get_post_meta( $prop_agent_id, 'fave_agent_email', true );
        

} elseif ( $agent_display_option == 'author_info' ) {
    $prop_agent_email = get_the_author_meta( 'email' );
}
?>

<?php get_template_part( 'property-details/agent', 'modal' ); ?>

<ul class="list-unstyled actions pull-right">

  <li data-container="body" class="hastooltip" data-toggle="popover" data-placement="top" data-content='<div class="circle_col">
      <h1>Project Summary</h1>
      <table>
      <?php if(!empty($project_size)) { ?>
          <tr>
              <td>Project Size</td>
              <td><?=$project_size;?></td>
          </tr>
          <?php } ?>
          <tr>
              <td>No. of Towers</td>
              <td><?php 
              echo count($Towers); 
              if (count($Towers)>1) { echo ' Towers'; } else { echo ' Tower'; } ?> </td>
          </tr>
          <?php if(!empty($no_floors_per_towers)) { ?>
          <tr>
              <td>Floors Per Tower</td>
              <td><?=$no_floors_per_towers;?></td>
          </tr>
          <?php } ?>
          <tr>
              <td>Total No. of Car Parkings &nbsp; </td>
              <td><?php echo $NumberOfParking; ?></td>
          </tr>
      </table>
      <!-- <h1>Project Development & Progress Updates</h1>  -->
      <!-- <div class="themb_images">
          <img src="<?php bloginfo("template_url"); ?>/images/img06.png" />
          <img src="<?php bloginfo("template_url"); ?>/images/img06.png" />
          <img src="<?php bloginfo("template_url"); ?>/images/img06.png" />
          <img src="<?php bloginfo("template_url"); ?>/images/img06.png" />
      </div> -->

  </div>'><!--i class="fa fa-info-circle"></i--> <i class="list-icon icon-info"></i></li> 
  
   <?php if( is_user_logged_in()) { ?><li data-toggle="modal" data-target="#modal_list_prop<?=$post->ID;?>"><i class="list-icon icon-phone"></i> </li> 
   <?php } else { ?>
        <li data-toggle="modal" data-target="#pop-login"><!--i class="fa fa-phone"></i--> <i class="list-icon icon-phone"></i></li> 
    <?php } ?>
 
    <li>
        <span data-placement="top" data-toggle="tooltip" data-original-title="<?php echo $houzez_local['favorite']; ?>">
            <?php get_template_part( 'template-parts/favorite' ); ?>
        </span>
    </li>
  <li class="share-btn">
        <?php get_template_part( 'template-parts/share' ); ?>
    </li>  
    <!--  <li><i class="fa fa-bell"></i></li>  
   <li>
        <span data-toggle="tooltip" data-placement="top" title="(<?php echo count( $prop_images ); ?>) <?php echo $houzez_local['photos']; ?>">
            <i class="fa fa-camera"></i>
            <span class="count">(<?php /*echo count( $prop_images ); */?>) </span>
        </span>
    </li> -->


    <?php if( $current_page_template == 'template/property-listing-template.php' ||
              $current_page_template == 'template/property-listing-fullwidth.php' ||
              $current_page_template == 'template/template-search.php' ||
              $taxonomy_name == 'property_status' ||
              $taxonomy_name == 'property_type' ||
              $taxonomy_name == 'property_area' ||
              $taxonomy_name == 'property_city' ||
              $taxonomy_name == 'property_feature' ||
              $taxonomy_name == 'property_state'
    ) {?>
    <!-- <li>
        <span id="compare-link-<?php esc_attr_e( $post->ID ); ?>" class="compare-property" data-propid="<?php esc_attr_e( $post->ID ); ?>" data-toggle="tooltip" data-placement="top" title="<?php esc_html_e( 'Compare', 'houzez' ); ?>">
            <i class="fa fa-plus"></i>
        </span>
    </li> -->
    <?php } ?> 
</ul>






 