<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 06/10/15
 * Time: 4:12 PM
 */

global $current_user, $hide_add_prop_fields, $required_fields;

wp_get_current_user();
$userID = $current_user->ID;

$enable_paid_submission = houzez_option('enable_paid_submission');
$user_pack_id              = get_the_author_meta( 'package_id' , $userID );
$remaining_listings     = houzez_get_remain_listing_user( $userID, $user_pack_id );

if( is_page_template( 'template/submit_property.php' ) ) {

    if( $enable_paid_submission == 'membership' && $remaining_listings != -1 && $remaining_listings < 1 ) {
        print '<div class="user_package_status"><h4>'.esc_html__('Your current package doesn\'t let you publish more properties! You need to upgrade your membership.','houzez' ).'</h4></div>';

    } else { ?>

        <form id="submit_property_form" name="new_post" method="post" action="#" enctype="multipart/form-data"
              class="add-frontend-property">

            <?php
            $layout = houzez_option('property_form_sections');
            $layout = $layout['enabled'];

            if ($layout): foreach ($layout as $key=>$value) {

                switch($key) {

                    case 'description-price':
                        get_template_part('template-parts/submit-property/description-price');
                        break;

                    case 'media':
                        get_template_part('template-parts/submit-property/media');
                        break;

                    case 'details':
                        get_template_part('template-parts/submit-property/details');
                        break;

                    case 'features':
                        get_template_part('template-parts/submit-property/features');
                        break;

                    case 'location':
                        get_template_part('template-parts/submit-property/location');
                        break;

                    case 'floorplans':
                        get_template_part('template-parts/submit-property/floorplans');
                        break;

                    case 'multi-units':
                        get_template_part('template-parts/submit-property/multi-units');
                        break;

                    case 'agent_info':
                        get_template_part('template-parts/submit-property/agent-info');
                        break;
                }

            }
            endif;
            ?>

            <div class="account-block text-right">
                <?php wp_nonce_field('submit_property', 'property_nonce'); ?>
                <input type="hidden" name="action" value="add_property"/>
                <input type="hidden" name="prop_featured" value="0"/>
                <!--<input type="hidden" name="prop_agent_display_option" value="author_info"/>-->
                <input type="hidden" name="prop_payment" value="not_paid"/>
                <button type="submit" id="add_new_property"
                        class="btn btn-primary"><?php esc_html_e('Submit Property', 'houzez'); ?></button>
            </div>

        </form>

        <?php
    }
}

