<?php
global $post, $splash_welcome_text, $page_head_subtitle, $keyword_field_placeholder;
$selected_city='';
if(isset($_COOKIE['selected_city'])){
    $selected_city=json_decode(stripslashes($_COOKIE['selected_city']),true);
    $selected_city=$selected_city['slug'];
}
$css_class = '';
$search_template = houzez_get_template_link('template/template-search.php');
$splash_v1_dropdown = houzez_option('splash_v1_dropdown');
$splash_banner_search_type = 'type1';//houzez_option('splash_banner_search_type');
$keyword_field = houzez_option('keyword_field');

if( $keyword_field == 'prop_title' ) {
    $keyword_field_placeholder = esc_html__('Enter keyword...', 'houzez');

} else if( $keyword_field == 'prop_city_state_county' ) {
    $keyword_field_placeholder = esc_html__('Search City, State or Area', 'houzez');
} else {
    $keyword_field_placeholder = esc_html__('Enter an address, town, street, zip or property ID', 'houzez');
}

if( $splash_v1_dropdown == 'property_status' ) {
    $dropdown_title = esc_html__('All Status', 'houzez');
} else if ( $splash_v1_dropdown == 'property_type' ) {
    $dropdown_title = esc_html__('All Types', 'houzez');
} else if ( $splash_v1_dropdown == 'property_area' ) {
    $dropdown_title = esc_html__('All Areas', 'houzez');
} else {
    $dropdown_title = esc_html__('All Cities', 'houzez');
}
?>

<?php if( !empty( $splash_welcome_text ) ) { ?>
    <h1><?php echo esc_html( $splash_welcome_text );?></h1>
<?php } ?>
<?php if( !empty( $page_head_subtitle ) ) { ?>
    <h2 class="banner-sub-title"><?php echo $page_head_subtitle; ?></h2>
<?php } ?>

<?php if( $splash_banner_search_type == "type1" ) { ?>
<div class="banner-search-main">
    <form method="get" action="<?php echo esc_url( $search_template ); ?>" class="form-inline">
        <div class="form-group">
            <select name="location" class="selectpicker" data-live-search="false" data-live-search-style="begins">
                <?php
                // All Option
                echo '<option value="all">'.$dropdown_title.'</option>';

                $prop_city = get_terms (
                    array(
                        $splash_v1_dropdown
                    ),
                    array(
                        'orderby' => 'name',
                        'order' => 'ASC',
                        'hide_empty' => false,
                        'parent' => 0
                    )
                );
                houzez_hirarchical_options( $splash_v1_dropdown, $prop_city, $selected_city );
                ?>
            </select>
            <div class="search input-search input-icon">
                <input type="text" class="form-control" name="keyword" placeholder="<?php esc_html_e( $keyword_field_placeholder ); ?>">
            </div>
            <div class="search-btn">
                <button class="btn btn-orange"><?php esc_html_e( 'Search', 'houzez' ); ?></button>
            </div>

        </div>
    </form>
</div>
<?php } elseif ( $splash_banner_search_type == "type2" ) { ?>

    <div class="banner-search-taber">
        <div class="banner-search-tabs">
            <div class="search-tab active">
                <div class="search-tab-inner">All</div>
            </div>
            <div class="search-tab">
                <div class="search-tab-inner">For Sale</div>
            </div>
            <div class="search-tab">
                <div class="search-tab-inner">For Rent</div>
            </div>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade active in">
                <?php get_template_part( 'template-parts/advanced-search/splash', 'banner' ); ?>
            </div>
            <div class="tab-pane fade">
                <?php get_template_part( 'template-parts/advanced-search/splash', 'banner' ); ?>
            </div>
            <div class="tab-pane fade">
                <?php get_template_part( 'template-parts/advanced-search/splash', 'banner' ); ?>
            </div>
        </div>
    </div>

<?php } ?>
        