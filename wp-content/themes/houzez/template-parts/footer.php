<?php 
$selected_city='';
$selected_location='';
if(isset($_COOKIE['selected_city'])){
    $selected_cookie=json_decode(stripslashes($_COOKIE['selected_city']),true);
    $selected_city='in '.$selected_cookie['name'];
    $selected_location=$selected_cookie['slug']; 
} 

if ( !is_active_sidebar( 'footer-sidebar-1' )
    && ! is_active_sidebar( 'footer-sidebar-2' )
    && ! is_active_sidebar( 'footer-sidebar-3' )
    && ! is_active_sidebar( 'footer-sidebar-4' ) )
    return;

$footer_cols = houzez_option('footer_cols');
if( $footer_cols == 'three_cols' ) {
    $f_3_classes = 'col-md-6 col-sm-12';
    $footer = 'footer footer-v2';
} else {
    $f_3_classes = 'col-md-3 col-sm-6';
    $footer = 'footer';
}
?>
<?php $copy_rights = houzez_option('copy_rights'); ?>
<div class="<?php echo esc_attr( $footer ); ?>">
    <div class="site_maps_section">
        <div class="container">
            <div class="row">
            <!-- upper footer 1 -->
                <div class="site_map1">
                    <div id="menu-f-1" class="">
                        <div class="widget-top">
                            <h5 class="widget-title">Residental Projects</h5>
                        </div>
                        <div class="menu-upper-footer-1-container">
                            <ul id="menu-upper-footer-1" class="menu">
                                <li><a href="<?=site_url().'/property/apartment-flat/'.$selected_location.'/'?>">Apartment/Flat <?= $selected_city?></a></li>

                                <li><a href="<?=site_url().'/property/floor/'.$selected_location.'/' ?>">Residental Floors <?= $selected_city?></a></li>

                                <li><a href="<?=site_url().'/property/plot/'.$selected_location.'/'?>">Residential Plots <?= $selected_city?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- upper footer 2 -->
                <div class="site_map1">
                    <div id="menu-f-1" class="">
                        <div class="widget-top">
                            <h5 class="widget-title">Commercial Projects</h5>
                        </div>
                        <div class="menu-upper-footer-1-container">
                            <ul id="menu-upper-footer-1" class="menu">
                                <li><a href="<?=site_url().'/property/office-space/'.$selected_location.'/'?>">Office Space <?= $selected_city?></a></li>
                                <li><a href="<?=site_url().'/property/plot-commercial/'.$selected_location.'/'?>">Plot Commercial <?= $selected_city?></a></li>
                                <li><a href="<?=site_url().'/property/retail-space/'.$selected_location.'/'?>">Retail Space <?= $selected_city?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- upper footer 3 -->
                <div class="site_map1">
                    <div id="menu-f-1" class="">
                        <div class="widget-top">
                            <h5 class="widget-title">Resale Property</h5>
                        </div>
                        <div class="menu-upper-footer-1-container">
                            <ul id="menu-upper-footer-1" class="menu">
                                <li><a href="<?=site_url().'/property/resale-property/'.$selected_location.'/'?>">Resale Property  <?= $selected_city?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>


                <!-- upper footer 4 -->
                <div class="site_map1">
                    <div id="menu-f-1" class="">
                        <div class="widget-top">
                            <h5 class="widget-title">Agricultural Property</h5>
                        </div>
                        <div class="menu-upper-footer-1-container">
                            <ul id="menu-upper-footer-1" class="menu">
                                <li><a href="<?=site_url().'/property/agriculture/'.$selected_location.'/'?>">Agricultural Property <?= $selected_city?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- upper footer 5 -->
                <div class="site_map1">
                    <div id="menu-f-1" class="">
                        <div class="widget-top">
                            <h5 class="widget-title">Institutional Property</h5>
                        </div>
                        <div class="menu-upper-footer-1-container">
                            <ul id="menu-upper-footer-1" class="menu">
                                <li><a href="<?=site_url().'/property/institutional-property/'.$selected_location.'/'?>">Institutional Property <?= $selected_city?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php if ( is_active_sidebar( 'footer-sidebar-1' ) ) : ?>
            <div class="col-sm-4">
                <?php dynamic_sidebar( 'footer-sidebar-1' ); ?> </div>
            <?php endif; ?>
            <?php if ( is_active_sidebar( 'footer-sidebar-2' ) ) : ?>
            <div class="col-sm-4 footer-colo1">
                <?php dynamic_sidebar( 'footer-sidebar-2' ); ?> </div>
            <?php endif; ?>
            <?php if ( is_active_sidebar( 'footer-sidebar-3' ) ) : ?>
            <div class="col-sm-4">
                <?php dynamic_sidebar( 'footer-sidebar-3' ); ?>
                <?php // echo esc_attr( $f_3_classes ); ?> </div>
            <?php endif; ?>
            <?php if( $footer_cols == 'four_cols' ) { ?>
            <?php if ( is_active_sidebar( 'footer-sidebar-4' ) ) : ?>
            <div class="col-sm-4">
                <?php dynamic_sidebar( 'footer-sidebar-4' ); ?> </div>
            <?php endif; ?>
            <?php } ?>
            <?php
              // Pages Menu
              if ( has_nav_menu( 'footer-menu' ) ) :
                wp_nav_menu( array (
                  'theme_location' => 'footer-menu',
                  'container' => '',
                  'container_class' => '',
                  'menu_class' => 'bottom_navi',
                  'menu_id' => 'footer-menu',
                  'depth' => 1
                ));
              endif;
              ?>
                <div class="copy_right">
                    <?php echo ( $copy_rights ); ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/footer_logo.png"></div>
        </div>
    </div>
</div>
