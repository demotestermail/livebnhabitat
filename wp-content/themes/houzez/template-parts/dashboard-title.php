<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 13/01/16
 * Time: 2:24 PM
 */
?>
<div class="page-title">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title-left">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="page-title-right">
                <?php get_template_part( 'inc/breadcrumb' ); ?>
            </div>
        </div>
    </div>
</div>