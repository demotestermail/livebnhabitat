<?php
/**
 * Invoices - template/user_dashboard_invoices
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 07/04/16
 * Time: 11:34 PM
 */
$fave_meta = houzez_get_invoice_meta( get_the_ID() );
?>
<tr>
    <td><?php the_title(); ?></td>
    <td><?php echo get_the_date(); ?> </td>
    <td><?php echo esc_attr( $fave_meta['invoice_billing_type'] );?></td>
    <td><?php echo esc_attr( $fave_meta['invoice_billion_for'] );?></td>
    <td>
        <?php
        $invoice_status = get_post_meta(  get_the_ID(), 'invoice_payment_status', true );
        if( $invoice_status == 0 ) {
            echo esc_html__('Not Paid','houzez');
        } else {
            echo esc_html__('Paid','houzez');
        }
        ?>
    </td>
    <td><?php echo houzez_get_invoice_price( $fave_meta['invoice_item_price'] );?></td>
</tr>
