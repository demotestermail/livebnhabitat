<?php
/**
 * Used for both single and index/archive/search.
 *
 */
?>
<div class="property-listing list-view">
                        <div class="row">
                            <div id="houzez_ajax_container">
                                <?php
                                 get_template_part('template-parts/property-for-listing');
                                ?>

                                <hr>
                                <!--start Pagination-->
                                <?php houzez_pagination( $latest_posts->max_num_pages, $range = 2 ); ?>
                                <!--start Pagination-->

                            </div>
                        </div>

                    </div>
