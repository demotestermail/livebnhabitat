<?php
/** Template Name: Advanced Search Results
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 20/01/16
 * Time: 6:44 PM
 */
get_header();
$listing_page_link = houzez_properties_listing_link();
$listing_view = 'list-view';

global $wp_query, $paged, $post, $search_qry;
if ( is_front_page()  ) {
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
}
$fave_prop_no = houzez_option('search_num_posts');
$number_of_prop = $fave_prop_no;
if(!$number_of_prop){
    $number_of_prop = 9;
}

$search_qry = array(
    'post_type' => 'property',
    'posts_per_page' => $number_of_prop,
    'paged' => $paged
);
$sortby = '';
if( isset( $_GET['sortby'] ) ) {
    $sortby = $_GET['sortby'];
}

$search_qry = apply_filters( 'houzez_search_parameters', $search_qry );

$search_qry = houzez_prop_sort ( $search_qry );
?>

<?php get_template_part('template-parts/page', 'title'); ?>

<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 list-grid-area container-contentbar">
        <div id="content-area">

            <!--start list tabs-->
            <div class="table-list full-width">
                <div class="tabs table-cell v-align-top">
                    <p><?php esc_html_e( 'Save this Search ?', 'houzez'); ?></p>
                </div>
                <div class="sort-tab table-cell text-right v-align-top">
                    <?php esc_html_e( 'Sort by:', 'houzez' ); ?>
                    <select id="sort_properties" class="selectpicker bs-select-hidden" title="<?php esc_html_e( 'Default Order', 'houzez' ); ?>" data-live-search-style="begins" data-live-search="false">
                        <option <?php if( $sortby == 'a_price' ) { echo "selected"; } ?> value="a_price"><?php esc_html_e( 'Price (Low to High)', 'houzez' ); ?></option>
                        <option <?php if( $sortby == 'd_price' ) { echo "selected"; } ?> value="d_price"><?php esc_html_e( 'Price (High to Low)', 'houzez' ); ?></option>
                        <option <?php if( $sortby == 'featured' ) { echo "selected"; } ?> value="featured"><?php esc_html_e( 'Featured', 'houzez' ); ?></option>
                        <option <?php if( $sortby == 'a_date' ) { echo "selected"; } ?> value="a_date"><?php esc_html_e( 'Date Old to New', 'houzez' ); ?></option>
                        <option <?php if( $sortby == 'd_date' ) { echo "selected"; } ?> value="d_date"><?php esc_html_e( 'Date New to Old', 'houzez' ); ?></option>
                    </select>
                </div>
            </div>
            <!--end list tabs-->

            <?php get_template_part( 'template-parts/save', 'search' ); ?>


            <!--start property items-->
            <div class="property-listing <?php echo esc_attr($listing_view); ?>">
                <div class="row">

                    <?php
                    $wp_query = new WP_Query( $search_qry );

                    if ( $wp_query->have_posts() ) :
                        while ( $wp_query->have_posts() ) : $wp_query->the_post();

                            get_template_part('template-parts/property-for-listing');

                        endwhile;
                        wp_reset_postdata();
                    else:
                       get_template_part('template-parts/property', 'none');
                    endif;
                    ?>

                </div>
            </div>
            <!--end property items-->

            <hr>

            <!--start Pagination-->
            <?php houzez_pagination( $wp_query->max_num_pages, $range = 2 ); ?>
            <!--start Pagination-->

        </div>
    </div><!-- end container-content -->

    <div class="col-lg-4 col-md-4 col-sm-6 col-md-offset-0 col-sm-offset-3 container-sidebar">
        <aside id="sidebar" class="sidebar-white">
            <?php
            if( is_active_sidebar( 'search-sidebar' ) ) {
                dynamic_sidebar( 'search-sidebar' );
            }
            ?>
        </aside>
    </div> <!-- end container-sidebar -->
</div>


<?php get_footer(); ?>
