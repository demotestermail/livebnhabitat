<?php
/**
 * Template Name: User Dashboard Favorite Properties
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 11/01/16
 * Time: 4:35 PM
 */
if ( !is_user_logged_in() ) {
    wp_redirect(  home_url() );
}

global $current_user;

wp_get_current_user();
$userID         = $current_user->ID;
$user_login     = $current_user->user_login;
$fav_ids = 'houzez_favorites-'.$userID;
$fav_ids = get_option( $fav_ids );

get_header();
?>
<?php get_template_part( 'template-parts/dashboard-title'); ?>

<div class="row">
    <div class="col-md-3 col-sm-4 user-dashboard-left">
        <?php get_template_part( 'template-parts/dashboard', 'sidebar' ); ?>
    </div>
    <div class="col-md-9 col-sm-8 user-dashboard-right">
        <div class="account-block">

            <!--start list tabs-->
            <?php //get_template_part( 'template-parts/listing', 'tabs' ); ?>
            <!--end list tabs-->

            <!--start property items-->
            <div class="property-listing list-view">
                <div class="row">

                    <?php
                    $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                    if( !empty( $fav_ids ) ) {
                    $args = array('post_type' => 'property', 'post__in' => $fav_ids,'paged' => $paged);

                        $myposts = query_posts($args);
                        if (have_posts()) : 

                        while (have_posts()) : the_post();

                            get_template_part('template-parts/property-for-listing');

                        endwhile;   endif;
                    } else {
                        echo '<div class="col-sm-12">';
                        esc_html_e('You don\'t have any favorite properties yet!', 'houzez');
                        echo '</div>';
                    }
                    ?>

                </div>
            </div>
            <!--end property items-->
        </div>
        <hr>

        <!--start Pagination-->
         <?php houzez_pagination(); ?>
        <!--start Pagination-->

    </div>
</div>

<?php get_footer(); ?>