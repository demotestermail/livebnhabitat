<?php
/**
 * Template Name: Template Reale State 
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 21/12/15
 * Time: 1:35 PM 
 */
get_header(); 
$develpers_project_link=site_url().'/realestate/developer-project';
$rent_link=site_url().'/realestate/for-rent';
$resale_link=site_url().'/realestate/for-resale';
$selected_city='';
if(isset($_COOKIE['selected_city'])){
    $selected_city=json_decode(stripslashes($_COOKIE['selected_city']),true);
   
      $selected_city_slug=$selected_city['slug'];
    $selected_city=$selected_city['name'];

	$develpers_project_link=site_url().'/realestate/developer-project/'.$selected_city_slug.'/';
	$rent_link=site_url().'/realestate/for-rent/'.$selected_city_slug.'/';
	$resale_link=site_url().'/realestate/for-resale/'.$selected_city_slug.'/';
}

?>

</div>

 <!-- Search bar section -->
<!-- <center class="search-bar-section">
	 <div class="search-bar-section_col">
<div class="dropdown">
<button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
RESIDENTIAL
<span class="caret"></span>
</button>
<ul class="dropdown-menu" aria-labelledby="dLabel">
 <li><a href="#"> Residential</a></li>
 <li><a href="#"> Commercial</a></li>
 <li><a href="#"> Industrial</a></li>
 <li><a href="#"> Institutional</a></li>
 <li><a href="#"> Agricultural</a></li>
</ul> 
</div>
<div class="search-col1">
	<input type="text" placeholder="Locality/Area/Village/Keyword" name=""> 
	<input type="submit" name="" value="Go">		
</div>


	 </div>

</center>-->

<div class="realestate_section">
	 
		<a href="<?=$develpers_project_link;?>" id="home-developer-projects-link" class="realestate_section_col home-block-links">
			<img src="<?php bloginfo("template_url"); ?>/images/img02.jpg">
			<h2>DEVELOPERS PROJECTS <small class="city_name"><?=$selected_city?></small></h2> 
		</a>

		<a href="<?=$resale_link;?>" id="home-resale-link" class="realestate_section_col home-block-links">
			<img src="<?php bloginfo("template_url"); ?>/images/img03.jpg">
			<h2>PROPERTIES IN RESALE  <small class="city_name"><?=$selected_city?></small></h2> 
		</a> 

		<a href="<?=$rent_link?>" id="home-rent-link" class="realestate_section_col home-block-links">
			<img src="<?php bloginfo("template_url"); ?>/images/img04.jpg">
			<h2>PROPERTIES FOR LEASE & RENT OUT  <small class="city_name"><?=$selected_city?></small></h2> 
		</a>  
		<a href="<?php echo site_url();?>/simple-listing-list-view/" class="realestate_section_col home-block-links">
			<img src="<?php bloginfo("template_url"); ?>/images/img05.jpg">
			<h2>YOUR RECENT ACTIVITIES   <small>24/08/2016</small></h2> 
		</a>   

	</div>
 

 <!-- Search bar section end -->



<?php
while ( have_posts() ): the_post();
the_content();
endwhile;
?>

<?php get_footer(); ?>
