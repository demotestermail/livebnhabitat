<?php
/**
 * Template Name: Stripe Charge Page
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 27/06/16
 * Time: 5:18 AM
 */

require_once( get_template_directory() . '/framework/stripe-php/init.php' );
$allowed_html =array();

$current_user = wp_get_current_user();
$userID       =   $current_user->ID;
$user_email   =   $current_user->user_email;
$username     =   $current_user->user_login;
$submission_currency = houzez_option('currency_paid_submission');

$stripe_secret_key = houzez_option('stripe_secret_key');
$stripe_publishable_key = houzez_option('stripe_publishable_key');
$stripe = array(
    "secret_key"      => $stripe_secret_key,
    "publishable_key" => $stripe_publishable_key
);
\Stripe\Stripe::setApiKey($stripe['secret_key']);


// Retrieve the request's body and parse it as JSON
$input = @file_get_contents("php://input");


if( is_email($_POST['stripeEmail']) ) {  // done
    $stripeEmail=  wp_kses ( esc_html($_POST['stripeEmail']) ,$allowed_html );
} else {
    wp_die('None Mail');
}

if( isset($_POST['userID']) && !is_numeric( $_POST['userID'] ) ) { //done
    die();
}

if( isset($_POST['propID']) && !is_numeric( $_POST['propID'] ) ) { //done
    die();
}

if( isset($_POST['submission_pay']) && !is_numeric( $_POST['submission_pay'] ) ) { //done
    die();
}

if( isset($_POST['pack_id']) && !is_numeric( $_POST['pack_id'] ) ){
    die();
}

if( isset($_POST['pay_ammout']) && !is_numeric( $_POST['pay_ammout'] ) ) { //done
    die();
}

if( isset($_POST['houzez_stripe_recurring']) && !is_numeric( $_POST['houzez_stripe_recurring'] ) ){
    die();
}

if( isset($_POST['featured_pay']) && !is_numeric( $_POST['featured_pay'] ) ){
    die();
}

if( isset($_POST['is_upgrade']) && !is_numeric( $_POST['is_upgrade'] ) ){
    die();
}


if ( isset ($_POST['submission_pay'])  && $_POST['submission_pay'] == 1  ) {
    try {
        $token  = wp_kses ( $_POST['stripeToken'] ,$allowed_html);

        $customer = \Stripe\Customer::create(array(
            "email" => $stripeEmail,
            "source" => $token // obtained with Stripe.js
        ));

        $userId      = intval( $_POST['userID'] );
        $listing_id  = intval( $_POST['propID'] );
        $pay_ammout  = intval( $_POST['pay_ammout'] );
        $is_featured = 0;
        $is_upgrade  = 0;

        if ( isset($_POST['featured_pay']) && $_POST['featured_pay']==1 ){
            $is_featured    =   intval($_POST['featured_pay']);
        }

        if ( isset($_POST['is_upgrade']) && $_POST['is_upgrade']==1 ){
            $is_upgrade    =   intval($_POST['is_upgrade']);
        }

        $charge = \Stripe\Charge::create(array(
            "amount" => $pay_ammout,
            'customer' => $customer->id,
            "currency" => $submission_currency,
            //"source" => "tok_18Qks9IwlqUqVdUMkzqkPsbV", // obtained with Stripe.js
            //"description" => ""
        ));


        $time = time();
        $date = date('Y-m-d H:i:s',$time);

        if( $is_upgrade == 1 ) {
            $invoice_id = houzez_add_invoice( 'Upgrade to Featured', 'one_time', $listing_id, $date, $userID, 0, 1, '' );
            houzez_email_to_admin('email_upgrade');
            update_post_meta( $listing_id, 'fave_featured', 1 );
            update_post_meta( $invoice_id, 'invoice_payment_status', 1 );
        } else {
            update_post_meta( $listing_id, 'fave_payment_status', 'paid' );

            $paid_submission_status    = houzez_option('enable_paid_submission');
            $listings_admin_approved = houzez_option('listings_admin_approved');

            if( $listings_admin_approved != 'yes'  && $paid_submission_status == 'per_listing' ){
                $post = array(
                    'ID'            => $listing_id,
                    'post_status'   => 'publish'
                );
                $post_id =  wp_update_post($post );
            } else {
                $post = array(
                    'ID'            => $listing_id,
                    'post_status'   => 'pending'
                );
                $post_id =  wp_update_post($post );
            }


            if( $is_featured == 1 ) {
                update_post_meta( $listing_id, 'fave_featured', 1 );
                $invoice_id = houzez_add_invoice( 'Publish Listing with Featured', 'one_time', $listing_id, $date, $userID, 1, 0, '' );
            } else {
                $invoice_id = houzez_add_invoice( 'Listing', 'one_time', $listing_id, $date, $userID, 0, 0, '' );
            }
            update_post_meta( $invoice_id, 'invoice_payment_status', 1 );
            houzez_email_to_admin('simple');
        }

        $redirect = houzez_dashboard_listings();
        wp_redirect($redirect); exit;

    }
    catch (Exception $e) {
        $error = '<div class="alert alert-danger">
                <strong>Error!</strong> '.$e->getMessage().'
                </div>';
        print $error;
    }

} else if ( isset ($_POST['houzez_stripe_recurring'] ) && $_POST['houzez_stripe_recurring'] == 1 ) {

    /*----------------------------------------------------------------------------------------
    * Payment for Stripe package recuring
    *-----------------------------------------------------------------------------------------*/
    try {
        $token          =   wp_kses ( esc_html($_POST['stripeToken']) ,$allowed_html);
        $pack_id        =   intval($_POST['pack_id']);
        $stripe_plan    =   esc_html(get_post_meta($pack_id, 'fave_package_stripe_id', true));
        $dash_profile_link = houzez_get_dashboard_profile_link();

        $customer = \Stripe\Customer::create(array(
            "email" => $stripeEmail,
            "source" => $token,
           // "card" =>  $token,
            "plan" =>  $stripe_plan
        ));

        $stripe_customer_id = $customer->id;
        $subscription_id = $customer->subscriptions['data'][0]['id'];


        if( houzez_check_package_downgrade_status($current_user->ID,$pack_id) ){
            houzez_downgrade_to_pack( $current_user->ID, $pack_id );
            houzez_update_membership($current_user->ID,$pack_id, 'recurring','');
        }else{
            houzez_update_membership($current_user->ID,$pack_id, 'recurring','');
        }

        $current_stripe_customer_id =  get_user_meta( $current_user->ID, 'fave_stripe_user_profile', true );
        $is_stripe_recurring        =   get_user_meta( $current_user->ID, 'houzez_has_stripe_recurring',true );
        if ($current_stripe_customer_id !=='' && $is_stripe_recurring == 1 ) {
            if( $current_stripe_customer_id !== $stripe_customer_id ){
                houzez_stripe_cancel_subscription_on_upgrade();
            }
        }


        update_user_meta( $current_user->ID, 'fave_stripe_user_profile', $stripe_customer_id );
        update_user_meta( $current_user->ID, 'houzez_stripe_subscription_id', $subscription_id );
        update_user_meta( $current_user->ID, 'houzez_has_stripe_recurring', 1 );

        wp_redirect( $dash_profile_link ); exit;

    }
    catch (Exception $e) {
        $error = '<div class="alert alert-danger">
                  <strong>Error!</strong> '.$e->getMessage().'
                  </div>';
        print $error;
    }

} else {

/*----------------------------------------------------------------------------------------
* Payment for Stripe package
*-----------------------------------------------------------------------------------------*/
    try {

        $token  = wp_kses (esc_html($_POST['stripeToken']),$allowed_html);
        $customer = \Stripe\Customer::create(array(
            "email" => $stripeEmail,
            "source" => $token // obtained with Stripe.js
        ));

        $dash_profile_link = houzez_get_dashboard_profile_link();
        $userId     =   intval($_POST['userID']);
        $pay_ammout =   intval($_POST['pay_ammout']);
        $pack_id    =   intval($_POST['pack_id']);

        $charge = \Stripe\Charge::create(array(
            "amount" => $pay_ammout,
            'customer' => $customer->id,
            "currency" => $submission_currency,
            //"source" => "tok_18Qks9IwlqUqVdUMkzqkPsbV", // obtained with Stripe.js
            //"description" => ""
        ));

        if( houzez_check_package_downgrade_status($current_user->ID,$pack_id) ){
            houzez_downgrade_to_pack( $current_user->ID, $pack_id );
            houzez_update_membership($current_user->ID, $pack_id, 'one_time', '');
        }else{
            houzez_update_membership($current_user->ID,$pack_id, 'one_time', '');
        }


        update_user_meta( $current_user->ID, 'houzez_has_stripe_recurring', 0 );
        wp_redirect( $dash_profile_link );  exit;

    }
    catch (Exception $e) {
        $error = '<div class="alert alert-danger">
                  <strong>Error!</strong> '.$e->getMessage().'
                  </div>';
        print $error;
    }


}

if( !function_exists('houzez_stripe_cancel_subscription_on_upgrade') ) {
    function houzez_stripe_cancel_subscription_on_upgrade() {
        global $current_user;
        require_once( get_template_directory() . '/framework/stripe-php/init.php' );

        $current_user = wp_get_current_user();
        $userID = $current_user->ID;

        $stripe_customer_id = get_user_meta($userID, 'fave_stripe_user_profile', true);
        $subscription_id = get_user_meta($userID, 'houzez_stripe_subscription_id', true);

        $stripe_secret_key = houzez_option('stripe_secret_key');
        $stripe_publishable_key = houzez_option('stripe_publishable_key');
        $stripe = array(
            "secret_key"      => $stripe_secret_key,
            "publishable_key" => $stripe_publishable_key
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $sub = \Stripe\Customer::retrieve($stripe_customer_id);
        $sub = \Stripe\Subscription::retrieve($subscription_id);
        $sub->cancel(
            array("at_period_end" => true)
        );
        update_user_meta($current_user->ID, 'houzez_stripe_subscription_id', '');
    }
}

?>