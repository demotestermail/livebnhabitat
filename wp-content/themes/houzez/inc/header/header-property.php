<?php
if ( is_page_template( 'template/template-splash.php' ) ) {
$css_class = 'header-section slpash-header';
} else {
$css_class = 'header-section-4 not-splash-header';
}
$allowed_html = array();
global $current_user, $post;
wp_get_current_user();
$userID  =  $current_user->ID;
$user_custom_picture =  get_the_author_meta( 'fave_author_custom_picture' , $userID );
$header_layout = houzez_option('header_4_width');
$main_menu_sticky = houzez_option('main-menu-sticky');
$header_4_menu_align = houzez_option('header_4_menu_align');
$top_bar = houzez_option('top_bar');
$trans_class = '';
$fave_main_menu_trans = get_post_meta( $post->ID, 'fave_main_menu_trans', true );
if( $fave_main_menu_trans == 'yes' ) {
$trans_class = 'houzez-header-transparent';
}
if( $top_bar != 0 ) {
//get_template_part('inc/header/top', 'bar');
}
$menu_righ_no_user = '';
$header_login = houzez_option('header_login');
if( $header_4_menu_align == 'nav-right' && $header_login != 'yes' ) {
$menu_righ_no_user = 'menu-right-no-user';
}
$selected_city='';
if(isset($_COOKIE['selected_city'])){
$selected_city=json_decode(stripslashes($_COOKIE['selected_city']),true);
$selected_city=$selected_city['slug'];
}
$header_id='';
if (!is_singular( 'property' ) ) {
   $header_id='header-section';
}
?>
    <!--start section header-->
    <header id="<?=$header_id;?>" class="houzez-header-main header-section8 <?php echo esc_attr( $css_class ).' '.esc_attr( $header_4_menu_align ).' '.esc_attr($trans_class).' '.esc_attr($menu_righ_no_user); ?> hidden-sm hidden-xs" data-sticky="1">
        <div class="<?php echo sanitize_html_class( $header_layout ); ?>">
            <div class="top_test">
                <?php if(is_user_logged_in()){
    
    ?>You are logged in as
                <a href="#">
                    <?= ucfirst(get_user_meta($userID,'nickname')[0]); ?>
                </a>
                <?php } else { ?>
                <a href="#" data-toggle="modal" data-target="#pop-login">
                    <i class="fa fa-user hidden-md hidden-lg"></i> <span class="hidden-sm hidden-xs">Login / Register</span></a>
                <?php } ?>
            </div>
            <div class="header-left header-left05">
                <div class="logo logo-desktop">
                    <?php get_template_part('inc/header/logo'); ?>
                </div>
                <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <ul id="main-nav" class="">
                        <?php if(is_user_logged_in()) {
                      ?>
                        <li id="menu-item-2728" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2728"> <a href="<?php echo site_url(); ?>/my-profile/">Profile</a></li>
                        <?php
                     } else {?>
                            <li id="menu-item-2728" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2728"><a data-toggle="modal" data-target="#pop-login" href="javascript:void(0)" onclick="closeNav()">Login / Register</a>
                                <?php } ?>
                            </li>
                            </li>
                    </ul>
                    <?php
// Pages Menu
if ( has_nav_menu( 'main-menu' ) ) :
    wp_nav_menu( array (
    'theme_location' => 'inner-menu',
    'container' => '',
    'container_class' => '', 
    'menu_class' => '',
    'menu_id' => 'main-nav',
    'depth' => 4
    ));
    endif;
?>
                </div>
                <nav class="navi main-nav top_header">
                    <ul class="menu_top_navi">
                       <!-- <li><a href="#"><i class="fa fa-home"></i></a></li>-->
                        <li> <a href="#">Projects</a>
                            <ul>
                                <?php
$property_status = get_terms (
array(
'property_status'  
),
array(
'orderby' => 'name',
'order' => 'ASC',
'hide_empty' => false,
'parent' => 0
)
);
foreach ($property_status as $property) {
$link_pro_status=get_term_link($property->term_id);
echo '<li><a href="'.$link_pro_status.'">'.$property->name.'</a></li>';
}
?>
                            </ul>
                        </li>
                        <li class="at_class">at</li>
                        <?php 
                        $selected_city='Select City';
                            if(isset($_COOKIE['selected_city'])){
                                $selected_city=json_decode(stripslashes($_COOKIE['selected_city']),true);
                                $selected_city=$selected_city['slug'];
                            } 
                        ?>
                        <li class="has-child select_icon">
                            <a href="#" id="city_selected_pro">
                                <?=ucwords($selected_city);?>
                            </a>
                            <ul class="top-selection-city">
                                <?php
                            $prop_city = get_terms (
                                array(
                                'property_city'  
                                ),
                                array(
                                'orderby' => 'name',
                                'order' => 'ASC',
                                'hide_empty' => true,
                                'parent' => 0
                                )
                            );
                            foreach ($prop_city as $city) {
                            $slug_city=$city->slug;
                            echo '<li id="'.$slug_city.'"><a href="#">'.$city->name.'</a></li>';
                            }
?>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <span class="menu_icon" onclick="openNav()">&#9776;</span>
            <?php if( class_exists('Houzez_login_register') ): ?>
            <?php if( houzez_option('header_login') != 'no' ): ?>
            <?php endif; ?>
            <?php endif; ?>
        </div>
    </header>
    <!--end section header-->
  <?php get_template_part( 'inc/header/mobile-header' ); ?>
    <div class="main-nav1">
        <div class="container">
            <div class="row">
                <nav class="navi main-nav">
                    <?php
                // Pages Menu
                if ( has_nav_menu( 'main-menu' ) ) :
                    wp_nav_menu( array (
                        'theme_location' => 'main-menu',
                        'container' => '',
                        'container_class' => '',
                        'menu_class' => '',
                        'menu_id' => 'main-nav',
                        'depth' => 4
                    ));
                endif;
                ?>
                </nav>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
    </script>