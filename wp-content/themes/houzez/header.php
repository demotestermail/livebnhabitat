<?php
global $houzez_options, $houzez_local,$post;
$houzez_local = houzez_get_localization();
$is_city_selected=is_tax('property_city');

if($is_city_selected==true){
	$queried_object = get_queried_object();
	$selected_city=$queried_object->slug;
	setcookie('selected_city', $selected_city, time() + (86400 * 30), "/");
}
/**
 * @package Houzez
 * @since Houzez 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="google-site-verification" content="DhukUh3xBWC7zNt5e0v7x2mFajhh1jR-yzZC4wZYOxU" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script>
		var base_url="<?php echo site_url(); ?>";
	</script>
	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700,800" rel="stylesheet">    
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/lightslider.css"> 
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/lightslider.js"></script>  
<?php $agent_id = get_post_meta( get_the_ID(), 'fave_agents','false' );
$agent_mobile = get_post_meta( $agent_id, 'fave_agent_mobile', 'false' );
if($agent_mobile){
	$agent_mobile= $agent_mobile;
}else
{
$agent_mobile = get_post_meta( '150', 'fave_agent_mobile', 'false' );
}
?>


<script type="text/javascript">
	/*Last Working*/
	$(document).ready(function(){		
		$('#submit-chat-login_button').click(function(event){  
			var username=$('#userName').val();
			var email=$('#email').val();
			var phone=$('#phone').val();

		  	if (!username||!email||!phone){
		    	alert('Please fill all Fields');
		    }else if(!isEmail(email)){
		      	alert('Please enter correct email Address');
		    }else if(!isPhone(phone)){
		      	alert('Please enter correct phone Number');
		    }
		    else{
		    	check_user(email);
		    	chatinit('91'+phone,username,email);
		    	
		    }
		    function isEmail(email) {
				var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			  	return regex.test(email);
			} 
			function isPhone(phone) {
				var regex = /^([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})+$/;
				return regex.test(phone);
			}
		   	function chatinit(phone,username,email) {
		      	$('#myModal').modal('toggle');
	      		var CONTACT_LIST_JSON = {"contacts": [{"userId": "919814224828", "displayName": "GPS Waraich", 
	            "imageLink": "https://www.applozic.com/resources/images/applozic_icon.png"}] };
				$applozic.fn.applozic({
					appId: 'realcrafttech1426ad12b1756dfc408bc07c6dc58b224', 
					userId: phone,                     //Logged in user's id, a unique identifier for user
					userName: username,                 //User's display name
					imageLink : '',                     //User's profile picture url
					email : email,    
					contactNumber: '<?php echo $agent_mobile;?>',
					loadOwnContacts : false,
					onInit : function(response) {
						$applozic.fn.applozic('loadContacts', CONTACT_LIST_JSON);
						$applozic.fn.applozic('sendMessage', {
							  'to': '<?php echo $agent_mobile;?>',            // userId of the receiver
							  'message' : 'Message from bnhabitat chat window', // message to send    
							  'type' : 3                    //(optional) DEFAULT(0), TEXT_HTML(3)
						});
						$applozic.fn.applozic('loadTab', '<?php echo $agent_mobile;?>');
					}
				});
			}
	    }); 
	});

	
	function check_user(email){		
		var ajaxurl = '<?php echo admin_url('admin-ajax.php') ?>';		
		$.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {action: 'get_prev', email: email},
            success: function(data){
            	console.log(data);
            }
        });
	}
</script>
<script>
if($( window ).width()<600){
 $(document).ready(function() {
        $('#vertical').lightSlider({
          gallery:true,
          item:1,
          verticalHeight:500,
          loop: true,
          vThumbWidth:100,
          thumbItem:5,
          thumbMargin:9,
          slideMargin:0
        });  
      }); }else{
        $(document).ready(function() {
        $('#vertical').lightSlider({
          gallery:true,
          item:1,
          vertical:true,
          loop: true,
          verticalHeight:500,
          vThumbWidth:100,
          thumbItem:5,
          thumbMargin:9,
          slideMargin:0
        });  
      }); 
        }
  </script> 
  
  
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">   
</head>

<?php
$fave_page_header_search = get_post_meta( $post->ID, 'fave_page_header_search', true );
$fave_head_trans = 'no';
$houzez_onepage_mode = '';
if( $fave_page_header_search != 'yes' ) {
	$fave_head_trans = get_post_meta($post->ID, 'fave_main_menu_trans', true);
}
if ( is_page_template( 'template/template-onepage.php' ) ) {
	$houzez_onepage_mode = 'houzez-onepage-mode';
}
?>

<body <?php body_class($houzez_onepage_mode.' transparent-'.$fave_head_trans); ?>>
<div class="loader-page" style="display: none;"></div>
<div id="fb-root"></div>

<?php get_template_part( 'inc/header/login-register-popup' ); ?>
<?php if ( !is_page_template( 'template/template-splash.php' ) ) { ?>

<?php
$houzez_header_style = houzez_option('header_style');
if( empty($houzez_header_style)) {
	$houzez_header_style = '1';
}

/* Add For Custom Property Header */
if((!is_page() || is_page('advanced-search') || is_page_template('template/developers-listing-template.php')  ) && !is_404() && !is_home())
{
	 $houzez_header_style='property';
}



get_template_part( 'inc/header/header', $houzez_header_style );
//get_template_part( 'inc/header/header', 'transparent' ); ?>

<?php
$search_enable = houzez_option('main-search-enable');
$search_position = houzez_option('search_position');
$search_pages = houzez_option('search_pages');

if( !is_404() && !is_search() ) {
	$adv_search_enable = get_post_meta($post->ID, 'fave_adv_search_enable', true);
	$adv_search = get_post_meta($post->ID, 'fave_adv_search', true);
	$adv_search_pos = get_post_meta($post->ID, 'fave_adv_search_pos', true);
}

if( isset( $_GET['search_pos'] ) ) {
	$search_enable = 1;
	$search_position = $_GET['search_pos'];
}

if( ! is_search() ) {
	if ((!empty($adv_search_enable) && $adv_search_enable != 'global')) {
		if ($adv_search_pos == 'under_menu') {
			if ($adv_search == 'show' || $adv_search == 'hide_show') {
				get_template_part('template-parts/advanced-search', 'undermenu');
			}
		}
	} else {
		if (!is_home() && !is_singular('post')) {
			if ($search_enable != 0 && $search_position == 'under_nav') {
				if ($search_pages == 'only_home') {
					if (is_front_page()) {
						get_template_part('template-parts/advanced-search', 'undermenu');
					}
				} elseif ($search_pages == 'all_pages') {
					get_template_part('template-parts/advanced-search', 'undermenu');

				} elseif ($search_pages == 'only_innerpages') {
					if (!is_front_page()) {
						get_template_part('template-parts/advanced-search', 'undermenu');
					}
				}
			}
		}
	}
}

if( !is_404() && !is_search() ) {
	$adv_search_enable = get_post_meta($post->ID, 'fave_adv_search_enable', true);
	$adv_search = get_post_meta($post->ID, 'fave_adv_search', true);
}
$section_body = '';
if( ( !empty( $adv_search_enable ) && $adv_search_enable != 'global' ) ) {
	if( $adv_search == 'hide_show' ) {
		$section_body = 'sticky_show_scroll_active';
	} else {
		$section_body = '';
	}
}
if ( is_page_template( 'template/property-listings-map.php' ) ) { $section_body .= 'houzez-body-half'; }
?>

<div id="section-body" class="<?php echo esc_attr( $section_body ); ?>">

	<?php
		get_template_part( 'template-parts/page-headers/page-header' );

		if( ! is_search() ) {
			if ((!empty($adv_search_enable) && $adv_search_enable != 'global')) {
				if ($adv_search_pos == 'under_banner') {
					if ($adv_search == 'show' || $adv_search == 'hide_show') {
						get_template_part('template-parts/advanced-search', 'undermenu');
					}
				}
			} else {
				if (!is_home() && !is_singular('post')) {
					if ($search_enable != 0 && $search_position == 'under_banner') {
						if ($search_pages == 'only_home') {
							if (is_front_page()) {
								get_template_part('template-parts/advanced-search', 'undermenu');
							}
						} elseif ($search_pages == 'all_pages') {
							get_template_part('template-parts/advanced-search', 'undermenu');

						} elseif ($search_pages == 'only_innerpages') {
							if (!is_front_page()) {
								get_template_part('template-parts/advanced-search', 'undermenu');
							}
						}
					}
				}
			}
		}
	?>

	<?php if( !is_singular( 'property' ) && !is_page_template( 'template/property-listings-map.php' ) ) { ?>
	
    
	<div class="container ">

	
	<?php } ?>

<?php } // End splash template if  ?>