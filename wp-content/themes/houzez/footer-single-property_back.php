<?php $copy_rights = houzez_option('copy_rights'); ?>
<!--start footer section-->
<footer id="footer-section">
    <?php get_template_part('template-parts/footer'); ?>
</footer>
<!--end footer section-->
<?php wp_footer(); ?>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enter Information</h4>
      </div>
      <div class="modal-body">
          <div id="form-chat-login" class="vertical">
          
            <div class="form-group">
              <input class="form-control" id="userName" name="userName" placeholder="Name" required="" type="text">
            </div>
            <div class="form-group">
              <input class="form-control" id="email" name="email" placeholder="Email" required="" type="email">
            </div>
            <div class="form-group">
              <input class="form-control" id="phone" name="phone" maxlength="10" placeholder="Phone Number" required="" type="text">
            </div>
            <div class="form-group last last-child text-center">
              <button type="submit" id="submit-chat-login_button" data-mck-id="" class="btn btn-primary">Start chat</button>
              <a href="#" class="applozic-launcher hidden" data-mck-id="PUT_OTHER_USERID_HERE" data-mck-name="PUT_OTHER_USER_DISPLAY_NAME_HERE">CHAT BUTTON</a>
            </div>
          </div>
      </div>
     
    </div> 

  </div>
</div>
</body>
</html>