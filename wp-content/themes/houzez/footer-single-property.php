<?php $copy_rights = houzez_option('copy_rights'); ?>
<!--start footer section-->
<footer id="footer-section">
    <?php get_template_part('template-parts/footer'); ?>
</footer>
<!--end footer section-->
<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.elevatezoom.js"></script>  
<script>
  $("#zoom_08").elevateZoom({
  zoomType  : "lens",
  lensShape : "round",
  lensSize    : 200
});
   $("#zoom_07").elevateZoom({
  zoomType  : "lens",
  lensShape : "round",
  lensSize    : 200
});
</script>
  <script type="text/javascript">
     (function(d, m){var s, h;       
     s = document.createElement("script");
     s.type = "text/javascript";
     s.async=true;
     s.src="https://apps.applozic.com/sidebox.app";
     h=document.getElementsByTagName('head')[0];
     h.appendChild(s);
     window.applozic=m;
     m.init=function(t){m._globals=t;}})(document, window.applozic || {});
   </script>
   <script type="text/javascript">
    var $original;
    if (typeof jQuery !== 'undefined') {
      $original = jQuery.noConflict(true);
      $ = $original;
      jQuery = $original;
    }
  </script>

<div class="mck-sidebox-launcher">
  <a href="#" class="btn btn-info btn-lg applozic-launcher1 mck-button-launcher" data-toggle="modal" data-target="#myModal">
    <span class="mck-icon-chat"></span>
  </a>
</div>
<!-- Modal-->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enter Information</h4>
      </div>
      <div class="modal-body">
          <div id="form-chat-login" class="vertical">
          
            <div class="form-group">
              <input class="form-control" id="userName" name="userName" placeholder="Name" required="" type="text">
            </div>
            <div class="form-group">
              <input class="form-control" id="email" name="email" placeholder="Email" required="" type="email">
            </div>
            <div class="form-group">
              <input class="form-control" id="phone" name="phone" maxlength="10" placeholder="Phone Number" required="" type="text">
            </div>
            <div class="form-group last last-child text-center">
              <button type="submit" id="submit-chat-login_button" data-mck-id="" class="btn btn-primary">Start chat</button>
              <a href="#" class="applozic-launcher hidden" data-mck-id="PUT_OTHER_USERID_HERE" data-mck-name="PUT_OTHER_USER_DISPLAY_NAME_HERE">CHAT BUTTON</a>
            </div>
          </div>
      </div>
     
    </div> 

  </div>
</div>
</body>
</html>