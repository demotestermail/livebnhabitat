<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div>
		<input value="" name="s" id="s" type="text" placeholder="<?php esc_html_e("Search", 'houzez' ); ?>">
		<button type="submit"></button>
	</div>
</form>