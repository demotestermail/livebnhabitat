<?php get_header();

global $post, $property_nav,$prop_agent_email;
$post_meta_data       = get_post_custom($post->ID);

$houzez_prop_id              = get_post_meta( get_the_ID(), 'fave_property_id', true );
$prop_images          = get_post_meta( get_the_ID(), 'fave_property_images', false );
$prop_address         = get_post_meta( get_the_ID(), 'fave_property_map_address', true );
$prop_featured        = get_post_meta( get_the_ID(), 'fave_featured', true );
$prop_video_img       = get_post_meta( get_the_ID(), 'fave_video_image', true );
$prop_video_url       = get_post_meta( get_the_ID(), 'fave_video_url', true );
$property_location    = get_post_meta( get_the_ID(), 'fave_property_location',true);
$virtual_tour         = get_post_meta( $post->ID, 'fave_virtual_tour', true );
$property_map         = get_post_meta( get_the_ID(), 'fave_property_map',true);
$property_streetView  = get_post_meta( get_the_ID(), 'fave_property_map_street_view',true);
$prop_floor_plan      = get_post_meta( get_the_ID(), 'fave_floor_plans_enable', true );
$floor_plans          = get_post_meta( get_the_ID(), 'floor_plans', true );
$prop_description     = get_the_content();
$enable_multi_units   = get_post_meta( get_the_ID(), 'fave_multiunit_plans_enable', true );
$multi_units          = get_post_meta( get_the_ID(), 'fave_multi_units', true );
$prop_features        = wp_get_post_terms( get_the_ID(), 'property_feature', array("fields" => "all"));
$sticky_sidebar = houzez_option('sticky_sidebar');
$prop_price = get_post_meta( get_the_ID(), 'fave_property_price', true );
$prop_size = get_post_meta( get_the_ID(), 'fave_property_size', true );
$bedrooms = get_post_meta( get_the_ID(), 'fave_property_bedrooms', true );
$bathrooms = get_post_meta( get_the_ID(), 'fave_property_bathrooms', true );
$year_built = get_post_meta( get_the_ID(), 'fave_property_year', true );
$garage = get_post_meta( get_the_ID(), 'fave_property_garage', true );
$garage_size = get_post_meta( get_the_ID(), 'fave_property_garage_size', true );
$specifications = get_post_meta( get_the_ID(), 'fave_specifications', true );
$sizess = get_post_meta( get_the_ID(), 'fave_property_sizes', true ); 
$towers=get_post_meta( get_the_ID(), 'Towers', true );
  $prop_agent_num = $agent_num_call = $prop_agent_email = $gallery_view = $map_view = $street_view = '';

$enableDisable_agent_forms = houzez_option('agent_forms');

if( $prop_agent_display != '-1' && $agent_display_option == 'agent_info' ) {
    $prop_agent_id = get_post_meta( get_the_ID(), 'fave_agents', true );
    $prop_agent_email = get_post_meta( $prop_agent_id, 'fave_agent_email', true );

} elseif ( $agent_display_option == 'author_info' ) {
    $prop_agent_email = get_the_author_meta( 'email' );
}
 
$houzez_prop_detail = false;

if( !empty( $houzez_prop_id ) ||
    !empty( $prop_price ) ||
    !empty( $prop_size ) ||
    !empty( $bedrooms ) ||
    !empty( $bathrooms ) ||
    !empty( $year_built ) ||
    !empty( $garage )
) {
    $houzez_prop_detail = true;
}

$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'houzez-single-big-size' );
$tab_height = $featured_image[2];

if( $tab_height < 600 ) {
    $tab_height = '600';
}

$property_top_area = houzez_option('prop-top-area');
/* For demo purpose only */
if( isset( $_GET['s_top'] ) ) {
    $property_top_area = $_GET['s_top'];
}

$property_layout = houzez_option('prop-content-layout');
if( isset( $_GET['s_layout'] ) ) {
    $property_layout = $_GET['s_layout'];
}

// Print property
$fave_print_page_link = houzez_get_template_link('template/template-print.php');
$print_link = add_query_arg( 'propID', $post->ID, $fave_print_page_link );
houzez_count_page_stats( $post->ID );
?>
<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
<!--start detail top-->
<?php
    if( $property_top_area == 'v1' ) {
       /* get_template_part('property-details/toparea', 'v1');*/
        get_template_part('property-details/toparea', 'v0');
    } elseif ( $property_top_area == 'v2' ) {
        get_template_part('property-details/toparea', 'v2');
    } elseif ( $property_top_area == 'v3' ) {
        get_template_part('property-details/toparea', 'v3');
    }
    ?>
    <!--end detail top-->
    <?php
    if ( $property_top_area == 'v4' ) {
        get_template_part( 'property-details/slideshow-for-v4');
    }

    ?>
        <!--start detail content-->
        <section class="section-detail-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <?php //get_template_part( 'property-details/property-description' ); ?>
                        <?php
$currentOffers = get_post_meta( get_the_ID(), 'currentOffers', true ); 

 if($currentOffers !=""){ ?>
                            <div id="sub_property" class="detail-multi-properties detail-block ">
                                <div class="table-wrapper">
                                    <?php echo $currentOffers;?> </div>
                            </div>
                            <?php } ?>
                            <div class="detail-bar">
                                <?php // get_template_part( 'property-details/custom-content' ); ?>
                                <?php
            
                        if ( $property_top_area == 'v3' ) {
                            get_template_part( 'property-details/slideshow');
                        }

                        if( $property_layout == 'tabs' ) {
                            get_template_part( 'property-details/single-property', 'tabs');
                        } else if( $property_layout == 'tabs-vertical' ) {
                            get_template_part( 'property-details/single-property', 'tabs-vertical');
                        } else {
                            get_template_part( 'property-details/single-property', 'simple');
                        }
                        ?>
                                
                                    <?php //get_template_part( 'property-details/next', 'prev' ); ?>
                            </div>
                    </div>
                   
                    <div id="sticky" class="col-sm-3 <?php if( $sticky_sidebar['single_property'] != 0 ){ echo 'houzez_sticky'; }?>">
                       <!--  <div class="box_shadow_col overview_sect request_butttons ">
                            <div class="row">
                                <div class="col-md-12">
                                <a href="<?=site_url();?>/add-new-property/">
<img src="<?=get_template_directory_uri()?>/images/got-unit.png">
                                 </a>
                                 </div>
                            </div>
                        </div> -->

                        <div class="box_shadow_col overview_sect request_butttons">
                            <div class="row">
                                <h2 class="title-left mb5">Communicate & Contact</h2> </div>
                            <div class="w100p ">
                                <a data-toggle="modal" <?php if(is_user_logged_in()){ echo 'data-target="#sitevisit"';}else{ echo ' data-target="#pop-login"'; }?> class="btn btn-primary" href="#">Request a Site Visit</a>
                                <a data-toggle="modal" <?php if(is_user_logged_in()){ echo 'data-target="#callbackform"'; }else{ echo ' data-target="#pop-login"'; }?> class="btn btn-primary" href="#">Request a Call Back</a>
                            </div>
                        </div>
                        <!--   <div class="box_shadow_col overview_sect projects_expert mt20">
                    <div class="row">
                        <h2 class="title-left mb5">Our Project Expert(s)</h2>  
                    </div>
                    <div class="projects_ex">
                            <img src="http://localhost/bnhabitat/wp-content/themes/houzez/images/img07.png"> 
                            <h5>GPS Waraich <small> Manager Sales &amp; Marketing</small></h5>
                            <h6>+91-9872XXXXXX    <span>gps.waraich@bnhabitat.com</span></h6>
                        </div>
                        <div class="projects_ex_right">
                            <textarea placeholder="Chat With Our Project Expert?"></textarea>
                        </div> 
                        <button type="button" class="btn btn-primary">Send</button> 
                        </div>
 -->
                        <?php


                  if( !empty( $prop_agent_email ) && $enableDisable_agent_forms != 0 ) { ?>
                            &nbsp;
                            <?php get_template_part( 'property-details/agent', 'form' );; ?>
                            <?php } ?>
                            <div class="cclearfix"></div>
                            <?php // get_sidebar('property'); ?>
                    </div>
                </div>
            </div>
        </section>
        <link rel="stylesheet" type="text/css" media="screen" href="https://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
        <!--end detail content-->
        </div>
        <!--Start in header, end #section-body-->
        <!--start lightbox-->
        <?php get_template_part( 'property-details/lightbox' ); ?>
        <!-- End Lightbox-->
        <?php endwhile; endif; ?>
        <?php

$agent_display_option = get_post_meta( get_the_ID(), 'fave_agent_display_option', true );
$prop_agent_display = get_post_meta( get_the_ID(), 'fave_agents', true );
$enable_contact_form_7_prop_detail = houzez_option('enable_contact_form_7_prop_detail');
$contact_form_agent_above_image = houzez_option('contact_form_agent_above_image');
$enable_direct_messages = houzez_option('enable_direct_messages');
$prop_agent_num = $agent_num_call = $prop_agent_email = '';
$listing_agent = '';
$is_single_agent = true;

if( $prop_agent_display != '-1' && $agent_display_option == 'agent_info' ) {

    $prop_agent_ids = get_post_meta( get_the_ID(), 'fave_agents' );
    $prop_agent_ids = array_filter( $prop_agent_ids, function($v){
        return ( $v > 0 );
    });
    // remove duplicated ids
    $prop_agent_ids = array_unique( $prop_agent_ids );

    if ( ! empty( $prop_agent_ids ) ) {
        $agents_count = count( $prop_agent_ids );
        if ( $agents_count > 1 ) :
            $is_single_agent = false;
        endif;
        $listing_agent = '';
        foreach ( $prop_agent_ids as $agent ) {
            if ( 0 < intval( $agent ) ) {

                $prop_agent_id = intval( $agent );
                $prop_agent_phone = get_post_meta( $prop_agent_id, 'fave_agent_office_num', true );
                $prop_agent_mobile = get_post_meta( $prop_agent_id, 'fave_agent_mobile', true );
                $prop_agent_email = get_post_meta( $prop_agent_id, 'fave_agent_email', true );
                $agent_num_call = str_replace(array('(',')',' ','-'),'', $prop_agent_mobile);
                $prop_agent = get_the_title( $prop_agent_id );
                $thumb_id = get_post_thumbnail_id( $prop_agent_id );
                $thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'thumbnail', true );
                $prop_agent_photo_url = $thumb_url_array[0];
                $prop_agent_permalink = get_post_permalink( $prop_agent_id );

                $agent_args = array();
                $agent_args[ 'agent_id' ] = $prop_agent_id;
                $agent_args[ 'agent_name' ] = $prop_agent;
                $agent_args[ 'agent_mobile' ] = $prop_agent_mobile;
                $agent_args[ 'agent_mobile_call' ] = str_replace(array('(',')',' ','-'),'', $prop_agent_mobile);
                $agent_args['agent_phone'] = $prop_agent_phone;
                $agent_args[ 'agent_email' ] = $prop_agent_email;
                $agent_args[ 'link' ] = $prop_agent_permalink;
                if( empty( $prop_agent_photo_url )) {
                    $agent_args[ 'picture' ] = get_template_directory_uri().'/images/profile-avatar.png';
                } else {
                    $agent_args[ 'picture' ] = $thumb_url_array[0];
                }
                $listing_agent .= houzez_get_agent_info_top( $agent_args, 'agent_form', $is_single_agent );
            }
        }
    }

} elseif( $agent_display_option == 'agency_info' ) {
    $prop_agent_id = get_post_meta( get_the_ID(), 'fave_property_agency', true );
    $prop_agent_phone = get_post_meta( $prop_agent_id, 'fave_agency_phone', true );
    $prop_agent_mobile = get_post_meta( $prop_agent_id, 'fave_agency_mobile', true );
    $prop_agent_email = get_post_meta( $prop_agent_id, 'fave_agency_email', true );
    $agent_num_call = str_replace(array('(',')',' ','-'),'', $prop_agent_mobile);
    $prop_agent = get_the_title( $prop_agent_id );
    $thumb_id = get_post_thumbnail_id( $prop_agent_id );
    $thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'thumbnail', true );
    $prop_agent_photo_url = $thumb_url_array[0];
    $prop_agent_permalink = get_post_permalink( $prop_agent_id );

    $agent_args = array();
    $agent_args[ 'agent_id' ] = $prop_agent_id;
    $agent_args[ 'agent_name' ] = $prop_agent;
    $agent_args[ 'agent_mobile' ] = $prop_agent_mobile;
    $agent_args[ 'agent_mobile_call' ] = str_replace(array('(',')',' ','-'),'', $prop_agent_mobile);
    $agent_args[ 'agent_phone' ] = $prop_agent_phone;
    $agent_args[ 'agent_email' ] = $prop_agent_email;
    $agent_args[ 'link' ] = $prop_agent_permalink;
    if( empty( $prop_agent_photo_url )) {
        $agent_args[ 'picture' ] = get_template_directory_uri().'/images/profile-avatar.png';
    } else {
        $agent_args[ 'picture' ] = $thumb_url_array[0];
    }
    $listing_agent .= houzez_get_agent_info_top( $agent_args, 'agent_form' );

} elseif ( $agent_display_option == 'author_info' ) {
    $prop_agent = get_the_author();
    $prop_agent_permalink = get_author_posts_url( get_the_author_meta( 'ID' ) );
    $prop_agent_phone = get_the_author_meta( 'fave_author_phone' );
    $prop_agent_mobile = get_the_author_meta( 'fave_author_mobile' );
    $agent_num_call = str_replace(array('(',')',' ','-'),'', $prop_agent_num);
    $prop_agent_photo_url = get_the_author_meta( 'fave_author_custom_picture' );
    $prop_agent_email = get_the_author_meta( 'email' );

    $agent_args = array();
    $agent_args[ 'agent_id' ] = get_the_author_meta( 'ID' );
    $agent_args[ 'agent_name' ] = $prop_agent;
    $agent_args[ 'agent_mobile' ] = $prop_agent_mobile;
    $agent_args[ 'agent_mobile_call' ] = str_replace(array('(',')',' ','-'),'', $prop_agent_mobile);
    $agent_args[ 'agent_phone' ] = $prop_agent_phone;
    $agent_args[ 'agent_email' ] = $prop_agent_email;
    $agent_args[ 'link' ] = $prop_agent_permalink;
    if( empty( $prop_agent_photo_url )) {
        $agent_args[ 'picture' ] = get_template_directory_uri().'/images/profile-avatar.png';
    } else {
        $agent_args[ 'picture' ] = $thumb_url_array[0];
    }
    $listing_agent .= houzez_get_agent_info_top( $agent_args, 'agent_form', false );
}
if( empty( $prop_agent_photo_url )) {
    $prop_agent_photo_url = get_template_directory_uri().'/images/profile-avatar.png';
}

$agent_name=$agent_args[ 'agent_name' ] ;
 $agent_email = is_email( $prop_agent_email );
$user_info = get_userdata( get_the_author_meta( 'ID' ) );
$user_role = implode(', ', $user_info->roles);
  wp_get_current_user();
                 
                      if($current_user->user_firstname!=''){
                        $c_user_name=$current_user->user_firstname.' '.$current_user->user_lastname;
                      } else {
                        $c_user_name=$current_user->user_login;
                      }
                     $c_mobile= get_user_meta( $current_user->ID,'fave_author_mobile',true);
?>
            <!-- Modal Site Visit-->
            <div class="modal modal_dv_call_bk modal_dv_call_detailpage fade" id="sitevisit" role="dialog">
                <div class="modal-dialog  modal-lg"> 
                    <div class="modal-content sitevisit when_popup">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">When ??</h4>
                        </div>
                        <form id="site_visit_form" action="">
                            <input type="hidden" name="phone" value="<?=$c_mobile;?>">
                            <input type="hidden" name="name" value="<?=$c_user_name;?>">
                            <input type="hidden" name="email" value="<?=$current_user->user_email;?>">
                            <input type="hidden" name="action" value="houzez_agent_send_site_visit">
                            <input type="hidden" name="agent_email" value="<?=$agent_email;?>">
                            <input type="hidden" name="agent_name" value="<?=$agent_name;?>">
                            <input type="hidden" name="property_title" value="<?php echo esc_attr(get_the_title($post->ID)); ?>" />
                            <input type="hidden" name="property_permalink" value="<?php echo esc_url(get_permalink($post->ID)); ?>" />
                            <div class="modal-body request_visit_hide">
                              <!--   Site Visit --> 
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="radio text-right">
                                            <label>As Soon Poosible
                                                <input type="radio" checked="" value="As Soon As possible" name="time" class="v_time"> 
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="radio text-center">
                                            <label>Any Time
                                                <input type="radio" value="Any Time" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 ">
                                        <div class="radio text-left">
                                            <label>Suitable Time
                                                <input type="radio" value="suitable_time" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="suitable_time_block display-hide">
                                    <hr>
                                     <div class="clearfix"></div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>Date</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker_date" class="input-append date datetimepicker_date">
                                                        <input type="text" name="v_date" value="<?php echo date('d-m-Y',time()); ?>">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>Between</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker2" class="input-append date">
                                                        <input type="text" name="v_time_between" placeholder="HH:MM" value="02:00 PM">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>To</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker" class="input-append date">
                                                        <input type="text" name="v_time_to" placeholder="HH:MM" value="05:00 PM">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                
                                </div>
                            </div>
                            <div class="modal-footer request_visit_hide">
                                <button type="button" id="" class="btn btn-primary submit_request_visit">Done</button>
                            </div>
                            <div class="succss_message_visit display-hide">
                                We Will Call you soon..
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Modal Reqest Call back-->
            <div class="modal fade modal_dv_call_bk modal_dv_call_detailpage" id="callbackform" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content callbackform when_popup">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">When ??</h4>
                        </div>
                        <form id="callback_form" action="">
                            <input type="hidden" name="phone" value="<?=$c_mobile;?>">
                            <input type="hidden" name="name" value="<?=$c_user_name;?>">
                            <input type="hidden" name="email" value="<?=$current_user->user_email;?>">
                            <input type="hidden" name="action" value="houzez_agent_send_callback">
                            <input type="hidden" name="agent_email" value="<?=$agent_email;?>">
                            <input type="hidden" name="agent_name" value="<?=$agent_name;?>">
                            <input type="hidden" name="property_title" value="<?php echo esc_attr(get_the_title($post->ID)); ?>" />
                            <input type="hidden" name="property_permalink" value="<?php echo esc_url(get_permalink($post->ID)); ?>" />
                            <div class="modal-body request_callback_hide">
                              <!--   Site Visit -->
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="radio text-right">
                                            <label>As Soon Poosible
                                                <input type="radio" checked="" value="As Soon As possible" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="radio text-center">
                                            <label>Any Time
                                                <input type="radio" value="Any Time" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="radio text-left">
                                            <label>Suitable Time
                                                <input type="radio" value="suitable_time" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="suitable_time_block display-hide">
                                    <hr>
                                   <div class="clearfix"></div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>Date</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker_date" class="input-append date datetimepicker_date">
                                                        <input type="text" name="v_date" value="<?php echo date('d-m-Y',time()); ?>">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>Between</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker2" class="input-append date">
                                                        <input type="text" name="v_time_between" placeholder="HH:MM" value="02:00 PM">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>To</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker" class="input-append date">
                                                        <input type="text" name="v_time_to" placeholder="HH:MM" value="05:00 PM">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   
                                </div>
                            </div> 
                            <div class="modal-footer request_callback_hide">
                                <button type="button" class="btn btn-primary" id="submit_request_callback">Done</button>
                            </div>
                            <div class="succss_message_call display-hide">
                                We Will Call you soon..
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- JavaScript jQuery code from Bootply.com editor  -->
            <script type='text/javascript'>
            $(document).ready(function() {

               


            });
            </script>
            <div id="singlePropertyMap" class="mapcheck">
                 <?php get_template_part( 'property-details/lightbox' ); ?>
            </div>
            <?php get_footer('single-property'); ?> 
            <script>
            $('a[href*="#"]:not([href="#"])').click(function() {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top-90
                        }, 5000);
                        return false;
                    }
                }
            });
            </script>
            <script>
            $(".v_time").on("change", function() {
                var this_val = $(this).val();
                if (this_val == 'suitable_time') {
                    $(".suitable_time_block").slideDown("500");
                } else {
                    $(".suitable_time_block").slideUp("500");
                }
            });

            $(".submit_request_visit").on("click", function() {

                var form_vals = $("#site_visit_form").serialize();
                var action = $('#site_visit_form').find('input[name="action"]').val();
                $.ajax({
                    'url': HOUZEZ_ajaxcalls_vars.admin_url + "admin-ajax.php",
                    'data': {
                        'action': action,
                        'form_data': form_vals
                    },
                    'type': 'post',
                    'success': function(data) {
                        $(".request_visit_hide").hide();
                        $(".succss_message_visit").show();
                    }
                });
            });

            $("#submit_request_callback").on("click", function() {

                var form_vals = $("#callback_form").serialize();
                var action = $('#callback_form').find('input[name="action"]').val();
                $.ajax({
                    'url': HOUZEZ_ajaxcalls_vars.admin_url + "admin-ajax.php",
                    'data': {
                        'action': action,
                        'form_data': form_vals
                    },
                    'type': 'post',
                    'success': function(data) {
                        $(".request_callback_hide").hide();
                        $(".succss_message_call").show();
                    }
                });
            });
            </script>
<!-- <script type="text/javascript">
    
    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var div_top = $('.detail-top-grid').offset().top;
        if (window_top > div_top) {
            $('#sticky').addClass('stick');
            $('.detail-top-grid').height($('#sticky').outerHeight());
        } else {
            $('#sticky').removeClass('stick');
            $('.detail-top-grid').height(0);
        }
    }

    $(function() {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });
</script>
 -->

 <script type="text/javascript">
     
     $(window).scroll(function(){

        var window_top = $(window).scrollTop();
        var div_top = $('#description').offset().top;

        //var maxTop = $("#footer").offset().top - elem.height();
        if(window_top >= div_top){
          // if (window_top > div_top) {
          /*if(window_top + $(window).height() < $(document).height() - $("#footer-section").height()) {
              $('#sticky').addClass('fixed');
          } else {
              $('#sticky').removeClass('fixed');
          }*/
		  $('#sticky').addClass('fixed');
        }

        else{
            $('#sticky').removeClass('fixed');
        }

        

      });

      /*var stickySidebar = $('#sticky');

        if (stickySidebar.length > 0) { 
          var stickyHeight = stickySidebar.height(),
              sidebarTop = stickySidebar.offset().top;
        }

        // on scroll move the sidebar
        $(window).scroll(function () {
          if (stickySidebar.length > 0) {   
            var scrollTop = $(window).scrollTop();
                    
            if (sidebarTop < scrollTop) {
              stickySidebar.css('top', scrollTop - sidebarTop);

              // stop the sticky sidebar at the footer to avoid overlapping
              var sidebarBottom = stickySidebar.offset().top + stickyHeight,
                  stickyStop = $('#description').offset().top + $('#description').height();
              if (stickyStop < sidebarBottom) {
                var stopPosition = $('#description').height() - stickyHeight;
                stickySidebar.css('top', stopPosition);
              }
            }
            else {
              stickySidebar.css('top', '0');
            } 
          }
        });

        $(window).resize(function () {
          if (stickySidebar.length > 0) {   
            stickyHeight = stickySidebar.height();
          }
        });*/


 </script>
<style type="text/css">
    /*#sticky.stick {
        margin-top: 0 !important;
        position: fixed;
        top: 0;
        z-index: 10000;
        border-radius: 0 0 0.5em 0.5em;
    }*/

   

</style>
