$(document).ready(function(){       
    $("#loanAmount").on("slide", function (e, ui) {        
        var P = ui.value;
        var R = $("#interest_rate").slider("value");            
        R = R/100;

        var display_interest_rate = $("#interest_rate").slider("value");

        var T = $("#tenor").slider("value");
        T = T * 12;

        var cal1 = Math.pow( (1+R/12), T);
        var cal2 = Math.pow( (1+R/12), T)-1;
  
        var calculate = (P * R/12) * ( cal1) / ( cal2);

        var final_val = Math.round(calculate);

        final_val=final_val.toString();
        var lastThree = final_val.substring(final_val.length-3);
        var otherNumbers = final_val.substring(0,final_val.length-3);
        if(otherNumbers != '')
            lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        $("#updateAmount > span").text(res);
        // $("#updateAmount > span").text(final_val);
        $("#updatePercentage > span").text(display_interest_rate);
        
    });

    $("#interest_rate").on("slide", function (e, ui) {
        
        var R = ui.value; 
        R = R/100;

        var display_interest_rate = ui.value;

        var P = $("#loanAmount").slider("value");            
        
        var T = $("#tenor").slider("value");
        T = T * 12;

        var cal1 = Math.pow( (1+R/12), T);
        var cal2 = Math.pow( (1+R/12), T)-1;
  
        var calculate = (P * R/12) * ( cal1) / ( cal2);

        var final_val = Math.round(calculate);

        final_val=final_val.toString();
        var lastThree = final_val.substring(final_val.length-3);
        var otherNumbers = final_val.substring(0,final_val.length-3);
        if(otherNumbers != '')
            lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        $("#updateAmount > span").text(res);
        // $("#updateAmount > span").text(final_val);
        $("#updatePercentage > span").text(display_interest_rate);
        
    });

    $("#tenor").on("slide", function (e, ui) {
        
        var T = ui.value; 
        T = T * 12;
        var R = $("#interest_rate").slider("value");            
        var display_interest_rate = $("#interest_rate").slider("value");            
        R = R/100;

        var P = $("#loanAmount").slider("value");            

        var cal1 = Math.pow( (1+R/12), T);
        var cal2 = Math.pow( (1+R/12), T)-1;
  
        var calculate = (P * R/12) * ( cal1) / ( cal2);

        var final_val = Math.round(calculate);

        final_val=final_val.toString();
        var lastThree = final_val.substring(final_val.length-3);
        var otherNumbers = final_val.substring(0,final_val.length-3);
        if(otherNumbers != '')
            lastThree = ',' + lastThree;
        var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

        $("#updateAmount > span").text(res);
        // $("#updateAmount > span").text(final_val);
        $("#updatePercentage > span").text(display_interest_rate);
        
    });

});

$(document).ready(function(){  

    var P = $("#loanAmountCount").val();

    /*var P_val = P;

    P_val=P_val.toString();
    var lastThree = P_val.substring(P_val.length-3);
    var otherNumbers = P_val.substring(0,P_val.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    var P_res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

    $("#loanAmountCount").val(P_res);*/

    var R = $("#interest_rate_count").val();            
    R = R/100;

    var display_interest_rate = $("#interest_rate_count").val();

    var T = $("#tenorCount").val();
    T = T * 12;

    var cal1 = Math.pow( (1+R/12), T);
    var cal2 = Math.pow( (1+R/12), T)-1;

    var calculate = (P * R/12) * ( cal1) / ( cal2);

    var final_val = Math.round(calculate);

    final_val=final_val.toString();
    var lastThree = final_val.substring(final_val.length-3);
    var otherNumbers = final_val.substring(0,final_val.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

    $("#updateAmount > span").text(res);
    // $("#updateAmount > span").text(final_val);
    $("#updatePercentage > span").text(display_interest_rate);   

    $( "#loanAmountCount" ).keyup(function() {            
            
        var P = $(this).val();
        if(P <= 10000000){
            var R = $("#interest_rate_count").val();            
            R = R/100;

            var display_interest_rate = $("#interest_rate_count").val();

            var T = $("#tenorCount").val();
            T = T * 12;

            var cal1 = Math.pow( (1+R/12), T);
            var cal2 = Math.pow( (1+R/12), T)-1;
      
            var calculate = (P * R/12) * ( cal1) / ( cal2);

            var final_val = Math.round(calculate);

            final_val=final_val.toString();
            var lastThree = final_val.substring(final_val.length-3);
            var otherNumbers = final_val.substring(0,final_val.length-3);
            if(otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            $("#updateAmount > span").text(res);
            // $("#updateAmount > span").text(final_val);
            $("#updatePercentage > span").text(display_interest_rate);  
        }     

        else{
            alert('Please enter amount upto 10000000');
            $("#loanAmountCount").val("10000000");
        }      
        
    });


    $( "#interest_rate_count" ).keyup(function() {
            
        var R = $(this).val();  

        var R = $(this).val(); 

        if(R == 0 || R == ''){
            alert('Please enter some value');
            $("#interest_rate_count").val('8.5');
        } 

        else{
            if(R > 50){
                alert('Please enter value upto 50');
                $("#interest_rate_count").val('50');

                R = 50; 
            }  
            R = R/100;

            var display_interest_rate = $(this).val();

            var P = $("#loanAmountCount").val(); 

            // var P_new=P.replace(/,/g,'');
            P=P.replace(/,/g,'');



            //console.log(P);

            var T = $("#tenorCount").val();
            T = T * 12;

            var cal1 = Math.pow( (1+R/12), T);
            var cal2 = Math.pow( (1+R/12), T)-1;
      
            var calculate = (P * R/12) * ( cal1) / ( cal2);

            var final_val = Math.round(calculate);

            final_val=final_val.toString();
            var lastThree = final_val.substring(final_val.length-3);
            var otherNumbers = final_val.substring(0,final_val.length-3);
            if(otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            $("#updateAmount > span").text(res);
            // $("#updateAmount > span").text(final_val);
            $("#updatePercentage > span").text(display_interest_rate);
        }
        
    });

    $( "#tenorCount" ).keyup(function() {
            
        var T = $(this).val(); 

        if(T == 0 || T == ''){
            alert('Please enter some value');
            $("#tenorCount").val('6');
        } 


        else{

            if(T > 40){
                alert('Please enter value upto 40');
                $("#tenorCount").val('40');

                T = 40; 
            }  
            T = T * 12;

            var R = $("#interest_rate_count").val();
            R = R/100;

            var display_interest_rate = $("#interest_rate_count").val();

            var P = $("#loanAmountCount").val(); 

            P=P.replace(/,/g,'');

            var cal1 = Math.pow( (1+R/12), T);
            var cal2 = Math.pow( (1+R/12), T)-1;
      
            var calculate = (P * R/12) * ( cal1) / ( cal2);

            var final_val = Math.round(calculate);

            final_val=final_val.toString();
            var lastThree = final_val.substring(final_val.length-3);
            var otherNumbers = final_val.substring(0,final_val.length-3);
            if(otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            $("#updateAmount > span").text(res);
            // $("#updateAmount > span").text(final_val);
            $("#updatePercentage > span").text(display_interest_rate);
        }
        
    });

});

function UpdateLoanSlider(PhaseInStatusReportVal) {        
    $("#loanAmount").slider("value", PhaseInStatusReportVal);
}

$(function(){
    $('#loanAmountCount').keyup(function(){
        UpdateLoanSlider(this.value);
    });
});

function UpdateInterestSlider(PhaseInStatusReportVal) {        
    $("#interest_rate").slider("value", PhaseInStatusReportVal);
}

$(function(){
    $('#interest_rate_count').keyup(function(){
        UpdateInterestSlider(this.value);
    });
});

function UpdateTenureSlider(PhaseInStatusReportVal) {        
    $("#tenor").slider("value", PhaseInStatusReportVal);
}

$(function(){
    $('#tenorCount').keyup(function(){
        UpdateTenureSlider(this.value);
    });
});