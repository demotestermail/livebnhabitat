<?php
global $post, $property_map, $property_streetView, $prop_agent_email;
$size                    = 'houzez-property-detail-gallery';
$properties_images       = rwmb_meta('fave_property_images', 'type=plupload_image&size=' . $size, $post->ID);

$gallery_view            = $map_view = $street_view = '';
$prop_default_active_tab = houzez_option('prop_default_active_tab');

$price_prop=numDifferentiation(get_post_meta( get_the_ID(), 'fave_property_price', true ));
$addr_prop=get_post_meta( get_the_ID(), 'fave_property_map_address', true );

if (!empty($properties_images)) {
?>

<div class="top_slier_info">
    <ul id="vertical">
        <?php
    if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
         echo ' <li data-thumb="' . esc_url(get_the_post_thumbnail_url()) . '">
                        <img src="' . esc_url(get_the_post_thumbnail_url($post->ID, 'full')) . '" /> 
                    </li>';
    }
    foreach ($properties_images as $prop_image_id => $prop_image_meta) {

        $sitePlanId = get_post_meta( get_the_ID(), 'sitePlanId', true );
        $masterPlanId = get_post_meta( get_the_ID(), 'masterPlanId', true );

        $sitePlanImage = wp_get_attachment_url( $sitePlanId );
        $masterPlanImage = wp_get_attachment_url( $masterPlanId );

        $image_thumb_url = wp_get_attachment_image_src($prop_image_id, $size = 'full', true);

        if($prop_image_meta['full_url'] != $sitePlanImage && $prop_image_meta['full_url'] != $masterPlanImage )
        {
            echo ' <li data-thumb="' . esc_url($image_thumb_url[0]) . '">
                <img src="' . esc_url($prop_image_meta['url']) . '" /> 
            </li>';
        }
        
        
        
    } 
?>
    </ul>
    <?php
}
?>
<div class="prop_desc_slider">
    <span>Price: <i class="fa fa-rupee"></i> <?=$price_prop?></span>
    <span><?=$addr_prop?></span>
   
</div>
</div>
