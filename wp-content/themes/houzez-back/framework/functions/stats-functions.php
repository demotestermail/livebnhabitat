<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 04/08/16
 * Time: 6:51 PM
 */

if( !function_exists('houzez_count_page_stats') ) {
    function houzez_count_page_stats( $prop_id ) {

        $total_views = intval( get_post_meta($prop_id, 'houzez_total_property_views', true) );

        if( $total_views != '' ) {
            $total_views++;
        } else {
            $total_views = 1;
        }
        update_post_meta( $prop_id, 'houzez_total_property_views', $total_views );

        $today = date('m-d-Y', time());
       //$today = date('m-d-Y', strtotime("-1 days"));
        $views_by_date = get_post_meta($prop_id, 'houzez_views_by_date', true);

        if( $views_by_date != '' || is_array($views_by_date) ) {
            if (!isset($views_by_date[$today])) {

                if (count($views_by_date) > 60) {
                    array_shift($views_by_date);
                }
                $views_by_date[$today] = 1;

            } else {
                $views_by_date[$today] = intval($views_by_date[$today]) + 1;
            }
        } else {
            $views_by_date = array();
            $views_by_date[$today] = 1;
        }

        update_post_meta($prop_id, 'houzez_views_by_date', $views_by_date);

    }
}



if( !function_exists('houzez_return_traffic_labels') ) {
    function houzez_return_traffic_labels( $prop_id ) {

        $record_days = houzez_option('houzez_stats_days');
        if( empty($record_days) ) {
            $record_days = 14;
        }

        $views_by_date = get_post_meta($prop_id, 'houzez_views_by_date', true);

        if (!is_array($views_by_date)) {
            $views_by_date = array();
        }
        $array_labels = array_keys($views_by_date);
        $array_labels = array_slice( $array_labels, -1 * $record_days, $record_days, false );

        return $array_labels;
    }
}


if( !function_exists('houzez_return_traffic_data') ) {
    function houzez_return_traffic_data($prop_id) {

        $record_days = houzez_option('houzez_stats_days');
        if( empty($record_days) ) {
            $record_days = 14;
        }

        $views_by_date = get_post_meta( $prop_id, 'houzez_views_by_date', true );
        if ( !is_array( $views_by_date ) ) {
            $views_by_date = array();
        }
        $array_values = array_values( $views_by_date );
        $array_values = array_slice( $array_values, -1 * $record_days, $record_days, false );

        return $array_values;
    }
}