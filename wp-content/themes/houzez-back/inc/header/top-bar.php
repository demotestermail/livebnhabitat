<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 16/06/16
 * Time: 1:32 AM
 */
$top_bar_left = houzez_option('top_bar_left');
$top_bar_right = houzez_option('top_bar_right');
$top_bar_mobile = houzez_option('top_bar_mobile');
$top_bar_width = houzez_option('top_bar_width');
$fave_main_menu_trans = get_post_meta( $post->ID, 'fave_main_menu_trans', true );
$splash_logo = houzez_option( 'custom_logo_splash', false, 'url' );
$custom_logo = houzez_option( 'custom_logo', false, 'url' );
$splash_logolink_type = houzez_option('splash-logolink-type');
$splash_logolink = houzez_option('splash-logolink');

if( empty( $custom_logo ) ) {
    $custom_logo = get_template_directory_uri() . '/images/logo/logo-houzez-white.png';
}
$nav_left_class = $nav_right_class = $top_bar_right_width = $top_bar_left_width = $top_has_nav = $hide_top_bar_mobile = $top_drop_downs_left = $top_drop_downs_right ='';

if( $top_bar_left == 'menu_bar' ) {
    $nav_left_class = 'top-nav-area';
    $top_has_nav = 'top-has-nav';

} elseif( $top_bar_right == 'menu_bar' ) {
    $nav_right_class = 'top-nav-area';
    $top_has_nav = 'top-has-nav';
}

if( $top_bar_left == 'none' ) {
    $top_bar_right_width = 'houzez-top-bar-full';
}

if( $top_bar_right == 'none' ) {
    $top_bar_left_width = 'houzez-top-bar-full';
}

if( $top_bar_mobile != 0 ) {
    $hide_top_bar_mobile = 'hide-top-bar-mobile';
}

if( $top_bar_left == 'houzez_switchers' ) {
    $top_drop_downs_left = 'top-drop-downs';
}
if( $top_bar_right == 'houzez_switchers' ) {
    $top_drop_downs_right = 'top-drop-downs';
}

?>

<?php
$splash_v1_dropdown = 'property_city';
$dropdown_title = esc_html__('All Areas', 'houzez');

$selected_city='';
if(isset($_COOKIE['selected_city'])){
    $selected_city=json_decode(stripslashes($_COOKIE['selected_city']),true);
    $selected_city=$selected_city['slug'];
}
$header_login = houzez_option('header_login');
if( $header_4_menu_align == 'nav-right' && $header_login != 'yes' ) {
    $menu_righ_no_user = 'menu-right-no-user';
}
?>


<div class="top-bar  top_header01 <?php echo esc_attr( $top_has_nav ).' '.esc_attr( $hide_top_bar_mobile );?>">
    <div class="container"> <!-- <?php echo esc_attr($top_bar_width);?>" -->
        <div class="row">
             

                <?php if( $top_bar_left != 'none' ) { ?>
                <div class="top-bar-left <?php echo esc_attr( $nav_left_class.' '.$top_bar_left_width ); ?>">
                    <?php if( $top_bar_left == 'social_icons' || $top_bar_left == 'contact_info' || $top_bar_left == 'contact_info_and_social_icons' || $top_bar_left == 'slogan' || $top_bar_left == 'houzez_switchers' ) { ?>
                        <div class="top-contact">
                            <ul class="<?php esc_attr_e($top_drop_downs_left);?>">
                                <?php
                                if( $top_bar_left == 'contact_info' ) {
                                    get_template_part( 'inc/header/top-bar-contact' );

                                } elseif ( $top_bar_left == 'social_icons' ) {
                                    get_template_part( 'inc/header/top-bar-social' );

                                } elseif ( $top_bar_left == 'contact_info_and_social_icons' ) {
                                    get_template_part( 'inc/header/top-bar-contact' );
                                    get_template_part( 'inc/header/top-bar-social' );

                                } elseif ( $top_bar_left == 'slogan' ) {
                                    get_template_part( 'inc/header/top-bar-slogan' );

                                } elseif ( $top_bar_left == 'houzez_switchers' ) {
                                    get_template_part( 'inc/header/top-bar-currency' );
                                    get_template_part( 'inc/header/top-bar-area' );
                                }
                                ?>
                            </ul>
                        </div>
                    <?php } //end top bar left social_icons || contact_info || contact_info_and_social_icons  ?>

                    <?php
                    if ( $top_bar_left == 'menu_bar' ) {
                        get_template_part( 'inc/header/top-bar-nav' );
                    } ?>
                </div>
                <?php } //end top bar left if ?>

                <?php if( $top_bar_right != 'none' ) { ?>
                <div class="top-bar-right top_header_bar <?php echo esc_attr( $nav_right_class.' '.$top_bar_right_width ); ?>">
              


<div class="logo logo-desktop">
                
    <a href="<?=site_url();?>">
                    <img src="<?php echo esc_url( $custom_logo ); ?>" alt="Common Idioms">
            </a>

</div> 

<div class="right_header">
<?php
/*if ( has_nav_menu( 'top-menu' ) ) :
    wp_nav_menu( array (
        'theme_location' => 'top-menu',
        'container' => 'div',
        'container_class' => 'navi top-nav',
        'menu_class' => '',
        'menu_id' => 'main-nav',
        'depth' => 4
    ));
endif;*/
?>  
  <div class="navi top-nav">
    <ul id="main-nav" class="">
      <?php 
                        $selected_city='Select City';
                            if(isset($_COOKIE['selected_city'])){
                                $selected_city=json_decode(stripslashes($_COOKIE['selected_city']),true);
                                $selected_city=$selected_city['slug'];
                            } 
                        ?>
        <li ><a href="#" id="city_selected_pro"><?= $selected_city;?></a>
           <ul class="top-selection-city">
                                <?php
                            $prop_city = get_terms (
                                array(
                                'property_city'  
                                ),
                                array(
                                'orderby' => 'name',
                                'order' => 'ASC',
                                'hide_empty' => false,
                                'parent' => 0
                                )
                            );
                            foreach ($prop_city as $city) {
                            $slug_city=$city->slug;
                            echo '<li id="'.$slug_city.'"><a href="#">'.$city->name.'</a></li>';
                            }
?>
                            </ul>
        </li>
    </ul>
</div>





<?php
if ( $top_bar_right == 'menu_bar' ) {
get_template_part( 'inc/header/top-bar-nav' );
} ?>


                    <?php if( $top_bar_right == 'social_icons' || $top_bar_right == 'contact_info' || $top_bar_right == 'contact_info_and_social_icons' || $top_bar_right == 'slogan' || $top_bar_right == 'houzez_switchers' ) { ?>
                    <div class="top-contact">

                        <ul class="<?php esc_attr_e($top_drop_downs_right);?>">
                            <?php
                            if( $top_bar_right == 'contact_info' ) {
                                get_template_part( 'inc/header/top-bar-contact' );

                            } elseif ( $top_bar_right == 'social_icons' ) {
                                get_template_part( 'inc/header/top-bar-social' );

                            } elseif ( $top_bar_right == 'contact_info_and_social_icons' ) {
                                get_template_part( 'inc/header/top-bar-contact' );
                                get_template_part( 'inc/header/top-bar-social' );

                            } elseif ( $top_bar_right == 'slogan' ) {
                                get_template_part( 'inc/header/top-bar-slogan' );

                            } elseif ( $top_bar_right == 'houzez_switchers' ) {
                                get_template_part( 'inc/header/top-bar-currency' );
                                get_template_part( 'inc/header/top-bar-area' );
                            }
                            ?>
                        </ul>
                    </div>
                    <?php } //end top bar right social_icons || contact_info || contact_info_and_social_icons  ?>

                  

                    <ul  class="icon_navi">
                    <li><a href="#"><img src="<?php bloginfo("template_url"); ?>/images/icon01.png"></a></li>
                    <li><a href="#"><img src="<?php bloginfo("template_url"); ?>/images/icon02.png"></a></li>
                    <li><a href="#"><img src="<?php bloginfo("template_url"); ?>/images/icon03.png"></a></li>
                    <li><a href="<?=site_url();?>/blog/"><img src="<?php bloginfo("template_url"); ?>/images/icon04.png"></a></li>
                     <li><a href="#"><img src="<?php bloginfo("template_url"); ?>/images/icon05.png"></a></li>
                </ul>


<?php if( class_exists('Houzez_login_register') ): ?>
            <?php if( houzez_option('header_login') != 'no' ): ?>
                 
                    <?php get_template_part('inc/header/login', 'nav'); ?>
               
            <?php endif; ?>
        <?php endif; ?> 


                </div>
                <?php } //end top bar right if ?>
</div> 
            
        </div>
    </div>
</div>

<div class="clearfix"></div>