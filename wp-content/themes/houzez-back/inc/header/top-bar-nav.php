<?php
$splash_v1_dropdown = 'property_city';
$dropdown_title = esc_html__('All Areas', 'houzez');

$selected_city='';
if(isset($_COOKIE['selected_city'])){
    $selected_city=json_decode(stripslashes($_COOKIE['selected_city']),true);
    $selected_city=$selected_city['slug'];
}
// Pages Menu
/*if ( has_nav_menu( 'top-menu' ) ) :
    wp_nav_menu( array (
        'theme_location' => 'top-menu',
        'container' => 'div',
        'container_class' => 'navi top-nav',
        'menu_class' => '',
        'menu_id' => 'main-nav',
        'depth' => 4
    ));
endif;*/
?>
<div class="navi top-nav">

    </div>
<div class="mobile-nav">
    <span class="nav-trigger"><i class="fa fa-navicon"></i></span>
    <div class="nav-dropdown top-nav-dropdown"></div>
</div>