<?php
/**
 * @package Houzez
 * @since Houzez 1.0
 */

get_header();
$sticky_sidebar = houzez_option('sticky_sidebar');
?>

<?php get_template_part( 'template-parts/page-title' ); ?>

<section class="section-detail-content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				  <div class="property-listing list-view">
                        <div class="row">
                            <div id="houzez_ajax_container">
                                <?php
                                global $wp_query, $paged;
                                if ( is_front_page()  ) {
                                    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
                                }

                                $latest_listing_args = array(
                                    'post_type' => 'property',
                                    'posts_per_page' => 10,//$posts_per_page,
                                    'paged' => $paged,
                                    'post_status' => 'publish'
                                );

                                //$latest_listing_args = apply_filters( 'houzez_search_parameters', $latest_listing_args );

                                $latest_listing_args = houzez_prop_sort ( $latest_listing_args );

                                $latest_posts = new WP_Query( $latest_listing_args );

                                if ( $latest_posts->have_posts() ) :
                                    while ( $latest_posts->have_posts() ) : $latest_posts->the_post();

                                        get_template_part('template-parts/property-for-listing');

                                    endwhile;
                                else:
                                    get_template_part('template-parts/property', 'none');
                                endif;
                                wp_reset_postdata();
                                ?>

                                <hr>
                                <!--start Pagination-->
                                <?php houzez_pagination( $latest_posts->max_num_pages, $range = 2 ); ?>
                                <!--start Pagination-->

                            </div>
                        </div>

                    </div>
			</div>
			<!-- <div class="col-lg-4 col-md-4 col-sm-6 col-md-offset-0 col-sm-offset-3 container-sidebar <?php if( $sticky_sidebar['default_sidebar'] != 0 ){ echo 'houzez_sticky'; }?>">
				<?php get_sidebar(); ?>
			</div> -->
		</div>
	</div>
</section>

<?php get_footer(); ?>