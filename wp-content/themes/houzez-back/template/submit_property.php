<?php
/**
 * Template Name: Submit Property
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 06/10/15
 * Time: 3:49 PM
 */
if ( !is_user_logged_in() ) {
    wp_redirect( home_url('url') );
}
set_time_limit (600);

global $current_user, $properties_page, $hide_add_prop_fields, $required_fields;

wp_get_current_user();
$userID = $current_user->ID;
$invalid_nonce = false;
$submitted_successfully = false;
$updated_successfully = false;
$dashboard_listings = houzez_dashboard_listings();
$hide_add_prop_fields = houzez_option('hide_add_prop_fields');
$required_fields = houzez_option('required_fields');

if( isset( $_POST['action'] ) ) {

    if (wp_verify_nonce($_POST['property_nonce'], 'submit_property')) {

        $new_property = array(
            'post_type'	=> 'property'
        );

        apply_filters( 'houzez_submit_listing', $new_property );

        if ( !empty( $dashboard_listings ) ) {
            $separator = ( parse_url( $dashboard_listings, PHP_URL_QUERY ) == NULL ) ? '?' : '&';
            $parameter = ( $updated_successfully ) ? 'property-updated=true' : 'property-added=true';
            wp_redirect( $dashboard_listings . $separator . $parameter );
        }

    }// end verify nonce
}


get_header(); ?>

<?php get_template_part( 'template-parts/dashboard-title'); ?>

<div class="row">
    <div class="col-md-3 col-sm-4 user-dashboard-left">
        <?php get_template_part( 'template-parts/dashboard', 'sidebar' ); ?>
    </div>
    <div class="col-md-9 col-sm-8 user-dashboard-right">
        <?php
        if (is_plugin_active('houzez-theme-functionality/houzez-theme-functionality.php')) {
            if (isset($_GET['edit_property']) && !empty($_GET['edit_property'])) {

                get_template_part('template-parts/property-edit');

            } else {

                get_template_part('template-parts/property-submit');

            } /* end of add/edit property*/
        } else {
            esc_html_e( 'Please install and activate Houzez theme functionality plugin', 'houzez' );
        }
        ?>
    </div>
</div>

<?php get_footer();?>
