<?php
/**
 * Template Name: Paypal Processor
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 03/02/16
 * Time: 1:23 AM
 */
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

$paid_submission_status    = houzez_option('enable_paid_submission');
$price_per_submission = houzez_option('price_listing_submission');
$price_featured_submission = houzez_option('price_featured_listing_submission');
$currency = houzez_option('currency_paid_submission');
$listings_admin_approved = houzez_option('listings_admin_approved');

global $current_user;
$userID = $current_user->ID;
wp_get_current_user();
$allowed_html   =   array();

if($paid_submission_status !='per_listing'){
    wp_die();
}

$is_paypal_live  =   houzez_option('paypal_api');
$host            =   'https://api.sandbox.paypal.com';

if( $is_paypal_live == 'live' ){
    $host = 'https://api.paypal.com';
}

$return_link            =   houzez_get_template_link('template/paypal-processor.php');
$clientId               =   houzez_option('paypal_client_id');
$clientSecret           =   houzez_option('paypal_client_secret_key');
$price_per_submission   =   floatval( $price_per_submission );
$price_per_submission   =   number_format($price_per_submission, 2, '.', '');
$submission_curency     =   esc_html( $currency );
$headers                =   'From: My Name <myname@example.com>' . "\r\n";


if ( isset($_GET['token']) && isset($_GET['PayerID']) ){
    $token    = wp_kses ( $_GET['token'], $allowed_html );
    $payerID  = wp_kses ( $_GET['PayerID'] ,$allowed_html);

    /* Get saved data in database during execution
     -----------------------------------------------*/
    $transfered_data = get_option('houzez_paypal_transfer');

    $payment_execute_url = $transfered_data[ $userID ]['payment_execute_url'];
    $token               = $transfered_data[ $userID ]['paypal_token'];
    $prop_id             = $transfered_data[ $userID ]['property_id'];
    $is_prop_featured    = $transfered_data[ $userID ]['is_prop_featured'];
    $is_prop_upgrade     = $transfered_data[ $userID ]['is_prop_upgrade'];

    $payment_execute = array(
        'payer_id' => $payerID
    );

    $json           = json_encode( $payment_execute );
    $json_response  = houzez_execute_paypal_request( $payment_execute_url, $json, $token );

    $transfered_data[$current_user->ID ]  =   array();
    update_option ('houzez_paypal_transfer',$transfered_data);

    //print_r($json_response);
    if( $json_response['state']=='approved' ) {

        $time = time();
        $date = date( 'Y-m-d H:i:s', $time );

        if( $is_prop_upgrade == 1 ) {

            update_post_meta( $prop_id, 'fave_featured', 1 );
            $invoiceID = houzez_add_invoice( 'Upgrade to Featured','one_time', $prop_id, $date, $userID, 0, 1, '' );

            houzez_email_to_admin('email_upgrade');
            update_post_meta( $invoiceID, 'invoice_payment_status', 1 );

        } else {

            update_post_meta( $prop_id, 'fave_payment_status', 'paid' );

            if( $listings_admin_approved != 'yes'  && $paid_submission_status == 'per_listing' ){
                $post = array(
                    'ID'            => $prop_id,
                    'post_status'   => 'publish'
                );
                $post_id =  wp_update_post($post );
            }  else {
                $post = array(
                    'ID'            => $prop_id,
                    'post_status'   => 'pending'
                );
                $post_id =  wp_update_post($post );
            }

            if( $is_prop_featured == 1 ) {
                update_post_meta( $prop_id, 'fave_featured', 1 );
                $invoiceID = houzez_add_invoice( 'Listing with Featured','one_time', $prop_id, $date, $userID, 1, 0, '' );
            } else {
                $invoiceID = houzez_add_invoice( 'Listing','one_time', $prop_id, $date, $userID, 0, 0, '' );
            }

            update_post_meta( $invoiceID, 'invoice_payment_status', 1 );

            // send email to admin
            houzez_email_to_admin('simple');
        }

    }


    $redirect = houzez_dashboard_listings();
    wp_redirect($redirect);

}
$token = '';

/* -----------------------------------------------------------------------------------------------------------
*  Process Messages from Paypal IPN
-------------------------------------------------------------------------------------------------------------*/
define('DEBUG',0);

$headers = 'From: noreply  <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n".
    'Reply-To: noreply@'.$_SERVER['HTTP_HOST']. "\r\n" .
    'X-Mailer: PHP/' . phpversion();

$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$houzezPost = array();

foreach ($raw_post_array as $keyval) {
    $keyval = explode ('=', $keyval);
    if (count($keyval) == 2)
        $houzezPost[$keyval[0]] = urldecode($keyval[1]);
}


/*
 * Read the post from PayPal system and add 'cmd'
 * ----------------------------------------------------*/
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
    $get_magic_quotes_exists = true;
}

foreach ( $houzezPost as $key => $value ) {
    if( $get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1 ) {
        $value = urlencode(stripslashes($value));
    } else {
        $value = urlencode($value);
    }
    $req .= "&$key=$value";
}


/*
 * Step 2: POST IPN data back to PayPal to validate
 * ----------------------------------------------------*/
$paypal_api_status  =   esc_html( houzez_option('paypal_api') );
if( $paypal_api_status == 'live' ) {
    $paypal_url = "https://www.paypal.com/cgi-bin/webscr";
} else {
    $paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
}



$ch = curl_init($paypal_url);
if ($ch == FALSE) {
    return FALSE;
}

curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

if(DEBUG == true) {
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
}


/*
 * Set TCP timeout to 30 seconds
 * ----------------------------------------------------*/
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

$res = curl_exec($ch);

if ( curl_errno($ch) != 0 ) // cURL error
{
    if( DEBUG == true ) {
        //error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
    }
    curl_close($ch);
    exit;

} else {
    // Log the entire HTTP response if debug is switched on.
    if( DEBUG == true ) {
        // Split response headers and payload
        list($headers, $res) = explode("\r\n\r\n", $res, 2);
    }
    curl_close($ch);
}


/*
 * Inspect IPN validation result and act accordingly
 * ----------------------------------------------------*/
if (strcmp ($res, "VERIFIED") == 0) {

    $allowed_html          =  array();
    $payment_status        =  wp_kses ( esc_html( $_POST['payment_status'] ),$allowed_html );
    $txn_id                =  wp_kses ( esc_html ($_POST['txn_id']),$allowed_html );
    $txn_type              =  wp_kses ( esc_html($_POST['txn_type']),$allowed_html );
    $paypal_receiving_email =  esc_html( houzez_option('paypal_receiving_email') );
    $receiver_email        =  wp_kses ( esc_html($_POST['receiver_email']),$allowed_html );
    $payer_id              =  wp_kses ( esc_html($_POST['payer_id']),$allowed_html );

    $payer_email           =  wp_kses ( esc_html($_POST['payer_email']) ,$allowed_html);
    $amount                =  wp_kses ( esc_html($_POST['amount']),$allowed_html );
    $recurring_payment_id  =  wp_kses ( esc_html($_POST['recurring_payment_id']),$allowed_html );

    $user_id               =  houzez_retrive_user_by_profile( $recurring_payment_id );
    $pack_id               =  get_user_meta( $user_id, 'package_id',true );
    $price                 =  get_post_meta( $pack_id, 'fave_package_price', true );


    $houzez_mail = '';
    foreach ( $_POST as $key => $value ) {
        $key          = sanitize_key( $key );
        $value        = wp_kses(esc_html( $value), $allowed_html );
        $houzez_mail .= '['.$key.']='.$value.'</br>';
    }


    if( $payment_status == 'Completed' ) {

        if( $receiver_email != $paypal_rec_email) {
            exit();
        }

        // Payment already processd
        if( houzez_retrive_invoice_by_taxid( $txn_id ) ) {
            exit();
        }

        // No User Exist
        if( $user_id == 0 ) {
            exit();
        }

        // Received payment diffrent than pack value
        if( $amount != $price){
            exit();
        }

        houzez_update_membership( $user_id, $pack_id, 'recurring', $txn_id );
        $args  =array(
            'recurring_package_name' => get_the_title($pack_id),
            'merchant'               => 'Paypal'
        );
        houzez_email_type( $receiver_email, 'recurring_payment', $args );


    } else {
        // payment not completed
        if( $txn_type != 'recurring_payment_profile_created' ) {
            houzez_downgrade_to_free($user_id);
        }
    }

} else if ( strcmp ($res, "INVALID") == 0 ) {
    exit('invalid');
}

/* -----------------------------------------------------------------------------------------------------------
*  Get user by paypal recurring profile
-------------------------------------------------------------------------------------------------------------*/
function houzez_retrive_user_by_profile ( $recurring_payment_id ) {
    if($recurring_payment_id!=''){
        $recurring_payment_id=  str_replace('-', 'xxx', $recurring_payment_id);
        $arg = array(
            'role'         => 'subscriber',
            'meta_key'     => 'fave_paypal_profile',
            'meta_value'   => $recurring_payment_id,
            'meta_compare' => '='
        );

        $userid = 0;
        $houzezusers = get_users( $arg );
        foreach ( $houzezusers as $user ) {
            $userid = $user->ID;
        }
        return $userid;
    }else{
        return 0;
    }
}

/* -----------------------------------------------------------------------------------------------------------
*  Invoice by tax id
-------------------------------------------------------------------------------------------------------------*/
function houzez_retrive_invoice_by_taxid( $tax_id ) {
    $args = array(
        'post_type' => 'houzez_invoice',
        'meta_query' => array(
            array(
                'key' => 'HOUZEZ_paypal_txn_id',
                'value' => $tax_id,
                'compare' => '='
            )
        )
    );

    $query = new WP_Query( $args );

    if( $query->have_posts() ){
        return true;
    }else{
        return false;
    }

}