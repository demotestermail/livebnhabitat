<?php
/**
 * Template Name: User Dashboard Profile Page
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 02/10/15
 * Time: 4:22 PM
 */

global $current_user;
wp_get_current_user();
$userID = $current_user->ID;
$dash_profile_link = houzez_get_dashboard_profile_link();

/*-----------------------------------------------------------------------------------*/
// Social Logins
/*-----------------------------------------------------------------------------------*/
if( ( isset($_GET['code']) && isset($_GET['state']) ) ){
    houzez_facebook_login($_GET);

} else if( isset( $_GET['openid_mode']) && $_GET['openid_mode'] == 'id_res' ) {
    houzez_openid_login($_GET);

} else if (isset($_GET['code'])){
    houzez_google_oauth_login($_GET);

} else {
    if ( !is_user_logged_in() ) {
        wp_redirect(  home_url() );
    }
}

/*-----------------------------------------------------------------------------------*/
// Paypal payments for membeship packages
/*-----------------------------------------------------------------------------------*/
if (isset($_GET['token']) ){
    $allowed_html   =   array();
    $token               =   wp_kses ( $_GET['token'] ,$allowed_html);
    $token_recursive     =   wp_kses ( $_GET['token'] ,$allowed_html);
         
    // get transfer data
    $save_data              =   get_option('houzez_paypal_package_transfer');
    $payment_execute_url    =   $save_data[$current_user->ID ]['payment_execute_url'];
    $token                  =   $save_data[$current_user->ID ]['paypal_token'];
    $pack_id                =   $save_data[$current_user->ID ]['pack_id'];
    $recursive              =   0;
    if (isset ( $save_data[$current_user->ID ]['recursive']) ){
        $recursive  = $save_data[ $current_user->ID ]['recursive']; 
    }

    if( $recursive != 1 ) {
        if( isset($_GET['PayerID']) ){
            $payerId  =  wp_kses ( $_GET['PayerID'],$allowed_html );  

            $payment_execute = array(
               'payer_id' => $payerId
              );
            $json = json_encode($payment_execute);
            $json_resp = houzez_execute_paypal_request( $payment_execute_url, $json, $token );

            $save_data[ $current_user->ID ] = array();
            update_option ('houzez_paypal_package_transfer',$save_data); 

            if( $json_resp['state']=='approved' ) {

                 if( houzez_check_package_downgrade_status( $userID, $pack_id ) ) {
                    houzez_downgrade_to_pack( $userID, $pack_id );
                    houzez_update_membership( $userID, $pack_id, 'one_time', '' );
                 } else {
                    houzez_update_membership( $userID, $pack_id, 'one_time', '' );
                 }
                 wp_redirect( $dash_profile_link ); 
            }
        } //end if Get
    } else {

        require( get_template_directory() . '/framework/paypal-recurring/class.paypal.recurring.php' );

        $billingPeriod = get_post_meta( $pack_id, 'fave_billing_time_unit', true );
        $billingFreq   = intval( get_post_meta( $pack_id, 'fave_billing_unit', true ) );
        $packPrice     = get_post_meta( $pack_id, 'fave_package_price', true );
        $packName      = get_the_title($pack_id);
        $environment   = houzez_option('paypal_api');

        $paypal_api_username  = houzez_option('paypal_api_username');
        $paypal_api_password  = houzez_option('paypal_api_password');
        $paypal_api_signature = houzez_option('paypal_api_signature');
        $submission_curency   = houzez_option('currency_paid_submission');

        $date                 = $save_data[$current_user->ID ]['date'];

        $obj = new houzez_paypal_recurring;

        $obj->environment   = esc_html( $environment );
        $obj->paymentType   = urlencode('Sale');          // or 'Sale' or 'Order'
        $obj->API_UserName  = urlencode( $paypal_api_username );
        $obj->API_Password  = urlencode( $paypal_api_password );
        $obj->API_Signature = urlencode( $paypal_api_signature );
        $obj->API_Endpoint  = "https://api-3t.paypal.com/nvp";
        $obj->paymentType   = urlencode('Sale');
        $obj->returnURL     = urlencode($dash_profile_link);
        $obj->cancelURL     = urlencode($dash_profile_link);
        $obj->paymentAmount = $packPrice;
        $obj->currencyID    = $submission_curency;
        $obj->startDate     = urlencode($date);
        $obj->billingPeriod = urlencode($billingPeriod);
        $obj->billingFreq   = urlencode($billingFreq);
        $obj->productdesc   = urlencode($packName.__(' package on ','houzez').get_bloginfo('name') );
        $obj->user_id       = $current_user->ID;
        $obj->pack_id       = $pack_id;

        if ( $obj->getExpressCheckout($token_recursive) ) {

            if( houzez_check_package_downgrade_status( $current_user->ID, $pack_id ) ) {
                houzez_downgrade_to_pack( $current_user->ID, $pack_id );
                houzez_update_membership( $current_user->ID, $pack_id, 'recurring', '' );
            }else{
                houzez_update_membership( $current_user->ID, $pack_id, 'recurring', '' );
            }
            wp_redirect( $dash_profile_link );
            exit;
        }
        
    }
                             
}

?>

<?php get_header(); ?>

<?php get_template_part( 'template-parts/dashboard-title'); ?>

<div class="row">
    <div class="col-md-3 col-sm-4 user-dashboard-left">
        <?php get_template_part( 'template-parts/dashboard', 'sidebar' ); ?>
    </div>
    <div class="col-md-9 col-sm-8 user-dashboard-right">
        <?php get_template_part('template-parts/user-profile'); ?>
    </div>
</div>

<?php get_footer(); ?>