<?php
/**
 * Template Name: Develpers Listing Template
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 16/12/15
 * Time: 3:27 PM
 */
get_header();
global $post, $listings_tabs, $fave_featured_listing, $current_page_template;
$listing_view = get_post_meta( $post->ID, 'fave_default_view', true );
$listings_tabs = get_post_meta( $post->ID, 'fave_listings_tabs', true );
$listings_tab_1 = get_post_meta( $post->ID, 'fave_listings_tab_1', true );
$listings_tab_2 = get_post_meta( $post->ID, 'fave_listings_tab_2', true );
$fave_featured_listing = get_post_meta( $post->ID, 'fave_featured_listing', true );
$fave_featured_prop_no = get_post_meta( $post->ID, 'fave_featured_prop_no', true );
$fave_prop_no = get_post_meta( $post->ID, 'fave_prop_no', true );
$sticky_sidebar = houzez_option('sticky_sidebar');

$listing_page_link = houzez_properties_listing_link();

if( $listing_view == 'grid_view' || $listing_view == 'grid_view_3_col' ) {
    $listing_view = 'grid-view';
} else {
    $listing_view = 'list-view';
}

if( isset($_GET['prop_featured']) && $_GET['prop_featured'] == 'no' ) {
    $fave_featured_listing = 'disable';
}
if( isset($_GET['tabs']) && $_GET['tabs'] == 'no' ) {
    $listings_tabs = 'disable';
}
$current_page_template = get_post_meta( $post->ID, '_wp_page_template', true );
?>
    <?php get_template_part('template-parts/properties-head'); ?>
    <?php
$developers = get_terms( array(
    'taxonomy' => 'developer',
    'hide_empty' => false,

) );

foreach ($developers as $developer) {
    $developer_id=$developer->term_id;
    $developer_name=$developer->name;
    $developer_description=$developer->description;
    $developer_logo = get_term_meta($developer->term_id,'wpcf-developer-logo',true);
    $developer_estableish_year = get_term_meta($developer->term_id,'wpcf-established-in',true);
    $developer_structure= get_term_meta($developer->term_id,'wpcf-structure',true);
    $developer_projects_count=$developer->count;
    $developer_link=get_term_link($developer_id);
    ?>
        <div class="profile-detail-block developers_list">
            <div class="developers_list_img"> <img src="<?=$developer_logo;?>"></div>
            <div class="media">
                <div class="media-body">
                    <div class="profile-description">
                        <!-- <h4 class="position">
                                    Title
                                </h4> -->
                        <table>
                            <tr>
                                <th>
                                    <?=$developer_name;?>
                                </th>
                                <th>
                                    <?php if($developer_estableish_year!='') { ?>
                                    <span><?php esc_html_e('Established in:', 'houzez'); ?> </span>
                                    <?php echo esc_attr( $developer_estableish_year ); ?>
                                    <?php } ?>
                                </th>
                            </tr>
                            <tr>
                            <?php if(!empty($developer_structure)) { ?>
                                <td><span><?php esc_html_e('Structure:', 'houzez'); ?> </span>
                                    <?php echo esc_attr( $developer_structure ); ?>
                                </td>
                            <?php } ?>    
                                <td><span><?php esc_html_e('Total Projects:', 'houzez'); ?> </span>
                                    <?php echo esc_attr( $developer_projects_count ); ?>
                                </td>
                            </tr>
                            <!-- <tr>
                                <td><span><?php esc_html_e('Upcoming Projects:', 'houzez'); ?> </span>
                                    <?php echo esc_attr( $developer_projects_count ); ?>
                                </td>
                                <td><span><?php esc_html_e($developer_name.' in Chandigarh', 'houzez'); ?> </span>
                                    <?php echo esc_attr( $developer_projects_count ); ?>
                                </td>
                            </tr> -->
                        </table>
                        <p>
                            <?php echo substr($developer_description,0,400); ?>
                            <!--  <a href="<?=$developer_link;?>" class="btn btn-primary btn-block hidden-xs">Read More</a>  -->
                            <a href="<?=$developer_link;?>" class="read_more">Know More.....</a>
                        </p>
                        <!-- <a href="#" class="btn btn-primary btn-block visible-xs">Read More</a> -->
                    </div>
                </div>
            </div>
        </div>
        <?php


}
?>
            <?php get_footer(); ?>
