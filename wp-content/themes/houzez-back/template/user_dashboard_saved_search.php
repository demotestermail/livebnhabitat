<?php
/**
 * Template Name: User Dashboard Saved Search
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 11/01/16
 * Time: 4:35 PM
 */
if ( !is_user_logged_in() ) {
    wp_redirect(  home_url() );
}

global $current_user;

wp_get_current_user();
$userID     = $current_user->ID;
$user_login = $current_user->user_login;
$fav_ids = 'houzez_favorites-'.$userID;
$fav_ids = get_option( $fav_ids );

get_header(); ?>

<?php get_template_part( 'template-parts/dashboard-title'); ?>

<div class="row">
    <div class="col-md-3 col-sm-4 user-dashboard-left">
        <?php get_template_part( 'template-parts/dashboard', 'sidebar' ); ?>
    </div>
    <div class="col-md-9 col-sm-8 user-dashboard-right">
        <div class="account-block">
            <div class="saved-search-list">
                <?php
                $args = array(
                    'post_type' => 'houzez_search',
                    'post_status' => 'any',
                    'posts_per_page' => -1,
                    'author' => $userID
                );

                $query = new WP_Query( $args );
                if( $query->have_posts() ): while( $query->have_posts() ): $query->the_post();
                    get_template_part( 'template-parts/search', 'list' );

                endwhile;

                else:
                    echo esc_html__("You don't have any saved search", "houzez");
                endif;
                ?>
            </div>
        </div>
    </div>
</div>



<?php get_footer(); ?>