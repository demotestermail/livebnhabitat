<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 16/12/15
 * Time: 6:21 PM
 */
global $post, $prop_images, $current_page_template;
$developer_logo ='';
$post_meta_data     = get_post_custom($post->ID);
$muti_units_tabs       = get_post_meta( get_the_ID(), 'fave_multi_units', true );
$prop_images        = get_post_meta( get_the_ID(), 'fave_property_images', false );
$prop_address       = get_post_meta( get_the_ID(), 'fave_property_map_address', true );
$prop_featured      = get_post_meta( get_the_ID(), 'fave_featured', true );
$prop_price_single      = get_post_meta( get_the_ID(), 'fave_property_price', true );
$price_prefix  = '&#8377;';
$agent_display_option = get_post_meta( get_the_ID(), 'fave_agent_display_option', true );
$prop_agent_display = get_post_meta( get_the_ID(), 'fave_agents', true );
$infobox_trigger = '';
$developers=wp_get_post_terms( get_the_ID(),'developer');
$dev_id=$developers[0]->term_id;
$dev_name=$developers[0]->name;
$dev_description=$developers[0]->description;
 if(count($developers)>0){
$developer_logo = get_term_meta($developers[0]->term_id,'wpcf-developer-logo',true);
}
$date_possession =get_post_meta( get_the_ID(),'wpcf-possession-date',true);
$prop_agent_num = $agent_num_call = $prop_agent = $prop_agent_link = '';
if( $prop_agent_display != '-1' && $agent_display_option == 'agent_info' ) {
    $prop_agent_id = get_post_meta( get_the_ID(), 'fave_agents', true );
    $prop_agent_num = get_post_meta( $prop_agent_id, 'fave_agent_mobile', true );
    $prop_agent_email = get_post_meta( $prop_agent_id, 'fave_agent_email', true );
    $agent_num_call = str_replace(array('(',')',' ','-'),'', $prop_agent_num);
    if( $prop_agent_id ) {
        $prop_agent = get_the_title( $prop_agent_id );
        $prop_agent_link = get_permalink($prop_agent_id);
    }
} elseif( $agent_display_option == 'author_info' ) {
    $prop_agent = get_the_author();
    $prop_agent_link = get_author_posts_url( get_the_author_meta( 'ID' ) );
    $prop_agent_num = get_the_author_meta( 'fave_author_mobile' );
    $agent_num_call = str_replace(array('(',')',' ','-'),'', $prop_agent_num);
    $prop_agent_email = get_the_author_meta( 'email' );
}
if( is_page_template( 'template/property-listings-map.php' ) ) { $infobox_trigger = 'infobox_trigger_none'; }
?>
    <div id="ID-<?php the_ID(); ?>" class="item-wrap <?php echo esc_attr( $infobox_trigger );?>">
        <div class="property-item table-list">
            <div class="table-cell">
                <div class="figure-block">
                    <figure class="item-thumb">
                        <?php get_template_part( 'template-parts/featured-property' ); ?>
                        <!-- <div class="label-wrap hide-on-list">
                            <?php
                            get_template_part('template-parts/listing', 'status' );
                        ?>
                        </div> -->
                        <!-- <div class="price hide-on-list">
                            <?php echo houzez_listing_price_v1(); ?>
                        </div> -->
                        <?php
                        if(count($developers)>0){
                        echo '<h5>A Project by '.$developers[0]->name.'</h5>';

}
                        ?>
                            <a class="hover-effect" href="<?php the_permalink() ?>">
                                <?php
                        if( has_post_thumbnail( $post->ID ) ) {
                            the_post_thumbnail( 'houzez-property-thumb-image' );
                        }else{
                            houzez_image_placeholder( 'houzez-property-thumb-image' );
                        }
                        ?>
                            </a>
                            <figcaption class="thumb-caption cap-actions clearfix">
                                <?php get_template_part( 'template-parts/share', 'favourite' ); ?>
                            </figcaption>
                    </figure>
                </div>
            </div>
            <div class="item-body table-cell">
                <div class="body-left table-cell">
                    <div class="info-row info-row_col">
                        <!-- <div class="label-wrap hide-on-grid">
                            <?php // get_template_part('template-parts/listing', 'status' ); ?>
                        </div> -->
                        <div class="row">
                            <div class="col-sm-9">
                                <table class="table" align="top">
                                    <tr>
                                        <td colspan="2">
                                            <?php
                                  echo '<h2><a href="'.esc_url( get_permalink() ).'">'. esc_attr( get_the_title() ). '</a></h2>';
                                  ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Residential Apartment</td>
                                        <td>
                                            <?php
                                  if( !empty( $prop_address )) {
                                        echo esc_attr( $prop_address );
                                    }
                                  ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Configuration</td>
                                        <td>
                                            <?php
                                        $bhk= get_post_meta( get_the_ID(), 'bhk', true );  
                                        if(!empty($bhk) && $bhk !=0){$bhk = $bhk." BHK"; } else{$bhk = ""; }
                                    ?>
                                                <?php echo $bhk;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sizes</td>
                                        <td>
                                            <?php echo houzez_listing_meta_v1(); ?>
                                            <?php if(houzez_listing_meta_max_v1()){
                                                echo houzez_listing_meta_max_v1(); 
                                                }?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Price Range <a data-container="body" class="hastooltip property_tool_t" data-toggle="popover" data-class="dynamic-class" data-placement="top" data-content='<div class="circle_col">
     
      <table>
     
      <tr>
      <th colspan="2">Unit &amp; Variant Summary</th>
      <th><!-- BSP Standard <i class="fa fa-rupee"></i> 2.280<br> BSP Furnished <i class="fa fa-rupee"></i> 3.280 --></th>
      </tr>
                <tr>
              <td><b>Variant</b></td>
              <td><b>Size in SQFT <i class="fa fa-sort-down"></i></b></td>
              <td><b>LUMSUM BSP </b></td> 
          </tr>
       
          
            <?php
            if(count($muti_units_tabs)>0){
              foreach ($muti_units_tabs as $flat_uni) {
                  echo ' <tr>
              <td>'.$flat_uni['fave_mu_title'].'</td>
              <td>'.$flat_uni['fave_mu_size'].' '.$flat_uni['fave_mu_size_postfix'].'</td>
              <td>'.numDifferentiation($flat_uni['fave_mu_price']).' onwards </td>
             
          </tr>';
              }

            }

             ?>
            
         <tr>
             <td colspan="3">Calculated Complete Price Details With Payment Plan. PLC. EDC. Club Charges. Service Tax Etc <span>Available Inside</span> </td> 
         </tr>
                    


      </table>
    
  </div>'><i class="fa fa-info-circle"></i></a>
                                        </td>
                                        <td>
                                            <?php
                                    if( has_term( 'for-rent', 'property_status', $post ) ) {
                                        if(!is_user_logged_in()){
                                                echo '&#x20b9; <a href="#" data-toggle="modal"  class="rent_list_page rent_list_page01" data-target="#pop-login"><i class="fa fa-user hidden-md hidden-lg"></i> <span class="hidden-sm hidden-xs">Register / Sign In  </span></a>';
                                        } else {
                                          echo  $price_prefix.' '.numDifferentiation($prop_price_single);  
                                        }

                                        }else{
                                            echo  $price_prefix.' '.numDifferentiation($prop_price_single);  
                                        }

                                     ?>
                                        </td>
                                    </tr>
                                    <!-- <tr>
                                <td>Type</td>
                                <td >
                                    <?php echo houzez_taxonomy_simple('property_type'); ?>
                                </td>
                            </tr> -->
                                    <?php if($date_possession!=''){ ?>
                                    <tr>
                                        <td>Date of Possession</td>
                                        <td>
                                            <?php echo date('M, Y',$date_possession);  ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td>
                                            <!--<sapn class="price_revising">Price Revising in DD/HH/MM<br>
inagugral Dsicount of 5% till 30th March 2016<br>
Construction starting in April 2016</sapn>-->
                                        </td>
                                        <td>
                                            <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-primary">
                                                <?php esc_html_e( 'FOR MORE DETAILS', 'houzez' ); ?> <i class="fa fa-angle-right fa-right"></i></a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-3">
                                <div class="info-row_col_right">
                                    <?php if($developer_logo!=''){ ?>
                                    <div class="propet_logos">
                                        <a href="#" data-toggle="modal" data-target="#developer-modal-<?=$dev_id?>"><img width="110px" src="<?=$developer_logo;?>" /></a>
                                        <!-- <a href="<?php echo site_url(); ?>/add-new-property/"><img src="<?php bloginfo("template_url"); ?>/images/logo_02.png" /></a> -->
                                    </div>
                                    <?php } ?>
                                    <!--<p  data-toggle="modal" data-target="#myModa2">4 Unit(s) Available in Resale<br>1 Unit(s) Available to Rent-Out</p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="info-row date hide-on-grid">
                        <?php if( !empty( $prop_agent ) ) { ?>
                        <p><i class="fa fa-user"></i>
                            <a href="<?php echo esc_url($prop_agent_link); ?>">
                                <?php echo esc_attr( $prop_agent ); ?>
                            </a> 
                        </p>
                        <?php } ?>
                        <p><i class="fa fa-calendar"></i>
                            <?php printf( __( '%s ago', 'houzez' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
                        </p>
                    </div> -->
                </div>
                <!-- <div class="table-list full-width hide-on-list">
                    <div class="cell">
                        <div class="info-row amenities">
                            <?php echo houzez_listing_meta_v1(); ?>
                            <p>
                                <?php echo houzez_taxonomy_simple('property_type'); ?>
                            </p>
                        </div>
                    </div>
                    <div class="cell">
                        <div class="phone">
                            <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-primary">
                                <?php esc_html_e( 'Details', 'houzez' ); ?> <i class="fa fa-angle-right fa-right"></i></a>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <!-- <div class="item-foot date hide-on-list">
            <div class="item-foot-left">
                <?php if( !empty( $prop_agent ) ) { ?>
                <p><i class="fa fa-user"></i>
                    <a href="<?php echo esc_url($prop_agent_link); ?>">
                        <?php echo esc_attr( $prop_agent ); ?> 
                    </a>
                </p>
                <?php } ?>
            </div>
            <!-- <div class="item-foot-right">
                <p><i class="fa fa-calendar"></i>
                    <?php printf( __( '%s ago', 'houzez' ), human_time_diff( get_the_time( 'U' ), current_time( 'timestamp' ) ) ); ?>
                </p>
            </div> 
        </div> -->
    </div>
    <!-- About Developer Modal -->
    <div class="modal fade modal01" id="developer-modal-<?=$dev_id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog01 modal-dialog019" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
                    <h4 class="modal-title modal-title000" id="myModalLabel">About <?=$dev_name;?> <img width="57px" src="<?=$developer_logo;?>" /></h4>
                </div>
                <div class="modal-body">
                    <p>
                        <?=$dev_description;?>
                    </p>
                    <a href="#" class="more_details">More Details</a> 
                </div>
            </div>
        </div>
    </div>
    <!-- / About Developer Modal -->
