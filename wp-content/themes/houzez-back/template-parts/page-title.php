<?php
global $status,$post,
       $search_template,
       $type, $location,
       $area,
       $measurement_unit_adv_search,
       $adv_search_price_slider,
       $hide_advanced,
       $keyword_field_placeholder,
       $adv_show_hide;
       ?>
<div class="page-title breadcrumb-top">
    <div class="row">
        <div class="col-sm-12">
            <?php get_template_part( 'inc/breadcrumb' ); ?>
            <div class="page-title-left">
                <h2>
                <?php
                    if (is_category()) {
                        single_cat_title();

                    }  
                    elseif(is_tag()) {
                        single_tag_title();

                    } elseif (is_day()) {
                        printf( esc_html__( '%s', 'houzez' ), get_the_date() );

                    } elseif (is_month()) {
                        printf( esc_html__( '%s', 'houzez' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'houzez' ) ) );

                    } elseif (is_year()) {
                        printf( esc_html__( '%s', 'houzez' ), get_the_date( _x( 'Y', 'yearly archives date format', 'houzez' ) ) );

                    } elseif ( get_post_format() ) {
                        echo get_post_format();

                    } elseif (is_author()) {
                        _e ( 'Author Archive', 'houzez' );
                    } elseif (is_archive()) {
                    }  else {
                        if( !is_front_page() ) {
                            if(is_page_template('template/template-search.php')) {
                                
                                /* Search Title For Keyword */
                                if(isset($_GET['keyword']) && $_GET['keyword']!='' ) {

                                 echo $_GET['keyword'];   
                                }
                                 /* Search Title For Only Location */
                                else if (isset($_GET['location']) && $_GET['location']!='') {
                                    $city_info=get_term_by('slug',$_GET['location'],'property_city');
                                    echo 'Properties in '.$city_info->name;   
                                }
                                 else if (isset($_GET['status']) && $_GET['status']!='') {
                                     $status_info=get_term_by('slug',$_GET['status'],'property_status');
                                      echo $status_info->name;   
                                }
                                
                                /* Title For Price range */
                                else if (isset($_GET['min-price']) && $_GET['min-price']!='' && isset($_GET['max-price']) && $_GET['max-price']!='') {
                                      echo 'Properties in Between Price '.$_GET['min-price'] .'-'.$_GET['max-price'] ;   
                                }

                                /* Title For Area range */
                                else if (isset($_GET['min-area']) && $_GET['min-area']!='' && isset($_GET['max-area']) && $_GET['max-area']!='') {
                                      echo 'Properties in Between Area '.$_GET['min-area'] .'-'.$_GET['max-area'] ;   
                                }
                                else{
                                    the_title();
                                }
                                
                            } 

                            else {
                            the_title();
                        }

                        }
                    }
                ?>
                </h2>
            </div>

            <?php if( is_page_template('template/template-search.php')) { ?>
            <div class="page-title-right">
                <!--<div class="view hidden-xs">
                    <div class="table-cell">
                        <span class="view-btn btn-list <?php if( $active == 'list-view' ) { echo 'active'; }?>"><i class="fa fa-th-list"></i></span>
                        <span class="view-btn btn-grid <?php if( $active == 'grid-view' ) { echo 'active'; }?>"><i class="fa fa-th-large"></i></span>
                    </div>
                </div> -->
            </div>
            <?php } ?>

        </div>
    </div>
</div>