<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 20/01/16
 * Time: 6:31 PM
 */
global $status,
       $search_template,
       $type, $location,
       $area,
       $measurement_unit_adv_search,
       $adv_search_price_slider,
       $hide_advanced,
       $keyword_field_placeholder,
       $adv_show_hide;
$keyword_field_placeholder="Locality/Area/Village/Keyword";
$search_width = houzez_option('search_width');
$search_sticky = houzez_option('main-search-sticky');
$search_style = houzez_option('search_style');
$main_menu_sticky = houzez_option('main-menu-sticky');
$features_limit = houzez_option('features_limit');

if( isset($_GET['search_style']) && $_GET['search_style'] == 'v1' ) {
    $search_style = 'style_1';
} else if( isset($_GET['search_style']) && $_GET['search_style'] == 'v2' ) {
    $search_style = 'style_2';

} else if( isset($_GET['search_style']) && $_GET['search_style'] == 'min1' ) {
    $search_style = 'style_1';
    $hide_advanced = true;

} else if( isset($_GET['search_style']) && $_GET['search_style'] == 'min2' ) {
    $search_style = 'style_2';
    $hide_advanced = true;
}

if( !is_404() && !is_search() ) {
    $adv_search_enable = get_post_meta($post->ID, 'fave_adv_search_enable', true);
    $adv_search = get_post_meta($post->ID, 'fave_adv_search', true);
}

$class = $sticky = '';

if( $main_menu_sticky != 1 ) {
    if ((!empty($adv_search_enable) && $adv_search_enable != 'global')) {
        if ($adv_search == 'hide_show') {
            $sticky = '1';
            $class = 'search-hidden';
        } else {
            $sticky = '0';
            $class = '';
        }
    } else {
        $sticky = $search_sticky;
    }
} else {
    $sticky = '0';
}

$selected_city='';
if(isset($_COOKIE['selected_city'])){
    $selected_city=json_decode(stripslashes($_COOKIE['selected_city']),true);
    $selected_city=$selected_city['name'];
}

/*if( $hide_advanced ) {
    $adv_col_class = 'col-sm-4';
    $location_select_class = '';
} else {
    $adv_col_class = 'col-sm-3';
    $location_select_class = 'location-select';
}*/

if( $adv_show_hide['cities'] != 1 && $adv_show_hide['areas'] != 1 && $hide_advanced == false ) {
    $col_1_classes = "col-md-6 col-sm-6";
    $col_2_classes = "col-md-6 col-sm-6";
    $adv_col_class = 'col-sm-3';
    $location_select_class = 'location-select';

} elseif( $adv_show_hide['cities'] != 1 && $adv_show_hide['areas'] != 1 && $hide_advanced == true ) {
    $col_1_classes = "col-md-6 col-sm-6";
    $col_2_classes = "col-md-6 col-sm-6";
    $adv_col_class = 'col-sm-4';
    $location_select_class = '';

} elseif( $adv_show_hide['cities'] != 0 && $adv_show_hide['areas'] != 0  && $hide_advanced == false ) {
    $col_1_classes = "col-md-8 col-sm-8";
    $col_2_classes = "col-md-4 col-sm-4";
    $adv_col_class = 'col-sm-6';

} elseif( $adv_show_hide['cities'] != 0 && $adv_show_hide['areas'] != 0  && $hide_advanced == true ) {
    $col_1_classes = "col-md-10 col-sm-10";
    $col_2_classes = "col-md-2 col-sm-2";
    $adv_col_class = 'col-sm-12';

} elseif( ( $adv_show_hide['cities'] != 0 || $adv_show_hide['areas'] != 0 ) && $hide_advanced != false ) {
    $col_1_classes = "col-md-8 col-sm-8";
    $col_2_classes = "col-md-4 col-sm-4";
    $adv_col_class = 'col-sm-6';

} elseif( ( $adv_show_hide['cities'] != 0 || $adv_show_hide['areas'] != 0 ) && $hide_advanced != true ) {
    $col_1_classes = "col-md-7 col-sm-7";
    $col_2_classes = "col-md-5 col-sm-5";
    $adv_col_class = 'col-sm-4';
}
$keyword_field = houzez_option('keyword_field');
 $search_template=site_url().'/advanced-search/';
/*if( $keyword_field == 'prop_geocomplete' && $hide_advanced != true ) {
    $col_1_classes = "col-md-8 col-sm-8";
    $col_2_classes = "col-md-4 col-sm-4";
    $adv_col_class = 'col-sm-4';
} elseif ( $keyword_field == 'prop_geocomplete' && $hide_advanced != false ) {
    $col_1_classes = "col-md-8 col-sm-8";
    $col_2_classes = "col-md-4 col-sm-4";
    $adv_col_class = 'col-sm-6';
}*/
?>

<!--start advanced search section-->
<div class="advanced-search advance-search-header houzez-adv-price-range home_advanced_sect <?php echo esc_attr( $class ); ?>" data-sticky='<?php echo esc_attr( $sticky ); ?>'>
    <div class="<?php echo esc_attr( $search_width ); ?>">
        <div class="row">
            <div class="col-sm-12">
                <form method="get" action="<?php echo esc_url( $search_template ); ?>">

                
                    <?php  if( $search_style == 'style_2' ) { ?>

                  <!--   <div class="form-group search-long">

                        <div class="search">
                            <div class="input-search input-icon">
                                <input class="houzez_geocomplete form-control" type="text" value="<?php echo isset ( $_GET['keyword'] ) ? $_GET['keyword'] : ''; ?>" name="keyword" placeholder="<?php echo $keyword_field_placeholder; ?>">
                            </div>
                            <input type="hidden" name="location" value="<?=$selected_city?>">

                            <?php if( $hide_advanced != true ) { ?>
                            <div class="advance-btn-holder">
                                <button class="advance-btn btn" type="button"><i class="fa fa-gear"></i> <?php esc_html_e( 'Advanced', 'houzez' ); ?></button>
                            </div>
                            <?php } ?> 

                        </div>
                        <div class="search-btn">
                            <button class="btn btn-orange"><?php esc_html_e( 'Go', 'houzez' ); ?></button>
                        </div>
                    </div> -->


                    <div class="search-bar-section_col">
<div class="dropdown">
<!-- <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
RESIDENTIAL
<span class="caret"></span>
</button> -->
<?php if( $adv_show_hide['type'] != 1 ) { ?>
                             
                                
                                    <select class="selectpicker" name="type" data-live-search="false" data-live-search-style="begins">
                                        <?php
                                        // All Option
                                        echo '<option value="">'.esc_html__( 'All Types', 'houzez' ).'</option>';

                                        $prop_type = get_terms (
                                            array(
                                                "property_type"
                                            ),
                                            array(
                                                'orderby' => 'name',
                                                'order' => 'ASC',
                                                'hide_empty' => false,
                                                'parent' => 0
                                            )
                                        );
                                        houzez_hirarchical_options('property_type', $prop_type, $type );
                                        ?>
                                    </select>
                                 
                            
                            <?php } ?>
<!-- <ul class="dropdown-menu" aria-labelledby="dLabel">
<li><a href="#"> Residential</a></li>
<li><a href="#"> Commercial</a></li>
<li><a href="#"> Industrial</a></li>
<li><a href="#"> Institutional</a></li> 
<li><a href="#"> Agricultural</a></li>
</ul> -->
</div>
<div class="search-col1">
   <!--  <input type="text" placeholder="Locality/Area/Village/Keyword" name=""> -->
    <input class="houzez_geocomplete home_search_input form-control" type="text" value="<?php echo isset ( $_GET['keyword'] ) ? $_GET['keyword'] : ''; ?>" name="keyword" placeholder="<?php echo $keyword_field_placeholder; ?>">

     <input type="hidden" name="location" value="<?=$selected_city?>">

    <input type="submit" name="" value="Go">  
     <!-- <button class="btn btn-orange"><?php esc_html_e( 'Go', 'houzez' ); ?></button>     --> 


<div class="ad_filter display-hide">
  <div class="range-advanced-main">
  <span>Budget</span>

                                            <div class="range-text">
                                                <input type="hidden" name="min-price" class="min-price-range-hidden range-input" readonly >
                                                <input type="hidden" name="max-price" class="max-price-range-hidden range-input" readonly >

                                                <p><!-- <span class="range-title"><?php echo esc_html_e('Price Range:','houzez');?></span>  -->

                                                <span class="min-price-range"><?php echo esc_html_e('from','houzez');?><?php echo esc_html_e('to','houzez');?></span>  <span class="max-price-range"></span>
                                                </p>
                                            </div>
                                            <div class="range-wrap">
                                                <div class="price-range-advanced"></div>
                                            </div>
                                        </div>
<div class="range-bedrooms-main">
   <?php if( $adv_show_hide['beds'] != 1 ) { ?>
                           <span>Bedrooms</span>
                              
                                    <select name="bedrooms" class="selectpicker" data-live-search="false" data-live-search-style="begins" title="">
                                        <option value=""><?php esc_html_e( 'Beds', 'houzez' ); ?></option>
                                        <?php houzez_number_list('bedrooms'); ?>
                                    </select> 
                                
                            
<?php } ?> 

</div>
<div class="range-area-main">
  <?php if( $adv_show_hide['min_area'] != 1 ) { ?>
   <span>Sizes</span>
                           <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="<?php echo isset ( $_GET['min-area'] ) ? $_GET['min-area'] : ''; ?>" name="min-area" placeholder="<?php esc_html_e( 'Min Area', 'houzez' ); ?>">
                                </div>
                            </div> 
                            <?php } ?>

                <?php if( $adv_show_hide['max_area'] != 1 ) { ?>
                            <div class="col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" value="<?php echo isset ( $_GET['max-area'] ) ? $_GET['max-area'] : ''; ?>" name="max-area" placeholder="<?php esc_html_e( 'Max Area', 'houzez' ); ?>">
                                </div>
                            </div> 
                <?php } ?>
         </div>                             
                                    

</div>


</div>


     </div> 


                    <?php } ?>

                    <div class="advance-fields" style="display: block;">
                        <div class="row">

                            <?php if( $adv_show_hide['status'] != 1 ) { ?>
                           <!-- <div class="col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <select class="selectpicker" id="selected_status" name="status" data-live-search="false" data-live-search-style="begins">
                                        <?php
                                        // All Option
                                        echo '<option value="">'.esc_html__( 'All Status', 'houzez' ).'</option>';

                                        $prop_status = get_terms (
                                            array(
                                                "property_status"
                                            ),
                                            array(
                                                'orderby' => 'name',
                                                'order' => 'ASC',
                                                'hide_empty' => false,
                                                'parent' => 0
                                            )
                                        );
                                        houzez_hirarchical_options('property_status', $prop_status, $status );
                                        ?>
                                    </select>
                                </div>
                            </div> -->
                            <?php } ?>

                            <!-- <?php if( $adv_show_hide['type'] != 1 ) { ?>
                            <div class="col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <select class="selectpicker" name="type" data-live-search="false" data-live-search-style="begins">
                                        <?php
                                        // All Option
                                        echo '<option value="">'.esc_html__( 'All Types', 'houzez' ).'</option>';

                                        $prop_type = get_terms (
                                            array(
                                                "property_type"
                                            ),
                                            array(
                                                'orderby' => 'name',
                                                'order' => 'ASC',
                                                'hide_empty' => false,
                                                'parent' => 0
                                            )
                                        );
                                        houzez_hirarchical_options('property_type', $prop_type, $type );
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php } ?> -->

                            <?php //if( $adv_show_hide['property_id'] != 1 ) { ?>
                                <!--<div class="col-sm-3 col-xs-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" value="<?php /*echo isset ( $_GET['propid'] ) ? $_GET['propid'] : ''; */?>" name="propid" placeholder="<?php /*esc_html_e( 'Property ID', 'houzez' ); */?>">
                                    </div>
                                </div>-->
                            <?php //} ?>

                            

                            <?php if( $adv_show_hide['baths'] != 1 ) { ?>
                            <!--<div class="col-sm-3 col-xs-6">
                                <div class="form-group">
                                    <select name="bathrooms" class="selectpicker" data-live-search="false" data-live-search-style="begins" title="">
                                        <option value=""><?php esc_html_e( 'Baths', 'houzez' ); ?></option>
                                        <?php houzez_number_list('bathrooms'); ?>
                                    </select>
                                </div>
                            </div>-->
                            <?php } ?>

                           

                            
                                <?php ?>
                                    
                             
                            

                            <?php if( $adv_show_hide['other_features'] != 1 ) { ?>
                            <!--<div class="col-sm-12 col-xs-12 features-list">

                                <label class="text-uppercase title"><?php esc_html_e( 'Other Features', 'houzez' ); ?></label>
                                <div class="clearfix"></div>
                                <?php

                                if( taxonomy_exists('property_feature') ) {
                                    $prop_features = get_terms(
                                        array(
                                            "property_feature"
                                        ),
                                        array(
                                            'orderby' => 'name',
                                            'order' => 'ASC',
                                            'hide_empty' => false,
                                            'parent' => 0
                                        )
                                    );
                                    $count = 0;
                                    if (!empty($prop_features)) {
                                        foreach ($prop_features as $feature):
                                            if( $features_limit != -1 ) {
                                                if ( $count == $features_limit ) break;
                                            }
                                            echo '<label class="checkbox-inline">';
                                            echo '<input name="feature[]" id="feature-' . esc_attr( $feature->slug ) . '" type="checkbox" value="' . esc_attr( $feature->slug ) . '">';
                                            echo esc_attr( $feature->name );
                                            echo '</label>';
                                        $count++;
                                        endforeach;
                                    }
                                }
                                ?>
                            </div>-->
                            <?php } ?>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>