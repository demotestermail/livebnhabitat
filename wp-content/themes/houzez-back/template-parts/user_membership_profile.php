<?php
global $current_user;      

wp_get_current_user();
$user_id                 = $current_user->ID;
$user_pack_id            = get_the_author_meta( 'package_id' , $user_id );
$user_package_activation = get_the_author_meta( 'package_activation' , $user_id );
$user_registered         = get_the_author_meta( 'user_registered' , $user_id );

$is_membership = 0;
$paid_submission_type = esc_html ( houzez_option('enable_paid_submission','') );
$membership_currency = houzez_option( 'currency_paid_submission' );
$where_currency = houzez_option( 'currency_position' );
$enable_wireTransfer = houzez_option('enable_wireTransfer');
$enable_paypal = houzez_option('enable_paypal');
$enable_stripe = houzez_option('enable_stripe');

if ( $paid_submission_type == 'membership' ) {
    houzez_get_user_current_package( $user_id, $user_pack_id, $user_registered, $user_package_activation );
    $is_membership = 1;             
}
?>

<?php if( $is_membership == 1 ) { ?>

<div class="my-widget widget-change">
    <div class="my-title"><?php esc_html_e( 'Change your membership', 'houzez' ); ?></div>
    <div class="my-widget-body">
        <div class="body-inner">
            <form>
                <div class="form-group">
                    <?php houzez_display_packages(); ?>
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="package_recuring" id="package_recuring" value="1" />
                            <?php esc_html_e( 'Set as recurring payment', 'houzez' ); ?>
                        </label>
                    </div>
                </div>
            </form>
            <?php if( $enable_paypal != 0 ) { ?>
                <div id="pick_package" class="btn btn-primary btn-block"><?php esc_html_e( 'PAY WITH PAYPAL', 'houzez' ); ?> <i class="fa fa-paypal"></i></div>
            <?php } ?>

            <?php
            if( $enable_stripe != 0 ) {
                houzez_show_stripe_form_membership();
            }
            ?>

            <?php if( $enable_wireTransfer != 0 ) { ?>
                <div id="direct_pay_package" class="btn btn-primary btn-block"><?php esc_html_e( 'PAY WITH WIRE TRANSFER', 'houzez' ); ?></div>
            <?php } ?>
        </div>
    </div>
</div>

<div class="my-widget widget-available">
    <div class="my-title"><?php esc_html_e( 'Available Membership', 'houzez' ); ?></div>
    <div class="my-widget-body">
        
        <?php
        $args = array(
            'post_type'       => 'houzez_packages',
            'posts_per_page'  => -1,
            'meta_query'      =>  array(
                    array(
                    'key' => 'fave_package_visible',
                    'value' => 'yes',
                    'compare' => '=',
                 )
              )
        );
        $fave_qry = new WP_Query($args);

        while( $fave_qry->have_posts() ): $fave_qry->the_post(); 

        $pack_price              = get_post_meta( get_the_ID(), 'fave_package_price', true );
        $pack_listings           = get_post_meta( get_the_ID(), 'fave_package_listings', true );
        $pack_featured_listings  = get_post_meta( get_the_ID(), 'fave_package_featured_listings', true );
        $pack_unlimited_listings = get_post_meta( get_the_ID(), 'fave_unlimited_listings', true );
        $pack_billing_period     = get_post_meta( get_the_ID(), 'fave_billing_time_unit', true );
        $pack_billing_frquency   = get_post_meta( get_the_ID(), 'fave_billing_unit', true );

        if( $pack_billing_frquency > 1 ) {
            $pack_billing_period .='s';
        }
        if ( $where_currency == 'before' ) {
            $price = $membership_currency . ' ' . $pack_price;
        } else {
            $price = $pack_price . ' ' . $membership_currency;
        }
        ?>

            <div class="body-inner">
                <h4 class="title-type"><?php the_title(); ?></h4>
                <p><strong><?php esc_html_e( 'Price:', 'houzez'); ?> </strong> <?php echo esc_attr( $price ); ?></p>
                <p><?php esc_html_e( 'Time Period:', 'houzez' ); ?> <?php echo esc_attr( $pack_billing_frquency ).' '.HOUZEZ_show_bill_period( $pack_billing_period ); ?></p>

                <?php if( $pack_unlimited_listings == 1 ) { ?>
                    <p><?php esc_html_e( 'Unlimited Listings', 'houzez' ); ?></p>
                <?php } else { ?>
                    <p><?php echo esc_attr( $pack_listings ); ?> <?php esc_html_e( 'Listings', 'houzez' ); ?></p>
                <?php } ?>

                <p><?php echo esc_attr( $pack_featured_listings ); ?> <?php esc_html_e( 'Featured Listings', 'houzez' ); ?></p>
            </div><!-- End body-inner -->

        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        
    </div>
</div>

<?php } ?>

<?php
function HOUZEZ_show_bill_period( $biling_period ){
  
    if( $biling_period == 'Day' ){
        return  esc_html__('day','houzez');
    }
    else if( $biling_period == 'Days' ){
       return  esc_html__('days','houzez');
    }
    else if( $biling_period == 'Week' ){
       return  esc_html__('week','houzez');
    }
    else if( $biling_period == 'Weeks'){
       return  esc_html__('weeks','houzez');
    }
    else if( $biling_period == 'Month' ){
        return  esc_html__('month','houzez');
    }
    else if( $biling_period == 'Months'){
        return  esc_html__('months','houzez');
    }
    else if( $biling_period=='Year'){
        return  esc_html__('year','houzez');
    }

}
?>