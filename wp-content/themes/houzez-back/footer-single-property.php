<?php $copy_rights = houzez_option('copy_rights'); ?>
<!--start footer section-->
<footer id="footer-section">
    <?php get_template_part('template-parts/footer'); ?>
</footer>
<!--end footer section-->
<?php wp_footer(); ?>
</body>
</html>