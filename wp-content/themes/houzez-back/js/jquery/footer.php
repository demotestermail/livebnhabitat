            <div class="row">
                <?php if( !empty($copy_rights) ) { ?>
                <div class="col-md-3 col-sm-3">
                    <div class="footer-col">
                        <p class="f-note"><?php echo ( $copy_rights ); ?>
                        <?php if( is_front_page() && !is_user_logged_in() ) { ?>
                        - <a title="<?php echo bloginfo();?>" href="<?php echo site_url(); ?>"><?php echo bloginfo();?></a>.
                        <?php } ?>
                        </p>
                    </div>
                </div>
                <?php } ?>