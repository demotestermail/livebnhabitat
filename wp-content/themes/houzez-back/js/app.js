//$('.banner').backstretch('images/banner2.png');
//$('.ownBusiness').backstretch('/assets/images/bg.png');

$(document).ready(function(){
						   
   //$('header').scrollToFixed();	
   
   $('.navIcon').click(function(){
		$('nav.menu').stop().slideToggle('fast');						
   });
   
   $('nav ul li').each(function(){
	$(this).click(function(){
		$(this).find('ul').stop().slideToggle('fast');
	});
   });
   
   $('.carousel').carousel({
    interval: 5000
   });
   
   $("#loanAmount").slider({
      range: "min",
      value: 25000,
      step: 100000,
      min: 100000,
      max: 10000000,
      slide: function(event, ui) {
        // $("#loanAmountCount").val(ui.value);
        //$("#loanAmountCount").val(ui.value);

        var final_val = ui.value;

            final_val=final_val.toString();
            var lastThree = final_val.substring(final_val.length-3);
            var otherNumbers = final_val.substring(0,final_val.length-3);
            if(otherNumbers != '')
                lastThree = ',' + lastThree;
            var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            $("#loanAmountCount").val(res);

      }
   });
   $("#loanAmountCount").val($("#loanAmount").slider("value"));
   
   $("#tenor").slider({
      range: "min",
      value: 6,
      step: 0.5,
      min: 3,
      max: 40,
      slide: function(event, ui) {
        $("#tenorCount").val(ui.value);
      }
   });
   $("#tenorCount").val($("#tenor").slider("value"));

   $("#interest_rate").slider({
      range: "min",
      value: 8.5,
      step: 0.5,
      min: 3,
      max: 30,
      slide: function(event, ui) {
        $("#interest_rate_count").val(ui.value);
      }
   });
   $("#interest_rate_count").val($("#interest_rate").slider("value"));
   
   
   
});