jQuery(document).ready(function($) {
    "use strict";
    function numDifferentiation(val) {
    if(val >= 10000000) val = (val/10000000).toFixed(2) + ' Cr';
    else if(val >= 100000) val = (val/100000).toFixed(2) + ' Lac';
    else if(val >= 1000) val = (val/1000).toFixed(2) + ' K';
    return val;
}
    if ("undefined" != typeof HOUZEZ_ajaxcalls_vars) {
        var houzezMap, markers = new Array,
            markerCluster = null,
            current_marker = 0,
            ajaxurl = HOUZEZ_ajaxcalls_vars.admin_url + "admin-ajax.php",
            login_sending = HOUZEZ_ajaxcalls_vars.login_loading,
            userID = HOUZEZ_ajaxcalls_vars.user_id,
            login_redirect_type = HOUZEZ_ajaxcalls_vars.redirect_type,
            login_redirect = HOUZEZ_ajaxcalls_vars.login_redirect,
            prop_lat = HOUZEZ_ajaxcalls_vars.property_lat,
            prop_lng = HOUZEZ_ajaxcalls_vars.property_lng,
            paypal_connecting = HOUZEZ_ajaxcalls_vars.paypal_connecting,
            mollie_connecting = HOUZEZ_ajaxcalls_vars.mollie_connecting,
            process_loader_refresh = HOUZEZ_ajaxcalls_vars.process_loader_refresh,
            process_loader_spinner = HOUZEZ_ajaxcalls_vars.process_loader_spinner,
            process_loader_circle = HOUZEZ_ajaxcalls_vars.process_loader_circle,
            process_loader_cog = HOUZEZ_ajaxcalls_vars.process_loader_cog,
            success_icon = HOUZEZ_ajaxcalls_vars.success_icon,
            confirm_message = HOUZEZ_ajaxcalls_vars.confirm,
            confirm_featured = HOUZEZ_ajaxcalls_vars.confirm_featured,
            confirm_featured_remove = HOUZEZ_ajaxcalls_vars.confirm_featured_remove,
            confirm_relist = HOUZEZ_ajaxcalls_vars.confirm_relist,
            is_singular_property = HOUZEZ_ajaxcalls_vars.is_singular_property,
            property_map = HOUZEZ_ajaxcalls_vars.property_map,
            property_map_street = HOUZEZ_ajaxcalls_vars.property_map_street,
            currency_symb = HOUZEZ_ajaxcalls_vars.currency_symbol,
            advanced_search_price_range_min = parseInt(HOUZEZ_ajaxcalls_vars.advanced_search_widget_min_price),
            advanced_search_price_range_max = parseInt(HOUZEZ_ajaxcalls_vars.advanced_search_widget_max_price),
            advanced_search_price_range_min_rent = parseInt(HOUZEZ_ajaxcalls_vars.advanced_search_min_price_range_for_rent),
            advanced_search_price_range_max_rent = parseInt(HOUZEZ_ajaxcalls_vars.advanced_search_max_price_range_for_rent),
            advanced_search_widget_min_area = parseInt(HOUZEZ_ajaxcalls_vars.advanced_search_widget_min_area),
            advanced_search_widget_max_area = parseInt(HOUZEZ_ajaxcalls_vars.advanced_search_widget_max_area),
            advanced_search_price_slide = HOUZEZ_ajaxcalls_vars.advanced_search_price_slide,
            fave_page_template = HOUZEZ_ajaxcalls_vars.fave_page_template,
            fave_prop_featured = HOUZEZ_ajaxcalls_vars.prop_featured,
            featured_listings_none = HOUZEZ_ajaxcalls_vars.featured_listings_none,
            prop_sent_for_approval = HOUZEZ_ajaxcalls_vars.prop_sent_for_approval,
            houzez_rtl = HOUZEZ_ajaxcalls_vars.houzez_rtl,
            infoboxClose = HOUZEZ_ajaxcalls_vars.infoboxClose,
            clusterIcon = HOUZEZ_ajaxcalls_vars.clusterIcon,
            paged = HOUZEZ_ajaxcalls_vars.paged,
            sort_by = HOUZEZ_ajaxcalls_vars.sort_by,
            google_map_style = HOUZEZ_ajaxcalls_vars.google_map_style,
            googlemap_default_zoom = HOUZEZ_ajaxcalls_vars.googlemap_default_zoom,
            googlemap_pin_cluster = HOUZEZ_ajaxcalls_vars.googlemap_pin_cluster,
            googlemap_zoom_cluster = HOUZEZ_ajaxcalls_vars.googlemap_zoom_cluster,
            map_icons_path = HOUZEZ_ajaxcalls_vars.map_icons_path,
            google_map_needed = HOUZEZ_ajaxcalls_vars.google_map_needed,
            simple_logo = HOUZEZ_ajaxcalls_vars.simple_logo,
            retina_logo = HOUZEZ_ajaxcalls_vars.retina_logo,
            retina_logo_mobile = HOUZEZ_ajaxcalls_vars.retina_logo_mobile,
            retina_logo_mobile_splash = HOUZEZ_ajaxcalls_vars.retina_logo_mobile_splash,
            retina_logo_splash = HOUZEZ_ajaxcalls_vars.retina_logo_splash,
            retina_logo_height = HOUZEZ_ajaxcalls_vars.retina_logo_height,
            retina_logo_width = HOUZEZ_ajaxcalls_vars.retina_logo_width,
            transparent_menu = HOUZEZ_ajaxcalls_vars.transparent_menu,
            transportation = HOUZEZ_ajaxcalls_vars.transportation,
            supermarket = HOUZEZ_ajaxcalls_vars.supermarket,
            schools = HOUZEZ_ajaxcalls_vars.schools,
            libraries = HOUZEZ_ajaxcalls_vars.libraries,
            pharmacies = HOUZEZ_ajaxcalls_vars.pharmacies,
            hospitals = HOUZEZ_ajaxcalls_vars.hospitals,
            currency_position = HOUZEZ_ajaxcalls_vars.currency_position,
            currency_updating_msg = HOUZEZ_ajaxcalls_vars.currency_updating_msg,
            submission_currency = HOUZEZ_ajaxcalls_vars.submission_currency,
            wire_transfer_text = HOUZEZ_ajaxcalls_vars.wire_transfer_text,
            direct_pay_thanks = HOUZEZ_ajaxcalls_vars.direct_pay_thanks,
            direct_payment_title = HOUZEZ_ajaxcalls_vars.direct_payment_title,
            direct_payment_button = HOUZEZ_ajaxcalls_vars.direct_payment_button,
            direct_payment_details = HOUZEZ_ajaxcalls_vars.direct_payment_details,
            measurement_unit = HOUZEZ_ajaxcalls_vars.measurement_unit,
            measurement_updating_msg = HOUZEZ_ajaxcalls_vars.measurement_updating_msg,
            thousands_separator = HOUZEZ_ajaxcalls_vars.thousands_separator,
            rent_status_for_price_range = HOUZEZ_ajaxcalls_vars.for_rent_price_range,
            current_tempalte = HOUZEZ_ajaxcalls_vars.current_tempalte,
            not_found = HOUZEZ_ajaxcalls_vars.not_found,
            property_detail_top = HOUZEZ_ajaxcalls_vars.property_detail_top,
            keyword_search_field = HOUZEZ_ajaxcalls_vars.keyword_search_field,
            keyword_autocomplete = HOUZEZ_ajaxcalls_vars.keyword_autocomplete,
            template_thankyou = HOUZEZ_ajaxcalls_vars.template_thankyou,
            direct_pay_text = HOUZEZ_ajaxcalls_vars.direct_pay_text,
            search_result_page = HOUZEZ_ajaxcalls_vars.search_result_page,
            houzez_default_radius = HOUZEZ_ajaxcalls_vars.houzez_default_radius,
            enable_radius_search = HOUZEZ_ajaxcalls_vars.enable_radius_search,
            enable_radius_search_halfmap = HOUZEZ_ajaxcalls_vars.enable_radius_search_halfmap,
            houzez_primary_color = HOUZEZ_ajaxcalls_vars.houzez_primary_color,
            houzez_geocomplete_country = HOUZEZ_ajaxcalls_vars.geocomplete_country,
            houzez_logged_in = HOUZEZ_ajaxcalls_vars.houzez_logged_in,
            ipinfo_location = HOUZEZ_ajaxcalls_vars.ipinfo_location,
            compare_button_url = HOUZEZ_ajaxcalls_vars.compare_button_url,
            compare_page_not_found = HOUZEZ_ajaxcalls_vars.compare_page_not_found;
        if (houzez_rtl = "yes" == houzez_rtl, "" !== retina_logo && "" !== retina_logo_width && "" !== retina_logo_height && 2 == window.devicePixelRatio && ("yes" == transparent_menu ? ($(".not-splash-header.houzez-header-transparent .logo-desktop img").attr("src", retina_logo_splash), $(".not-splash-header.houzez-header-transparent .logo-desktop img").attr("width", retina_logo_width), $(".not-splash-header.houzez-header-transparent .logo-desktop img").attr("height", retina_logo_height), $(".sticky_nav.header-section-4 .logo-desktop img").attr("src", retina_logo), $(".sticky_nav.header-section-4 .logo-desktop img").attr("width", retina_logo_width), $(".sticky_nav.header-section-4 .logo-desktop img").attr("height", retina_logo_height)) : ($(".not-splash-header .logo-desktop img").attr("src", retina_logo), $(".not-splash-header .logo-desktop img").attr("width", retina_logo_width), $(".not-splash-header .logo-desktop img").attr("height", retina_logo_height))), "" !== retina_logo_splash && "" !== retina_logo_width && "" !== retina_logo_height && 2 == window.devicePixelRatio && ($(".slpash-header .logo-desktop img").attr("src", retina_logo_splash), $(".slpash-header .logo-desktop img").attr("width", retina_logo_width), $(".slpash-header .logo-desktop img").attr("height", retina_logo_height)), "" !== retina_logo_mobile && 2 == window.devicePixelRatio && $(".logo-mobile img").attr("src", retina_logo_mobile), "" !== retina_logo_mobile_splash && 2 == window.devicePixelRatio && $(".logo-mobile-splash img").attr("src", retina_logo_mobile_splash), "yes" == google_map_needed) {
            var placesIDs = new Array,
                transportationsMarkers = new Array,
                supermarketsMarkers = new Array,
                schoolsMarkers = new Array,
                librariesMarkers = new Array,
                pharmaciesMarkers = new Array,
                hospitalsMarkers = new Array,
                drgflag = !0,
                houzez_is_mobile = !1; /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && (drgflag = !1, houzez_is_mobile = !0);
            var houzezMapoptions = { zoom: parseInt(googlemap_default_zoom), mapTypeId: google.maps.MapTypeId.ROADMAP, panControl: !1, draggable: drgflag, zoomControl: !1, mapTypeControl: !1, scaleControl: !1, streetViewControl: !1, overviewMapControl: !1, zoomControlOptions: { style: google.maps.ZoomControlStyle.SMALL, position: google.maps.ControlPosition.RIGHT_TOP }, streetViewControlOptions: { position: google.maps.ControlPosition.RIGHT_TOP } },
                houzezHeaderMapOptions = { maxZoom: 20, disableDefaultUI: !0, scrollwheel: !1, scroll: { x: $(window).scrollLeft(), y: $(window).scrollTop() }, zoom: parseInt(googlemap_default_zoom), mapTypeId: google.maps.MapTypeId.ROADMAP, draggable: drgflag },
                infobox = new InfoBox({ disableAutoPan: !0, maxWidth: 275, alignBottom: !0, pixelOffset: new google.maps.Size(-122, -48), zIndex: null, closeBoxMargin: "0 0 -16px -16px", closeBoxURL: infoboxClose, infoBoxClearance: new google.maps.Size(1, 1), isHidden: !1, pane: "floatPane", enableEventPropagation: !1 }),
                poiInfo = new InfoBox({ disableAutoPan: !1, maxWidth: 250, pixelOffset: new google.maps.Size(-72, -70), zIndex: null, boxStyle: { background: "#ffffff", opacity: 1, padding: "6px", "box-shadow": "0 1px 2px 0 rgba(0, 0, 0, 0.12)", width: "145px", "text-align": "center", "border-radius": "4px" }, closeBoxMargin: "28px 26px 0px 0px", closeBoxURL: "", infoBoxClearance: new google.maps.Size(1, 1), pane: "floatPane", enableEventPropagation: !1 }),
                houzezGetPOIs = function(a, b, c) {
                    var d = new google.maps.places.PlacesService(b),
                        e = b.getBounds(),
                        f = new Array;
                    switch (c) {
                        case "transportations":
                            f = ["bus_station", "subway_station", "train_station", "airport"];
                            break;
                        case "supermarkets":
                            f = ["grocery_or_supermarket", "shopping_mall"];
                            break;
                        case "schools":
                            f = ["school", "university"];
                            break;
                        case "libraries":
                            f = ["library"];
                            break;
                        case "pharmacies":
                            f = ["pharmacy"];
                            break;
                        case "hospitals":
                            f = ["hospital"] }
                    d.nearbySearch({ location: a, bounds: e, radius: 2e3, types: f }, function(d, e) {
                        if (e === google.maps.places.PlacesServiceStatus.OK)
                            for (var f = 0; f < d.length; f++) jQuery.inArray(d[f].place_id, placesIDs) == -1 && (houzezCreatePOI(d[f], b, c), placesIDs.push(d[f].place_id)) }) },
                houzezCreatePOI = function(a, b, c) {
                    var d;
                    switch (c) {
                        case "transportations":
                            d = new google.maps.Marker({ map: b, position: a.geometry.location, icon: map_icons_path + "transportation.png" }), transportationsMarkers.push(d);
                            break;
                        case "supermarkets":
                            d = new google.maps.Marker({ map: b, position: a.geometry.location, icon: map_icons_path + "supermarket.png" }), supermarketsMarkers.push(d);
                            break;
                        case "schools":
                            d = new google.maps.Marker({ map: b, position: a.geometry.location, icon: map_icons_path + "school.png" }), schoolsMarkers.push(d);
                            break;
                        case "libraries":
                            d = new google.maps.Marker({ map: b, position: a.geometry.location, icon: map_icons_path + "libraries.png" }), librariesMarkers.push(d);
                            break;
                        case "pharmacies":
                            d = new google.maps.Marker({ map: b, position: a.geometry.location, icon: map_icons_path + "pharmacy.png" }), pharmaciesMarkers.push(d);
                            break;
                        case "hospitals":
                            d = new google.maps.Marker({ map: b, position: a.geometry.location, icon: map_icons_path + "hospital.png" }), hospitalsMarkers.push(d) }
                    google.maps.event.addListener(d, "mouseover", function() { poiInfo.setContent(a.name), poiInfo.open(b, this) }), google.maps.event.addListener(d, "mouseout", function() { poiInfo.open(null, null) }) },
                houzezTooglePOIs = function(a, b) {
                    for (var c = 0; c < b.length; c++) null != b[c].getMap() ? b[c].setMap(null) : b[c].setMap(a) },
                houzezPoiControls = function(a, b, c) { a.style.clear = "both";
                    var d = document.createElement("div");
                    d.id = "transportation", d.class = "transportation", d.title = transportation, a.appendChild(d);
                    var e = document.createElement("div");
                    e.id = "transportationIcon", e.innerHTML = '<div class="icon"><img src="' + map_icons_path + 'transportation-panel-icon.png" alt=""></div><span>' + transportation + "</span>", d.appendChild(e);
                    var f = document.createElement("div");
                    f.id = "supermarkets", f.title = supermarket, a.appendChild(f);
                    var g = document.createElement("div");
                    g.id = "supermarketsIcon", g.innerHTML = '<div class="icon"><img src="' + map_icons_path + 'supermarket-panel-icon.png" alt=""></div><span>' + supermarket + "</span>", f.appendChild(g);
                    var h = document.createElement("div");
                    h.id = "schools", h.title = schools, a.appendChild(h);
                    var i = document.createElement("div");
                    i.id = "schoolsIcon", i.innerHTML = '<div class="icon"><img src="' + map_icons_path + 'school-panel-icon.png" alt=""></div><span>' + schools + "</span>", h.appendChild(i);
                    var j = document.createElement("div");
                    j.id = "libraries", j.title = libraries, a.appendChild(j);
                    var k = document.createElement("div");
                    k.id = "librariesIcon", k.innerHTML = '<div class="icon"><img src="' + map_icons_path + 'libraries-panel-icon.png" alt=""></div><span>' + libraries + "</span>", j.appendChild(k);
                    var l = document.createElement("div");
                    l.id = "pharmacies", l.title = pharmacies, a.appendChild(l);
                    var m = document.createElement("div");
                    m.id = "pharmaciesIcon", m.innerHTML = '<div class="icon"><img src="' + map_icons_path + 'pharmacy-panel-icon.png" alt=""></div><span>' + pharmacies + "</span>", l.appendChild(m);
                    var n = document.createElement("div");
                    n.id = "hospitals", n.title = hospitals, a.appendChild(n);
                    var o = document.createElement("div");
                    o.id = "hospitalsIcon", o.innerHTML = '<div class="icon"><img src="' + map_icons_path + 'hospital-panel-icon.png" alt=""></div><span>' + hospitals + "</span>", n.appendChild(o), d.addEventListener("click", function() {
                        var a = this;
                        $(this).hasClass("active") ? ($(this).removeClass("active"), houzezTooglePOIs(b, transportationsMarkers)) : ($(this).addClass("active"), houzezGetPOIs(c, b, "transportations"), houzezTooglePOIs(b, transportationsMarkers)), google.maps.event.addListener(b, "bounds_changed", function() {
                            if ($(a).hasClass("active")) {
                                var c = b.getCenter();
                                houzezGetPOIs(c, b, "transportations") } }) }), f.addEventListener("click", function() {
                        var a = this;
                        $(this).hasClass("active") ? ($(this).removeClass("active"), houzezTooglePOIs(b, supermarketsMarkers)) : ($(this).addClass("active"), houzezGetPOIs(c, b, "supermarkets"), houzezTooglePOIs(b, supermarketsMarkers)), google.maps.event.addListener(b, "bounds_changed", function() {
                            if ($(a).hasClass("active")) {
                                var c = b.getCenter();
                                houzezGetPOIs(c, b, "supermarkets") } }) }), h.addEventListener("click", function() {
                        var a = this;
                        $(this).hasClass("active") ? ($(this).removeClass("active"), houzezTooglePOIs(b, schoolsMarkers)) : ($(this).addClass("active"), houzezGetPOIs(c, b, "schools"), houzezTooglePOIs(b, schoolsMarkers)), google.maps.event.addListener(b, "bounds_changed", function() {
                            if ($(a).hasClass("active")) {
                                var c = b.getCenter();
                                houzezGetPOIs(c, b, "schools") } }) }), j.addEventListener("click", function() {
                        var a = this;
                        $(this).hasClass("active") ? ($(this).removeClass("active"), houzezTooglePOIs(b, librariesMarkers)) : ($(this).addClass("active"), houzezGetPOIs(c, b, "libraries"), houzezTooglePOIs(b, librariesMarkers)), google.maps.event.addListener(b, "bounds_changed", function() {
                            if ($(a).hasClass("active")) {
                                var c = b.getCenter();
                                houzezGetPOIs(c, b, "libraries") } }) }), l.addEventListener("click", function() {
                        var a = this;
                        $(this).hasClass("active") ? ($(this).removeClass("active"), houzezTooglePOIs(b, pharmaciesMarkers)) : ($(this).addClass("active"), houzezGetPOIs(c, b, "pharmacies"), houzezTooglePOIs(b, pharmaciesMarkers)), google.maps.event.addListener(b, "bounds_changed", function() {
                            if ($(a).hasClass("active")) {
                                var c = b.getCenter();
                                houzezGetPOIs(c, b, "pharmacies") } }) }), n.addEventListener("click", function() {
                        var a = this;
                        $(this).hasClass("active") ? ($(this).removeClass("active"), houzezTooglePOIs(b, hospitalsMarkers)) : ($(this).addClass("active"), houzezGetPOIs(c, b, "hospitals"), houzezTooglePOIs(b, hospitalsMarkers)), google.maps.event.addListener(b, "bounds_changed", function() {
                            if ($(a).hasClass("active")) {
                                var c = b.getCenter();
                                houzezGetPOIs(c, b, "hospitals") } }) }) },
                houzezSetPOIControls = function(a, b) {
                    var c = document.createElement("div");
                    new houzezPoiControls(c, a, b);
                    c.index = 1, c.style["padding-left"] = "10px", a.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(c) } }
        var has_compare = $("#compare-controller").length;
        if (1 == has_compare) {
            var compare_panel = function() { $(".panel-btn").on("click", function() { $(".compare-panel").hasClass("panel-open") ? $(".compare-panel").removeClass("panel-open") : $(".compare-panel").addClass("panel-open") }) },
                compare_panel_close = function() { $(".compare-panel").hasClass("panel-open") && $(".compare-panel").removeClass("panel-open") },
                compare_panel_open = function() { $(".compare-panel").addClass("panel-open") };
            if ($(".compare-property").length > 0) {
                var houzez_compare_listing = function() { $(".compare-property").click(function(a) { a.preventDefault();
                        var b = $(this),
                            c = b.attr("data-propid"),
                            d = { action: "houzez_compare_add_property", prop_id: c };
                        b.find("i.fa-plus").addClass("fa-spin"), $.post(ajaxurl, d, function(a) {
                            var c = { action: "houzez_compare_update_basket" };
                            b.find("i.fa-plus").removeClass("fa-spin"), $.post(ajaxurl, c, function(a) { $("div#compare-properties-basket").replaceWith(a), compare_panel(), compare_panel_open() }) }) }) };
                houzez_compare_listing(), $(document).on("click", "#compare-properties-basket .compare-property-remove", function(a) { a.preventDefault();
                    var b = jQuery(this).parent().attr("property-id");
                    $(this).parent().block({ message: '<i class="' + process_loader_spinner + '"></i>', css: { border: "none", backgroundColor: "none", fontSize: "16px" } });
                    var c = { action: "houzez_compare_add_property", prop_id: b };
                    $.post(ajaxurl, c, function(a) {
                        var b = { action: "houzez_compare_update_basket" };
                        $.post(ajaxurl, b, function(a) { $("div#compare-properties-basket").replaceWith(a), compare_panel() }) }) }), jQuery(document).on("click", ".compare-properties-button", function() {
                    return "" != compare_button_url ? window.location.href = compare_button_url : alert(compare_page_not_found), !1 }) } }
        if ($("#houzez-print").length > 0 && $("#houzez-print").click(function(e) { e.preventDefault();
                var propID, printWindow;
                propID = $(this).attr("data-propid"), printWindow = window.open("", "Print Me", "width=700 ,height=842"), $.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_create_print", propid: propID }, success: function(a) { printWindow.document.write(a), printWindow.document.close(), printWindow.focus() }, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) }), houzez_rtl) {
            var visual_composer_stretch_row = function() {
                var a = $('[data-vc-full-width="true"]');
                $.each(a, function(a, b) {
                    var c = $(this);
                    c.addClass("vc_hidden");
                    var d = c.next(".vc_row-full-width"),
                        e = parseInt(c.css("margin-left"), 10),
                        f = parseInt(c.css("margin-right"), 10),
                        g = 0 - d.offset().left - e,
                        h = $(window).width();
                    if (c.css({ position: "relative", left: g, right: g, "box-sizing": "border-box", width: $(window).width() }), !c.data("vcStretchContent")) {
                        var i = -1 * g;
                        0 > i && (i = 0);
                        var j = h - i - d.width() + e + f;
                        0 > j && (j = 0), c.css({ "padding-left": i + "px", "padding-right": j + "px" }) }
                    c.attr("data-vc-full-width-init", "true"), c.removeClass("vc_hidden") }) };
            visual_composer_stretch_row(), $(window).resize(function() { visual_composer_stretch_row() }) }
        $(".view-btn ").click(function() { $.removeCookie("properties-layout"), $.removeCookie("layout-btn"), $(this).hasClass("btn-list") ? ($.cookie("properties-layout", "list-view"), $.cookie("layout-btn", "btn-list")) : $(this).hasClass("btn-grid") ? ($.cookie("properties-layout", "grid-view"), $.cookie("layout-btn", "btn-grid")) : $(this).hasClass("btn-grid-3-col") && ($.cookie("properties-layout", "grid-view-3-col"), $.cookie("layout-btn", "btn-grid-3-col")) }), "undefined" != $.cookie("properties-layout") && ("list-view" == $.cookie("properties-layout") && "template-search.php" != fave_page_template && "user_dashboard_favorites.php" != fave_page_template ? ($(".property-listing").removeClass("grid-view grid-view-3-col"), $(".property-listing").addClass("list-view")) : "grid-view" == $.cookie("properties-layout") && "template-search.php" != fave_page_template && "user_dashboard_favorites.php" != fave_page_template ? ($(".property-listing").removeClass("list-view grid-view grid-view-3-col"), $(".property-listing").addClass("grid-view")) : "grid-view-3-col" == $.cookie("properties-layout") && "template-search.php" != fave_page_template && "user_dashboard_favorites.php" != fave_page_template && ($(".property-listing").removeClass("list-view grid-view"), $(".property-listing").addClass("grid-view grid-view-3-col"))), "undefined" != $.cookie("layout-btn") && ("btn-list" == $.cookie("layout-btn") ? ($(".view-btn").removeClass("active"), $(".view-btn.btn-list").addClass("active")) : "btn-grid" == $.cookie("layout-btn") ? ($(".view-btn").removeClass("active"), $(".view-btn.btn-grid").addClass("active")) : "btn-grid-3-col" == $.cookie("layout-btn") && ($(".view-btn").removeClass("active"), $(".view-btn.btn-grid-3-col").addClass("active")));
        var addCommas = function(a) { a += "";
                for (var b = a.split("."), c = b[0], d = b.length > 1 ? "." + b[1] : "", e = /(\d+)(\d{3})/; e.test(c);) c = c.replace(e, "$1" + thousands_separator + "$2");
                return c + d },
            properties_module_section = $("#properties_module_section");
        if (properties_module_section.length > 0) {
            var properties_module_container = $("#properties_module_container"),
                paginationLink = $(".property-item-module ul.pagination li a"),
                fave_loader = $(".fave-svg-loader");
            $("body").on("click", ".fave-load-more a", function(a) { a.preventDefault();
                var b = $(this),
                    c = b.attr("href"),
                    d = 0;
                b.prepend('<i class="fa-left ' + process_loader_spinner + '"></i>'), $("<div>").load(c, function() {
                    var a = d.toString(),
                        e = b.closest("#properties_module_section").find("#module_properties"),
                        f = $(this).find("#properties_module_section .item-wrap").addClass("fave-post-new-" + a);
                    return f.hide().appendTo(e).fadeIn(400), $(this).find(".fave-load-more").length ? b.closest("#properties_module_section").find(".fave-load-more").html($(this).find(".fave-load-more").html()) : b.closest("#properties_module_section").find(".fave-load-more").fadeOut("fast").remove(), c != window.location && window.history.pushState({ path: c }, "", c), d++, houzez_compare_listing(), !1 }) }) }
        var fave_property_status_changed = function(a, b) { a == HOUZEZ_ajaxcalls_vars.for_rent ? (b.find(".prices-for-all").addClass("hide"), b.find(".prices-for-all select").attr("disabled", "disabled"), b.find(".prices-only-for-rent").removeClass("hide"), b.find(".prices-only-for-rent select").removeAttr("disabled", "disabled"), b.find(".prices-only-for-rent select").selectpicker("refresh")) : (b.find(".prices-only-for-rent").addClass("hide"), b.find(".prices-only-for-rent select").attr("disabled", "disabled"), b.find(".prices-for-all").removeClass("hide"), b.find(".prices-for-all select").removeAttr("disabled", "disabled")) };
        $('select[name="status"]').change(function(a) {
            var b = $(this).val(),
                c = $(this).parents("form");
            fave_property_status_changed(b, c) });
        var selected_status_header_search = $('select[name="status"]').val();
        if (selected_status_header_search == HOUZEZ_ajaxcalls_vars.for_rent || "" == selected_status_header_search) {
            var $form = $(".advance-search-header");
            fave_property_status_changed(selected_status_header_search, $form) }
        $(".advanced-search-mobile #selected_status_mobile").change(function(a) {
            var b = $(this).val(),
                c = $(this).parents("form");
            fave_property_status_changed(b, c) });
        var selected_status_header_search = $(".advanced-search-mobile #selected_status_mobile").val();
        if (selected_status_header_search == HOUZEZ_ajaxcalls_vars.for_rent || "" == selected_status_header_search) {
            var $form = $(".advanced-search-mobile");
            fave_property_status_changed(selected_status_header_search, $form) }
        $(".advanced-search-module #selected_status_module").change(function(a) {
            var b = $(this).val(),
                c = $(this).parents("form");
            fave_property_status_changed(b, c) });
        var selected_status_module_search = $(".advanced-search-module #selected_status_module").val();
        if (selected_status_module_search == HOUZEZ_ajaxcalls_vars.for_rent || "" == selected_status_module_search) {
            var $form = $(".advanced-search-module");
            fave_property_status_changed(selected_status_module_search, $form) }
        $("#save_search_click").click(function(e) { e.preventDefault();
            var $this = $(this),
                $from = $(".save_search_form");
            0 === parseInt(userID, 10) ? $("#pop-login").modal("show") : $.ajax({ url: ajaxurl, data: $from.serialize(), method: $from.attr("method"), dataType: "JSON", beforeSend: function() { $this.children("i").remove(), $this.prepend('<i class="fa-left ' + process_loader_spinner + '"></i>') }, success: function(a) { a.success && $("#save_search_click").addClass("saved") }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) }, complete: function() { $this.children("i").removeClass(process_loader_spinner) } }) }), $(".remove-search").click(function(e) { e.preventDefault();
            var $this = $(this),
                prop_id = $this.data("propertyid"),
                removeBlock = $this.parents(".saved-search-block");
            confirm(confirm_message) && $.ajax({ url: ajaxurl, dataType: "JSON", method: "POST", data: { action: "houzez_delete_search", property_id: prop_id }, beforeSend: function() { $this.children("i").remove(), $this.prepend('<i class="' + process_loader_spinner + '"></i>') }, success: function(a) { a.success && removeBlock.remove() }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) } }) }), $(".agent_contact_form").click(function(e) { e.preventDefault();
            var $this = $(this),
                $form = $this.parents("form"),
                $result = $form.find(".form_messages");
            $.ajax({ url: ajaxurl, data: $form.serialize(), method: $form.attr("method"), dataType: "JSON", beforeSend: function() { $this.children("i").remove(), $this.prepend('<i class="fa-left ' + process_loader_spinner + '"></i>') }, success: function(a) { a.success ? ($result.empty().append(a.msg), $form.find("input").val(""), $form.find("textarea").val("")) : $result.empty().append(a.msg) }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) }, complete: function() { $this.children("i").removeClass(process_loader_spinner), $this.children("i").addClass(success_icon) } }) });
        var agent_contact_email = function(current_element, target_email, sender_name, sender_phone, sender_email, sender_message, sender_nonce) { jQuery.ajax({ type: "post", url: ajaxurl, dataType: "json", data: { action: "houzez_contact_agent", target_email: target_email, sender_name: sender_name, sender_phone: sender_phone, sender_email: sender_email, sender_msg: sender_message, sender_nonce: sender_nonce }, beforeSend: function() { current_element.children("i").remove(), current_element.prepend('<i class="fa-left ' + process_loader_spinner + '"></i>') }, success: function(a) { current_element.children("i").removeClass(process_loader_spinner), a.success ? ($("#form_messages").empty().append(a.msg), current_element.children("i").addClass(success_icon)) : $("#form_messages").empty().append(a.msg) }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) } }) };
        $("#agent_detail_contact_btn").click(function(a) { a.preventDefault();
            var b = $(this),
                c = $("#name").val(),
                d = $("#phone").val(),
                e = $("#email").val(),
                f = $("#message").val(),
                g = $("#target_email").val(),
                h = $("#agent_detail_ajax_nonce").val();
            agent_contact_email(b, g, c, d, e, f, h) }), $(".resend-for-approval-perlisting").click(function(a) { a.preventDefault();
            var b = $(this).attr("data-propid");
            resend_for_approval_perlisting(b, $(this)), $(this).unbind("click") });
        var resend_for_approval_perlisting = function(prop_id, currentDiv) { $.ajax({ type: "POST", url: ajaxurl, dataType: "JSON", data: { action: "houzez_resend_for_approval_perlisting", propid: prop_id }, success: function(a) { a.success ? currentDiv.parent().empty().append('<span class="label-success label">' + a.msg + "</span>") : currentDiv.parent().empty().append('<div class="alert alert-danger">' + a.msg + "</div>") }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) } }) };
        $(".add_fav").click(function() {
            var a = $(this).children("i"),
                b = $(this).attr("data-propid");
            add_to_favorite(b, a) }), $(".remove_fav").click(function() {
            var a = $(this),
                b = $(this).attr("data-propid");
            add_to_favorite(b, a);
            a.parents(".item-wrap").remove() });
        var add_to_favorite = function(propID, curnt) { 0 === parseInt(userID, 10) ? $("#pop-login").modal("show") : jQuery.ajax({ type: "post", url: ajaxurl, dataType: "json", data: { action: "houzez_add_to_favorite", property_id: propID }, beforeSend: function() { curnt.addClass("faa-pulse animated") }, success: function(a) { a.added ? curnt.removeClass("fa-heart-o").addClass("fa-heart") : curnt.removeClass("fa-heart").addClass("fa-heart-o"), curnt.removeClass("faa-pulse animated") }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) } }) };
        $(".fave-login-button").click(function(a) { a.preventDefault();
            var b = $(this);
            houzez_login(b) }), $(".fave-register-button").click(function(a) { a.preventDefault();
            var b = $(this);
            houzez_register(b) });
        var houzez_login = function(currnt) {
                var $form = currnt.parents("form"),
                    $messages = currnt.parents(".login-block").find(".houzez_messages");
                $.ajax({ type: "post", url: ajaxurl, dataType: "json", data: $form.serialize(), beforeSend: function() { 
					$messages.empty().append('<p class="success text-success"> ' + login_sending + "</p>") }, success: function(a) { a.success ? ($messages.empty().append('<p class="success text-success"><i class="fa fa-check"></i> ' + a.msg + "</p>"), "same_page" == login_redirect_type ? window.location.reload() : window.location.href = login_redirect) : $messages.empty().append('<p class="error text-danger"><i class="fa fa-close"></i> ' + a.msg + "</p>") }, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) },
            houzez_register = function(currnt) {
    var $form = currnt.parents("form"),
        $messages = currnt.parents(".class-for-register-msg").find(".houzez_messages_register");
    $.ajax({
        type: "post",
        url: ajaxurl,
       // dataType: "json",
        data: $form.serialize(),
        beforeSend: function() {
            $messages.empty().append('<p class="success text-success"> ' + login_sending + "</p>")
        },
       success: function( response ) {
       	response=JSON.parse(response);
                    if( response.success ) {
                        $messages.empty().append('<p class="success text-success"><i class="fa fa-check"></i> '+ response.msg +'</p>');
                    } else {
                        $messages.empty().append('<p class="error text-danger"><i class="fa fa-close"></i> '+ response.msg +'</p>');
                    }
                },
        error: function(xhr, status, error) {
            var err = eval("(" + xhr.responseText + ")");
            console.log(err.Message)
        }
    })
};
        $("#houzez_forgetpass").click(function() {
            var user_login = $("#user_login_forgot").val(),
                security = $("#fave_resetpassword_security").val();
            $.ajax({ type: "post", url: ajaxurl, dataType: "json", data: { action: "houzez_reset_password", user_login: user_login, security: security }, beforeSend: function() { $("#houzez_msg_reset").empty().append('<p class="success text-success"> ' + login_sending + "</p>") }, success: function(a) { a.success ? $("#houzez_msg_reset").empty().append('<p class="success text-success"><i class="fa fa-check"></i> ' + a.msg + "</p>") : $("#houzez_msg_reset").empty().append('<p class="error text-danger"><i class="fa fa-close"></i> ' + a.msg + "</p>") }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) } }) }), $("#houzez_reset_password").length > 0 && $("#houzez_reset_password").click(function(a) { a.preventDefault();
            var b = $(this),
                c = $('input[name="rp_login"]').val(),
                d = $('input[name="rp_key"]').val(),
                e = $('input[name="pass1"]').val(),
                f = $('input[name="pass2"]').val(),
                g = $('input[name="fave_resetpassword_security"]').val();
            $.ajax({ type: "post", url: ajaxurl, dataType: "json", data: { action: "houzez_reset_password_2", rq_login: c, password: e, confirm_pass: f, rp_key: d, security: g }, beforeSend: function() { b.children("i").remove(), b.prepend('<i class="fa-left ' + process_loader_spinner + '"></i>') }, success: function(a) { a.success ? (jQuery("#password_reset_msgs").empty().append('<p class="success text-success"><i class="fa fa-check"></i> ' + a.msg + "</p>"), jQuery("#oldpass, #newpass, #confirmpass").val("")) : jQuery("#password_reset_msgs").empty().append('<p class="error text-danger"><i class="fa fa-close"></i> ' + a.msg + "</p>") }, error: function(a) {}, complete: function() { b.children("i").removeClass(process_loader_spinner) } }) }), $("#houzez_complete_order").click(function(a) { a.preventDefault();
            var b, c, d, e, f, g;
            c = $("input[name='houzez_payment_type']:checked").val(), f = $("input[name='featured_pay']").val(), g = $("input[name='is_upgrade']").val(), e = $("#houzez_property_id").val(), d = $("#houzez_listing_price").val(), "paypal" == c ? (fave_processing_modal(paypal_connecting), fave_paypal_payment(e, f, g)) : "stripe" == c ? (b = $(this).parents("form"), "1" === f ? b.find(".houzez_stripe_simple_featured button").trigger("click") : b.find(".houzez_stripe_simple button").trigger("click")) : "direct_pay" == c && (fave_processing_modal(direct_pay_text), direct_bank_transfer(e, d)) });
        var fave_processing_modal = function(a) {
                var b = '<div class="modal fade" id="fave_modal" tabindex="-1" role="dialog" aria-labelledby="faveModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-body houzez_messages_modal">' + a + "</div></div></div></div></div>";
                jQuery("body").append(b), jQuery("#fave_modal").modal() },
            fave_processing_modal_close = function() { jQuery("#fave_modal").modal("hide") },
            fave_paypal_payment = function(property_id, is_prop_featured, is_prop_upgrade) { $.ajax({ type: "post", url: ajaxurl, data: { action: "houzez_property_paypal_payment", prop_id: property_id, is_prop_featured: is_prop_featured, is_prop_upgrade: is_prop_upgrade }, success: function(a) { window.location.href = a }, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) },
            houzez_membership_data = function(a) {
                var b = $("input[name='houzez_payment_type']:checked").val(),
                    c = $("input[name='houzez_package_price']").val(),
                    d = $("input[name='houzez_package_id']").val(),
                    e = $("#houzez_package_name").text();
                if ("paypal" == b) fave_processing_modal(paypal_connecting),
                    $("#paypal_package_recurring").is(":checked") ? houzez_recuring_paypal_package_payment(c, e, d) : houzez_paypal_package_payment(c, e, d);
                else if ("mollie" == b) fave_processing_modal(mollie_connecting), houzez_mollie_package_payment(c, e, d);
                else if ("stripe" == b) {
                    var f = a.parents("form");
                    f.find(".houzez_stripe_membership button").trigger("click") } else "direct_pay" == b ? (fave_processing_modal(direct_pay_text), direct_bank_transfer_package(d, c, e)) : "2checkout" == b || houzez_free_membership_package(d);
                return !1
            },
            houzez_register_user_with_membership = function(currnt) {
                var $form = currnt.parents("form"),
                    $messages = currnt.parents(".class-for-register-msg").find(".houzez_messages_register");
                $.ajax({ type: "post", url: ajaxurl, dataType: "json", data: $form.serialize(), beforeSend: function() { $messages.empty().append('<p class="success text-success"> ' + login_sending + "</p>") }, success: function(a) { a.success ? houzez_membership_data(currnt) : $messages.empty().append('<p class="error text-danger"><i class="fa fa-close"></i> ' + a.msg + "</p>") }, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) };
        $("#houzez_complete_membership").click(function(a) { a.preventDefault();
            var b = $(this);
            return "no" == houzez_logged_in ? void houzez_register_user_with_membership(b) : void houzez_membership_data(b) });
        var houzez_paypal_package_payment = function(houzez_package_price, houzez_package_name, houzez_package_id) { $.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_paypal_package_payment", houzez_package_price: houzez_package_price, houzez_package_name: houzez_package_name, houzez_package_id: houzez_package_id }, success: function(a) { window.location.href = a }, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) },
            houzez_recuring_paypal_package_payment = function(houzez_package_price, houzez_package_name, houzez_package_id) { jQuery.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_recuring_paypal_package_payment", houzez_package_name: houzez_package_name, houzez_package_id: houzez_package_id, houzez_package_price: houzez_package_price }, success: function(a) { window.location.href = a }, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) },
            houzez_mollie_package_payment = function(houzez_package_price, houzez_package_name, houzez_package_id) { $.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_mollie_package_payment", houzez_package_price: houzez_package_price, houzez_package_name: houzez_package_name, houzez_package_id: houzez_package_id }, success: function(a) { window.location.href = a }, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) },
            direct_bank_transfer_package = function(a, b, c) { jQuery.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_direct_pay_package", selected_package: a }, success: function(a) { window.location.href = a }, error: function(a) {} }) },
            houzez_free_membership_package = function(a) { jQuery.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_free_membership_package", selected_package: a }, success: function(a) { window.location.href = a }, error: function(a) {} }) };
        $(".resend-for-approval").click(function(a) {
            if (a.preventDefault(), confirm(confirm_relist)) {
                var b = $(this).attr("data-propid");
                resend_for_approval(b, $(this)), $(this).unbind("click") } });
        var resend_for_approval = function(prop_id, currentDiv) { $.ajax({ type: "POST", url: ajaxurl, dataType: "JSON", data: { action: "houzez_resend_for_approval", propid: prop_id }, success: function(a) {
                    if (a.success) { currentDiv.parent().empty().append('<span class="label-success label">' + a.msg + "</span>");
                        var b = parseInt(jQuery(".listings_remainings").text(), 10);
                        jQuery(".listings_remainings").text(b - 1) } else currentDiv.parent().empty().append('<div class="alert alert-danger">' + a.msg + "</div>") }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) } }) };
        $(".make-prop-featured").click(function(a) {
            if (a.preventDefault(), confirm(confirm_featured)) {
                var b = $(this).attr("data-propid"),
                    c = $(this).attr("data-proptype");
                make_prop_featured(b, $(this), c), $(this).unbind("click") } });
        var make_prop_featured = function(prop_id, currentDiv, prop_type) { $.ajax({ type: "POST", url: ajaxurl, dataType: "JSON", data: { action: "houzez_make_prop_featured", propid: prop_id, prop_type: prop_type }, success: function(a) {
                    if (a.success) {
                        var b = currentDiv.parents(".item-wrap");
                        b.find(".item-thumb").append('<span class="label-featured label">' + fave_prop_featured + "</span>"), currentDiv.remove();
                        var c = parseInt(jQuery(".featured_listings_remaining").text(), 10);
                        jQuery(".featured_listings_remaining").text(c - 1) } else currentDiv.parent().empty().append('<div class="alert alert-danger">' + featured_listings_none + "</div>") }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) } }) };
        $(".remove-prop-featured").click(function(a) {
            if (a.preventDefault(), confirm(confirm_featured_remove)) {
                var b = $(this).attr("data-propid");
                remove_prop_featured(b, $(this)), $(this).unbind("click") } });
        var remove_prop_featured = function(prop_id, currentDiv) { $.ajax({ type: "POST", url: ajaxurl, dataType: "JSON", data: { action: "houzez_remove_prop_featured", propid: prop_id }, success: function(a) {
                        if (a.success) {
                            var b = currentDiv.parents(".item-wrap");
                            b.find(".label-featured").remove(), currentDiv.remove() } }, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) },
            direct_bank_transfer = function(a, b) {
                var c = $('input[name="featured_pay"]').val(),
                    d = $('input[name="is_upgrade"]').val();
                jQuery.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_direct_pay_per_listing", prop_id: a, is_featured: c, is_upgrade: d }, success: function(a) { window.location.href = a }, error: function(a) {} }) };
        $(".yahoo-login").click(function() {
            var a = $(this);
            houzez_login_via_yahoo(a) });
        var houzez_login_via_yahoo = function(current) {
            var $form = current.parents("form"),
                $messages = current.parents(".login-block").find(".houzez_messages");
            $.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_yahoo_login" }, beforeSend: function() { $messages.empty().append('<p class="success text-success"> ' + login_sending + "</p>") }, success: function(a) { window.location.href = a }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) } }) };
        $(".google-login").click(function() {
            var a = $(this);
            houzez_login_via_google(a) });
        var houzez_login_via_google = function(current) {
            var $form = current.parents("form"),
                $messages = current.parents(".login-block").find(".houzez_messages");
            $.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_google_login_oauth" }, beforeSend: function() { $messages.empty().append('<p class="success text-success"> ' + login_sending + "</p>") }, success: function(a) { window.location.href = a }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) } }) };
        $(".facebook-login").click(function() {
            var a = $(this);
            houzez_login_via_facebook(a) });
        var houzez_login_via_facebook = function(current) {
            var $form = current.parents("form"),
                $messages = current.parents(".login-block").find(".houzez_messages");
            $.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_facebook_login_oauth" }, beforeSend: function() { $messages.empty().append('<p class="success text-success"> ' + login_sending + "</p>") }, success: function(a) { window.location.href = a }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) } }) };
        $("#invoice_status, #invoice_type").change(function() { houzez_invoices_filter() }), $("#startDate, #endDate").focusout(function() { houzez_invoices_filter() });
        var houzez_invoices_filter = function() {
                var inv_status = $("#invoice_status").val(),
                    inv_type = $("#invoice_type").val(),
                    startDate = $("#startDate").val(),
                    endDate = $("#endDate").val();
                $.ajax({ url: ajaxurl, dataType: "json", type: "POST", data: { action: "houzez_invoices_ajax_search", invoice_status: inv_status, invoice_type: inv_type, startDate: startDate, endDate: endDate }, success: function(a) { a.success && ($("#invoices_content").empty().append(a.result), $("#invoices_total_price").empty().append(a.total_price)) }, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) },
            houzezAddMarkers = function(a, b) { $.each(a, function(a, c) {
                    var d = new google.maps.LatLng(c.lat, c.lng),
                        e = c.icon,
                        f = new google.maps.Size(44, 56);
                    window.devicePixelRatio > 1.5 && c.retinaIcon && (e = c.retinaIcon, f = new google.maps.Size(44, 56));
                    var g = { url: e, size: f, scaledSize: new google.maps.Size(44, 56), origin: new google.maps.Point(0, 0), anchor: new google.maps.Point(7, 27) },
                        h = new google.maps.Marker({ position: d, map: b, icon: g, draggable: !1, animation: google.maps.Animation.DROP, title: "marker-" + c.sanitizetitle }),
                        i = c.data ? c.data.post_title : c.title,
                        j = document.createElement("div");
                    j.className = "property-item item-grid map-info-box", j.innerHTML = '<div class="figure-block"><figure class="item-thumb"><div class="price hide-on-list"><span class="item-price">' + c.price + '</span></div><a target="_blank" href="' + c.url + '" class="hover-effect" tabindex="0">' + c.thumbnail + '</a></figure></div><div class="item-body"><div class="body-left"><div class="info-row"><h2><a target="_blank" href="' + c.url + '">' + i + "</a></h2><h4>" + c.address + '</h4></div><div class="table-list full-width info-row"><div class="cell"><div class="info-row amenities">' + c.prop_meta + "<p>" + c.type + "</p></div></div></div></div></div>", google.maps.event.addListener(h, "click", function(a, c) {
                        return function() {
                            var c = Math.pow(2, b.getZoom()),
                                d = 100 / c || 0,
                                e = b.getProjection(),
                                f = a.getPosition(),
                                g = e.fromLatLngToPoint(f),
                                h = new google.maps.Point(g.x, g.y - d),
                                i = e.fromPointToLatLng(h);
                            b.setCenter(i), infobox.setContent(j), infobox.open(b, a) } }(h, a)), markers.push(h) }) },
            houzez_map_zoomin = function(a) { google.maps.event.addDomListener(document.getElementById("listing-mapzoomin"), "click", function() {
                    var b = parseInt(a.getZoom(), 10);
                    b++, b > 20 && (b = 20), a.setZoom(b) }) },
            houzez_map_zoomout = function(a) { google.maps.event.addDomListener(document.getElementById("listing-mapzoomout"), "click", function() {
                    var b = parseInt(a.getZoom(), 10);
                    b--, b < 0 && (b = 0), a.setZoom(b) }) },
            houzez_change_map_type = function(a, b) {
                return "roadmap" === b ? a.setMapTypeId(google.maps.MapTypeId.ROADMAP) : "satellite" === b ? a.setMapTypeId(google.maps.MapTypeId.SATELLITE) : "hybrid" === b ? a.setMapTypeId(google.maps.MapTypeId.HYBRID) : "terrain" === b && a.setMapTypeId(google.maps.MapTypeId.TERRAIN), !1 },
            houzez_map_next = function(a) {
                for (current_marker++, current_marker > markers.length && (current_marker = 1); markers[current_marker - 1].visible === !1;) current_marker++, current_marker > markers.length && (current_marker = 1);
                a.getZoom() < 15 && a.setZoom(15), google.maps.event.trigger(markers[current_marker - 1], "click") },
            houzez_map_prev = function(a) {
                for (current_marker--, current_marker < 1 && (current_marker = markers.length); markers[current_marker - 1].visible === !1;) current_marker--, current_marker > markers.length && (current_marker = 1);
                a.getZoom() < 15 && a.setZoom(15), google.maps.event.trigger(markers[current_marker - 1], "click") },
            houzez_map_search_field = function(a, b) {
                var c = new google.maps.places.SearchBox(b);
                a.controls[google.maps.ControlPosition.TOP_LEFT].push(b), a.addListener("bounds_changed", function() { c.setBounds(a.getBounds()) });
                var d = [];
                c.addListener("places_changed", function() {
                    var b = c.getPlaces();
                    if (0 != b.length) { d.forEach(function(a) { a.setMap(null) }), d = [];
                        var e = new google.maps.LatLngBounds;
                        b.forEach(function(b) {
                            var c = { url: b.icon, size: new google.maps.Size(71, 71), origin: new google.maps.Point(0, 0), anchor: new google.maps.Point(17, 34), scaledSize: new google.maps.Size(25, 25) };
                            d.push(new google.maps.Marker({ map: a, icon: c, title: b.name, position: b.geometry.location })), b.geometry.viewport ? e.union(b.geometry.viewport) : e.extend(b.geometry.location) }), a.fitBounds(e) } }) },
            houzez_map_parallax = function(a) {
                var b = $(a.getDiv()).offset();
                a.panBy((houzezHeaderMapOptions.scroll.x - b.left) / 3, (houzezHeaderMapOptions.scroll.y - b.top) / 3), google.maps.event.addDomListener(window, "scroll", function() {
                    var b = $(window).scrollTop(),
                        c = $(window).scrollLeft(),
                        d = a.get("scroll");
                    d && a.panBy(-((d.x - c) / 3), -((d.y - b) / 3)), a.set("scroll", { x: c, y: b }) }) },
            reloadMarkers = function() {
                for (var a = 0; a < markers.length; a++) markers[a].setMap(null);
                markers = [] },
            houzezGeoLocation = function(a) {
                var b = !0,
                    c = !!window.chrome && !!window.chrome.webstore;
                c && "http:" === document.location.protocol && 0 != ipinfo_location && (b = !1), b ? navigator.geolocation && navigator.geolocation.getCurrentPosition(function(b) {
                    var c = { lat: b.coords.latitude, lng: b.coords.longitude },
                        d = new google.maps.Geocoder;
                    d.geocode({ location: c }, function(b, d) {
                        if ("OK" === d)
                            if (b[1]) { console.log(b[1]);
                                new google.maps.Marker({ position: c, map: a }) } else window.alert("No results found");
                        else window.alert("Geocoder failed due to: " + d) });
                    var e = new google.maps.Circle({ radius: 2e3, center: c, map: a, fillColor: houzez_primary_color, fillOpacity: .1, strokeColor: houzez_primary_color, strokeOpacity: .3 });
                    a.fitBounds(e.getBounds()) }, function() { handleLocationError(!0, a, a.getCenter()) }) : $.getJSON("http://ipinfo.io", function(b) {
                    var c = b.loc,
                        c = c.split(","),
                        c = { lat: 1 * c[0], lng: 1 * c[1] },
                        d = new google.maps.Circle({ radius: 1e3, center: c, map: a, icon: clusterIcon, fillColor: houzez_primary_color, fillOpacity: .2, strokeColor: houzez_primary_color, strokeOpacity: .6 });
                    a.fitBounds(d.getBounds());
                    new google.maps.Marker({ position: c, animation: google.maps.Animation.DROP, map: a });
                    a.setCenter(c) }) },
            houzezLatLng = function(a) {
                var b = new google.maps.Geocoder;
                b.geocode({ address: a }, function(a, b) {
                    if ("OK" == b) return a[0].geometry.location }) },
            half_map_ajax_pagi = function() {
                return $(".half_map_ajax_pagi a").click(function(a) { a.preventDefault();
                    var b = $(this).data("houzepagi"),
                        c = $(".advanced-search form"),
                        d = $(this).parents(".widget_houzez_advanced_search");
                    houzez_search_on_change(c, d, b) }), !1 },
            houzez_header_listing_map = function(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v) {
                var w = $("#securityHouzezHeaderMap").val(),
                    x = HOUZEZ_ajaxcalls_vars.header_map_selected_city;
                $.ajax({ type: "POST", dataType: "json", url: ajaxurl, data: { action: "houzez_header_map_listings", initial_city: x, keyword: a, country: b, state: c, location: d, area: e, status: f, type: g, label: h, property_id: i, bedrooms: j, bathrooms: k, min_price: l, max_price: m, min_area: n, max_area: o, features: p, publish_date: q, search_lat: r, search_long: s, use_radius: v, search_location: u, search_radius: t, security: w }, beforeSend: function() { $("#houzez-map-loading").show() }, success: function(a) {
                        if (houzezMap = new google.maps.Map(document.getElementById("houzez-listing-map"), houzezHeaderMapOptions), google.maps.event.trigger(houzezMap, "resize"), "" !== google_map_style) {
                            var b = JSON.parse(google_map_style);
                            houzezMap.setOptions({ styles: b }) }
                        var c = new google.maps.LatLng("", "");
                        if (houzezMap.setCenter(c), houzezMap.setZoom(parseInt(googlemap_default_zoom)), document.getElementById("listing-mapzoomin") && houzez_map_zoomin(houzezMap), document.getElementById("listing-mapzoomout") && houzez_map_zoomout(houzezMap), document.getElementById("google-map-search")) {
                            var d = document.getElementById("google-map-search");
                            houzez_map_search_field(houzezMap, d) }
                        $(".houzezMapType").click(function() {
                            var a = $(this).data("maptype");
                            houzez_change_map_type(houzezMap, a) }), $("#houzez-gmap-next").click(function() { houzez_map_next(houzezMap) }), $("#houzez-gmap-prev").click(function() { houzez_map_prev(houzezMap) }), document.getElementById("houzez-gmap-location") && google.maps.event.addDomListener(document.getElementById("houzez-gmap-location"), "click", function() { houzezGeoLocation(houzezMap) }), remove_map_loader(houzezMap), document.getElementById("houzez-gmap-full") && (houzez_is_mobile ? $("#houzez-gmap-full").toggle(function() { google.maps.event.trigger(houzezMap, "resize"), houzezMap.setOptions({ draggable: !0 }) }, function() { google.maps.event.trigger(houzezMap, "resize"), houzezMap.setOptions({ draggable: !1 }) }) : $("#houzez-gmap-full").toggle(function() { google.maps.event.trigger(houzezMap, "resize"), houzezMap.setOptions({ scrollwheel: !0 }) }, function() { google.maps.event.trigger(houzezMap, "resize"), houzezMap.setOptions({ scrollwheel: !1 }) })), houzez_map_parallax(houzezMap), a.getProperties === !0 ? (reloadMarkers(), houzezAddMarkers(a.properties, houzezMap), houzezMap.fitBounds(markers.reduce(function(a, b) {
                            return a.extend(b.getPosition()) }, new google.maps.LatLngBounds)), google.maps.event.trigger(houzezMap, "resize"), markerCluster = new MarkerClusterer(houzezMap, markers, { maxZoom: 18, gridSize: 60, styles: [{ url: clusterIcon, width: 48, height: 48, textColor: "#fff" }] })) : $("#houzez-listing-map").empty().html('<div class="map-notfound">' + not_found + "</div>") }, error: function(a, b, c) { console.log(a.status), console.log(a.responseText), console.log(c) } }) },
            houzez_half_map_listings = function(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x) {
                var y = $("#securityHouzezHeaderMap").val(),
                    z = $("#houzez_ajax_container");
                return void 0 != x && (paged = x), $.ajax({ type: "POST", dataType: "json", url: ajaxurl, data: { action: "houzez_half_map_listings", keyword: a, location: d, country: b, state: c, area: e, status: f, type: g, label: h, property_id: i, bedrooms: j, bathrooms: k, min_price: l, max_price: m, min_area: n, max_area: o, features: p, publish_date: q, search_lat: s, search_long: t, use_radius: w, search_location: v, search_radius: u, security: y, paged: paged, post_per_page: r }, beforeSend: function() { $("#houzez-map-loading").show(), z.empty().append('<div class="list-loading"><div class="list-loading-bar"></div><div class="list-loading-bar"></div><div class="list-loading-bar"></div><div class="list-loading-bar"></div></div>') }, success: function(a) {
                        if ("" != a.query && $('input[name="search_args"]').val(a.query), houzezMap = new google.maps.Map(document.getElementById("mapViewHalfListings"), houzezMapoptions), "" !== google_map_style) {
                            var b = JSON.parse(google_map_style);
                            houzezMap.setOptions({ styles: b }) }
                        var c = new google.maps.LatLng("", "");
                        if (houzezMap.setCenter(c), houzezMap.setZoom(parseInt(googlemap_default_zoom)), document.getElementById("houzez-gmap-location") && google.maps.event.addDomListener(document.getElementById("houzez-gmap-location"), "click", function() { houzezGeoLocation(houzezMap) }), document.getElementById("houzez-gmap-full") && (houzez_is_mobile ? $("#houzez-gmap-full").toggle(function() { google.maps.event.trigger(houzezMap, "resize"), houzezMap.setOptions({ draggable: !0 }) }, function() { google.maps.event.trigger(houzezMap, "resize"), houzezMap.setOptions({ draggable: !1 }) }) : $("#houzez-gmap-full").toggle(function() { google.maps.event.trigger(houzezMap, "resize"), houzezMap.setOptions({ scrollwheel: !0 }) }, function() { google.maps.event.trigger(houzezMap, "resize"), houzezMap.setOptions({ scrollwheel: !1 }) })), document.getElementById("listing-mapzoomin") && houzez_map_zoomin(houzezMap), document.getElementById("listing-mapzoomout") && houzez_map_zoomout(houzezMap), document.getElementById("google-map-search")) {
                            var d = document.getElementById("google-map-search");
                            houzez_map_search_field(houzezMap, d) }
                        return $(".houzezMapType").click(function() {
                            var a = $(this).data("maptype");
                            houzez_change_map_type(houzezMap, a) }), $("#houzez-gmap-next").click(function() { houzez_map_next(houzezMap) }), $("#houzez-gmap-prev").click(function() { houzez_map_prev(houzezMap) }), remove_map_loader(houzezMap), a.getProperties === !0 ? (reloadMarkers(), houzezAddMarkers(a.properties, houzezMap), z.empty().html(a.propHtml), half_map_ajax_pagi(), houzez_infobox_trigger(), houzezMap.fitBounds(markers.reduce(function(a, b) {
                            return a.extend(b.getPosition()) }, new google.maps.LatLngBounds)), google.maps.event.trigger(houzezMap, "resize"), markerCluster = new MarkerClusterer(houzezMap, markers, { maxZoom: 12, gridSize: 60, styles: [{ url: clusterIcon, width: 48, height: 48, textColor: "#fff" }] })) : ($("#mapViewHalfListings").empty().html('<div class="map-notfound">' + not_found + "</div>"), z.empty().html('<div class="map-notfound">' + not_found + "</div>")), !1 }, error: function(a, b, c) { console.log(a.status), console.log(a.responseText), console.log(c) } }), !1 },
            houzez_search_on_change = function(a, b, c, d, e, f, g, h) {
                var i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D;
                return null != d && null != e ? (s = d, t = e) : b.hasClass("widget") || 0 != advanced_search_price_slide ? (s = a.find('input[name="min-price"]').val(), t = a.find('input[name="max-price"]').val()) : (s = a.find('select[name="min-price"]:not(:disabled)').val(), t = a.find('select[name="max-price"]:not(:disabled)').val()), j = a.find('select[name="state"]').val(), k = a.find('select[name="location"]').val(), "" != k && null != k && "undefined" != typeof k || (k = "all"), "yes" != f && (l = a.find('select[name="area"]').val()), "yes" == g && (l = "", k = "all"), "yes" == h && (j = "", l = "", k = "all"), i = a.find('select[name="country"]').val(), m = a.find('select[name="status"]').val(), n = a.find('select[name="type"]').val(), q = a.find('select[name="bedrooms"]').val(), r = a.find('select[name="bathrooms"]').val(), o = a.find('select[name="label"]').val(), p = a.find('input[name="property_id"]').val(), u = a.find('input[name="min-area"]').val(), v = a.find('input[name="max-area"]').val(), w = a.find('input[name="keyword"]').val(), x = a.find('input[name="publish_date"]').val(), D = a.find(".features-list input[type=checkbox]:checked").map(function(a, b) {
                    return $(b).val() }).toArray(), y = a.find('input[name="lat"]').val(), z = a.find('input[name="lng"]').val(), B = a.find('input[name="search_location"]').val(), A = "template/property-listings-map.php" == current_tempalte ? a.find('input[name="search_radius"]').val() : a.find('select[name="radius"]').val(), C = $(a.find('input[name="use_radius"]')).is(":checked") ? "on" : "off", "template/property-listings-map.php" == current_tempalte ? houzez_half_map_listings(w, i, j, k, l, m, n, o, p, q, r, s, t, u, v, D, x, search_no_posts, y, z, A, B, C, c) : houzez_header_listing_map(w, i, j, k, l, m, n, o, p, q, r, s, t, u, v, D, x, y, z, A, B, C), !1 },
            populate_area_dropdown = function(a) {
                var b;
                b = a.find('select[name="location"] option:selected').val(), "" != b ? (a.find('select[name="area"]').selectpicker("val", ""), a.find('select[name="area"] option').each(function() {
                    var a = $(this).data("parentcity"); "" != $(this).val() && $(this).css("display", "none"), a == b && $(this).css("display", "block") })) : (a.find('select[name="area"]').selectpicker("val", ""), a.find('select[name="area"] option').each(function() { $(this).css("display", "block") })), a.find('select[name="area"]').selectpicker("refresh") },
            populate_city_dropdown = function(a) {
                var b;
                b = a.find('select[name="state"] option:selected').val(), "" != b ? (a.find('select[name="location"], select[name="area"]').selectpicker("val", ""), a.find('select[name="location"] option').each(function() {
                    var a = $(this).data("parentstate"); "" != $(this).val() && $(this).css("display", "none"), a == b && $(this).css("display", "block") })) : (a.find('select[name="location"], select[name="area"]').selectpicker("val", ""), a.find('select[name="location"] option').each(function() { $(this).css("display", "block") }), a.find('select[name="area"] option').each(function() { $(this).css("display", "block") })), a.find('select[name="location"], select[name="area"]').selectpicker("refresh") },
            populate_state_dropdown = function(a) {
                var b;
                b = a.find('select[name="country"] option:selected').val(), "" != b ? (a.find('select[name="location"], select[name="area"], select[name="state"]').selectpicker("val", ""), a.find('select[name="state"] option').each(function() {
                    var a = $(this).data("parentcountry"); "undefined" != typeof a && (a = a.toUpperCase()), "" != $(this).val() && $(this).css("display", "none"), a == b && $(this).css("display", "block") })) : (a.find('select[name="location"], select[name="area"], select[name="state"]').selectpicker("val", ""), a.find('select[name="state"] option').each(function() { $(this).css("display", "block") }), a.find('select[name="area"] option').each(function() { $(this).css("display", "block") })), a.find('select[name="location"], select[name="area"], select[name="state"]').selectpicker("refresh") };
        if ($("#houzez-listing-map").length > 0 || $("#mapViewHalfListings").length > 0) {
            var current_page = 0;
            if ($('select[name="area"], select[name="label"], select[name="bedrooms"], select[name="bathrooms"], select[name="min-price"], select[name="max-price"], input[name="min-price"], input[name="max-price"], input[name="min-area"], input[name="max-area"], select[name="type"], input[name="property_id"]').on("change", function() {
                    var a = $(this).parents("form"),
                        b = $(this).parents(".widget_houzez_advanced_search");
                    houzez_search_on_change(a, b, current_page) }), $('input[name="keyword"]').on("change", function() {
                    var a = $(this).parents("form"),
                        b = $(this).parents(".widget_houzez_advanced_search");
                    setTimeout(function() { houzez_search_on_change(a, b, current_page) }, 100) }), $("input.search_location").geocomplete({ details: "form", country: houzez_geocomplete_country, geocodeAfterResult: !0 }).bind("geocode:result", function(a, b) {
                    var c = $(this).parents("form"),
                        d = $(this).parents(".widget_houzez_advanced_search");
                    houzez_search_on_change(c, d, current_page), console.log(b) }), $("#half_map_update").on("click", function(a) { a.preventDefault();
                    var b = $(this).parents("form"),
                        c = $(this).parents(".widget_houzez_advanced_search");
                    houzez_search_on_change(b, c, current_page) }), $('select[name="radius"]').on("change", function() {
                    var a = $(this).parents("form"),
                        b = $(this).parents(".widget_houzez_advanced_search");
                    houzez_search_on_change(a, b, current_page) }), $('select[name="country"]').on("change", function() {
                    var a = $(this).parents("form"),
                        b = $(this).parents(".widget_houzez_advanced_search"),
                        c = "yes";
                    houzez_search_on_change(a, b, current_page, "", "", "", "", c), populate_state_dropdown(a) }), $('select[name="state"]').on("change", function() {
                    var a = $(this).parents("form"),
                        b = $(this).parents(".widget_houzez_advanced_search"),
                        c = "yes";
                    houzez_search_on_change(a, b, current_page, "", "", "", c), populate_city_dropdown(a) }), $('select[name="location"]').on("change", function() {
                    var a = $(this).parents("form"),
                        b = $(this).parents(".widget_houzez_advanced_search"),
                        c = "yes";
                    houzez_search_on_change(a, b, current_page, "", "", c), populate_area_dropdown(a) }), $('input[name="feature[]"]').on("change", function() {
                    var a = $(this).parents("form"),
                        b = $(this).parents(".widget_houzez_advanced_search");
                    houzez_search_on_change(a, b, current_page) }), $(".search-date").on("change", function(a) {
                    var b = $(this).parents("form"),
                        c = $(this).parents(".widget_houzez_advanced_search");
                    houzez_search_on_change(b, c, current_page) }), $('select[name="status"]').on("change", function() {
                    var a = $(this).parents("form"),
                        b = $(this).parents(".widget_houzez_advanced_search"),
                        c = $(this).val();
                    c == rent_status_for_price_range ? 0 != advanced_search_price_slide ? houzez_search_on_change(a, b, current_page, advanced_search_price_range_min_rent, advanced_search_price_range_max_rent) : houzez_search_on_change(a, b, current_page) : 0 != advanced_search_price_slide ? houzez_search_on_change(a, b, current_page, advanced_search_price_range_min, advanced_search_price_range_max) : houzez_search_on_change(a, b, current_page) }), "template/property-listings-map.php" == current_tempalte) {
                var state = HOUZEZ_ajaxcalls_vars.search_state,
                    country = HOUZEZ_ajaxcalls_vars.search_country,
                    keyword = HOUZEZ_ajaxcalls_vars.search_keyword,
                    location = HOUZEZ_ajaxcalls_vars.search_city,
                    features = HOUZEZ_ajaxcalls_vars.search_feature,
                    area = HOUZEZ_ajaxcalls_vars.search_area,
                    status = HOUZEZ_ajaxcalls_vars.search_status,
                    type = HOUZEZ_ajaxcalls_vars.search_type,
                    label = HOUZEZ_ajaxcalls_vars.search_label,
                    property_id = HOUZEZ_ajaxcalls_vars.property_id,
                    bedrooms = HOUZEZ_ajaxcalls_vars.search_bedrooms,
                    bathrooms = HOUZEZ_ajaxcalls_vars.search_bathrooms,
                    min_price = HOUZEZ_ajaxcalls_vars.search_min_price,
                    max_price = HOUZEZ_ajaxcalls_vars.search_max_price,
                    min_area = HOUZEZ_ajaxcalls_vars.search_min_area,
                    max_area = HOUZEZ_ajaxcalls_vars.search_max_area,
                    publish_date = HOUZEZ_ajaxcalls_vars.search_publish_date,
                    search_no_posts = HOUZEZ_ajaxcalls_vars.search_no_posts,
                    search_lat = HOUZEZ_ajaxcalls_vars.search_lat,
                    search_long = HOUZEZ_ajaxcalls_vars.search_long,
                    search_radius = HOUZEZ_ajaxcalls_vars.search_radius,
                    search_location = HOUZEZ_ajaxcalls_vars.search_location,
                    use_radius = HOUZEZ_ajaxcalls_vars.use_radius;
                houzez_half_map_listings(keyword, country, state, location, area, status, type, label, property_id, bedrooms, bathrooms, min_price, max_price, min_area, max_area, features, publish_date, search_no_posts, search_lat, search_long, search_radius, search_location, use_radius) } else houzez_header_listing_map() } else $('select[name="country"]').on("change", function() {
            var a = $(this).parents("form");
            populate_state_dropdown(a) }), $('select[name="location"]').on("change", function() {
            var a = $(this).parents("form");
            populate_area_dropdown(a) }), $('select[name="state"]').on("change", function() {
            var a = $(this).parents("form");
            populate_city_dropdown(a) }), $("input.search_location").length > 0 && $("input.search_location").geocomplete({ details: "form", country: houzez_geocomplete_country, geocodeAfterResult: !0 });
        var remove_map_loader = function(a) { google.maps.event.addListener(a, "tilesloaded", function() { jQuery("#houzez-map-loading").hide() }) },
            price_range_main_search = function(a, b) {
                if ($(".price-range-advanced").slider({ range: !0, min: a, max: b, values: [a, b], slide: function(a, b) {
                            if ("after" == currency_position) var c = addCommas(b.values[0]) + currency_symb,
                                d = addCommas(b.values[1]) + currency_symb;
                            else var c = currency_symb + numDifferentiation(b.values[0]),
                                d = currency_symb + numDifferentiation(b.values[1]);

                            $(".min-price-range-hidden").val(currency_symb + addCommas(b.values[0])),

                            $(".max-price-range-hidden").val(currency_symb + addCommas(b.values[1])), $(".min-price-range").text(c), 

                            $(".max-price-range").text(d),
                            console.log(d)

                             }, stop: function(a, b) {
                            if ($("#houzez-listing-map").length > 0 || $("#mapViewHalfListings").length > 0) {
                                var c = 0,
                                    d = $(this).parents("form"),
                                    e = $(this).parents("form");
                                houzez_search_on_change(d, e, c) } } }), "after" == currency_position) var c = addCommas($(".price-range-advanced").slider("values", 0)) + currency_symb,
                    d = addCommas($(".price-range-advanced").slider("values", 1)) + currency_symb;
                else var c = currency_symb + numDifferentiation($(".price-range-advanced").slider("values", 0)),
                    d = currency_symb + numDifferentiation($(".price-range-advanced").slider("values", 1));
                $(".min-price-range-hidden").val(currency_symb + addCommas($(".price-range-advanced").slider("values", 0))), 
                $(".max-price-range-hidden").val(currency_symb + addCommas($(".price-range-advanced").slider("values", 1))), 
                $(".min-price-range").text(c), $(".max-price-range").text(d) };
        $(".price-range-advanced").length > 0 && price_range_main_search(advanced_search_price_range_min, advanced_search_price_range_max), $('.houzez-adv-price-range select[name="status"]').on("change", function() {
            var a = $(this).val();
            a == rent_status_for_price_range ? price_range_main_search(advanced_search_price_range_min_rent, advanced_search_price_range_max_rent) : price_range_main_search(advanced_search_price_range_min, advanced_search_price_range_max) });
        var selected_status_adv_search = $('.houzez-adv-price-range select[name="status"]').val();
        selected_status_adv_search == rent_status_for_price_range && price_range_main_search(advanced_search_price_range_min_rent, advanced_search_price_range_max_rent);
        var price_range_widget = function(a, b) { $("#slider-price").slider({ range: !0, min: a, max: b, values: [a, b], slide: function(a, b) { $("#min-price").val(currency_symb + addCommas(b.values[0])), $("#max-price").val(currency_symb + addCommas(b.values[1])) }, stop: function(a, b) {
                    if ($("#houzez-listing-map").length > 0) {
                        var c = $(this).parents("form"),
                            d = $(this).parents(".widget_houzez_advanced_search");
                        houzez_search_on_change(c, d) } } }), $("#min-price").val(currency_symb + addCommas($("#slider-price").slider("values", 0))), $("#max-price").val(currency_symb + addCommas($("#slider-price").slider("values", 1))) };
        $("#slider-price").length > 0 && price_range_widget(advanced_search_price_range_min, advanced_search_price_range_max), $("#widget_status").on("change", function() {
            var a = $(this).val();
            a == rent_status_for_price_range ? price_range_widget(advanced_search_price_range_min_rent, advanced_search_price_range_max_rent) : price_range_widget(advanced_search_price_range_min, advanced_search_price_range_max) });
        var selected_status_widget_search = $("#widget_status").val();
        selected_status_widget_search == rent_status_for_price_range && price_range_widget(advanced_search_price_range_min_rent, advanced_search_price_range_max_rent), $("#slider-size").length > 0 && ($("#slider-size").slider({ range: !0, min: advanced_search_widget_min_area, max: advanced_search_widget_max_area, values: [advanced_search_widget_min_area, advanced_search_widget_max_area], slide: function(a, b) { $("#min-size").val(b.values[0] + " " + measurement_unit), $("#max-size").val(b.values[1] + " " + measurement_unit) }, stop: function(a, b) {
                if ($("#houzez-listing-map").length > 0) {
                    var c = 0,
                        d = $(this).parents("form"),
                        e = $(this).parents(".widget_houzez_advanced_search");
                    houzez_search_on_change(d, e, c) } } }), $("#min-size").val($("#slider-size").slider("values", 0) + " " + measurement_unit), $("#max-size").val($("#slider-size").slider("values", 1) + " " + measurement_unit));
        var radius_search_slider = function(a) {
            $("#radius-range-slider").slider({
                value: a,
                min: 0,
                max: 100,
                step: 1,
                slide: function(a, b) {
                    $("#radius-range-text").html(b.value),
                        $("#radius-range-value").val(b.value)
                },
                stop: function(a, b) {
                    if ($("#houzez-listing-map").length > 0 || $("#mapViewHalfListings").length > 0) {
                        var c = 0,
                            d = $(this).parents("form"),
                            e = $(this).parents("form");
                        houzez_search_on_change(d, e, c) } }
            }), $("#radius-range-text").html($("#radius-range-slider").slider("value")), $("#radius-range-value").val($("#radius-range-slider").slider("value"))
        };
        $("#radius-range-slider").length > 0 && radius_search_slider(houzez_default_radius);
        var houzez_infobox_trigger = function() {
                return $(".infobox_trigger").each(function(a) { $(this).on("mouseenter", function() { houzezMap && google.maps.event.trigger(markers[a], "click") }), $(this).on("mouseleave", function() { infobox.open(null, null) }) }), !1 },
            currencySwitcherList = $("#houzez-currency-switcher-list");
        currencySwitcherList.length > 0 && ($("#houzez-currency-switcher-list > li").click(function(e) { e.stopPropagation(), currencySwitcherList.slideUp(200);
            var selectedCurrencyCode = $(this).data("currency-code");
            if (selectedCurrencyCode) { $("#houzez-selected-currency span").html(selectedCurrencyCode), $("#houzez-switch-to-currency").val(selectedCurrencyCode);
                var security = $("#currency_switch_security").val(),
                    houzez_switch_to_currency = $("#houzez-switch-to-currency").val();
                fave_processing_modal('<i class="' + process_loader_spinner + '"></i> ' + currency_updating_msg), $.ajax({ url: ajaxurl, dataType: "JSON", method: "POST", data: { action: "houzez_switch_currency", switch_to_currency: houzez_switch_to_currency, security: security }, success: function(a) { a.success ? window.location.reload() : console.log(a) }, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) } }), $("#houzez-selected-currency").click(function(a) { currencySwitcherList.slideToggle(200), a.stopPropagation() }), $("html").click(function() { currencySwitcherList.slideUp(100) }));
        var areaSwitcherList = $("#houzez-area-switcher-list");
        if (areaSwitcherList.length > 0 && ($("#houzez-area-switcher-list > li").click(function(e) { e.stopPropagation(), areaSwitcherList.slideUp(200);
                var selectedAreaCode = $(this).data("area-code"),
                    houzez_switch_area_text = $("#houzez_switch_area_text").val();
                if (selectedAreaCode) { $("#houzez-selected-area span").html(houzez_switch_area_text), $("#houzez-switch-to-area").val(selectedAreaCode);
                    var security = $("#area_switch_security").val(),
                        houzez_switch_to_area = $("#houzez-switch-to-area").val();
                    fave_processing_modal('<i class="' + process_loader_spinner + '"></i> ' + measurement_updating_msg), $.ajax({ url: ajaxurl, dataType: "JSON", method: "POST", data: { action: "houzez_switch_area", switch_to_area: houzez_switch_to_area, security: security }, success: function(a) { a.success ? window.location.reload() : console.log(a) }, error: function(xhr, status, error) {
                            var err = eval("(" + xhr.responseText + ")");
                            console.log(err.Message) } }) } }), $("#houzez-selected-area").click(function(a) { areaSwitcherList.slideToggle(200), a.stopPropagation() }), $("html").click(function() { areaSwitcherList.slideUp(100) })), 0 != keyword_autocomplete) {
            var houzezAutoComplete = function() {
                var a = 0,
                    b = $(".auto-complete");
                $('input[name="keyword"]').keyup(function() {
                    var b = $(this),
                        c = b.parents("form"),
                        d = c.find(".auto-complete");
                    $(this).val().length >= 2 ? (d.fadeIn(), $.ajax({ type: "POST", url: ajaxurl, data: { action: "houzez_get_auto_complete_search", key: $(this).val() }, beforeSend: function() { a++, 1 == a && d.html('<div class="result"><p><i class="fa fa-spinner fa-spin fa-fw"></i> Searching </p></div>') }, success: function(b) { a--, 0 == a && (d.show(), "" != b && d.empty().html(b).bind()) }, error: function(b) { a--, 0 == a && d.html('<div class="result"><p><i class="fa fa-spinner fa-spin fa-fw"></i> Searching </p></div>') } })) : d.fadeOut() }), b.on("click", "li", function() { $('input[name="keyword"]').val($(this).data("text")), b.fadeOut() }).bind() };
            houzezAutoComplete() }
        $(".start_thread_form").click(function(e) { e.preventDefault();
            var $this = $(this),
                $form = $this.parents("form"),
                $result = $form.find(".form_messages");
            $.ajax({ url: ajaxurl, data: $form.serialize(), method: $form.attr("method"), dataType: "JSON", beforeSend: function() { $this.children("i").remove(), $this.prepend('<i class="fa-left ' + process_loader_spinner + '"></i>') }, success: function(a) { a.success ? ($result.empty().append(a.msg), $form.find("input").val(""), $form.find("textarea").val("")) : $result.empty().append(a.msg) }, error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message) }, complete: function() { $this.children("i").removeClass(process_loader_spinner), $this.children("i").addClass(success_icon) } }) });
        var houzez_message_notifications = function() { $.ajax({ url: ajaxurl, data: { action: "houzez_chcek_messages_notifications" }, method: "POST", dataType: "JSON", beforeSend: function() {}, success: function(a) { a.success && (a.notification ? ($(".user-alert").show(), $(".msg-alert").show()) : ($(".user-alert").hide(), $(".msg-alert").hide())) } }) };
        $(document).ready(function() { houzez_message_notifications(), setInterval(function() { houzez_message_notifications() }, 1e4) }), $(".start_thread_message_form").click(function(a) { a.preventDefault();
            var b = $(this),
                c = b.parents("form");
            c.find(".form_messages");
            $.ajax({ url: ajaxurl, data: c.serialize(), method: c.attr("method"), dataType: "JSON", beforeSend: function() { b.children("i").remove(), b.prepend('<i class="fa-left ' + process_loader_spinner + '"></i>') }, success: function(a) { window.location.replace(a.url) }, complete: function() { b.children("i").removeClass(process_loader_spinner), b.children("i").addClass(success_icon) } }) });
        var agency_listings_ajax_pagi = function() {
            return $("body.single-houzez_agency ul.pagination li a").click(function(e) { e.preventDefault();
                var current_page = $(this).data("houzepagi"),
                    agency_id_pagi = $("#agency_id_pagi").val(),
                    ajax_container = $("#houzez_ajax_container");
                $.ajax({ url: ajaxurl, method: "POST", data: { action: "houzez_ajax_agency_filter", paged: current_page, agency_id: agency_id_pagi }, beforeSend: function() { ajax_container.empty().append('<div class="list-loading"><div class="list-loading-bar"></div><div class="list-loading-bar"></div><div class="list-loading-bar"></div><div class="list-loading-bar"></div></div>') }, success: function(a) { ajax_container.empty().html(a), agency_listings_ajax_pagi() }, complete: function() {}, error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message) } }) }), !1 };
        if ($("body.single-houzez_agency").length > 0 && agency_listings_ajax_pagi(), "yes" == is_singular_property) {
            if ("v3" == property_detail_top) var houzezSlidesToShow = "8";
            else var houzezSlidesToShow = "11";
            var gallery_autoplay = HOUZEZ_ajaxcalls_vars.gallery_autoplay;
            gallery_autoplay = "1" === gallery_autoplay;
            var houzez_detail_slider_main_settings = function() {
                    return { speed: 500, autoplay: gallery_autoplay, autoplayHoverPause: !0, autoplaySpeed: 4e3, rtl: houzez_rtl, slidesToShow: 1, slidesToScroll: 1, arrows: !0, asNavFor: ".slider-thumbs" } },
                houzez_detail_slider_nav_settings = function() {
                    return { speed: 500, autoplay: !1, autoplaySpeed: 4e3, rtl: houzez_rtl, slidesToShow: houzezSlidesToShow, slidesToScroll: 1, asNavFor: ".detail-slider", arrows: !1, dots: !1, centerMode: !0, focusOnSelect: !0, responsive: [{ breakpoint: 991, settings: { slidesToShow: 8 } }, { breakpoint: 767, settings: { slidesToShow: 4 } }] } },
                property_detail_slideshow = function() { $(".detail-slider").slick(houzez_detail_slider_main_settings()), $(".slider-thumbs").slick(houzez_detail_slider_nav_settings()) };
            property_detail_slideshow() }
        if ("yes" == is_singular_property) { $("#property-rating").rating({ step: .5, showClear: !1 }), $(".rating-display-only").rating({ disabled: !0, showClear: !1 }), $(".property_rating").click(function(a) { a.preventDefault();
                var b = $(this),
                    c = b.parents("form");
                c.find(".form_messages");
                $.ajax({ url: ajaxurl, data: c.serialize(), method: c.attr("method"), dataType: "JSON", beforeSend: function() { b.children("i").remove(), b.prepend('<i class="fa-left ' + process_loader_spinner + '"></i>') }, success: function(a) { window.location.reload() }, complete: function() { b.children("i").removeClass(process_loader_spinner), b.children("i").addClass(success_icon) } }) });
            var tabsHeight = function() { jQuery("#singlePropertyMap,#street-map").css("min-height", jQuery(".detail-media #gallery").innerHeight()) };
            if (jQuery(window).on("load", function() { tabsHeight() }), jQuery(window).on("resize", function() { tabsHeight() }), jQuery('a[href="#gallery"]').on("shown.bs.tab", function(a) { $(".detail-slider").slick("unslick"), $(".slider-thumbs").slick("unslick"), $(".detail-slider").slick(houzez_detail_slider_main_settings()), $(".slider-thumbs").slick(houzez_detail_slider_nav_settings()) }), 0 != property_map) {
                var map = null,
                    panorama = null,
                    fenway = new google.maps.LatLng(prop_lat, prop_lng),
                    mapOptions = { center: fenway, zoom: 15, scrollwheel: !1 },
                    panoramaOptions = { position: fenway, pov: { heading: 34, pitch: 10 } },
                    initialize = function() { map = new google.maps.Map(document.getElementById("singlePropertyMap"), mapOptions), $("#street-map").length > 0 && (panorama = new google.maps.StreetViewPanorama(document.getElementById("street-map"), panoramaOptions));
                        var a = $("#securityHouzezMap").val();
                        $.ajax({ type: "POST", dataType: "json", url: ajaxurl, data: { action: "houzez_get_single_property", prop_id: $("#prop_id").val(), security: a }, success: function(a) {
                                if ("" !== google_map_style) {
                                    var b = JSON.parse(google_map_style);
                                    map.setOptions({ styles: b }) }
                                a.getprops === !0 && (houzezAddMarkers(a.props, map), houzezSetPOIControls(map, map.getCenter())) }, error: function(a) {} }) };
                jQuery('a[href="#singlePropertyMap"]').on("shown.bs.tab", function(a) { google.maps.event.trigger(map, "resize"), map.setCenter(fenway) }), jQuery('a[href="#street-map"]').on("shown.bs.tab", function(a) { fenway = panorama.getPosition(), panoramaOptions.position = fenway, panorama = new google.maps.StreetViewPanorama(document.getElementById("street-map"), panoramaOptions) }), google.maps.event.addDomListener(window, "load", initialize) }
            $(".houzez-gallery-prop-v2:first a[rel^='prettyPhoto']").prettyPhoto({ animation_speed: "normal", slideshow: 5e3, autoplay_slideshow: !1, allow_resize: !0, keyboard_shortcuts: !0, theme: "pp_default" }) }
    }
});
