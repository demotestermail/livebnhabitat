<div class="clearfix"></div>
<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content.
 *
 * @package Houzez
 * @since Houzez 1.0
 */
$copy_rights = houzez_option('copy_rights');
?>
    <?php if ( !is_page_template( 'template/template-splash.php' ) ) { ?>
        <?php if( !is_singular( 'property' ) ) { ?>
            </div>
            <!--Start in header-->
            <?php } ?>
                </div>
                <!--Start in header end #section-body-->
                <!--start footer section-->
                <footer id="footer-section">
                    <?php get_template_part('template-parts/footer'); ?>
                        <!-- <div class="footer-bottom">

      <div class="container">
           <?php include('js/jquery/footer.php'); ?>
                <div class="col-md-6 col-sm-6">
                    <div class="footer-col">
                        <div class="navi">
                          <?php
              // Pages Menu
              if ( has_nav_menu( 'footer-menu' ) ) :
                wp_nav_menu( array (
                  'theme_location' => 'footer-menu',
                  'container' => '',
                  'container_class' => '',
                  'menu_class' => '',
                  'menu_id' => 'footer-menu',
                  'depth' => 1
                ));
              endif;
              ?>
            </div>

                    </div>
                </div>
                <?php if( houzez_option('social-footer') != '0' ): ?>
                <div class="col-md-3 col-sm-3">
                    <div class="footer-col foot-social">
                        <p>
                            <?php esc_html_e( 'Follow us', 'houzez' ); ?>
                           
                            <?php if( houzez_option('fs-facebook') != '' ){ ?>
                    <a target="_blank" href="<?php echo esc_url(houzez_option('fs-facebook')); ?>"><i class="fa fa-facebook-square"></i></a>
                  <?php } ?>

                  <?php if( houzez_option('fs-twitter') != '' ){ ?>
                      <a target="_blank" href="<?php echo esc_url(houzez_option('fs-twitter')); ?>"><i class="fa fa-twitter-square"></i></a>
                  <?php } ?>

                  <?php if( houzez_option('fs-linkedin') != '' ){ ?>
                      <a target="_blank" href="<?php echo esc_url(houzez_option('fs-linkedin')); ?>"><i class="fa fa-linkedin-square"></i></a>
                  <?php } ?>

                  <?php if( houzez_option('fs-googleplus') != '' ){ ?>
                      <a target="_blank" href="<?php echo esc_url(houzez_option('fs-googleplus')); ?>"><i class="fa fa-google-plus-square"></i></a>
                  <?php } ?>

                  <?php if( houzez_option('fs-instagram') != '' ){ ?>
                      <a target="_blank" href="<?php echo esc_url(houzez_option('fs-instagram')); ?>"><i class="fa fa-instagram"></i></a>
                  <?php } ?>
                        </p>
                    </div>
                </div>
                <?php endif; ?>

            </div>
        </div>--></div>
                        <!-- End footer bottom -->
                </footer>
                <!--end footer section-->
                <?php } // End splash template if ?>
                    <?php wp_footer(); ?>
                        <!-- Summary of Unit in Resale of listing page -->
                        <div class="modal fade modal01" id="myModa2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-dialog01" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Summary of Unit in Resale</h4> </div>
                                    <div class="modal-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <th>Variant </th>
                                                    <th>Size</th>
                                                    <th>Demand</th>
                                                    <th align="center">Negotiable</th>
                                                    <th></th>
                                                </tr>
                                                <tr>
                                                    <td>2BHK</td>
                                                    <td>1200 SQFT</td>
                                                    <td><i class="fa fa-inr"></i> 40.00.000</td>
                                                    <td align="center">yes</td>
                                                    <td><a href="#">View Details</a></td>
                                                </tr>
                                                <tr>
                                                    <td>3BHK</td>
                                                    <td>1700 SQFT</td>
                                                    <td><i class="fa fa-inr"></i> 50.00.000</td>
                                                    <td align="center">No</td>
                                                    <td><a href="#">View Details</a></td>
                                                </tr>
                                                <tr>
                                                    <td>2BHK</td>
                                                    <td>1200 SQFT</td>
                                                    <td><i class="fa fa-inr"></i> 40.00.000</td>
                                                    <td align="center">Yes</td>
                                                    <td><a href="#">View Details</a></td>
                                                </tr>
                                                <tr>
                                                    <td>4BHK</td>
                                                    <td>2280 SQFT</td>
                                                    <td><i class="fa fa-inr"></i> 60.00.000</td>
                                                    <td align="center">Yes</td>
                                                    <td><a href="#">View Details</a></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> --></div>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                var carousel = $(".slider_item");
                                carousel.owlCarousel({
                                    items: 2
                                    , navigation: false
                                    , margin: 15
                                    , responsive: {
                                        0: {
                                            items: 1
                                            , nav: true
                                        }
                                        , 600: {
                                            items: 2
                                            , nav: false
                                        }
                                        , 1000: {
                                            items: 2
                                            , nav: true
                                            , loop: false
                                        }
                                    }
                                    /* navigationText: [
                                     "<i class='fa fa-chevron-left'></i>",
                                     "<i class='fa fa-chevron-right'></i>" 
                                     ],*/
                                });
                            });
                        </script>
                        <script>
$(document).ready(function(){
    var property_tool_t= $('.property_tool_t');
    for (var i = 0; i < property_tool_t.length; i++) {
      $(property_tool_t[i]).data('bs.popover')
      .tip()
      .addClass('my-prop-unit');
    }
});
</script>
<script>

$(".add_fav").click(function(){
  if ( $(this).hasClass("fa-heart-o") ) {
     $(this).addClass("fa-heart");
     $(this).removeClass("fa-heart-o");
  } else{
    $(this).addClass("fa-heart-o");
     $(this).removeClass("fa-heart");
  }
});
</script>
<script>
  $( "li .text" ).each(function( index ) {
if($(this).text().substring(0,1)!="-"){
 $(this).html('<b>'+$(this).text()+'</b>');
  }
});

</script>
<script>
   $( ".selectpicker" ).change(function() {
  var sizes = ["plot", "plot-commercial", "office-space", "plot-industrial", "retail-space", "service-apartment"]; 
  var selected_val =$(this).val();
  if(jQuery.inArray(selected_val, sizes) !== -1){
  $('.range-bedrooms-main').hide();
  $('.range-area-main').show();
  }
  else{
    $('.range-bedrooms-main').show();
    $('.range-area-main').hide();
    
  }
});
</script>
</body>

</html>