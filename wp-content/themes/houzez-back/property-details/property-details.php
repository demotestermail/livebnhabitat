<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 08/01/16
 * Time: 4:23 PM
 */
global $post_meta_data;
$FAQs = get_post_meta( get_the_ID(), 'FAQs', true );
 $post_meta=get_post_meta( get_the_ID());

$prop_id = get_post_meta( get_the_ID(), 'fave_property_id', true );
$amenities = get_post_meta( get_the_ID(), 'amenities', true );
$projectId=$amenities[0]['ProjectId'];
$prop_price = get_post_meta( get_the_ID(), 'fave_property_price', true );
$prop_size = get_post_meta( get_the_ID(), 'fave_property_size', true );
$bedrooms = get_post_meta( get_the_ID(), 'fave_property_bedrooms', true );
$bathrooms = get_post_meta( get_the_ID(), 'fave_property_bathrooms', true );
$year_built = get_post_meta( get_the_ID(), 'fave_property_year', true );
$garage = get_post_meta( get_the_ID(), 'fave_property_garage', true );
$garage_size = get_post_meta( get_the_ID(), 'fave_property_garage_size', true );
$additional_features_enable = get_post_meta( get_the_ID(), 'fave_additional_features_enable', true );
$additional_features = get_post_meta( get_the_ID(), 'additional_features', true );
$specifications = get_post_meta( get_the_ID(), 'fave_specifications', true );
$status = get_the_terms(get_the_ID(),'property_status',true);
$array_status=array();
 foreach ($status as  $status) {
     $array_status[]=$status->slug;
 }

$prop_details = false;
$towers=get_post_meta( get_the_ID(), 'Towers', true );
if( !empty( $prop_id ) ||
    !empty( $prop_price ) ||
    !empty( $prop_size ) ||
    !empty( $bedrooms ) ||
    !empty( $bathrooms ) ||
    !empty( $year_built ) ||
    !empty( $garage )
) {
    $prop_details = true;
}

$hide_detail_prop_fields = houzez_option('hide_detail_prop_fields');

if( $prop_details ) { 
?>
    <!-- <div id="detail" class="detail-list detail-block">
    <div class="detail-title">
        <h2 class="title-left"><?php esc_html_e( 'Detail', 'houzez' ); ?></h2>

        <?php if( $hide_detail_prop_fields['updated_date'] != 1 ) { ?>
        <div class="title-right">
            <p><?php esc_html_e( 'Updated on', 'houzez' ); ?> <?php the_modified_time('F j, Y'); ?> <?php esc_html_e( 'at', 'houzez' ); ?> <?php the_modified_time('g:i a'); ?> </p>
        </div>
        <?php } ?>

    </div>
    <div class="alert alert-info">
        <ul class="list-three-col">
            <?php
            if( !empty( $prop_id ) && $hide_detail_prop_fields['prop_id'] != 1 ) {
                echo '<li><strong>'.esc_html__( 'Property ID:', 'houzez').'</strong> '.esc_attr( $prop_id ).'</li>';
            }
            if( !empty( $prop_price ) && $hide_detail_prop_fields['sale_rent_price'] != 1 ) {
                echo '<li><strong>'.esc_html__( 'Price:', 'houzez'). '</strong> '.houzez_listing_price().'</li>';
            }
            if( !empty( $prop_size ) && $hide_detail_prop_fields['area_size'] != 1 ) {
                echo '<li><strong>'.esc_html__( 'Property Size:', 'houzez'). '</strong> '.houzez_property_size( 'after' ).'</li>';
            }
            if( !empty( $bedrooms ) && $hide_detail_prop_fields['bedrooms'] != 1 ) {
                echo '<li><strong>'.esc_html__( 'Bedrooms:', 'houzez').'</strong> '.esc_attr( $bedrooms ).'</li>';
            }
            if( !empty( $bathrooms ) && $hide_detail_prop_fields['bathrooms'] != 1 ) {
                echo '<li><strong>'.esc_html__( 'Bathrooms:', 'houzez').'</strong> '.esc_attr( $bathrooms ).'</li>';
            }
            if( !empty( $garage ) && $hide_detail_prop_fields['garages'] != 1 ) {
                echo '<li><strong>'.esc_html__( 'Garage:', 'houzez').'</strong> '.esc_attr( $garage ).'</li>';
            }
            if( !empty( $garage_size ) && $hide_detail_prop_fields['garages'] != 1 ) {
                echo '<li><strong>'.esc_html__( 'Garage Size:', 'houzez').'</strong> '.esc_attr( $garage_size ).'</li>';
            }
            if( !empty( $year_built ) && $hide_detail_prop_fields['year_built'] != 1 ) {
                echo '<li><strong>'.esc_html__( 'Year Built:', 'houzez').'</strong> '.esc_attr( $year_built ).'</li>';
            }
            ?>
        </ul>
    </div>

    <?php if( $additional_features_enable != 'disable' && !empty( $additional_features ) && $hide_detail_prop_fields['additional_details'] != 1 ) { ?>
        <div class="detail-title-inner">
            <h4 class="title-inner"><?php esc_html_e( 'Additional details', 'houzez' ); ?></h4>
        </div>
        <ul class="list-three-col">
            <?php
            foreach( $additional_features as $ad_del ):
                echo '<li><strong>'.esc_attr( $ad_del['fave_additional_feature_title'] ).':</strong> '.esc_attr( $ad_del['fave_additional_feature_value'] ).'</li>';
            endforeach;
            ?>
        </ul>
    <?php } ?>
</div> -->
<?php if(trim($specifications)!="Specifications not found!") {?>
    <div id="specifications" class="location-advantages">
        <?php echo $specifications; ?>
    </div>
    <?php } ?>
    <?php if (!empty($projectId) && in_array('developer-project',$array_status)) {?>
    <div id="price_units" class="location-advantages unit-variants mt20">
        <div class="accord-block">
            <div class="table-responsive">
                <table class="table_2">
                    <tr class="tr_table_90">
                        <td>
                            <h5>Unit & Variants</h5></td>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="row">
                <?php  if(is_user_logged_in()) { ?>
                <div class="unit_vari col-md-12 col-xs-12 ">
                    <iframe onload="resizeIframe(this)" name="frame" id="frame" src="http://catalystadminqa.realsynergy.in/Pricedetail/<?=$projectId;?>/" allowtransparency="true" frameborder="0" width="100%" height="1055px"></iframe>
                </div>
                <?php } else { ?>
                <div class="login_regsiter">
                       <a href="#" data-toggle="modal" data-target="#pop-login">
                       <i class="fa fa-user hidden-md hidden-lg"></i>
                       <span class="hidden-sm hidden-xs">Login / Register</span>
                       </a>    
                   </div>
                <div class=" col-md-12 col-xs-12 blur_block_unit">
                    <img src="<?=get_template_directory_uri()?>/images/unit-price.png">
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if(count($towers)>0) { ?>
    <div id="tower-details" class="property-description box_shadow_col overview_sect mt20 tower-details">
        <div class="row">
            <h2 class="title-left col-md-12">Tower Details</h2>
            <div class="slider_item">
                <?php 
    foreach ($towers as $single_tower) {
        $tower_name=$single_tower['TowerName'];
        $tower_stories=$single_tower['Stories'];
        $tower_basments=$single_tower['Basement'];
         $tower_fooer_type=$single_tower['floorType'];
        ?>
                <div class="item">
                    <div class="tower_d_col">
                        <div class="tower_d_col1">
                            <div class="col-sm-6">
                                <div class="img_colt"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/tabre-.png" /></div>
                            </div>
                            <div class="col-sm-6">
                                <div class="text_colt">
                                    <h6><?=$tower_name?></h6>
                                    <div class="table-responsive">
                                        <table>
                                            <tr>
                                                <td>No. Of Stories </td>
                                                <td>
                                                    <?=$tower_stories?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Basements</td>
                                                <td>
                                                    <?=$tower_basments?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Floor Type</td>
                                                <td>
                                                    <?=$tower_fooer_type?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
    }



    ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <div id="finance" class="property-description box_shadow_col overview_sect mt20">
        <div class="row">
            <h2 class="title-left">Finance</h2>
            <ul class="finance_logos">
                <?php
$banks_data=wp_get_object_terms( get_the_ID(), 'bank');
foreach ($banks_data as $bank) {
    $bank_logo = get_term_meta($bank->term_id,'wpcf-bank-logo',true);
    echo '<li><img src="'.$bank_logo.'" alt="'.$bank->name.'"></li>';
}

 ?>
            </ul>
        </div>
        <!-- Finace Calculator -->
          <div class="row">
             <div class="tab-content" style="padding: 20px;">
                    <div role="tabpanel" class="tab-pane active" id="personalLoan">
                    <ul class="loan">
                        <li>
                          
                            <span class="rangeSliderHeadding">Loan Amount</span>
                             <div class="form_input_icon"> 
                            <input class="form-control" id="loanAmountCount" name="loanAmountCount" type="text">
                             <span class="input-group-addon">₹</span>  
                            </div>
                            <div class="rangeSlider">
                                <div id="loanAmount"></div>
                            </div>
                            <p class="amount loan"><input type="text" id="loanAmountCount" readonly/>

                            </p>
                        </li>

                        <li>
                            <span class="rangeSliderHeadding">Interest Rate</span>
                        <div class="form_input_icon">
                            <input class="form-control" id="interest_rate_count" name="interest_rate_count" type="text">
                             <span class="input-group-addon">%</span>
                            </div>
                            <div class="rangeSlider">
                                <div id="interest_rate"></div>
                            </div>
                            <p class="amount"><input type="text" id="interest_rate_count" readonly/></p>
                        </li>

                        <li>
                            <span class="rangeSliderHeadding">Tenure (Year) </span>
                            <div class="form_input_icon">
                            <input class="form-control" id="tenorCount" name="tenorCount" type="text">
                            <span class="input-group-addon">Yr</span> 
                            </div> 

                            <div class="rangeSlider">
                                <div id="tenor"></div>
                            </div>
                            <p class="amount"><input type="text" id="tenorCount" readonly/> months</p>
                        </li>
                    </ul> 

                    <div class="amountTotal clearfix">
                        <div class="pull-left emi" id="updatePercentage">Emi @ <span> </span> % p.a</div>
                        <div class="pull-right totalAmount" id="updateAmount"><i class="fa fa-rupee"></i> <span></span> per month</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row loan_apply_section">
            <div class="col-md-3">
                <button class="btn btn-primary">Request For Callback</button>
            </div>
            <div class="col-md-3">
                <button class="btn btn-warning" data-toggle="modal" data-target="#loanmodal">Apply For Loan</button>
            </div>
            <div class="col-md-3">
            </div>
        </div>
        <div class="row desclaimer">
            <div class="col-md-3"> Desclaimer :</div>
        </div>
        <!-- Loan Modal-->
        <div id="loanmodal" class="modal  fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]');?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Loan Modal-->
    </div>
    <?php if( !empty( $FAQs ) ) {  ?>
    <div id="faq" class="property-description box_shadow_col overview_sect mt20">
        <div class="row">
            <h2 class="title-left">FAQ'S</h2></div>
        <div class="table-responsive responsive-table">
            <div class="accord-block">
                <?php foreach($FAQs as $FAQ){ ?>
                <div class="accord-tab">
                    <h3><?php echo $FAQ['Question'];?> </h3>
                    <div class="expand-icon"></div>
                </div>
                <div class="accord-content" style="display: none">
                    <p>
                        <?php echo $FAQ['Answer'];?>
                    </p>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php } ?>
    <script type="text/javascript">
    $(document).ready(function() {
        var carousel = $(".slider_item");
        carousel.owlCarousel({
            items: 1,
            navigation: false,
            margin: 15,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 1,
                    nav: true,
                    loop: false
                }
            }
            /* navigationText: [
             "<i class='fa fa-chevron-left'></i>",
             "<i class='fa fa-chevron-right'></i>" 
             ],*/
        });
    });

    function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
    </script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/app.js"></script>  
<script src="<?php echo get_template_directory_uri(); ?>/js/calculate_loan.js"></script>  
