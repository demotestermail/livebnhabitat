<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 08/01/16
 * Time: 4:24 PM
 */
global $post_meta_data, $prop_description;
$documents_download = houzez_option('documents_download');
//$prop_description     = get_the_content();
$sitePlanId = get_post_meta( get_the_ID(), 'sitePlanId', true );
$masterPlanId = get_post_meta( get_the_ID(), 'masterPlanId', true );
$LocationAdvantages = get_post_meta( get_the_ID(), 'LocationAdvantages', true );
$SpecialFeatures = get_post_meta( get_the_ID(), 'SpecialFeatures', true );
$amenities = get_post_meta( get_the_ID(), 'amenities', true );
$ProjectLenders = get_post_meta( get_the_ID(), 'ProjectLenders', true );
$FAQs = get_post_meta( get_the_ID(), 'FAQs', true );
$desc=get_the_content();
if( !empty($desc) ) {
?>
    <div id="description" class="property-description box_shadow_col overview_sect ">
        <div class="row">
            <h2 class="title-left"><?php esc_html_e( 'Overview', 'houzez' ); ?></h2></div>
        <p><?php echo $desc; ?></p>
        <?php if( !empty($post_meta_data['fave_attachments']) ): ?>
        <div class="detail-title-inner">
            <h4 class="title-inner"><?php esc_html_e( 'Property Documents', 'houzez' ); ?></h4>
        </div>
        <ul class="document-list">
            <?php foreach( $post_meta_data['fave_attachments'] as $attachment_id ): ?>
            <?php $attachment_meta = houzez_get_attachment_metadata($attachment_id);?>
            <li>
                <div class="pull-left">
                    <i class="fa fa-file-o"></i>
                    <?php echo esc_attr( $attachment_meta->post_title ); ?>
                </div>
                <div class="pull-right">
                    <?php if( $documents_download == 1 ) {
                    if( is_user_logged_in() ) { ?>
                    <a href="<?php echo esc_url( $attachment_meta->guid ); ?>" download>
                        <?php esc_html_e( 'DOWNLOAD', 'houzez' ); ?>
                    </a>
                    <?php } else { ?>
                    <a href="#" data-toggle="modal" data-target="#pop-login">
                        <?php esc_html_e( 'DOWNLOAD', 'houzez' ); ?>
                    </a>
                    <?php } ?>
                    <?php } else { ?>
                    <a href="<?php echo esc_url( $attachment_meta->guid ); ?>" download>
                        <?php esc_html_e( 'DOWNLOAD', 'houzez' ); ?>
                    </a>
                    <?php } ?>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
    </div>
    <div class="clearfix"></div>
    <?php }  
if( !empty( $masterPlanId ) ) {
  add_thickbox(); ?>
    <div id="mplan" style="display:none;">
        <p>
            <img style='height: 100%; width: 100%; object-fit: contain' src=" <?php  echo wp_get_attachment_url( $masterPlanId );  ?>  ">
        </p>
    </div>
    <div id="splan" style="display:none;">
        <p>
            <img style='height: 100%; width: 100%; object-fit: contain' src=" <?php  echo wp_get_attachment_url( $sitePlanId );  ?>  ">
        </p>
    </div>
    <div class="detail-multi-properties box_shadow_col overview_sect mt20"> 
        <div class="row">
            <h2 class="title-left">Site Plan & Master Plan</h2>  
        </div>
        <div class="master_plan_col">
            <div class="row">
                <div class="col-sm-6">
                    <a href="#TB_inline?width=800&height=600&inlineId=mplan" class="thickbox"><img style="width:335px" src=" <?php  echo wp_get_attachment_url( $masterPlanId );  ?>  "></a>
                    <b> Master Plan</b>
                </div>
                <div class="col-sm-6">
                    <a href="#TB_inline?width=800&height=600&inlineId=splan" class="thickbox"><img style="width:335px" src=" <?php  echo wp_get_attachment_url( $sitePlanId );  ?>  "></a>
                    <b> Site Plan</b>
                </div>

            </div>

        </div>

        
    </div>
    <div class="clearfix"></div>
    <?php } 

        
        
        ?>
<?php if(!empty($LocationAdvantages)) { ?> 
<div class="location-advantages mt20">
                   <h2>Location Advantages</h2> 
                   <!--  <div class="bg_image">
                        <img src="<?php echo get_bloginfo('template_url') ?>/images/img06.jpg">
                    </div> -->

                    <table class="table  table-striped table-multi-properties one_row_table">
                        <thead>
                            <tbody><tr>
                            <?php  $loc_counter=0;  ?>
                                <?php

                               

                                 foreach($LocationAdvantages as $place){ 
                                 if($loc_counter % 5 ==0)
                                        echo "</tr><tr>"; ?>
                                    <td>
                                     <p><?php echo $place['NameOfLocation']; ?></p>    
                                    <img src="<?php echo SiteLink.$place['Image']; ?>">
                                    <strong><?php echo $place['Distance']; ?> <?php echo $place['DistanceUnit']; ?>   </strong>
                                    </td>
                               
                                 <?php 
                                        $loc_counter++;
                                    }   ?>  </tr>
                            </tbody>
                    </table>
                </div>
                <?php }?>
                <?php if(!empty($amenities)) { ?> 
                <div class="location-advantages mt20">
                   <h2>Amenities</h2> 
                    <table class="table  table-striped table-multi-properties one_row_table">
                        <thead>
                            <tbody>
                                <tr>
                                    <?php  $counter=0;  ?>
                                    <?php 
                                    foreach($amenities as $data){  
                                        if($counter % 5 ==0)
                                        echo "</tr><tr>"; ?>
                                        <td>
                                            <p><?php echo $data['Amenity']; ?></p>    
                                            <img src="<?php echo SiteLink.$data['Image']; ?>">
                                            <strong><?php echo $data['Description']; ?></strong>
                                        </td>                               
                                        <?php 
                                        $counter++;
                                    }   ?> 
                                </tr>
                            </tbody>
                    </table>

                </div>
                <?php }?>
                <?php if(!empty($SpecialFeatures)) { ?> 
                <div class="location-advantages mt20" id="features_col">
                   <h2>Special Features</h2> 
                    <table class="table  table-striped table-multi-properties one_row_table">
                        <thead>
                            <tbody>
                                <tr>
                                    <?php  $counter=0;  ?>
                                    <?php 
                                    foreach($SpecialFeatures as $Features){  
                                        if($counter % 5 ==0)
                                        echo "</tr><tr>"; ?>
                                        <td>
                                            <p><?php echo $Features['FeatureName']; ?></p>    
                                           <img src="<?php echo SiteLink.$Features['Image']; ?>">
                                            <strong><?php echo $Features['Description']; ?></strong>
                                        </td>                               
                                        <?php 
                                        $counter++;
                                    }   ?> 
                                </tr>
                            </tbody>
                    </table>

                </div>
                <?php }?>
                 




   <!--  <div id="LocationAdvantages" class="property-plans detail-block">
        <div class="table-wrapper">
            <div class="accord-block">
                <?php if( !empty( $LocationAdvantages ) ) { ?>


                
                <div class="clearfix"></div>

 
              


                <?php } ?>
                <?php if( !empty( $SpecialFeatures ) ) { 
     
    ?>
                
                <div class="accord-content" style="display: none">
                    <table class="table  table-striped table-multi-properties">
                        <thead>
                            <tbody>
                                <?php foreach($SpecialFeatures as $Features){ ?>
                                <tr>
                                    <td><img style="width:100px;" src="<?php echo SiteLink.$Features['Image']; ?>"></td>
                                    <td> <strong><?php echo $Features['FeatureName']; ?></strong></td>
                                    <td><strong><?php echo $Features['Description']; ?>  </strong></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                    </table>
                </div>
                <?php } ?>
                <?php if( !empty( $amenities ) ) {  
     
    ?>
                
                <?php } ?>
            </div>
        </div>
    </div> -->
    <?php if( !empty( $ProjectLenders ) ) {  
     
    ?>
  <!--   <div class="detail-block">
        <div class="table-wrapper">
            <h3> Loan Available From </h3>
            <table class="table  table-striped table-multi-properties">
                <thead>
                    <tbody>
                        <?php foreach($ProjectLenders as $Lenders){ ?>
                        <tr>
                            <td><img style="width:100px;" src="<?php echo SiteLink.$Lenders['Logo']; ?>"></td>
                            <td> <strong><?php echo $Lenders['Name']; ?></strong></td>
                        </tr>
                        <?php } ?>
                    </tbody>
            </table>
        </div>
    </div> -->
    <?php } ?>
    
