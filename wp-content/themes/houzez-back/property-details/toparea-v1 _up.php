<?php
/**
 * Property Top Area V1
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 08/01/16
 * Time: 2:44 PM
 */
global $post, $property_map, $property_streetView, $prop_agent_email;

$agent_display_option = get_post_meta( get_the_ID(), 'fave_agent_display_option', true );
$prop_agent_display = get_post_meta( get_the_ID(), 'fave_agents', true );
$prop_agent_num = $agent_num_call = $prop_agent_email = $gallery_view = $map_view = $street_view = '';

$enableDisable_agent_forms = houzez_option('agent_forms');

if( $prop_agent_display != '-1' && $agent_display_option == 'agent_info' ) {
    $prop_agent_id = get_post_meta( get_the_ID(), 'fave_agents', true );
    $prop_agent_email = get_post_meta( $prop_agent_id, 'fave_agent_email', true );

} elseif ( $agent_display_option == 'author_info' ) {
    $prop_agent_email = get_the_author_meta( 'email' );
}

$prop_default_active_tab = houzez_option('prop_default_active_tab');
if( $prop_default_active_tab == "image_gallery" ) {
    $gallery_view = 'in active';
} elseif( $prop_default_active_tab == "map_view" ) {
    $map_view = 'in active';
} elseif( $prop_default_active_tab == "street_view" ) {
    $street_view = 'in active';
} else {
    $gallery_view = 'in active';
}


?>
<section class="detail-top detail-top-grid">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php get_template_part( 'property-details/header', 'detail' );?>
                </div>    

                <div class="col-sm-9">
                <?php if( has_post_thumbnail() ) { ?>

                <div class="detail-media">
                    <div class="tab-content">

                        <div id="gallery" class="tab-pane fade <?php echo esc_attr( $gallery_view );?>">
                            <a href="#" class="popup-trigger">
                                <?php the_post_thumbnail('houzez-single-big-size'); ?>
                            </a>
                            <?php if( !empty( $prop_agent_email ) && $enableDisable_agent_forms != 0 ) { ?>
                            <div class="form-small form-media">
                                <?php get_template_part( 'property-details/agent', 'form' ); ?>
                            </div>
                            <?php } ?>

                        </div>

                        <?php if( $property_map != 0 ) { ?>
                        <div id="singlePropertyMap" class="tab-pane fade <?php echo esc_attr( $map_view );?>">
                            <div class="mapPlaceholder">
                                <div class="loader-ripple">
                                    <div></div>
                                    <div></div>
                                </div>
                            </div>
                        </div>
                         <?php wp_nonce_field('houzez_map_ajax_nonce', 'securityHouzezMap', true); ?>
                         <input type="hidden" name="prop_id" id="prop_id" value="<?php echo esc_attr($post->ID); ?>" />
                        <?php } ?>

                        <?php if( $property_streetView != 'hide' ) { ?>
                        <div id="street-map" class="tab-pane fade <?php echo esc_attr( $street_view );?>"></div>
                        <?php } ?>

                    </div>
                    <?php get_template_part( 'property-details/media', 'tabs' );?>
                </div>
                <?php } // End has post thumbnail ?>


            </div> 
            <div class="col-sm-3">
            <?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 08/01/16
 * Time: 2:06 PM
 */
 ?>
 <?php 
    $types = get_the_terms(get_the_ID(),'property_type');
    $status = get_the_terms(get_the_ID(),'property_status');
    $sizess = get_post_meta( get_the_ID(), 'fave_property_sizes', true ); 
    $prop_price = get_post_meta( get_the_ID(), 'fave_property_price', true );
    $prop_size = get_post_meta( get_the_ID(), 'fave_property_size', true );
    $Towers = get_post_meta( get_the_ID(), 'Towers', true ); 
    $NoOfUnits = get_post_meta( get_the_ID(), 'NoOfUnits', true );
    $NumberOfParking = get_post_meta( get_the_ID(), 'NumberOfParking', true );
    $PossessionMonth = get_post_meta( get_the_ID(), 'PossessionMonth', true );
    $PossessionYear = get_post_meta( get_the_ID(), 'PossessionYear', true ); 

           $bhk= get_post_meta( get_the_ID(), 'bhk', true ); 
if(!empty($bhk) && $bhk !=0){$bhk = $bhk." BHK"; } else{$bhk = ""; }
$Possessiondate = date('F', mktime(0, 0, 0, $PossessionMonth, 10))." ". $PossessionYear; 
    ?>
    
 
                <div class="detail-multi-properties" id="sub_property">
                   <div class="box_shadow_col quick_facts_sect">

                        <div class="row"><h2 class="title-left">Quick Facts <img class="pull-right" src="<?php bloginfo("template_url"); ?>/images/logo_01.png" /></h2></div>  

                        <ul class="quick_facts_list">
                            <li>Configuration: <small><?php echo $bhk;?> <?php foreach($types as $type){
                         echo $type->name;
                         }
                         ?>: <?php echo $prop_size;?> Sq Ft</small></li>
                         <li>Area Starts From : <small><?php echo $sizess;?> Sq Ft </small></li>
                         <li>Status : <small><?php foreach($status as $sta){
                         echo $sta->name;
                         }
                         ?></small></li>
                         <li>Price Range : <small><?php echo GetPrice(intval($prop_price));?> onward</small></li>
                         <li>Towers : <small><?php 
                         echo count($Towers);
                         
                         ?></small></li>
                         <li>Units : <small><?php echo $NoOfUnits;?></small></li>
                         <li>No. of parking : <small><?php 
                         echo $NumberOfParking;
                         
                         ?></small></li>
                         <li>Possession Date : <small><?php echo $Possessiondate;?></small></li>
                        </ul>

                   </div> 

             
        </div> 

            </div>
            

        </div>
    </div>
</section>
