<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 08/01/16
 * Time: 2:06 PM
 */
 ?>
 <?php 
	$types = get_the_terms(get_the_ID(),'property_type');
	$status = get_the_terms(get_the_ID(),'property_status');
	$sizess = get_post_meta( get_the_ID(), 'fave_property_sizes', true ); 
	$prop_price = get_post_meta( get_the_ID(), 'fave_property_price', true );
	$prop_size = get_post_meta( get_the_ID(), 'fave_property_size', true );
	$Towers = get_post_meta( get_the_ID(), 'Towers', true ); 
	$NoOfUnits = get_post_meta( get_the_ID(), 'NoOfUnits', true );
	$NumberOfParking = get_post_meta( get_the_ID(), 'NumberOfParking', true );
  	$PossessionMonth = get_post_meta( get_the_ID(), 'PossessionMonth', true );
  	$PossessionYear = get_post_meta( get_the_ID(), 'PossessionYear', true ); 

           $bhk= get_post_meta( get_the_ID(), 'bhk', true ); 
if(!empty($bhk) && $bhk !=0){$bhk = $bhk." BHK"; } else{$bhk = ""; }
$Possessiondate = date('F', mktime(0, 0, 0, $PossessionMonth, 10))." ". $PossessionYear; 
	?>
	<div class="detail-multi-properties detail-block" id="sub_property">
            <div class="table-wrapper"><h2 class="title-left">Quick Facts</h2>
                <table class="table  table-striped table-multi-properties">
                     <tr>
                        <td width="50%"><b><?php echo $bhk;?> <?php foreach($types as $type){
						 echo $type->name;
						 }
						 ?>: <?php echo $prop_size;?> Sq Ft</b></td>
                         <td width="50%" ><b>Area Starts From : <?php echo $sizess;?> Sq Ft </b></td>  
                    </tr> 
					<tr>
                        <td width="50%"><b>Status : <?php foreach($status as $sta){
						 echo $sta->name;
						 }
						 ?></b></td>
                         <td width="50%" ><b>Price Range : <?php echo GetPrice(intval($prop_price));?> onward</b></td>  
                    </tr>
					<tr>
                        <td width="50%"><b>Towers : <?php 
						 echo count($Towers);
						 
						 ?></b></td>
                         <td width="50%" ><b>Units : <?php echo $NoOfUnits;?></b></td>  
                    </tr>	
						<tr>
                        <td width="50%"><b>No. of parking : <?php 
						 echo $NumberOfParking;
						 
						 ?></b></td>
                         <td width="50%" ><b>Possession Date : <?php echo $Possessiondate;?></b></td>  
                    </tr>					</table>
            </div>
        </div>
