<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 08/01/16
 * Time: 4:24 PM
 */
global $post_meta_data, $prop_description;
$documents_download = houzez_option('documents_download');

$sitePlanId = get_post_meta( get_the_ID(), 'sitePlanId', true );
$masterPlanId = get_post_meta( get_the_ID(), 'masterPlanId', true );
$LocationAdvantages = get_post_meta( get_the_ID(), 'LocationAdvantages', true );
$SpecialFeatures = get_post_meta( get_the_ID(), 'SpecialFeatures', true );
$amenities = get_post_meta( get_the_ID(), 'amenities', true );
$ProjectLenders = get_post_meta( get_the_ID(), 'ProjectLenders', true );
$FAQs = get_post_meta( get_the_ID(), 'FAQs', true );
 
if( !empty( $prop_description ) ) {
?>
<div id="description" class="property-description box_shadow_col overview_sect ">
    <div class="row">
        <h2 class="title-left"><?php esc_html_e( 'Overview', 'houzez' ); ?></h2></div> 
     

    <?php echo $prop_description; ?>

    <?php if( !empty($post_meta_data['fave_attachments']) ): ?>
    <div class="detail-title-inner">
        <h4 class="title-inner"><?php esc_html_e( 'Property Documents', 'houzez' ); ?></h4>
    </div>
    <ul class="document-list">

        <?php foreach( $post_meta_data['fave_attachments'] as $attachment_id ): ?>
            <?php $attachment_meta = houzez_get_attachment_metadata($attachment_id);?>
        <li>
            <div class="pull-left">
                <i class="fa fa-file-o"></i> <?php echo esc_attr( $attachment_meta->post_title ); ?>
            </div>
            <div class="pull-right">
                <?php if( $documents_download == 1 ) {
                    if( is_user_logged_in() ) { ?>
                    <a href="<?php echo esc_url( $attachment_meta->guid ); ?>" download><?php esc_html_e( 'DOWNLOAD', 'houzez' ); ?></a>
                    <?php } else { ?>
                        <a href="#" data-toggle="modal" data-target="#pop-login"><?php esc_html_e( 'DOWNLOAD', 'houzez' ); ?></a>
                    <?php } ?>
                <?php } else { ?>
                    <a href="<?php echo esc_url( $attachment_meta->guid ); ?>" download><?php esc_html_e( 'DOWNLOAD', 'houzez' ); ?></a>
                <?php } ?>
            </div>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</div>


<div class="multi_units_section">
   <?php
/**
 * Multi Units / Sub Properties
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 14/07/16
 * Time: 11:13 AM
 */
global $enable_multi_units, $multi_units;
$mu_price_postfix = '';
if( $enable_multi_units != 'disable' && !empty( $enable_multi_units ) ) {

    if (!empty($multi_units)) {
        ?>
        <div id="sub_property" class="detail-multi-properties detail-block ">
            <div class="table-wrapper">
                <table class="table  table-striped table-multi-properties">
                    <thead>
                    <tr>
                        <th><?php esc_html_e('Title', 'houzez'); ?></th>
                        <th><?php esc_html_e('Property Type', 'houzez'); ?></th>
                        <th><?php esc_html_e('Price', 'houzez'); ?></th>
                        <th><?php esc_html_e('Beds', 'houzez'); ?></th>
                        <th><?php esc_html_e('Baths', 'houzez'); ?></th>
                        <th><?php esc_html_e('Property Size', 'houzez'); ?></th>
                        <th><?php esc_html_e('Availability Date', 'houzez'); ?></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach( $multi_units as $mu ):

                        if( !empty( $mu['fave_mu_price_postfix'] ) ) {
                            $mu_price_postfix = ' / '.$mu['fave_mu_price_postfix'];
                        }
                        ?>

                        <tr>
                        <td class="title blue" width="25%">
                            <p data-toggle="popover" data-content='
                                                <table class="table table-popover">
                                                    <tr>
                                                        <td class="table-popover-title"><?php esc_html_e('Title', 'houzez'); ?></td>
                                                        <td><a><?php echo esc_attr( $mu['fave_mu_title'] ); ?></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="table-popover-title"><?php esc_html_e('Property Type', 'houzez'); ?></td>
                                                        <td><?php echo esc_attr( $mu['fave_mu_type'] ); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="table-popover-title"><?php esc_html_e('Price', 'houzez'); ?></td>
                                                        <td><?php echo houzez_get_property_price( $mu['fave_mu_price'] ).$mu_price_postfix; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="table-popover-title"><?php esc_html_e('Beds', 'houzez'); ?></td>
                                                        <td><?php echo esc_attr( $mu['fave_mu_beds'] ); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="table-popover-title"><?php esc_html_e('Baths', 'houzez'); ?></td>
                                                        <td><?php echo esc_attr( $mu['fave_mu_baths'] ); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="table-popover-title"><?php esc_html_e('Property Size', 'houzez'); ?></td>
                                                        <td><?php echo houzez_get_area_size($mu['fave_mu_size']).' '.houzez_get_size_unit( $mu['fave_mu_size_postfix'] ); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="table-popover-title"><?php esc_html_e('Availability Date', 'houzez'); ?></td>
                                                        <td><?php echo esc_attr( $mu['fave_mu_availability_date'] ); ?></td>
                                                    </tr>
                                                </table>
                                            '>
                                <?php echo esc_attr( $mu['fave_mu_title'] ); ?>
                            </p>
                        </td>
                        <td><?php echo esc_attr( $mu['fave_mu_type'] ); ?></td>
                        <td><?php echo houzez_get_property_price( $mu['fave_mu_price'] ).$mu_price_postfix; ?></td>
                        <td><?php echo esc_attr( $mu['fave_mu_beds'] ); ?></td>
                        <td><?php echo esc_attr( $mu['fave_mu_baths'] ); ?></td>
                        <td><?php echo houzez_get_area_size($mu['fave_mu_size']).' '.houzez_get_size_unit( $mu['fave_mu_size_postfix'] ); ?></td>
                        <td><?php echo esc_attr( $mu['fave_mu_availability_date'] ); ?></td>
                    </tr>

                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <?php
    }
} ?>  

</div>


<div class="clearfix"></div>
<?php }  
if( !empty( $masterPlanId ) ) {
  add_thickbox(); ?>
<div id="mplan" style="display:none;">
     <p> 
	 <img  style='height: 100%; width: 100%; object-fit: contain' src=" <?php  echo wp_get_attachment_url( $masterPlanId );  ?>  ">
   
     </p>
</div>
<div id="splan" style="display:none;">
     <p>
         <img  style='height: 100%; width: 100%; object-fit: contain' src=" <?php  echo wp_get_attachment_url( $sitePlanId );  ?>  ">  
     </p>
</div>
 
<div class="detail-multi-properties detail-block">
            <div class="table-wrapper">  
                <table class="table  table-striped table-multi-properties">
                     <tr>
                        <th width="50%"><b> Master Plan
						  </b></th>
                         <th width="50%" ><b> Site Plan
						  </b></th>  
                    </tr> 
					   <tr>
                        <td width="50%"><a href="#TB_inline?width=800&height=600&inlineId=mplan" class="thickbox"><img style="width:335px" src=" <?php  echo wp_get_attachment_url( $masterPlanId );  ?>  "></a></td>
                         <td width="50%" ><a href="#TB_inline?width=800&height=600&inlineId=splan" class="thickbox"><img style="width:335px" src=" <?php  echo wp_get_attachment_url( $sitePlanId );  ?>  "></a></td>  
                    </tr> 
					 	
						 					</table>
            </div>
        </div>
		<?php } 

		
		
		?>
	 
           
<div id="LocationAdvantages" class="property-plans detail-block">
  <div class="table-wrapper"> 
    <div class="accord-block">
	<?php if( !empty( $LocationAdvantages ) ) { ?>
            <div class="accord-tab">
                <h3>Location Advantages</h3>
                 
                <div class="expand-icon"></div>
            </div>
            <div class="accord-content" style="display: none">
			<table class="table  table-striped table-multi-properties">
                    <thead>
                  <tbody>
                <?php foreach($LocationAdvantages as $place){ ?>
				<tr> 	 <td><img src="<?php echo SiteLink.$place['Image']; ?>"></td>
                        <td> <strong><?php echo $place['NameOfLocation']; ?></strong></td> 
                        <td><strong><?php echo $place['Distance']; ?> <?php echo $place['DistanceUnit']; ?>   </strong></td>
                       
                    </tr>  
				<?php } ?>
            </tbody>  </table></div>
	<?php } ?>
	<?php if( !empty( $SpecialFeatures ) ) { 
	 
	?>
            <div class="accord-tab">
                <h3>Special Features</h3>
                 
                <div class="expand-icon"></div>
            </div>
            <div class="accord-content" style="display: none">
			<table class="table  table-striped table-multi-properties">
                    <thead>
                  <tbody>
                <?php foreach($SpecialFeatures as $Features){ ?>
				<tr> 	
                        <td><img style="width:100px;" src="<?php echo SiteLink.$Features['Image']; ?>"></td>
                        <td> <strong><?php echo $Features['FeatureName']; ?></strong></td> 
                        <td><strong><?php echo $Features['Description']; ?>  </strong></td>
                    </tr>  
				<?php } ?>
            </tbody>  </table></div>
	<?php } ?>
         	<?php if( !empty( $amenities ) ) {  
	 
	?>
            <div class="accord-tab">
                <h3>Amenities</h3>
                 
                <div class="expand-icon"></div>
            </div>
            <div class="accord-content" style="display: none">
			<table class="table  table-striped table-multi-properties">
                    <thead>
                  <tbody>
                <?php foreach($amenities as $data){ ?>
				<tr> 	
                        <td><img style="width:100px;" src="<?php echo SiteLink.$data['Image']; ?>"></td>
                        <td> <strong><?php echo $data['Amenity']; ?></strong></td> 
                        <td><strong><?php echo $data['Description']; ?>  </strong></td>
                    </tr>  
				<?php } ?>
            </tbody>  </table></div>
	<?php } ?>

    </div>
    </div>
	

</div>
<?php if( !empty( $ProjectLenders ) ) {  
	 
	?>
	<div class="detail-block">
		<div class="table-wrapper"> <h3> Loan Available From </h3>
			<table class="table  table-striped table-multi-properties"> 
                    <thead>
                  <tbody>
                <?php foreach($ProjectLenders as $Lenders){ ?>
					<tr> 	
                        <td><img style="width:100px;" src="<?php echo SiteLink.$Lenders['Logo']; ?>"></td>
                        <td> <strong><?php echo $Lenders['Name']; ?></strong></td>  
                    </tr>  
				<?php } ?>
            </tbody>  </table> 
		</div>
	</div>
		 <?php } ?>
<?php if( !empty( $FAQs ) ) {  
	 
	?>
	<div class="detail-block">	  
               <h3> Broker's Faq </h3>
                 
            
		<div class="accord-block">
		
                <?php foreach($FAQs as $FAQ){ ?> 
				<div class="accord-tab">
               <h3><?php echo $FAQ['Question'];?> </h3>
                <div class="expand-icon"></div>
            </div>
			<div class="accord-content" style="display: none"> 
			<p> <?php echo $FAQ['Answer'];?></p>
			</div>
				<?php } ?>
              
 
		</div>
	</div>
		 <?php } ?>