<?php
/**
 * Created by PhpStorm.
 * User: waqasriaz
 * Date: 07/01/16
 * Time: 7:56 PM
 */
global $post,$current_user,$user_id;

$agent_display_option = get_post_meta( get_the_ID(), 'fave_agent_display_option', true );
$prop_agent_display = get_post_meta( get_the_ID(), 'fave_agents', true );
$enable_contact_form_7_prop_detail = houzez_option('enable_contact_form_7_prop_detail');
$contact_form_agent_above_image = houzez_option('contact_form_agent_above_image');
$enable_direct_messages = houzez_option('enable_direct_messages');
$prop_agent_num = $agent_num_call = $prop_agent_email = '';
$listing_agent = '';
$is_single_agent = true;
$user_id=get_current_user_id();
$user_phn=get_user_meta($user_id,'fave_author_mobile',true);


if( $prop_agent_display != '-1' && $agent_display_option == 'agent_info' ) {

    $prop_agent_ids = get_post_meta( get_the_ID(), 'fave_agents' );
    $prop_agent_ids = array_filter( $prop_agent_ids, function($v){
        return ( $v > 0 );
    });
    // remove duplicated ids
    $prop_agent_ids = array_unique( $prop_agent_ids );

    if ( ! empty( $prop_agent_ids ) ) {
        $agents_count = count( $prop_agent_ids );
        if ( $agents_count > 1 ) :
            $is_single_agent = false;
        endif;
        $listing_agent = '';
        foreach ( $prop_agent_ids as $agent ) {
            if ( 0 < intval( $agent ) ) {

                $prop_agent_id = intval( $agent );
                $prop_agent_phone = get_post_meta( $prop_agent_id, 'fave_agent_office_num', true );
                $prop_agent_mobile = get_post_meta( $prop_agent_id, 'fave_agent_mobile', true );
                $prop_agent_email = get_post_meta( $prop_agent_id, 'fave_agent_email', true );
                $agent_num_call = str_replace(array('(',')',' ','-'),'', $prop_agent_mobile);
                $prop_agent = get_the_title( $prop_agent_id );
                $thumb_id = get_post_thumbnail_id( $prop_agent_id );
                $thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'thumbnail', true );
                $prop_agent_photo_url = $thumb_url_array[0];
                $prop_agent_permalink = get_post_permalink( $prop_agent_id );

                $agent_args = array();
                $agent_args[ 'agent_id' ] = $prop_agent_id;
                $agent_args[ 'agent_name' ] = $prop_agent;
                $agent_args[ 'agent_mobile' ] = $prop_agent_mobile;
                $agent_args[ 'agent_mobile_call' ] = str_replace(array('(',')',' ','-'),'', $prop_agent_mobile);
                $agent_args['agent_phone'] = $prop_agent_phone;
                $agent_args[ 'agent_email' ] = $prop_agent_email;
                $agent_args[ 'link' ] = $prop_agent_permalink;
                if( empty( $prop_agent_photo_url )) {
                    $agent_args[ 'picture' ] = get_template_directory_uri().'/images/profile-avatar.png';
                } else {
                    $agent_args[ 'picture' ] = $thumb_url_array[0];
                }
                $listing_agent .= houzez_get_agent_info_top( $agent_args, 'agent_form', $is_single_agent );
            }
        }
    }

} elseif( $agent_display_option == 'agency_info' ) {
    $prop_agent_id = get_post_meta( get_the_ID(), 'fave_property_agency', true );
    $prop_agent_phone = get_post_meta( $prop_agent_id, 'fave_agency_phone', true );
    $prop_agent_mobile = get_post_meta( $prop_agent_id, 'fave_agency_mobile', true );
    $prop_agent_email = get_post_meta( $prop_agent_id, 'fave_agency_email', true );
    $agent_num_call = str_replace(array('(',')',' ','-'),'', $prop_agent_mobile);
    $prop_agent = get_the_title( $prop_agent_id );
    $thumb_id = get_post_thumbnail_id( $prop_agent_id );
    $thumb_url_array = wp_get_attachment_image_src( $thumb_id, 'thumbnail', true );
    $prop_agent_photo_url = $thumb_url_array[0];
    $prop_agent_permalink = get_post_permalink( $prop_agent_id );

    $agent_args = array();
    $agent_args[ 'agent_id' ] = $prop_agent_id;
    $agent_args[ 'agent_name' ] = $prop_agent;
    $agent_args[ 'agent_mobile' ] = $prop_agent_mobile;
    $agent_args[ 'agent_mobile_call' ] = str_replace(array('(',')',' ','-'),'', $prop_agent_mobile);
    $agent_args[ 'agent_phone' ] = $prop_agent_phone;
    $agent_args[ 'agent_email' ] = $prop_agent_email;
    $agent_args[ 'link' ] = $prop_agent_permalink;
    if( empty( $prop_agent_photo_url )) {
        $agent_args[ 'picture' ] = get_template_directory_uri().'/images/profile-avatar.png';
    } else {
        $agent_args[ 'picture' ] = $thumb_url_array[0];
    }
    $listing_agent .= houzez_get_agent_info_top( $agent_args, 'agent_form' );

} elseif ( $agent_display_option == 'author_info' ) {
    $prop_agent = get_the_author();
    $prop_agent_permalink = get_author_posts_url( get_the_author_meta( 'ID' ) );
    $prop_agent_phone = get_the_author_meta( 'fave_author_phone' );
    $prop_agent_mobile = get_the_author_meta( 'fave_author_mobile' );
    $agent_num_call = str_replace(array('(',')',' ','-'),'', $prop_agent_num);
    $prop_agent_photo_url = get_the_author_meta( 'fave_author_custom_picture' );
    $prop_agent_email = get_the_author_meta( 'email' );

    $agent_args = array();
    $agent_args[ 'agent_id' ] = get_the_author_meta( 'ID' );
    $agent_args[ 'agent_name' ] = $prop_agent;
    $agent_args[ 'agent_mobile' ] = $prop_agent_mobile;
    $agent_args[ 'agent_mobile_call' ] = str_replace(array('(',')',' ','-'),'', $prop_agent_mobile);
    $agent_args[ 'agent_phone' ] = $prop_agent_phone;
    $agent_args[ 'agent_email' ] = $prop_agent_email;
    $agent_args[ 'link' ] = $prop_agent_permalink;
    if( empty( $prop_agent_photo_url )) {
        $agent_args[ 'picture' ] = get_template_directory_uri().'/images/profile-avatar.png';
    } else {
        $agent_args[ 'picture' ] = $thumb_url_array[0];
    }
    $listing_agent .= houzez_get_agent_info_top( $agent_args, 'agent_form', false );
}
if( empty( $prop_agent_photo_url )) {
    $prop_agent_photo_url = get_template_directory_uri().'/images/profile-avatar.png';
}

$agent_email = is_email( $prop_agent_email );
$user_info = get_userdata( get_the_author_meta( 'ID' ) );
$user_role = implode(', ', $user_info->roles);

if( $enable_contact_form_7_prop_detail == 0 ) {
    ?>
     <div class="modal modal01 fade" id="modal_list_prop<?=$post->ID;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form method="post" action="#">
        <?php

}
?>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="pop_up_top">
                            <a href="#" class="btn btn-primary call_req_modal" data-target="#sitevisit<?=get_the_ID();?>" data-toggle="modal" data-mod-id="<?=get_the_ID();?>" >Request a site Visit</a>
                            <a href="#" class="btn btn-primary call_req_modal"  data-target="#callbackform<?=get_the_ID();?>" data-toggle="modal" data-mod-id="<?=get_the_ID();?>" >Request a Call back</a>
                            <?php if(!empty($user_phn)){?>
                                <p class="para_graf">Your Registered Number is ******<?=substr($user_phn,6)?></p>
                                <?php } ?>
                            
                            <?php if(is_user_logged_in()){
  $userID=get_current_user_id();
    ?>
                            <p class="pull-right">You Logged in as
                                <a href="#">
                                    <?= ucfirst(get_user_meta($userID,'nickname')[0]); ?>
                                </a>
                                <i class="fa fa-info"></i></p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="our-projects">
                        <h3>Our Projects Expert</h3>
                        <div class="col-sm-6">
                            <div class="projects_ex">
                                <a href="<?php echo esc_url($prop_agent_permalink); ?>" class="images-agent-model">
                            <img src="<?php echo esc_url( $prop_agent_photo_url ); ?>"  alt="<?php echo esc_attr( $prop_agent ); ?>"> 
                            </a>
                                <h5><?php echo esc_attr( $prop_agent ); ?><!-- <small> Manager Sales & Marketing</small> --></h5>
                                <h6><?php echo esc_attr( $prop_agent_mobile );?>  <span><?php echo esc_attr( $prop_agent_email );?></span></h6>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="projects_ex_right">
                                <?php
if( $enable_contact_form_7_prop_detail != 0 ) {
    if( !empty($contact_form_agent_above_image) ) {
        echo do_shortcode($contact_form_agent_above_image);
    }
} else {
    if ($agent_email) { ?>
                                    <?php if( $is_single_agent == true && is_user_logged_in() && $enable_direct_messages != 0 ) { ?>
                                    <?php if ( $is_single_agent == true ) : ?>
                                    <input type="hidden" name="target_email" value="<?php echo antispambot($agent_email); ?>">
                                    <?php endif; ?>
                                    <input type="hidden" name="agent_contact_form_ajax" value="<?php echo wp_create_nonce('agent-contact-form-nonce'); ?>" />
                                    <input type="hidden" name="property_permalink" value="<?php echo esc_url(get_permalink($post->ID)); ?>" />
                                    <input type="hidden" name="property_title" value="<?php echo esc_attr(get_the_title($post->ID)); ?>" />
                                    <input type="hidden" name="action" value="houzez_agent_send_message">
                                    <?php 
                      wp_get_current_user();
                 
                      if($current_user->user_firstname!=''){
                        $c_user_name=$current_user->user_firstname.' '.$current_user->user_lastname;
                      } else {
                        $c_user_name=$current_user->user_login;
                      }
                     $c_mobile= get_user_meta( $current_user->ID,'fave_author_mobile',true);
                    
                ?>
                                    <input type="hidden" name="phone" value="<?=$c_mobile;?>">
                                    <input type="hidden" name="name" value="<?=$c_user_name;?>">
                                    <input type="hidden" name="email" value="<?=$current_user->user_email;?>">
                                    <div class="form-group">
                                        <textarea class="form-control" name="message" rows="3" placeholder="<?php esc_html_e('Description', 'houzez'); ?>"><?php esc_html_e("Hello, I am interested in", "houzez"); ?>[<?php echo get_the_title(); ?>]</textarea>
                                    </div>
                                    <div class="form_messages"></div>
                                    <?php
    } else {
        if ($agent_email) { ?>
                                        <?php if ( $is_single_agent == true ) : ?>
                                        <input type="hidden" name="target_email" value="<?php echo antispambot($agent_email); ?>">
                                        <?php endif; ?>
                                        <input type="hidden" name="agent_contact_form_ajax" value="<?php echo wp_create_nonce('agent-contact-form-nonce'); ?>" />
                                        <input type="hidden" name="property_permalink" value="<?php echo esc_url(get_permalink($post->ID)); ?>" />
                                        <input type="hidden" name="property_title" value="<?php echo esc_attr(get_the_title($post->ID)); ?>" />
                                        <input type="hidden" name="action" value="houzez_agent_send_message">
                                        <div class="form-group">
                                            <input class="form-control" name="name" type="text" placeholder="<?php esc_html_e('Your Name', 'houzez'); ?>">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" name="phone" type="text" placeholder="<?php esc_html_e('Phone', 'houzez'); ?>">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" name="email" type="email" placeholder="<?php esc_html_e('Email', 'houzez'); ?>">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" name="message" rows="5" placeholder="<?php esc_html_e('Description', 'houzez'); ?>">
                                                <?php _e("Hello, I am interested in", "houzez"); ?> [
                                                <?php echo get_the_title(); ?>]</textarea>
                                        </div>
                                        <button class="agent_contact_form btn btn-primary btn-block">
                                            <?php esc_html_e('Request info', 'houzez'); ?>
                                        </button>
                                        <?php if( $is_single_agent == true  && $enable_direct_messages != 0 ) { ?>
                                        <button type="button" class="btn btn-primary btn-trans btn-block" data-toggle="modal" data-target="#pop-login">
                                            <?php esc_html_e('Send Direct Message', 'houzez'); ?> </button>
                                        <?php } ?>
                                        <div class="form_messages"></div>
                                        <?php 
            
       } } }
    
}?>
                                        <!-- <button type="button" class="btn btn-primary">Send</button>  -->
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        <button class="start_thread_form btn btn-primary ">
                            <?php esc_html_e('Send', 'houzez'); ?>
                        </button>
                    </div>
                </div>
            </div>
    </form>
    <div class="media agent-media">
        <div class="media-left">
        </div>
        <div class="media-body">
            <h4 class="media-heading"></h4>
            <ul>
                <?php if( !empty( $prop_agent ) ) { ?>
                <li><i class="fa fa-user"></i>
                    <?php echo esc_attr( $prop_agent ); ?>
                </li>
                <?php } ?>
                <?php if( !empty( $prop_agent_mobile ) ) { ?>
                <li><i class="fa fa-phone"></i>
                    <?php echo esc_attr( $prop_agent_mobile );?>
                </li>
                <?php } ?>
            </ul>
            <a href="<?php echo esc_url($prop_agent_permalink); ?>" class="view">
                <?php esc_html_e('View my listing', 'houzez' ); ?>
            </a>
        </div>
    </div>
     </div>
 <!-- Modal Site Visit-->
            <div class="modal fade modal_dv_call_bk" id="sitevisit<?=get_the_ID();?>" role="dialog">
                <div class="modal-dialog  modal-lg">
                    <div class="modal-content sitevisit when_popup">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">When ??</h4>
                        </div>
                        <form id="site_visit_form<?=get_the_ID()?>" action="">
                            <input type="hidden" name="phone" value="<?=$c_mobile;?>">
                            <input type="hidden" name="name" value="<?=$c_user_name;?>">
                            <input type="hidden" name="email" value="<?=$current_user->user_email;?>">
                            <input type="hidden" name="action" value="houzez_agent_send_site_visit">
                            <input type="hidden" name="agent_email" value="<?=$agent_email;?>">
                            <input type="hidden" name="agent_name" value="<?=$agent_name;?>">
                            <input type="hidden" name="property_title" value="<?php echo esc_attr(get_the_title($post->ID)); ?>" />
                            <input type="hidden" name="property_permalink" value="<?php echo esc_url(get_permalink($post->ID)); ?>" />
                            <div class="modal-body request_visit_hide">
                               <!--  Site Visit -->
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="radio text-right">
                                            <label>As Soon Poosible
                                                <input type="radio" checked="" value="As Soon As possible" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div> 
                                    <div class="col-sm-4">
                                        <div class="radio text-center">
                                            <label>Any Time
                                                <input type="radio" value="Any Time" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="radio text-left">
                                            <label>Suitable Time
                                                <input type="radio" value="suitable_time" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div> 
                                </div>
                                <div class="suitable_time_block display-hide">
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>Date</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker_date" class="input-append date datetimepicker_date">
                                                        <input type="text" name="v_date" value="<?php echo date('d-m-Y',time()); ?>">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>Between</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker2" class="input-append date">
                                                        <input type="text" name="v_time_between" placeholder="HH:MM" value="02:00 PM">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>To</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker" class="input-append date">
                                                        <input type="text" name="v_time_to" placeholder="HH:MM" value="05:00 PM">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer request_visit_hide">
                                <button type="button" id="" class="btn btn-primary submit_request_callback_agent">Done</button>
                            </div>
                            <div class="succss_message_visit display-hide">
                                We Will Call you soon..
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Modal Reqest Call back-->
            <div class="modal fade modal_dv_call_bk" id="callbackform<?=get_the_ID();?>" role="dialog">
                <div class="modal-dialog modal-lg when_popup">
                    <div class="modal-content callbackform">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">When ??</h4>
                        </div>
                        <form id="callback_form<?=get_the_ID();?>" action="">
                            <input type="hidden" name="phone" value="<?=$c_mobile;?>">
                            <input type="hidden" name="name" value="<?=$c_user_name;?>">
                            <input type="hidden" name="email" value="<?=$current_user->user_email;?>">
                            <input type="hidden" name="action" value="houzez_agent_send_callback">
                            <input type="hidden" name="agent_email" value="<?=$agent_email;?>">
                            <input type="hidden" name="agent_name" value="<?=$agent_name;?>">
                            <input type="hidden" name="property_title" value="<?php echo esc_attr(get_the_title($post->ID)); ?>" />
                            <input type="hidden" name="property_permalink" value="<?php echo esc_url(get_permalink($post->ID)); ?>" />
                            <div class="modal-body request_callback_hide">
                              <!--   Site Visit -->
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="radio text-right">
                                            <label>As Soon Poosible
                                                <input type="radio" checked="" value="As Soon As possible" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="radio text-center">
                                            <label>Any Time
                                                <input type="radio" value="Any Time" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="radio text-left"> 
                                            <label>Suitable Time
                                                <input type="radio" value="suitable_time" name="time" class="v_time">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="suitable_time_block display-hide">
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>Date</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker_date" class="input-append date datetimepicker_date">
                                                        <input type="text" name="v_date" value="<?php echo date('d-m-Y',time()); ?>">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>Between</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker2" class="input-append date">
                                                        <input type="text" name="v_time_between" placeholder="HH:MM" value="02:00 PM">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <strong>To</strong>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="datetimepicker" class="input-append date">
                                                        <input type="text" name="v_time_to" placeholder="HH:MM" value="05:00 PM">
                                                        <span class="add-on">
    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
  </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer request_callback_hide">
                                <button type="button" class="btn btn-primary submit_request_callback_agent" id="submit_request_callback" >Done</button>
                            </div>
                            <div class="succss_message_call display-hide">
                                We Will Call you soon..
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <script >
                $(".call_req_modal").on("click",function(){
                        var modal_id=$(this).attr("data-mod-id");
                        $("#modal_list_prop"+modal_id).modal('hide');
                });


            </script>