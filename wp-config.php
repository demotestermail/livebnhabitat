<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'habitat_newwp');

/** MySQL database username */
define('DB_USER', 'habitat_newuser');

/** MySQL database password */
define('DB_PASSWORD', 'b[+mQR]ANQ_2');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[xOi<yZ?P9!fvwj5HPQbi/4}L=4G6xg0`mW<H:%{opzhK^T&GIpMy!y3~ic$;LyS');
define('SECURE_AUTH_KEY',  '9iUO-y2Oo]C^HE|rYT`Ha1%E>F-tP&qtjas=-cI o6o_o#}= P3o-O_])7A1=TOS');
define('LOGGED_IN_KEY',    '+u2U8Wi{aXG%{hNM>V(H%K&v)CN}ki=kZ*ya|=m2mx56kn9SgX{y9/A+N.t{)K4W');
define('NONCE_KEY',        'RRUE~8RQYel%lUF+33PDM.(FRbUi=!c~!9|hWSPwHi_XE xxWX>3C)%_OIBcyTOh');
define('AUTH_SALT',        'YR{<p#Ef!l>?t8}-*RL1WvnT-$tsK8H= G5oSS.fq{q]D&-o5uL)0,PH6!pGrY(*');
define('SECURE_AUTH_SALT', '~-[?d0wdaH;OIrg[wz$HCOg&0X8 `^G{/FS1N 0/fI/i%9kMPbl/ZjQ l]V~8mHa');
define('LOGGED_IN_SALT',   '[[Sk$D1&-5HYZ<%j3G1~}WVXRi5vXNq}8jHeM lX.x+8>F;ws)%UyILNk,o}E;~`');
define('NONCE_SALT',       '<;qzCfw$1Um]5V%L7sIJpx^euu<G7A!QWSv xyU{lFPe!R|9(v)aHrpyM{R~k3.!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
